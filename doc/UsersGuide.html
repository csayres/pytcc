<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>TCC User's Guide</title>
</head>
<body>
<h1><a href="index.html">TCC</a> User's Guide</h1>

<ul>
    <li><a href="#Introduction">Introduction</a>
    <li><a href="#EnablingTheAxes">Enabling the Axes</a>
    <li><a href="#HaltingtheTelescope">Halting the Telescope</a>
    <li><a href="#TrackingAnObject">Tracking an Object</a>
    <ul>
        <li><a href="#SlewingOptions">Slewing Options</a>
        <li><a href="#Offsetting">Offsetting</a>
        <li><a href="#RefractionCorrection">Refraction Correction</a>
        <li><a href="#AtmosphericDispersionCorrection">Atmospheric Dispersion Correction</a>
    </ul>
    <li><a href="#TheInstrumentRotator">The Instrument Rotator</a>
    <ul>
        <li><a href="#RotationOptions">Rotation Options</a>
        <li><a href="#UsingRotatingField">Using a Rotating Field</a>
    </ul>
    <li><a href="#Guiding">Guiding</a>
    <li><a href="#Focusing">Focusing</a>
    <li><a href="#ChangingInstruments">Changing Instruments</a>
    <li><a href="#DriftScanning">Drift Scanning</a>
    <li><a href="#AdditionalOperations">Additional Operations</a>
</ul>

<h2><a name="Introduction"></a>Introduction</h2>

<p>This manual discusses operation of the Telescope Control Computer (TCC) from a user's perspective. It is intended to help the programmer of the TCC graphical interface, and to serve as a basis for a user guide for that interface. It is not aimed at the typical astronomer, but I hope sections of it can be expanded for that purpose.

<p>This guide should be used in conjunction with the <a href="OperatorsManual.html">TCC Operator's Manual</a> and <a href="Commands.html">TCC Commands Manual</a>. The former describes operator-oriented functions such as starting the TCC and generating telescope models. The latter gives a detailed description of each TCC command.

<p>Also, as the TCC software is updated this manual may tend to get behind, whereas the Operator's Manual, Commands Manual and Keywords Dictionary should always be current. Please feel free to note any problems in any of the manuals.

<h2><a name="EnablingTheAxes"></a>Enabling the Axes</h2>

<p>Before attempting to move the telescope, check that the telescope can move safely (no danger to people or physical obscructions). Also, with remote observation it is very important to keep an eye on the weather. Once the initial check is completed, make sure all the red safety buttons are out (enabled). Then issue the following TCC command:

<dl>
    <dt>AXIS INIT
</dl>

<p>After that you may specify the current instrument using SET INSTRUMENT and acquire a target using the TRACK command.

<h2><a name="HaltingtheTelescope"></a>Halting the Telescope</h2>

<p>In a real emergency, IF YOU ARE AT THE SITE, PUSH A RED STOP BUTTON; if you are not, notify the telescope operator at the site. The remaining techniques below explain how to stop things by sending commands to the TCC.

<ul>
    <li>To stop motion quickly, especially if there is a problem with the axes: AXIS INIT
</ul>

<p>To shut down and restart the TCC software (which will get you out of most problems):

<ul>
    <li>AXIS INIT (halts the axis controllers and puts them in a known state) or push in a red stop button.
    
    <li>EXIT (exits the TCC command interpreter and returns a $ prompt); if you are ignored, use ^Y instead to kill the interpeter.
    
    <li>Log into the TCC and kill the tcc process, then start it again.
</ul>

<p>For more information, see the <a href="OperatorsManual.html">TCC Operator's Manual</a>.

<h2><a name="TrackingAnObject"></a>Tracking an Object</h2>

<p>To track an object, specify its position using the TRACK command. The telescope will then slew to the object and track it. To move to a new object, simply issue another TRACK command. To halt the telescope, use the command TRACK/STOP. Emergency halting was discussed above (please look it over now, before you need it!). The available coordinate systems and various options for tracking are discussed below.

<p>You can specify an object's position in a number of different coordinate systems. See the <a href="Commands.html">TCC Commands Manual</a> for details.

<h3><a name="SlewingOptions"></a>Slewing Options</h3>

<p>The default tracking behavior of the telescope should be adequate most of the time. However, there are a few options available that are worth knowing about. Some, such as pointing error correction, can make your life much easier. Others are useful for special circumstances.

<h4>Wrap</h4>

<p>You may force the telescope to use a particular "wrap" for the azimuth axis and instrument axis, using /TEL1WRAP and /ROTWRAP. The default is usually satisfactory, but a different choice of wrap may sometimes lengthen your tracking time. For the azimuth, use positive wrap to follow circumpolar stars. It is harder to predict the correct wrap for the rotator, and the TCC should probably be enhanced to select the longer-tracking wrap automatically.

<h4>Ignore The Absolute Encoders</h4>

<p>Normally when a slew occurs, the absolute encoders are used. These encoders are sloppy compared to the incremental encoders, which is why they are only used during slewing, never during tracking. Normally this small additional error is perfectly acceptable; but if accurate relative positioning is very important, you may ignore the absolute encoders by using /NOABSREFCORRECT.

<p>Warning: ignoring the absolute encoders allows incremental encoder error to accumulate. Performing many such slews sequentially can seriously degrade pointing accuracy.

<h3><a name="Offsetting"></a>Offsetting</h3>

<p>Offseting is done using the OFFSET and GENOFFSET commands. OFFSET is a simple command which supports all kinds of offsets. GENOFFSET is less flexible, but smarter. It takes input in a form especially suited for graphical interfaces that let you drag around real images. For a detailed discussion of GENOFFSET see the paper <a href="http://www.apo.nmsu.edu/Telescopes/eng.papers/offsetting/Offsetting.html">Offsetting a Telescope Via Remote Control</a>.

<p>The TCC has several types of offsets, two of which are of special interest to the astronomer: arc and instrument-plane offsets. Both allow specifying velocity, as well as position.

<h4>Arc Offset</h4>

<p>Arc offsets are great circle arcs in the target's coordinate system (often ICRS RA, Dec).

<h4>Instrument-Plane Offset</h4>

<p>The instrument-plane offset specifies the position of the object with respect to the instrument. Its uses include moving an object along a slit or onto a different CCD of a mosaic camera. Instrument-plane offsets are specified in the frame of reference of the instrument, regardless of what the rotator (if present) is doing. The x component is typically defined to be along the rows of a CCD, and the y component along the columns. If the sky is rotating on the instrument, the instrument-plane offset specifies the one point on the instrument that does not rotate (the "boresight"); for more information see <a href="#UsingRotatingField">Using a Rotating Field</a>.

<p>One subtlety of instrument-plane offsets is their handedness. Some instruments return images "sky right", while others return them reversed; all positions and offsets including instrument-plane offsets are mirrored accordingly. In other words, instrument-plane offsets don't know how the CCD is read out.

<h3><a name="RefractionCorrection"></a>Refraction Correction</h3>

<p>Using SET WAVELENGTH you may choose the wavelength for which the refraction correction will be computed. Furthermore, you may choose a different wavelength for the object and the guide star. This is needed to maintain long-term guiding accuracy if the instrument and guide camera are using different portions of the spectrum.

<h3><a name="AtmosphericDispersionCorrection"></a>Atmospheric Dispersion Correction</h3>

<p>The telescope does not (at this time) have an atmospheric
dispersion corrector.

<h2><a name="TheInstrumentRotator"></a>The Instrument Rotator</h2>

<p>Some instruments (including any mounted at the community Nasmyth focus) have an instrument rotator. Because the telescope has an azimuth/altitude mount, you must use the rotator if you wish the orientation of your object to stay fixed on the instrument as the telescope tracks. The commands are ROTATE and TRACK/ROTATE; use whichever is more convenient.

<h3><a name="RotationOptions"></a>Rotation Options</h3>

<h4>Rotate With The Horizon</h4>

<p>You may use the rotator to keep the orientation of the horizon fixed on your instrument. Keeping a slit perpendicular to the horizon eliminates the effects of differential atmospheric dispersion on your spectra. To do this, specify a rotation type of "horizon".

<p>In this mode, your object does not have a fixed orientation on the slit (or whatever). The obvious consequence is that your spectra may be useless if you have an extended object. A subtler consequence is that it can be very difficult to hold the object at a fixed position on the slit, unless you can guide on your object (offset guiding will not help). See <a href="#UsingRotatingField">Using a Rotating Field</a> for help making this work.

<h4>Other Rotation Types</h4>

<p>You can specify a fixed angle for the rotator, either in "physical" or "mount" coordinates. Specifying a mount angle is useful when changing instruments.

<p>Note: leaving the rotator in a fixed position on an az/alt telescope can lead to odd results. For an instrument at a bent Cassegraine port the orientation will be fixed with respect to the horizon, which may be fine, but for an instrument at Nasmyth the orientation will be neither fixed with respect to the sky nor the horizon.

<h4>Specify "Wrap"</h4>

<p>A rotator can usually turn more than 360 degrees (though the exact limits depend on the rotator and the instrument), which offers ambiguity for some angles. Normally the middle wrap is used for slewing. However, you may use /ROTWRAP to explicitly specify the wrap, e.g. to allow longer uninterrupted tracking.

<h3><a name="UsingRotatingField"></a>Using a Rotating Field</h3>

<p>Using a rotating field can be difficult, but sometimes you have no choice. Some instruments have no rotators and some science is better performed by keeping the instrument at a fixed angle to the horizon, not the stars. The problem you face is keeping the correct point of the sky centered on the correct part of the instrument, while the rest of the field revolves around it.

<p>To help you deal with this problem, the TCC allows you to specify a "boresight". This is that point on the instrument which is the center of rotation of the field. Normally, of course, this point is the center of the instrument. However, you may change it if you like. With the Echelle Spectrograph (which has no rotator) this allows you to set the center of rotation anywhere along the slit; with an array of detectors, you may set the boresight to be at the center of any detector. The boresights (a.k.a. "instrument-plane offset") can be set with the OFFSET or GENOFFSET commands.

<p>Unfortunately, boresights are not perfect. They can only be as accurate as the telescope's tracking. To maximize tracking accuracy you should "recalibrate" the pointing of the telescope on a nearby position reference star before guiding or taking data, using the TRACK/PTERR command. For maximum accuracy, you can use the guide star itself as a position reference, but this requires finding a guide star with very accurate coordinates, which is difficult. This technique is described in detail under Guiding, Special Techniques.

<p>For more information about working with a rotating field, see the paper <a href="http://www.apo.nmsu.edu/Telescopes/eng.papers/offsetting/Offsetting.html">Offsetting a Telescope Via Remote Control</a>.

<h2><a name="Guiding"></a>Guiding</h2>

<p>The guider consists of one or more guide "probes". If there is only one guide camera then the entire CCD is considered to be a guide probe. The SDSS spectrographic guider has many fiber optic bundles, each of which is a guide probe. The guider is capable of guiding on at most one guide star per guide probe.

<p>The TCC provides information about the guide probes to the hub. Guiding is performed by an actor in the hub.

<h2><a name="Focusing"></a>Focusing</h2>

<p>The focus of the telescope and guide camera is automatically adjusted to compensate for changing temperature, flexure of the telescope, and various guide camera filters. The automatic adjustment is usually good, but never perfect, hence a user-specified offset may be applied.

<p>Use SET FOCUS to tweak the focus of the telescope (by adjusting the secondary mirror). Use SET GFOCUS to set the focus of the guide camera. For best results you should check and fine-tune the focus regularly, more frequently when the temperature is rapidly changing (e.g. at the beginning of the night). To help you choose the best focus, the MC offers a command to take a sequence of focus images.

<h2><a name="ChangingInstruments"></a>Changing Instruments</h2>

<p>First make sure the instrument you wish to use is mounted on the telescope. Then issue the SET INSTRUMENT command to select it. Note that several instruments can be mounted on the telescope at one time (but only in very restricted combinations), using the rotating tertiary mirror to select among them.

<p>The SET INSTRUMENT command reads information about the instrument, such as its position in the focal plane. However it does <b>not</b> move the tertiary rotator on the 3.5m telescope; you must do that separately.

</body>
</html>
