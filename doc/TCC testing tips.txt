TCC testing tips

To turn on the trunk version of the hub:
log into tron@hub35m-p
setup tron trunk
tron restart

To restore the normal version of the hub:
setup tron
tron restart



The guiders can replay a previous night.

gcam replay UT140213

to replay that night, and

gcam replay off

to turn off the replay.

This uses the tcc to calculate the sky positions and get the boresight.  This should test everything the guider needs.


Telemetry monitor for axis controllers (a Java app, unfortunately; note that it requires the VPN):

http://www.apo.nmsu.edu/35m_operations/35m_Telemetry.html
