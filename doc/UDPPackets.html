<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <title>TCC UDP Packets</title>
</head>
<body>
<h1>UDP Packets Sent by the <a href="index.html">TCC</a></h1>

<h2>Contents</h2>

<ul>
    <li><a href="#Introduction">Introduction</a>
    <li><a href="#PacketFormat">Packet Format</a>
    <li><a href="#VersionHistory">Version History</a>
</ul>

<h2><a name="Introduction"></a>Introduction</h2>

<p>The TCC broadcasts UDP packets containing position information. These packets are broadcast 1/second (even if the axes are halted). Remember that UDP is not a reliable protocol, so integrity of the packets cannot be guaranteed.

<p>The broadcast port depends on the telescope. As of 2007-09-04 these ports are:
<ul>
    <li>SDSS 2.5m telescope (tcc25m): port 1200
    <li>3.5m telescope (tcc35m): port 1235
</ul>

<h2><a name="PacketFormat"></a>Packet Format</h2>

<p>The format of version 2.3 (major.minor) packets is as follows:

<p>Each packet contains two components: a header, followed immediately by data.

<table border=1 summary="Packet header">
<tr><th colspan=3>Packet Header</th></tr>
<tr align=left><th>Variable</th> <th>Type</th> <th>Description</th></tr>
<tr><td>Size</td><td>int4</td><td>Number of bytes in this packet (including this header).</td></tr>
<tr><td>Type</td><td>int4</td><td>A code identifying the type of packet; codes are defined where?</td></tr>

<tr><td>MajorVers</td><td>int4</td><td>Major version number; incremented if packet is changed in a way that is more than simply an extension. Users should check that the major version number is exactly as expected.</td></tr>
<tr><td>MinorVers</td><td>int4</td><td>Minor version number; incremented if packet data is extended. Users should check that the minor version number is no smaller than expected.</td></tr>
</table>

<table border=1 summary="Packet data">
<tr><th colspan=3>Packet Data</th></tr>
<tr align=left><th>Variable</th> <th>Type</th> <th>Description</th></tr>
<tr><td>TAIDate</td><td>double</td><td>Date at which data was or will be current (TAI, MJD seconds)</td></tr>
<tr><td>SlewEndtime</td><td>double</td><td>Date at which current/last slew ends/ended (TAI, MJD seconds); NaN if the telescope is not slewing (or, possibly, tracking).</td></tr>
<tr><td>CoordSys(8)</td><td>byte</td><td>Coordinate system (as a string)</td></tr>
<tr><td>Epoch</td><td>double</td><td>Epoch (decimal year; Besselian for FK5, Julian otherwise).</td></tr>
<tr><td>ObjNetPos(2)</td><td><a href="#posVelDouble">pos, vel double</a></td><td>Net target position in user coordinates; axis 1,2 (e.g. RA,Dec or Az,Alt).</td></tr>
<tr><td>Boresight(2)</td><td><a href="#posVelDouble">pos, vel double</a></td><td>Boresight position (instrument x,y).</td></tr>
<tr><td>RotType</td><td><a href="#paddedInt4">padded int4</a></td><td>User-specified type of rotation; see <a href="#RotTypeCodes">rotation type codes</a>.</td></tr>
<tr><td>RotPos</td><td><a href="#posVelDouble">pos, vel double</a></td><td>User-specified rotation angle; the exact meaning depends on RotType.</td></tr>
<tr><td>ObjInstAng</td><td><a href="#posVelDouble">pos, vel double</a></td><td>Orientation of the object with respect to the instrument.</td></tr>
<tr><td>SpiderInstAng</td><td><a href="#posVelDouble">pos, vel double</a></td><td>Orientation of the secondary spider with respect to the instrument.</td></tr>
<tr><td>TCCPos(3)</td><td><a href="#posVelDouble">pos, vel double</a></td><td>Mount position of (azimuth, altitude, instrument rotator) axes, as requested by the TCC.</td></tr>
<tr><td>SecFocus</td><td>double</td><td>User-set secondary mirror focus offset.</td></tr>
<tr><td>AxisCmdState(3)</td><td><a href="#paddedInt4">padded int4</a></td><td>Commanded state of (azimuth, altitude, instrument rotator) axes; see <a href="#AxisCmdStateCodes">axis commanded state codes</a>.</td></tr>
<tr><td>AxisErrCode(3)</td><td><a href="#paddedInt4">padded int4</a></td><td>Error code of each axis; if the TCC has commanded a halt, this explains why (azimuth, altitude, instrument rotator) axes; see <a href="#AxisErrCodes">axis error codes</a>.</td></tr>
<tr><td>ActMount(3)</td><td><a href="#posVelTimeDouble">pos, vel, time double</a></td><td>Actual mount position, velocity and time as reported by the azimuth, altitude and rotator axis controllers.</td></tr>
<tr><td>AxisStatusWord(3)</td><td><a href="#paddedInt4">padded int4</a></td><td><a href="http://www.apo.nmsu.edu/Telescopes/HardwareControllers/AxisControllers.html#StatusBits">Status word</a> reported by azimuth, altitude and rotator axis controller. Invalid if the corresponding axis does not exist.</td></tr>
</table>

<p><a name="paddedInt4"></a>"padded int4" means the data is contained in the first 4 bytes and the last 4 bytes are padding for data alignment. The padding bytes will probably be 0 but please do not rely on that.

<p><a name="posVelDouble"></a>"pos, vel double" is position, velocity as a pair of double precision numbers. Position is in degrees, velocity in deg/sec, and the time is given by the TAIDate field.

<p><a name="posVelTimeDouble"></a>"pos, vel, time double" is position, velocity and time as three double precision numbers. Position is in degrees, velocity in deg/sec and time is TAI (MJD, seconds).

<p><a name="RotTypeCodes"></a>Rotation type codes are as follows (as defined in tinc:axedef.for). The corresponding names are described in <a href="Commands.html#Rotate">TCC Commands: Rotate.</a>
<table border=1 summary="RotType values">
<tr><th>RotType</th><th>Description</th></tr>
<tr><td>0</td><td>None</td></tr>
<tr><td>1</td><td>Object</td></tr>
<tr><td>2</td><td>Horizon</td></tr>
<tr><td>3</td><td>Physical</td></tr>
<tr><td>4</td><td>Mount</td></tr>
</table>

<p><a name="AxisCmdStateCodes"></a>Axis commanded state codes are as follows. This corresponds to <a href="MessageKeywords.html">TCC Message Keyword</a> AxisCmdState, but with integer values (as defined in tcc include file basics.h) instead of strings.
<table border=1 summary="AxisCmdState values">
<tr><th>AxisCmdState</th><th>Description</th></tr>
<tr><td>-1</td><td>NotAvailable: axis is not available</td></tr>
<tr><td>0</td><td>Halted</td></tr>
<tr><td>1</td><td>Drifting</td></tr>
<tr><td>2</td><td>Slewing</td></tr>
<tr><td>3</td><td>Halting</td></tr>
<tr><td>4</td><td>Tracking</td></tr>
<tr><td>5</td><td>BadCode: unknown command state</td></tr>
</table>

<p><a name="AxisErrCodes"></a>Axis commanded state codes are as follows. This corresponds to <a href="MessageKeywords.html">TCC Message Keyword</a> AxisErrCode, but with integer values (as defined in tcc include file basics.h) instead of strings.
<table border=1 summary="AxisErrCode values">
<tr><th>AxisErrCode</th><th>Description</th></tr>
<tr><td>-3</td><td>HaltRequested: halt requested</td></tr>
<tr><td>-2</td><td>NoRestart: axis left halted via /NoRestart</td></tr>
<tr><td>-1</td><td>NotAvailable: axis not available</td></tr>
<tr><td>0</td><td>OK (no error)</td></tr>
<tr><td>1</td><td>MinPos: position too small</td></tr>
<tr><td>2</td><td>MaxPos: position too large</td></tr>
<tr><td>3</td><td>MaxVel: velocity to large</td></tr>
<tr><td>4</td><td>MaxAccel: acceleration too large</td></tr>
<tr><td>5</td><td>MaxJerk: jerk too large</td></tr>
<tr><td>6</td><td>CannotCompute: could not compute the position</td></tr>
<tr><td>7</td><td>ControllerErr: could not communicate with the axis controller</td></tr>
<tr><td>8</td><td>TCCBug: a TCC bug</td></tr>
<tr><td>9</td><td>BadCode: unknown error code</td></tr>
</table>

<p>Notes:
<ul>
    <li>See the <a href= "MessageKeywords.html">TCC Message Keywords Dictionary</a> for full definitions of most items in the "data" section of the packet.
    <li>pos, vel double is a structure containing two doubles: "pos" and "vel"; it contains the same information as the TCC's "coordinates" (p,v,t triplets), but with time given by the "TAIDate" entry in the packet.
    <li>Comparing SlewEndtime and TAIDate will tell you if the telescope is still slewing (as of when the packet data was generated).
    <li>CoordSys and Epoch are guaranteed not to change unless SlewEndtime also changes.
</ul>

<h2><a name="VersionHistory"></a>Version History</h2>

<h3>2.4 2014-09</h3>

<p>Added AxisErrCode, ActMount and AxisStatusWord fields.

<h3>2.3.1 2015-05</h3>

<p>Added reference to tcc35m-1-p and made minor tweaks to the text.

<h3>2.3 2009-01</h3>

<p>Added AxisCmdState field.

<h3>2.2 2000-10</h3>

<p>Added SecFocus field.

<h3>2.1 1995-10</h3>

<p>New packet format.

</body>
</html>