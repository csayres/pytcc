<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Instrument Data Fitting</title>
</head>
<body>
<h2>Instrument Data Fitting for the <a href="index.html">TCC</a></h2>

<p>Version 2.0.2 Last updated 2015-09-10

<p>Inst Data Fit allows one to determine various parameters needed for instrument data files, including: scale, orientation and the position of the rotator with respect to the instrument. Inst Data Fit can handle both instruments and guide cameras. Some sample data files are included.

<h2><a name="ScaleAndOrientation"></a>Scale and Orientation</h2>

<h3>Collect the Data</h3>

<ul>
    <li>Move to a fairly bright (for the instrument) isolated star high in the sky and near the meridian. For a guider or slit viewer you can use
    tcc command <code>track/pterr 10,70 obs</code>.
    
    <li>Rotate with the object at zero degrees: <code>rotate 0 object</code>
    
    <li>Move the boresight to various locations on the instrument using tcc command <code>offset/pabs bore <i>x</i>, <i>y</i></code>; note that x, y are in degrees, so use small values.
    
    <li><a name="FindingCentroids"></a>At each offset position measure and record the centroid in pixels, being aware of the
    <a href="#ZeroConventions">zero convention</a>.
    
    <li>Record the boresight position (in degrees) and the centroid (in pixels) in a text file, in 4 columns, as follows.
    <a name="DataFileFormatNotes"></a>Columns may be separated using spaces or a single tab. Leading and trailing lines that do not begin with digits will be ignored.
</ul>

<pre>
boresight x     boresight y     centroid x     centroid y
   (deg)           (deg)         (pixels)       (pixels)
</pre>

<h3>Fit the Data</h3>

<ul>
    <li>Run Inst Data Fit. <b>Note that it has balloon help.</b>
    
    <li>Make sure the type of data is Instrument (Guider is obsolete). You can use the popup menu or the appropriate entry in the Macros menu.
    
    <li>Specify all fields above the "Fit Scale and Orientation" button <i>New Rot_Inst_ang</i>. Use the balloon help if needed. You may leave <i>Scale</i> alone <b>unless</b> you check the <i>Aspect Ratio is Exact</i> box, in which case you must specify <i>Scale X</i> and <i>Y</i> such that their <b>ratio</b> (including the <b>sign</b> of the ratio) is exactly correct. (The magnitude need not be particularly close).
    
    <ul>
        <li>Regarding GIm_Ctr or IIm_Ctr: this is the desired center of the image. Use the <b>exact</b> physical center of the imaging area unless you have some overwhelming reason to do otherwise; any other choice will cause great confusion among other astronomers and engineers. For example: a 1024x1024 CCD has a center position of (512,512), using the standard <a href="#ZeroConventions">zero convention</a>.
    </ul>
        
    <li>Push the <i>Fit Scale and Orientation</i> button.
    
    <li>It will ask you for a file; feed it your scale data file.
    
    <li>A graph is drawn which indicates if the fit converged properly.
    
    <ul>
        <li>If it does not converge and the <i>Aspect Ratio is Exact</i> box is checked then you may have the sign of <i>Scale X</i> wrong. Reverse the sign of Scale X, double check that the ratio is correct (i.e. for square pixels the absolute value of
        <i>Scale X</i> should equal the absolute value of <i>Scale Y</i>) and perform the fit again.
        
        <li>If that doesn't work, uncheck the <i>Aspect Ratio is Exact</i> box and fit again. If the fit converges but the ratio of the scales is way off, then your data is defective.
        
        <li>If the fit fails to converge with <i>Aspect Ratio is Exact</i> unchecked, then your data is probably defective.
    </ul>
        
    <li>Results appear in the lower window (<i>Inst Data Fit</i>...), including error estimates. The main panel is also updated. Pay careful attention to the sign of scale x (scale y is always positive). Please be sure to <a href="#ApplyScaleAndOrientationResults">apply these results</a> to the instrument block before measuring
    <a href="#CenterOfRotation">Center of Rotation</a> data. If you fail to update the instrument data file <b>and load it</b>, the center of rotation data may be useless.
</ul>

<h3><a name="ApplyScaleAndOrientationResults"></a>Apply the Results</h3>

<ul>
    <li>Edit the instrument data file (on the TCC in directory TINST:); the relevant parameters are:
    
    <ul>
        <li>for an instrument: <code>IIm_Scale</code> (scale) and <code>Rot_Inst_ang</code> (orientation)
        
        <li>for a guider, scale is entered as <code>GIm_Scale</code>, but orientation is entered as GProbe data: <code>angle of rot w.r.t. image</code>
    </ul>
    
    <li>Load the new instrument data file using tcc command <code>set instrument=name</code>
    
    <li>Verify that the new values have been loaded using tcc command <code>show block inst</code>
</ul>

<h2>Center of Image and <a name="CenterOfRotation"></a>Position of Rotator or Guide Probe</h2>

<h3>Collect the Data</h3>

<ul>
    <li>Make sure scale and orientation are set correctly in the TCC, e.g. using tcc command <code>show block inst</code>. This is especially important if you computed new scalen and orientation values, i.e. make sure you successfully <a href="#ApplyScaleAndOrientationResults">applied and saved the results</a>.
    
    <li>Move to an FK5 star which is high in the sky and near the meridian, e.g. <code>track/pterr 10, 70 obs</code>
    
    <li>Zero the boresight position (instrument-plane offset) using tcc command <code>offset/pabs bore 0, 0</code>
    
    <li>Move the star part-way between the center of the CCD and the edge using an <b>arc</b> offset: <code>offset arc <i>ra</i>, <i>dec</i></code>. <b>Do not</b> use boresight offsets; the boresight must stay at (0,0)!
    
    <li>Rotate to various angles using tcc command <code>rotate/rotwrap=near <i>ang</i> obj</code> (/rotwrap=near is optional, but may help reduce unexpected unwrapping). At each angle measure the centroid of the star in pixels, as <a href="#FindingCentroids">above</a>.
    
    <li>Record rotator angle (in degrees) and centroid (in pixels) in a text file, in 3 columns, as follows (see also <a href="#DataFileFormatNotes">format notes</a>):
</ul>

<pre>
angle     centroid x     centroid y
(deg)      (pixels)       (pixels)
</pre>

<h3>Fit the Data</h3>

<ul>
    <li>Make sure the type of data is Instrument (Guider is obsolete). You can use the popup menu or the appropriate entry in the Macros menu.
    
    <li>Make sure all values except the last new_..._xy values above the <i>Fit Center of Rotation</i> button are correct and were the values used to take the data.
    
    <ul>
        <li>Regarding GIm_Ctr or IIm_Ctr: this is the desired center of the image. Use the <b>exact</b> physical center of the imaging area unless you have some overwhelming reason to do otherwise; any other choice may cause confusion among other astronomers and engineers. For example: a 1024x1024 CCD has a center position of (512,512) (using using the standard <a href="#ZeroConventions">zero convention</a>).
    </ul>
    
    
    <li>Push the <i>Fit Center of Rotation</i> button.
    
    <li>Select your data file when requested.
    
    <li>A graph is drawn which indicates if the fit converged properly. If the fit does not converge:
    
    <ul>
        <li>Make sure the <i>Scale X</i> and <i>Y</i> values in the
        <i>Fit Instrument Data</i> window are correct, including
        <b>sign</b>.
        
        <li>Make sure you entered the offsets and/or rotator angles by talking directly to the TCC. If you enter these into the MC or Remark, instead, you may get odd sign changes that mess up your data. If this is the problem then I suggest retaking all data, including that for scale and orientation.
        
        <li>Make sure the scale and orientation data in the TCC is correct, including <b>sign</b>. If they are wrong, you must fix the problem and then take the center-of-rotation data again.
    </ul>
    
    <li>Results appear in the lower window (<i>Inst Data Fit</i>...), including error estimates. The main panel is also updated. Pay careful attention to the sign of scale x (scale y is always positive).
</ul>

<h3>Apply The Results</h3>

<ul>
    <li>Edit the instrument data file (on the TCC in directory TINST:); the relevant parameters are:
    
    <ul>
        <li>for an instrument: <code>IIm_Ctr</code> (desired center) and <code>Rot_Inst_xy</code> (center of rotation with respect to instrument)
        
        <li>for a guider: <code>GIm_Ctr</code> (desired center) and guide probe <code>pos. of rot w.r.t. image</code> (cnter of rotator with respect to guide probe).
    </ul>

    <li>Load the new data using <code>set instrument=<i>name</i></code>
    
    <li>Verify that the new values have been loaded using <code>show block inst</code>
</ul>

<h2><a name="ZeroConventions"></a>Zero Conventions</h2>

<ul>
    <li>The zero convention used by the TCC and by APO guiders is: (0,0) is the lower left corner of the chip, so (0.5,0.5) is the center of the lower left pixel. However, FITS and ds9 use (1, 1) as the center of the lower left corner pixel.
</ul>

<h2>Revision History</h2>

<ul>
    <li>2.0.2 2015-09-10 R.O. Updated the information for the new tcc, including always using the Instrument panel (never the Guider panel) of the fitter, updating TCC commands (especially track/pterr) and correcting the discussion of zero convention.

    <li>2.0.1 2007-09-04 R.O. Minor html cleanup.
     
    <li>2.0 2001-09-28 R.O. Added support for "full-frame" guiders. One can get the gp_rot_xy from the instrument center-of-rotation data instead of having to take a redundant third set of data (or skipping that step and getting into trouble).
    
    <li>1.9 3/15/01 Minor update to comments. Final release of 1.9.
    
    <li>1.9b5 3/15/00 Added output of the desired and fit center when fitting center of rotation. Removed a constraint on the scale and orientation graph that wasn't doing anything useful. Looked for more errors like the ones fixed in 1.9b4 and found none. R.O.
    
    <li>1.9b4 3/14/00 Fixed two bugs in center of rotation fitting: the y component of new_rot_inst_xy was wrong and the desired center was being mis-displayed. Both errors were caused by typos involving confusing _y and _x. Not publicly released. R.O.
    
    <li>1.9b3 5/28/99 Improved the controls layout and documentation. The main motivation was to make it clear that xIm_Ctr must be specified before fitting scale and orientation). Also fixed a zero convention error in the balloon help. R.O.
    
    <li>1.9b2 5/19/99 Added support for new guider handling. Made separate guider and instrument panels.R.O.
    
    <li>1.8.1 6/19/97 Fixed a sign error in scale and orientation fitting. Also improved the instructions and documented the iraf/new guider zero convention. R.O.
    
    <li>1.7 11/7/96 Added handling for the binning factor. Modified the instructions so that new scale and orientation values are applied to the instrument block before measuring center-of-rotation data (to solve a problem brought to light by SPICam). Rewrote the documentation in HTML format. R.O.
    
    <li>1.6 3/25/96 Fixed sign/quadrant bugs in scale and orientation fitting. Modified so that y_scale is always positive; this is correct for drift-scanning. Added graphs to show fit and measured data (Dan Long's suggestion). R.O.
    
    <li>1.5 4/19/94 Another attempt to fix Rot_Inst_xy fitting. R.O.
    
    <li>1.4 4/14/94 Corrected bugs in Rot_Inst_xy; removed centering requirements. R.O.
    
    <li>1.3.1 4/11/94 Slight modifications to this Readme; no change to the application. R.O.
    
    <li>1.3 4/8/94 Added experimental Rot_Inst_xy; first version with revision history. R.O.
</ul>

</body>
</html>
