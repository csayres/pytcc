import os
import platform
import subprocess

DoDebug = True # if True, compile with no optimization and with -g

isMac = platform.system().lower() == "darwin"

installDir = "/usr/local"           # destination directory for installs
includeList = ["tcs.h", "tcssys.h"] # list of include files you want installed

if isMac:
    archFlags = "-arch i386 -arch x86_64"
    cc = "clang"
    cFlags = ""
    extraLibs = []
else:
    archFlags = ""
    cc = "gcc"
    cFlags = ""
    extraLibs = ["m"]

if DoDebug:
    print "WARNING: compiling in debug mode; disabling optimization"
    cFlags += " -O0 -g"
else:
    cFlags += " -O3"

env = Environment(
    CC = cc,
    CFLAGS = "%s -Wall -pedantic -I include/ %s" % (archFlags, cFlags),
    LINKFLAGS = archFlags,
)

exampleSrcList = Glob("examples/*.c")
srcList = Glob("src/*.c")

sharedLib = env.SharedLibrary(
    target = "lib/tcs",
    source = srcList,
    LIBS = ["sla"],
)

for examplePath in exampleSrcList:
    exampleName = os.path.splitext(os.path.basename(str(examplePath)))[0]
    env.Program("examples/%s" % (exampleName,),
        source = ["examples/%s.c" % (exampleName,), sharedLib],
        LIBS = ["sla"] + extraLibs,
    )

def buildManual(target, source, env):
    print "building the manual"
    subprocess.call("./manual", shell=True, cwd="./src")
    print "moving the manual to doc/"
    os.rename("src/manual.txt", "doc/tcspk manual.txt")
env.Command('doc/tcspk manual.txt', [], buildManual)

env.Alias("install", [
        env.Install(os.path.join(installDir, "lib"), sharedLib),
        env.Install(os.path.join(installDir, "include"), [os.path.join("include", name) for name in includeList]),
    ]
)
