#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Take a fresh copy of tcSpk and rearrange the files so that they can be built with scons and managed by eups
"""
import glob
import os
import sys
import shutil
import stat

def arrangeTCSpk(path):
    """Arrange the files in a fresh distribution of tcspk
    """
    if not os.path.isdir(path):
        raise RuntimeError("Cannot find path: %r" % (path,))

    mainHeaderPath = os.path.join(path, "tcs.h")
    if not os.path.isfile(mainHeaderPath):
        raise RuntimeError("Cannot find the main header file: %r" % (mainHeaderPath,))
    
    print "creating subdirectories"
    for subdir in ("doc", "examples", "src", "include", "lib", "ups", "misc"):
        os.mkdir(os.path.join(path, subdir))
        
    print "move doc files"
    docFiles = glob.glob(os.path.join(path, "*.pdf"))
    if path.endswith("/"):
        pathParent = os.path.dirname(path[0:-1])
    else:
        pathParent = os.path.dirname(path)
    docFiles += glob.glob(os.path.join(pathParent, "tcspk.pdf"))
    docFiles += glob.glob(os.path.join(path, "read.me"))
    for fPath in docFiles:
        fName = os.path.basename(fPath)
        os.rename(fPath, os.path.join(path, "doc", fName))

    print "move example files"
    exampleFiles = glob.glob(os.path.join(path, "tcs_*.c"))
    exampleFiles += glob.glob(os.path.join(path, "*.mod"))
    for fPath in exampleFiles:
        fName = os.path.basename(fPath)
        os.rename(fPath, os.path.join(path, "examples", fName))

    manExePath = os.path.join(path, "manual")
    if os.path.isfile(manExePath):
        manFlags = os.stat(manExePath).st_mode
        if not (manFlags & stat.S_IXUSR) != 0:
            print "setting owner executable bit on %r" % (manExePath,)
            os.chmod(manExePath, manFlags | stat.S_IXUSR)
    else:
        manExePath = None
    
    print "move misc files"
    miscFiles = glob.glob(os.path.join(path, "refsub*.txt"))
    miscFiles += glob.glob(os.path.join(path, "makefile"))
    for fPath in miscFiles:
        fName = os.path.basename(fPath)
        os.rename(fPath, os.path.join(path, "misc", fName))

    print "move source files"
    srcFiles = glob.glob(os.path.join(path, "*.c"))
    if manExePath:
        srcFiles.append(manExePath)
    for fPath in srcFiles:
        fName = os.path.basename(fPath)
        os.rename(fPath, os.path.join(path, "src", fName))
    
    print "move header files"
    headerFiles = glob.glob(os.path.join(path, "*.h"))
    for fPath in headerFiles:
        fName = os.path.basename(fPath)
        os.rename(fPath, os.path.join(path, "include", fName))
    
    print "copy ups files"
    upsFromPath = os.path.join(os.path.dirname(__file__), "filesForTCSpk", "ups")
    upsFiles = glob.glob(os.path.join(upsFromPath, "*"))
    for fPath in upsFiles:
        shutil.copy2(fPath, os.path.join(path, "ups"))
    
    print "copy SConstruct file"
    sconsFromPath = os.path.join(os.path.dirname(__file__), "filesForTCSpk", "SConstruct")
    shutil.copy2(sconsFromPath, path)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        raise RuntimeError("Must supply the path to the newly unpacked tcSpk dir")
    path = sys.argv[1]
    arrangeTCSpk(path)
