#include <sstream>

for (int i = 0; i < MAXTRM; ++i) {
    std::cout << "pmod.model[" << i << "] " << pmod.model[i] << " # Term number for current model (0 = end)" << std::endl;
    std::cout << "pmod.coeffv[" << i << "] " << pmod.coeffv[i] << " # Coefficient value (radians)" << std::endl;
    if (pmod.model[i] == 0) break;
}
std::cout << "pmod.nterml " << pmod.nterml << " # Number of local terms" << std::endl;
std::cout << "pmod.ntermx " << pmod.nterml << " # Number of terms implemented explicitly (local+standard)" << std::endl;
std::cout << "pmod.nterms " << pmod.nterml << " # Number of terms available currently (local+standard+generic)" << std::endl;
for (int i = 0; i < NTROOM; ++i) {
    std::cout << "pmod.coeffn[" << i << "] " << pmod.coeffn[i] << " # Coefficient names (local, standard, generic)" << std::endl;
    std::cout << "pmod.coform[" << i << "] " << pmod.coform[i] << " # Format of generic terms added to coeffn" << std::endl;
}

std::cout << "tar.t0 " << tar.t0 << " # Reference time (TAI MJD)" << std::endl;
for (int i = 0; i < 2; ++i) {
    std::cout << "tar.p0[" << i << "] " << tar.p0[i] << " # Position at reference time (spherical coordinates, radians)" << std::endl;
    std::cout << "tar.dt[" << i << "] " << tar.dt[i] << " # Differential rates (radians/day)" << std::endl;
    for (int j = 0; j < 3; ++j) {
        std::cout << "tar.ob[" << j << "][" << i << "] " << tar.ob[j][i] << " # Offsets from base (radians, threefold, additive)" << std::endl;
    }
    std::cout << "tar.op0[" << i << "] " << tar.op0[i] << " # Position at reference time including offsets from base" << std::endl;
    std::cout << "tar.p[" << i << "] " << tar.p[i] << " # Current position including offsets and differential tracking" << std::endl;
}
