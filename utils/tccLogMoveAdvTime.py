#!/usr/bin/env python2
"""Translate part of an axis controller log file, showing how far in advance MOVE PVT commands are

Here is a sample line with inadequate timestamp format
Jul 23 08:43:29 tcc35m-1-p tcc35m.py: AxisDevice(alt) read 'MOVE 20.0133116 0.0000000 31410.16710 OK'; curr cmd='MOVE 20.0133116 0.0000000 31410.16710' running
Here is the high precision version:
2014-08-09T23:18:46.294568+00:00 tcc35m tcc35m.py: AxisDevice(alt) read 'MOVE 20.0133116 0.0000000 31410.16710 OK'; curr cmd='MOVE 20.0133116 0.0000000 31410.16710' running
"""
import re
import sys

import RO.StringUtil

sampleStr = "2014-08-09T23:18:46.294568+00:00 tcc35m tcc35m.py: AxisDevice(alt) read 'MOVE 20.0133116 0.0000000 31410.16710 OK'; curr cmd='MOVE 20.0133116 0.0000000 31410.16710' running"
movePVTRE = re.compile(r"\d\d\d\d-\d\d-\d\dT(\d\d:\d\d:\d\d\.\d+)\+00:00 tcc.+: AxisDevice\(([a-zA-Z0-9]+)\) read 'MOVE .* (\d+\.\d+)(?: +OK')")
# movePVTRE.match(sampleStr)

def translateFile(filePath):
    """Parse log of output from an axis controller

    Print ech MOVE P V T command, including how far in advance each MOVE P V T command is (sec,
    positive numbers are in advance)
    """
    with open(filePath) as f:
        for line in f:
            line = line.strip()
            match = movePVTRE.match(line)
            if not match:
                continue
            logHSMStr, axis, pvtSecStr = match.groups()
            logSec = RO.StringUtil.degFromDMSStr(logHSMStr) * 3600.0
            pvtSec = float(pvtSecStr)
            print "%s: %0.3f" % (line, axis, pvtSec - logSec)

if len(sys.argv) < 2:
    print """Usage: tccLogMoveAdvTime.py filePath
where filePath is a tcc log file
"""
    sys.exit(1)

filePath = sys.argv[1]
translateFile(filePath)

