#!/usr/bin/env python2
"""Translate part of an axis controller log file, showing how far in advance MOVE PVT commands are
"""
import re
import sys

import RO.StringUtil

movePVTRE = re.compile(r"\d\d\d\d-\d\d-\d\d (\d\d:\d\d:\d\d\.\d+)Z +MOVE .* (\d+\.\d+)(?: +OK)?$")

def translateFile(filePath, minDeltaT=None):
    """Parse log of output from an axis controller

    Print ech MOVE P V T command, including how far in advance each MOVE P V T command is (sec,
    positive numbers are in advance)
    """
    with open(filePath) as f:
        for line in f:
            line = line.strip()
            match = movePVTRE.match(line)
            if not match:
                continue
            logHSMStr, pvtSecStr = match.groups()
            logSec = RO.StringUtil.degFromDMSStr(logHSMStr) * 3600.0
            pvtSec = float(pvtSecStr)
            dt = pvtSec - logSec
            if minDeltaT is not None and dt > minDeltaT:
                continue
            if dt <= 0:
                print "%s: %0.3f **********" % (line, dt)
            else:
                print "%s: %0.3f" % (line, dt)

if len(sys.argv) < 2:
    print """Usage: axisLogMoveAdvTime.py filePath [min_dt]
where filePath is a log file of output from an axis controller
and min_dt is minimum dt to report (no mininum if omitted)
"""
    sys.exit(1)

filePath = sys.argv[1]
if len(sys.argv) > 2:
    minDeltaT = float(sys.argv[2])
else:
    minDeltaT = None

translateFile(filePath, minDeltaT)
