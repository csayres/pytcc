#pragma once
/*
Chebyshev polynomial
*/
#include <stdexcept>
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>

namespace tcc {

    /**
     * 1-dimensional weighted sum of Chebyshev polynomials of the first kind.
     *
     * f(x) = c0 T0(x') + c1 T1(x') + c2 T2(x') + ...
     *      = c0 + c1 T1(x') + c2 T2(x') + ...
     * where:
     * * Tn(x) is the nth Chebyshev function of the first kind:
     *      T0(x) = 1
     *      T1(x) = 2
     *      Tn+1(x) = 2xTn(x) + Tn-1(x)
     * * x' is x offset and scaled to range [-1, 1] as x ranges over [minX, maxX]
     *
     * If the independent variable is outside the range [minX, maxX] then evaluation returns nan.
     *
     * @ingroup afw
     */
    class ChebyshevPolynomial {
    public:
        /**
         * Construct a Chebyshev polynomial with specified parameters and range.
         *
         * @throw std::runtime_error if params is empty or minX >= maxX
         */
        explicit ChebyshevPolynomial(
            std::vector<double> params, ///< polynomial coefficients
            double minX = -1,   ///< minimum allowed x
            double maxX = 1)    ///< maximum allowed x
        :
            params(params)
        {
            if (params.size() < 1) {
                throw std::runtime_error("ChebyshevPolynomial called with empty vector");
            }
            if (minX >= maxX) {
                std::ostringstream os;
                os << "ChebyshevPolynomial minX=" << minX << " >= " << maxX << "=maxX";
                throw std::runtime_error(os.str());
            }
            _initialize(minX, maxX);
        }
        
        /**
         * Default constructor
         */
        explicit ChebyshevPolynomial()
        :
            params(1)
        {
            _initialize(-1.0, 1.0);
        }

        /**
         * Get minimum allowed x
         */
        double getMinX() const { return _minX; };

        /**
         * Get maximum allowed x
         */
        double getMaxX() const { return _maxX; };

        /**
         * Get the polynomial order
         */
        unsigned int getOrder() const { return this->params.size() - 1; };

        /**
        * Evaluate polynomial
        */
        double operator() (double x) const {
            double xPrime = (x + _offset) * _scale;

            if ((xPrime < -1.0) || (xPrime > 1.0)) {
                return std::numeric_limits<double>::quiet_NaN();
            }
            
            // Clenshaw function for solving the Chebyshev polynomial
            // Non-recursive version from Kresimir Cosic
            int const order = getOrder();
            if (order == 0) {
                return this->params[0];
            } else if (order == 1) {
                return this->params[0] + (this->params[1] * xPrime);
            }
            double cshPrev = this->params[order];
            double csh = (2 * xPrime * this->params[order]) + this->params[order-1];
            for (int i = order - 2; i > 0; --i) {
                double cshNext = (2 * xPrime * csh) + this->params[i] - cshPrev;
                cshPrev = csh;
                csh = cshNext;
            }
            return (xPrime * csh) + this->params[0] - cshPrev;
        }

        /**
        Return a string representation (especially for Python)
        */
        std::string __repr__() const;
        
        std::vector<double> params;

    private:
        double _minX;    ///< minimum allowed x
        double _maxX;    ///< maximum allowed x
        double _scale;   ///< x' = (x + _offset) * _scale
        double _offset;  ///< x' = (x + _offset) * _scale

        /**
         * initialize private constants
         */
        void _initialize(double minX, double maxX) {
            _minX = minX;
            _maxX = maxX;
            _scale = 2.0 / (_maxX - _minX);
            _offset = -(_minX + _maxX) * 0.5;
        }
    };

    std::ostream &operator<<(std::ostream &os, ChebyshevPolynomial const &cheby);

}
