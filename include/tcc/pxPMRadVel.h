#pragma once

#include <string>
#include <iostream>
#include "coordConv/coordConv.h"

namespace tcc {

    /**
    An object that holds parallax, proper motion and radial velocity

    Normally this information is held in a coordConv.Coord or PVTCoord,
    but those are intrinsically equatorial and focal plane coordinates are Cartesian,
    which leads to trouble for large (admittedly unrealistically large) focal plane positions.
    */
    class PxPMRadVel {
    public:
        /**
        Construct a PxPMRadVel with no spatial motion
        */
        PxPMRadVel();

        /**
        Construct a PxPMRadVel using the same conventions as for coordConv.Coord

        @param[in] parallax  parallax (arcsec)
        @param[in] equatPM  equatorial proper motion (arcsec/century);
            this is dEquatAng/dt, so it gets large near the pole
        @param[in] polarPM  polar proper motion (arcsec/century)
        @param[in] radVel  radial velocity (km/sec, positive receding)
        */
        PxPMRadVel(double parallax, double equatPM, double polarPM, double radVel);
        
        bool operator==(PxPMRadVel const &rhs);
        
        bool operator!=(PxPMRadVel const &rhs);

        /**
        Return a string representation
        */
        std::string __repr__() const;

        double parallax;    ///< parallax (arcsec)
        double equatPM;     ///< equatorial proper motion (arcsec/century); this is dEquatAng/dt, so it gets large near the pole
        double polarPM;     ///< polar proper motion (arcsec/century)
        double radVel;      ///< radial velocity (km/sec, positive receding)
    };

    std::ostream &operator<<(std::ostream &os, PxPMRadVel const &pxPMRadVel);

}
