#pragma once
#include "coordConv/coordConv.h"
#include "tcc/inst.h"
#include "tcc/obj.h"
#include "tcc/tcsSite.h"
#include "tcc/telMod.h"

namespace tcc {

    /**
    Compute obsPos and related products from netUserPos
    
    @param[in,out] obj  object data
        Input fields include:
        - site
        - netUserPos
        - arcOrient
        - arcUserNoArcUserAng
        Ouput fields include:
        - obs
        - objUserObjAzAng
        - objUserObjInstAng
        - objAzInstAng
    @param[in] tai  TAI date at which to compute obs and other values (MJD, seconds)
    */
    void obsFromNetUser(
        Obj &obj,
        double tai
    );

}
