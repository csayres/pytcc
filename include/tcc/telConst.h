#pragma once
/*
Site-dependant and other telescope-related constants,
for the ARC 3.5m telescope at Apache Point, New Mexico

History:
- R. Owen  12/86  UW/ARC
- Changed SCT_min_LCT's name and units; changed Elevation's units. R.O. 8/87
- Rename Latitude -> MeanLat, Longitude -> MeanLong. R.O. 2/88
- Improved values for Lat, Long, El, removed SCT_min_LCT. R.O. 2/89
- Changed longitude to east (was west). R.O. 9/92
- Switched to values from WSMR. R.O. 4/99
- Adapted to C++ and changed units of elevation from km to m. R.O. 2011-12
*/
namespace tcc {

    // position of 3.5m optical axis, measured by the WSMR team
    const double MeanLong  =  -105.820417;  // longitude east, deg
    const double MeanLat   =    32.780361;  // latitude north, deg
    const double Elevation =  2788;         // elevation above mean sea level, m

}
