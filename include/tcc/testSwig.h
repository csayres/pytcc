#pragma once
#include "boost/shared_ptr.hpp"
#include "boost/make_shared.hpp"
#include "coordConv/coordConv.h"

namespace tcc {

    /**
    Test some SWIG issues with access to atrributes of structs

    pvt fields could not be set until I found a fix
    obsSys in Obj cannot be manipulated at all and I have not found a fix,
    though a hackaround is to save a shared_ptr
    */
    class TestSwig {
    public:
        TestSwig() : pvt(), obsSys(2012.5), ptrObsSys(boost::make_shared<coordConv::ObsCoordSys>(2013.5)) {}
        coordConv::PVT pvt;
        coordConv::ObsCoordSys obsSys;
        boost::shared_ptr<coordConv::ObsCoordSys> ptrObsSys;
    };

}

/*
Demo program

python

import tcc.base
testSwig = tcc.base.TestSwig()
testSwig.pvt.pos = 125
assert testSwig.pvt.pos == 125

assert testSwig.ptrObsSys.getDate() == 2013.5

try:
    assert testSwig.obsSys.getDate() == 2012.5
except AttributeError:
    print "FAILED: testSwig.obsSys has no attribute getDate"

*/