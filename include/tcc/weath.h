#pragma once
#include "coordConv/site.h"

/*
Weather block
*/
namespace tcc {

    class Weath {
    public:
        double airTemp;             ///< ambient air temperature (C)
        double secTrussTemp;        ///< secondary truss temperature (C)
        double primFrontTemp;       ///< primary mirror front temperature (C)
        double primBackFrontTemp;   ///< primary mirror back - front temperature difference (C)
        double secFrontTemp;        ///< secondary mirror front temperature (C)
        double secBackFrontTemp;    ///< secondary mirror back - front temperature difference (C)
        double press;               ///< air pressure (Pascals)
        double humid;               ///< humidity (fraction)
        double tempLapseRate;       ///< temperature lapse rate (C/km); 6.5 is a good default
        double windSpeed;           ///< wind speed (m/sec)
        double windDir;             ///< wind direction (degrees, from the south = 0, from the east = 90)
        double timestamp;           ///< TAI date of last update (MJD, sec)
        
        explicit Weath();
        
        /**
        Compute refraction coefficients A and B

        unrefractedZD = refractedZD + deltaZD, where:
        * deltaZD = A tan(refractedZD) + B tan(refractedZD)^3
        * refractedZD is the observed zenith distance on earth (rad)
        * unrefractedZD is the zenith distance in vacuum (rad)
        (The equation is backwards from what we need to compute refracted zenith distance.)
        
        @param[out] refCoA  refraction coefficient A (deg)
        @param[out] refCoB  refraction coefficient B (deg)
        @param[in] wavelen  wavelength (Angstroms)
        @param[in] elev  height of the observer above sea level (m)
        @param[in] lat  latitude of the observer (deg)
        */
        void computeRefCo(double &refCoA, double &refCoB, double wavelen, double elev, double lat) const;
        
        /**
        Update refraction coefficients in site information
        
        @param[in,out] site information
            fields read:
            - elev
            - meanLat
            - wavelen
            fields written:
            - refCoA
            - refCoB
        */
        void updateSite(coordConv::Site &site) const;
    };

}
