#pragma once
/*
Basic definitions.

Note:
  The units used are degrees and seconds,
  and absolute time is TAI in MJD (in sec), unless otherwise noted.
*/
#include <cmath>
#include <map>
#include <string>

namespace tcc {

    const int NAxes = 3;    //< Number of telescope mount axes (Az, Alt, Rot)
    
    const double DeltaTForVel = 0.1;    //< time between adjacent coordinate conversions for computing velocity (sec)
    
    /**
    * Return the current TAI date (MJD seconds)
    *
    * @warning Assumes that the system clock is synchronized to TAI (not UTC, as is usually the case)!
    * Also assumes that the time function returns seconds since 1970-01-01 00:00:00 (true on unix and MacOS X).
    */
    double tai();
    
    /**
    Types of motion for the instrument rotator
    
    @warning: if you change this, also edit enumDicts.i
    */
    enum RotTypeEnum {
        RotType_Begin   = 0, // invalid value used for range checking
        RotType_None    = 0, // do not rotate (obj.rotUser is ignored)
        RotType_Object  = 1, // angle of user coord axis 1 w.r.t. inst x at the un-arc-offset object position
        RotType_Horizon = 2, // angle of increasing azimuth w.r.t. inst x at the un-arc-offset object position
                       // 3 was used for physical rotation
        RotType_Mount   = 4, // angle of rotator w.r.t. its mount, in controller units, with no wrap
        RotType_End     = 4  // invalid value used for range checking
    };
    
    /**
    Wrap preferences
    
    @warning: if you change this, also edit enumDicts.i
    */
    enum WrapTypeEnum {
        WrapType_Begin    = -1,  // invalid value used for range checking
        WrapType_None     = -1,  // no wrap specified; only permitted for mount coordinates
        WrapType_Nearest  =  0,  // choose wrap with nearest position
        WrapType_Negative =  1,  // choose wrap with most negative value
        WrapType_Middle   =  2,  // choose wrap closest to middle of total range
        WrapType_Positive =  3,  // choose wrap with most positive position
        WrapType_NoUnwrap =  4,  // like nearest, but keep moving into a limit rather than unwrapping
        WrapType_End      =  4   // invalid value used for range checking
    };
    
    /**
    States (especially commanded states) for axes
    
    @warning: if you change this, also edit enumDicts.i
    */
    enum AxisStateEnum {
        AxisState_Begin        = -1,
        AxisState_NotAvailable = -1,
        AxisState_Halted       =  0,
        AxisState_Drifting     =  1,
        AxisState_Slewing      =  2,  // slewing to track
        AxisState_Halting      =  3,  // slewing to a controlled halt
        AxisState_Tracking     =  4,
        AxisState_BadCode      =  5,
        AxisState_End          =  5
    };
    
    
    /**
    Error conditions for axes
    
    These are error conditions assigned by the TCC, not error conditions reported by the axes controllers
    (for the latter see obj.axisStatusWord).
    
    @warning: if you change this, also edit enumDicts.i
    */
    enum AxisErrEnum {
        AxisErr_Begin         = -3,
        AxisErr_HaltRequested = -3,  // user explicitly halted the axis
        AxisErr_NoRestart     = -2,  // axis left halted via /NoRestart
        AxisErr_NotAvailable  = -1,  // device not available
        AxisErr_OK            =  0,
        AxisErr_MinPos        =  1,
        AxisErr_MaxPos        =  2,
        AxisErr_MaxVel        =  3,
        AxisErr_MaxAccel      =  4,
        AxisErr_MaxJerk       =  5,
        AxisErr_CannotCompute =  6,
        AxisErr_ControllerErr =  7,  // controller or communication error
        AxisErr_TCCBug        =  8,
        AxisErr_BadCode       =  9,  // invalid error code
        AxisErr_End           =  9
    };
    
    struct IPDevice {
        std::string ipAddrStr;  // IP address string
        float port;             // IP port; 0 if no device
    };

}
