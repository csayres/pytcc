#pragma once
/*
Earth orientation predictions
*/
#include <boost/tr1/array.hpp> // so array works with old and new compilers
#include "coordConv/site.h"

namespace tcc {

    /**
    * Earth-orientation predications (e.g. UT1-TAI) computed from NCSA Earth Orientation Bulletin A
    */
    class Earth {
    public:
        int utcDay;                 // UTC day of the beginning of the data (MJD, days)
        std::tr1::array<double, 3> startTAI;        // TAI date of start of UTC_day and the next two UTC days (MJD, sec)
        std::tr1::array<double, 2> ut1_taiIntercept;    // UT1-TAI intercept at TAI[i] (sec)
        std::tr1::array<double, 2> poleInterceptX;  // pole x offset intercept at TAI[i] (deg)
        std::tr1::array<double, 2> poleInterceptY;  // pole y offset intercept at TAI[i] (deg)
        std::tr1::array<double, 2> ut1_taiSlope;    // UT1-TAI slope over range TAI[i] <= date < TAI[i+1] (sec/sec)
        std::tr1::array<double, 2> poleSlopeX;      // pole x offset slope over range TAI[i] <= date < TAI[i+1] (deg/sec)
        std::tr1::array<double, 2> poleSlopeY;      // pole y offset slope over range TAI[i] <= date < TAI[i+1] (deg/sec)

        /**
         * Construct an empty Earth object
         *
         * Use Python bindings to load data from a file of earth orientation predictions
         */
        explicit Earth();
    
        /** 
        Update earth-orientation-related site parameters
        
        @param[in,out] site  site information. The following fields are written (all others are left alone):
        - utc_tai
        - ut1_tai
        (by calling site.setPoleWander)
        - poleX
        - poleY
        - corrLong
        - corrLat
        - azCorr
        - diurAbMag
        - pos
        @param[in] tai  TAI date (MJD, sec)
        */
        void updateSite(coordConv::Site &site, double tai) const;
    };

}
