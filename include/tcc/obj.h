#pragma once
/*
Object block

@todo Add Obj.desMount (mount of target); set cmdMount from the path generator.
But the path generator computes acceleration as well as pvt!

Units are degrees and seconds; absolute time is TAI (MJD, in seconds).

"p,v,t triplets", a.k.a. "coordinates" are position, velocity, time
triplets such that, given (po,vo,to): p(t) = po + vo * (t - to)
All refer to the current telescope position unless otherwise noted.

All three-axis items are in the order: az, alt, rotator

Regarding axes in the focal plane:
- Az/alt means the direction of increasing azimuth/altitude
- User 1/2 means the direction of increasing distance in the user's coordinate system (for example RA/Dec for FK5 coordinates).
  User 1/2 is always a rotation of az/alt; there cannot be a mirror flip.
- Instrument x,y is always defined such that it is simply a rotation of az/alt; there cannot be a mirror flip of either axis.
  Typically:
  - y is along increasing columns of the CCD,
  - x along the rows of the CCD, but with the necessary sign such that x,y is a rotation of az/alt
- 0 degrees is along the first axis (x, az or user 1)
- 90 degrees is along the second axis (y, alt or user 2)
*/
#include <boost/tr1/array.hpp> // so array works with old and new compilers
#include "boost/shared_ptr.hpp"
#include "coordConv/coordConv.h"
#include "tcc/basics.h"
#include "tcc/cheby.h"
#include "tcc/pxPMRadVel.h"
#include "tcc/telConst.h"

namespace tcc {

    /**
    * An object position and associated computed elements
    */
    class Obj {
    public:
        std::string name;           // Name of object
        coordConv::CoordSys::Ptr userSys;   // Pointer to coordinate system code and epoch of object
        std::tr1::array<coordConv::PVT, 2> userPos; // position of object in the specified userSys; ignored if useCheby true
        PxPMRadVel userPxPMRadVel;  // parallax, proper motion and radial velocity of object; ignored if useCheby true
        // coordConv::PVTCoord userPos;        // Position, proper motion, etc. of object in the specified userSys; ignored if useCheby true
        bool useCheby;              // Is the object position obtained from Chebyshev polynomials?
                                    // If true: position and distance are obtained from cheby; userPos is ignored
                                    // and the position is only valid if tai is in range [cheby.startTime, cheby.endTime]
                                    // If false: position and parallax are obtained from userPos; chebyUser1, chebyUser2 and chebyDist are ignored.
        RotTypeEnum rotType;        // type of rotation (e.g. with object or horizon); see basics.h for codes
        coordConv::PVT rotUser;     // Rotator angle; the meaning depends on rot_type:
                                    // - object: the angle of the user coordinate system w.r.t. the instrument.
                                    // - horizon: the angle of az/alt w.r.t the instrument
                                    // - mount: the angle sent to the axis controller
                                    // - none: no rotation
        WrapTypeEnum azWrapPref;    // azimuth wrap preference
        WrapTypeEnum rotWrapPref;   // rotator wrap preference
        double mag;                 // predicted magnitude
        coordConv::Site site;       // site information
        coordConv::Site gsSite;     // site information for guide star; just a different wavelength and refraction coefficients
        // The following Chebyshev polynomials are used to describe object position and distance;
        // They are ignored unless useCheby is true
        ChebyshevPolynomial chebyUser1; // Object position axis 1 (TAI, MJD sec -> deg)
        ChebyshevPolynomial chebyUser2; // Object position axis 2 (TAI, MJD sec -> deg)
        ChebyshevPolynomial chebyDist;  // Object distance (TAI, MJD sec -> au)

        // user-specified offsets and positions
        std::tr1::array<coordConv::PVT, 2> userArcOff;  // Offset of object position along a great circle
//         bool doArcOffTDICorr;    // correct arc offset velocity for drift scanning (TDI)?
//                                  // if false, velocity is constant (for tracking moving objects);
//                                  // if true, velocity is adjusted for refraction such that a constant readout rate may be used (for drift scanning)
        
        std::tr1::array<coordConv::PVT, 2> objInstXY;       // user-set boresight pos. (aka instrument-plane offset (inst x,y);
        std::tr1::array<coordConv::PVT, NAxes> guideOff;    // net guiding correction (mount)
        std::tr1::array<coordConv::PVT, NAxes> calibOff;    // calibration offset (mount)

        double updateTime;          // Time of last tracking update; TAI, MJD sec; 0 if none
        double slewEndTime;         // End time of slew or halt; TAI, MJD sec; 0 or stale if not slewing or halting

//         coordConv::PVT arcVelCorr;   // Correction of user-specified arc offset velocity
//                                      // (the velocity component is always zero);
//                                      // useful because velocity varies slightly along the path in a complex way
//         coordConv::PVT prevArcVelCorr;   // Previously computed value of arcVelCorr;
//                                          // again it helps handle the varying velocity of drift scanning
        coordConv::PVTCoord zpmUserPos;     // User position with parallax and proper motion removed to current date
            // (but only for mean coordinates; it is simply a copy of userCoord for apparent coords).
        coordConv::PVTCoord netUserPos;     // Net user position, including all offsets; in "user" coordinates
        coordConv::PVT arcUserNoArcUserAng; // Orientation of arc (of arc offset) at netUserPos (the offset position)
                                            // minus orientation of arc at zpmUserPos (the un-offset position)
        boost::shared_ptr<coordConv::ObsCoordSys> obsSysPtr;   // Observed coordinate system for observed coordinates;
                                            // I use a pointer because I can then access it in Python;
                                            // it's an opaque object in Python if I use the object instead of a pointer
        coordConv::PVTCoord obsPos;         // Observed position at boresight (netUserPos converted to observed coordinates)
        coordConv::PVT objUserObjAzAng;     // Angle from azimuth axis to userSys equatorial axis (e.g. RA) at netUserPos
        coordConv::PVT objAzInstAng;        // Angle from instrument x axis to object azimuth axis
        coordConv::PVT objUserInstAng;      // Angle from instrument x axis to object userSys equatorial axis (e.g. RA)
        coordConv::PVT rotAzObjAzAng;   // Direction of increasing azimuth at center of rotator - direction of increasing azimuth at boresight
        coordConv::PVT rotAzRotAng;     // Direction of increasing azimuth at center of rotator in rotator frame
        coordConv::PVT spiderInstAng;   // Direction of increasing azimuth at optical axis (center of rotator) in the instrument frame;
                                        // spiderInstAng = rotAzInstAng = rotAzRotAng - inst.inst_rot_ang
        coordConv::PVT rotPhys;         // Rotator physical angle: rotator angle before applying inst.rot_offset, rot_scale and wrap;
                                        // if there is no rotator then rotPhys = inst.rot_fixed_phys

        std::tr1::array<coordConv::PVT, NAxes> targetMount; // Target mount position of each axis (az, alt and rot)
        std::tr1::array<AxisStateEnum, NAxes> axisCmdState; // Commanded state of each axis
        std::tr1::array<AxisErrEnum, NAxes> axisErrCode;    // Error code of each axis; if the TCC has commanded a halt, this explains why
        std::tr1::array<bool, NAxes> axisIsSignificant;     // Is axis significant for the current slew command?

        std::tr1::array<coordConv::PVT, NAxes> actMount;    // Actual mount position of each axis, as read from the axis controller
        std::tr1::array<int, NAxes> axisStatusWord;         // Status word last read from each axis controller
        std::tr1::array<double, NAxes> axisStatusTime;      // Time at which status was last read from each axis controller; TAI, MJD sec
        
        /**
        * construct an Obj
        */
        explicit Obj();

        /**
        * Is the specified axis commanded to be moving?
        *
        * @param[in] axis  axis to test (0-2)
        */
        bool isMoving(int axis) {
            return (axisCmdState.at(axis) == AxisState_Tracking) || isSlewing(axis);
        }

        /**
        * Is any axis commanded to be moving?
        */
        bool isMoving();

        /**
        * Is the specified axis commanded state slewing or halting?
        *
        * @param[in] axis  axis to test (0-2)
        */
        bool isSlewing(int axis) {
            return ((axisCmdState.at(axis) == AxisState_Slewing)
                 || (axisCmdState.at(axis) == AxisState_Halting)
            );
        }

        /**
        * Is any axis commanded to be slewing or halting?
        */
        bool isSlewing();
    };

}
