#pragma once
/*
Basic definitions.

Note:
  The units used are degrees and seconds,
  and absolute time is TAI in MJD (in sec), unless otherwise noted.
*/
#include <cmath>
#include <map>
#include <string>

namespace tcc {
    
    /**
    * Instrument Position
    *
    *  The following instrument positions are supported by the TCC,
    *  but not necessarily by a given telescope:
    *
    * * PF1 = prime focus
    * the following require a secondary mirror:
    * * CA1 = Cassegraine focus
    * the following require a tertiary mirror:
    * * BCx = bent Cassegraine foci at top and bottom of mirror cell
    *        (when telescope is at horizon)
    * * NAx = Nasmyth foci (on the forks)
    * * TRx = foci on the "truncated" corners of the mirror cell
    * * the following require a 4th mirror:
    * * SKx = foci on the sky side of the mirror cell
    * * MBx = foci on the back side of the mirror cell
    *
    *                 BC1
    *             -----------
    *           /             \
    *      TR1 / SK1       SK2 \ TR2
    *         / MB1   ---   MB2 \
    *      |--|     / PF1 \     |--|
    *  NA1 |  |     | CA1 |     |  | NA2
    *      |--|     \     /     |--|
    *         \       ---       /
    *      TR4 \ SK4       SK3 / TR3
    *           \ MB4     MB3 /
    *             -----------
    *                 BC2
    *
    *     front view of mirror cell
    */
    class InstPosition {
    public:
        explicit InstPosition(std::string const &name = "?");
        
        /*
        Get current instrument position name
        */
        std::string getName() const { return _name; };
        
        /*
        * Set current instrument position name
        */
        void setName(std::string const &name);
        
        /**
        Get constant offset of RotAz and Mech angle at current instrument position
        
        RotAz-Mech angle = const + (mult * altitude)
        where mult = +1 at NA1, -1 at NA2 and 0 at all other instrument positions
        
        RotAz-Mech angle is the orientation at the instrument port of an object pointing towards
        increasing azimuth, where zero is gravitationally down when the telescope is:
        - at zenith for TR and BC positions (on the side of the mirror cell)
        - at horizon for all others
        and positive angles increase (as seen by instrument):
        - counter-clockwise for PF, BC and NA (even # of mirrors; inverted image)
        - clockwise for CA, SK and MB (odd # of mirrors; non-inverted image)
        */
        double getRotAzMechConst() const { return _rotAzMechConst; };

        operator bool() const { return _name != "?"; }

    private:
        std::string _name;
        double _rotAzMechConst;
        static std::map<std::string, double> const _nameAngMap; // map of name: rotAzMechAng
    };

}
