#pragma once
/**
Instrument block (especially the information needed by TCSpk)
*/
#include <boost/tr1/array.hpp> // so array works with old and new compilers
#include <vector>
#include "tcc/instPos.h"

namespace tcc {
    
    /**
    Information about a guide probe
    */
    class GuideProbe {
    public:
        bool exists;                        /// does this probe exist? This allows disabling an existing probe.
	    std::tr1::array<double, 2> ctr;     /// position of center of guide probe on CCD (unbinned pixels)
	    std::tr1::array<double, 2> minXY;   /// minimum x,y of probe on CCD (unbinned pixels)
	    std::tr1::array<double, 2> maxXY;   /// maximum x,y of probe on CCD (unbinned pixels)
	    std::tr1::array<double, 2> rot_gp_xy;   /// position of rotator w.r.g. center of guide probe (x,y deg)
	    double rot_gim_ang;                /// angle from probe image x to rotator x (deg)
    };

    /**
    Information about the current instrument position.
    note: for mechanical motion limits, see the AxeLim block;
    for information about the mirror actuators see the Mir block.
    
    Description
      The instrument block is set whenver a new instrument is chosen. It is set
      from a sequence of three or four files, one each for:
      the telescope, instrument position, instrument, and possibly the current guide camera view.
    
      In addition, there are a few items int this block -- e.g. the name of
      the instrument and where it is located -- that are not contained inside
      any of these files, but instead are coded into the file name.
      This is unusual, but it allows the TCC to dynamically determine
      which instruments are available (by the existence of an appropriately
      named file).
    */
    class Inst {
    public:

        /*
        Data encoded in the NAME of the instrument data files
        (values specified in the contents of such files will be ignored)
        */
        InstPosition instPos;       /// instrument position
        std::string instName;       /// instrument name
        std::string gcViewName;     /// name of current guide camera view

        /*
        Instrument-Independent Preferences
        These values are held fixed or reset during an instrument change;
        if you specify them in instrument data files they will be ignored.
        */
        double secUserFocus;        /// user-set secondary mirror focus offset (um)
        double gcUserFocus;         /// user-set guide camera focus offset (um)
        double scaleFac;            /// scale factor = actual focal plane scale / nominal scale (no units)
                                    /// larger scale gives higher resolution (e.g. more pixels/arcsec)
        
        /*
        Mirror collimation coefficients
        value = coeff[0] + coeff[1] sin(alt) + coeff[2] cos(alt)
        */
        std::tr1::array<double, 3> primPistCoef;    /// primary mirror piston coefficients (um)
        std::tr1::array<double, 3> primXTiltCoef;   /// primary mirror x tilt coefficients (arcsec)
        std::tr1::array<double, 3> primYTiltCoef;   /// primary mirror y tilt coefficients (arcsec)
        std::tr1::array<double, 3> primXTransCoef;  /// primary mirror x translation coefficients (um)
        std::tr1::array<double, 3> primYTransCoef;  /// primary mirror y translation coefficients (um)
        std::tr1::array<double, 3> secPistCoef;     /// secondary mirror...
        std::tr1::array<double, 3> secXTiltCoef;
        std::tr1::array<double, 3> secYTiltCoef;
        std::tr1::array<double, 3> secXTransCoef;
        std::tr1::array<double, 3> secYTransCoef;
        std::tr1::array<double, 3> tertPistCoef;    /// tertiary mirror...
        std::tr1::array<double, 3> tertXTiltCoef;
        std::tr1::array<double, 3> tertYTiltCoef;
        std::tr1::array<double, 3> tertXTransCoef;
        std::tr1::array<double, 3> tertYTransCoef;
        
        std::tr1::array<double, 5> secPistTempCoef; /// secondary piston (focus) temperature coefficients (in um/C)
                                    /// offset = coef1 * secondary truss temp
                                    ///          + coef2 * primary front temp   + coef3 * primary back-front delta-t
                                    ///          + coef4 * secondary front temp + coef5 * secondary back-front delta-t

        double maxScaleFac;         /// maximum allowed scale factor; minimum is 1/maxScaleFac;
                                    /// set to 1.0 if scale factor is not adjustable
        double primPistScaleCoef;   /// primary scale coefficient (um): delta piston = coeff * (scaleFac - 1)
        double secPistScaleCoef;    /// secondary scale coefficient (um): delta piston = coeff * (scaleFac - 1)

        /*
        Instrument
        */
        std::tr1::array<double, 2> iim_ctr;     /// desired center of instrument image (unbinned pixels)
                                    /// location of zero boresight; need not be geometric center
        std::tr1::array<double, 2> iim_scale;   /// instrument image scale (unbinned pixels/deg on the sky)
        std::tr1::array<double, 2> iim_minXY;   /// minimum position on instrument image (unbinned pixels)
        std::tr1::array<double, 2> iim_maxXY;   /// maximum position on instrument image (unbinned pixels)

        double inst_foc;            /// focus (secondary piston) offset due to instrument (um)
        std::tr1::array<double, 2> rot_inst_xy;     /// position of the center of the rotator in instrument frame (x,y deg)
        double rot_inst_ang;        /// angle from the instrument x axis to the rotator x axis (deg)
        
        std::tr1::array<double, 2> instPosLim;  /// rotator position limits due to the instrument: min pos (deg), max pos (deg)
                                    /// note: the actual rotator position limits are the intersection of this
                                    /// with the position components of rotLim
        /*
        Instrument rotator
        */
        int rotID;                  /// rotator ID, or 0 if no rotator
        double rot_fixed_phys;      /// physical position used for pointing error measurements,
                                    /// and the fixed physical rotation if there is no rotator

        /// mount pos = offet + (scale * physical pos)
        double rot_offset;          /// rotator mount offset (mount units)
        double rot_scale;           /// rotator mount scale (mount units/degree)

        std::tr1::array<double, 5> rotLim;  /// rotator limits due to the rotator:
                                    /// min pos(deg), max pos (deg), vel (deg/sec), accel (deg/sec^2) and jerk (deg/sec^3)
                                    /// note: the true position limits are the intersection of this with instPosLim

        int gcamID;                 /// guide camera ID
        std::tr1::array<double, 2> gim_ctr;     /// desired center of guide image (unbinned pixels)
                                    /// location of zero boresight; need not be geometric center
        std::tr1::array<double, 2> gim_scale;   /// guide image scale (unbinned pixels/deg on the sky)
        std::tr1::array<double, 2> gim_minXY;   /// minimum position on guide image (unbinned pixels)
        std::tr1::array<double, 2> gim_maxXY;   /// maximum position on guide image (unbinned pixels)
        
        std::vector<GuideProbe> gProbe;     /// guide probe data
        
        int ptErrProbe;     /// number of probe (starting from 1) used for pointing error measurements;
                            /// 0 if no probe
        
        int gmechID;                /// guider mechanical controller ID
        double gcNomFocus;          /// nominal focus for guide camera
        
        explicit Inst()
        :
            instPos(),
            instName("?"),
            gcViewName(),
            secUserFocus(0),
            gcUserFocus(0),
            scaleFac(1.0),
            
            primPistCoef(),
            primXTiltCoef(),
            primYTiltCoef(),
            primXTransCoef(),
            primYTransCoef(),
            secPistCoef(),
            secXTiltCoef(),
            secYTiltCoef(),
            secXTransCoef(),
            secYTransCoef(),
            tertPistCoef(),
            tertXTiltCoef(),
            tertYTiltCoef(),
            tertXTransCoef(),
            tertYTransCoef(),
            
            secPistTempCoef(),
            maxScaleFac(1.0),
            primPistScaleCoef(0),
            secPistScaleCoef(0),

            iim_ctr(),
            iim_scale(),
            iim_minXY(),
            iim_maxXY(),
            inst_foc(0),
            rot_inst_xy(),
            rot_inst_ang(0),
            instPosLim(),
            
            rotID(0),
            rot_fixed_phys(0),
            rot_offset(0),
            rot_scale(0),
            rotLim(),

            gcamID(0),
            gim_ctr(),
            gim_scale(),
            gim_minXY(),
            gim_maxXY(),
            
            gProbe(),
            ptErrProbe(0),
            
            gmechID(0),
            gcNomFocus(0)
        {}
        
        bool hasRotator() const { return rotID != 0; }
    };
}
