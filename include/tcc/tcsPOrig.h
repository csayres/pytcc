#pragma once

#include <iostream>
#include <string>
#include "tcs.h"
#include "tcc/basics.h"
#include "tcc/inst.h"
#include "tcc/tcsSite.h"

namespace tcc {

    /**
    A wrapper around TCSpk's PORIG structure
    */
    class TcsPOrig {
    public:
        /**
        Create a TcsPOrig
        
        @param[in] objInstX  x object position on instrument (aka boresight offset) (deg)
        @param[in] objInstY  y object position on instrument (aka boresight offset) (deg)
        @param[in] inst  instrument data; the following fields are read: rot_inst_xy and rot_inst_ang
        */
        TcsPOrig(
            double objInstX,
            double objInstY,
            Inst const &inst
        );

        /**
        Return a detailed string representation (especially for Python)
        */
        std::string __repr__() const;

        PORIG porig;     ///< pointing origin
    };

    std::ostream &operator<<(std::ostream &os, TcsPOrig const &tcsPOrig);

}
