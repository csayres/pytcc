#pragma once
/*
* Pointing model block
*/
#include <iostream>
#include <string>
#include <vector>
#include "tcs.h"

namespace tcc {

    class TelMod {
    public:
        /**
        * Construct an empty model
        */
        explicit TelMod() {
            init();
        }
        
        /**
        * Construct a model from a file specified by path
        */
        explicit TelMod(std::string const &filePath) {
            loadPath(filePath);
        }
        
        ~TelMod() {};
    
        /*
        * Initialize a model
        */
        void init();
        
        /*
        * Load a pointing model from a file specified by path, replacing all existing data
        *
        * @note
        *  1  The file containing the pointing model must be set out exactly as
        *     written by TPOINT's OUTMOD command (which indeed is the ideal way
        *     of generating the file).  Unlike TPOINT's INMOD command, the
        *     present function does not support departures from the stated
        *     format nor the presence of comment records (though trailing
        *     comments, beginning with the character '!', are permitted).  The
        *     first two records, which are ignored, may be used to record
        *     comments;  however, both must be present, as must the END marker.
        *     In the term records, the term name must start in column 3, and
        *     the coefficient value must not start before column 11.
        *
        *  2  Here is an example file (for an equatorial):
        *
        *            AAT  f/15  1979/06/11
        *            T   49   1.1206   52.015  -0.0624
        *              IH       +174.7543     1.18854
        *             =ZH         +3.5100
        *              ID        +23.0464     0.35693
        *            &=HFX        +1.0000
        *            &=HFD        +1.0000
        *            & HF        -18.7283     0.48982
        *            & X2HC       -3.1122     0.26477
        *            & NP         +2.9636     0.79658
        *            & CH        -18.6889     1.20785
        *             =ZE         +0.7000
        *            & ME        +58.2504     0.43977
        *            & MA         +2.9518     0.24207
        *              TF         +8.9655     0.54629
        *            & TFP        +1.1686     0.47733
        *            END
        *
        *     Note the two initial records, which must be present but which are
        *     not interpreted, and the mandatory END record.  Each of the
        *     remaining records defines a term and the corresponding
        *     coefficient value (in arcseconds).  For example, the record
        *
        *            & ME        +58.2504     0.43977
        *
        *     defines a term called ME with a value of +58.2504 arcsec.  The
        *     additional flag and number are ignored.
        *
        *  3  Any terms with a TPOINT "data subset" tag are ignored, as are the
        *     special terms POX and POY.
        *
        *  4  If any invalid record is detected, the pointing model is reset
        *     and the rest of the input file is ignored.
        */
        void loadPath(std::string const &filePath);
        
        /*
        * Add a term to the pointing model
        *
        * @param termName  name of new term
        * @param termValue  value of new term (arcsec)
        * @return the number of the added term (always > 0)
        */
        int addTerm(
            std::string const &termName,
            double termValue
        );
        
        /**
        * Get the number of terms in the model
        *
        * @return number of terms
        */
        int getNumTerms() const;
        
        /**
        * Get the names of the terms in the model
        *
        * @return a vector of term names, in order
        */
        std::vector<std::string> getTermNames() const;
        
        /**
        * Get the value of a term, by name
        * 
        * @param termName  name of term
        * @return the value of the specified term (arcsec)
        */
        double getTerm(
            std::string const &termName
        ) const;
    
        /*
        * Set the value of an existing term, by name
        *
        * @param termName  name of term
        * @param termValue  value of term (arcsec)
        */
        void setTerm(
            std::string const &termName,
            double termValue
        );
        
        /**
        Return a detailed string representation (especially for Python)
        */
        std::string __repr__();

    public:
        // This could be private, but that would complicate usage in TCSpk
        TPMOD _model;
    };

    std::ostream &operator<<(std::ostream &os, TelMod const &telMod);

}
