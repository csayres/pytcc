#pragma once
#include "coordConv/site.h"
#include "tcc/inst.h"
#include "tcc/obj.h"
#include "tcc/telMod.h"

namespace tcc {

    /**
    Compute obj mount fields based on user-set fields
    
    @param[in,out] obj  object data
    
    TO DO:
    * Compute missing angles, but ONLY if we need them:
        PVT objUserObjAzAng;        // angle from object azimuth x axis to object user coordsys axis 1;
        PVT objAzInstAng;           // angle from instrument x axis to object azimuth axis
        std::tr1::array<PVT, 2> mechObjXY;           // position of mechanical center in object frame (x,y)
                                    //   note: by definition obj x,y axes have same orientation as inst x,y axes,
                                    //   but obj x,y is centered at the location of the object on the image,
                                    //   whereas inst x,y is centered at the nominal center of the instrument.
                                    //   So mechInstAng = mechObjAng.
        PVT mechInstAng;            // angle from mechanical x axis to instrument x axis
        PVT rotAzMechAng;           // angle from mechanical x axis to rotator azimuth axis
        PVT rotAzRotAng;            // angle from rotator x axis to rotator azimuth axis
        in each case figure out if we need them, then compute them
        The bottom line is that we need to be able to compute User (or ICRS) orientation and az orientation
        at any location on any guide probe
    */
    void mountFromObs(
        Obj &obj,               ///< [in,out] object data
        Inst const &inst,       ///< [in] instrument data
        TelMod const &telMod,   ///< [in] telescope model
        double tai              ///< [in] TAI date at which to compute position (MJD seconds)
    );

}
