#pragma once

#include <iostream>
#include <string>
#include "tcs.h"
#include "coordConv/coordConv.h"
#include "tcc/basics.h"

namespace tcc {

    /**
     * A wrapper around TCSpk's ASTROM structures for mount and rotator
     */
    class TcsAstrom {
    public:
        /**
         * Create a TcsAstrom
         *
         * Note that other TCSpk subroutines, most notably tcsMedium are needed to compute
         * additional aspects of the data
         */
        TcsAstrom(
            double wavelen              ///< [in] wavelength (Angstroms)
        );

        /**
         * Return a detailed string representation
         */
        std::string toString();

        ASTROM m_ast;       ///< mount astrometry information
        ASTROM r_ast;       ///< rotator astrometry information

        /**
         Initialize a target structure
         
         @param[out] tar  TCSpk target struct
         @param[in] tai  TAI date (MJD, sec)
         @param[in] obsPos  desired observed (refracted apparent topocentric) position
         */
        void initTarget(
            TARG &tar,
            coordConv::PVTCoord const &obsPos,
            double tai
        ) const;

        std::string __repr__() const;
    };

    std::ostream &operator<<(std::ostream &os, TcsAstrom const &tcsAstrom);
}
