#pragma once
#include "coordConv/site.h"
#include "tcc/inst.h"
#include "tcc/obj.h"
#include "tcc/telMod.h"

namespace tcc {

/**
Low-level computation of obj mount fields that is meant to be called by mountFromObs

This implements the case that user coordinates are not mount and all user inputs are known.
The new computation of obj.targetMount is very sensitive to the previous value, so mountFromObs
calls this routine repeatedly until obj.targetMount converges.

@param[in,out] obj  object data
*/
    void basicMountFromObs(
        Obj &obj,               ///< [in,out] object data
        Inst const &inst,       ///< [in] instrument data
        TelMod const &telMod,   ///< [in] telescope model
        double tai              ///< [in] TAI date at which to compute position (MJD seconds)
    );

}
