#pragma once
#include <boost/tr1/array.hpp> // so array works with old and new compilers
#include "tcc/basics.h"

namespace tcc {

    /**
    Motion limits for all axes
    
    See also AxisLim: motion limits for a single axis (python only);
    The python version of AxeLim is an iterator over AxisLim.
    
    Note: AxeLim uses separate arrays for position, velocity, etc. instead of containing a vector of AxisLim,
    so that the load/dump file format is backward compatible with the old TCC.
    */
    class AxeLim {
    public:

        std::tr1::array<double, 3> minPos;  /// minimum position (deg)
        std::tr1::array<double, 3> maxPos;  /// maximum position (deg)
        std::tr1::array<double, 3> vel;     /// maximum speed (deg/sec)
        std::tr1::array<double, 3> accel;   /// maximum acceleration (deg/sec^2)
        std::tr1::array<double, 3> jerk;    /// maximum jerk (deg/sec^3)
        int badStatusMask;              /// fail if any of these bits are high in an axis controller's status word
        int warnStatusMask;             /// warn if any of these bits are high in an axis controller's status word
        double maxDTime;                /// warn if axis controller and TCC clock disagree by this much (sec)
    };
}
