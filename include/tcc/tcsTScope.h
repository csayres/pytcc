#pragma once

#include <iostream>
#include <string>
#include "tcs.h"
#include "tcc/basics.h"
#include "tcc/tcsSite.h"

namespace tcc {

    /**
    A wrapper around TCSpk's TSCOPE structure
    */
    class TcsTScope {
    public:
        /**
        Create a TcsTScope
        
        Note that other TCSpk subroutines, most notably tcsMedium are needed to compute
        additional aspects of the data
        */
        TcsTScope(
            TcsSite const &tcsSite,         ///< [in] TCSpk site information
            InstPosition const &instPos     ///< [in] instrument position
        );

        /**
        Return a detailed string representation (especially for Python)
        */
        std::string __repr__();

        TSCOPE tel;     ///< telescope information
    };

    std::ostream &operator<<(std::ostream &os, TcsTScope const &tcsTScope);

}
