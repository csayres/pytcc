#pragma once
#include "tcc/inst.h"
#include "tcc/obj.h"
#include "tcc/tcsSite.h"
#include "tcc/telMod.h"

namespace tcc {

    /**
    Compute netUserPos from other user-specified fields
    
    @param[in,out] obj  object data
        Input fields include:
        - useCheby
        - userPos (if useCheby false)
        - userPxPMRadVel (if useCheby false)
        - chebyUser1 (if useCheby true)
        - chebyUser2 (if useCheby true)
        - chebyUserDist (if useCheby true)
        - userArcOffset
        Ouput fields include:
        - zpmUserPos
        - netUserPos
        - arcUserNoArcUserAng
    @param[in] tai  TAI date at which to compute netUserPos and other values (MJD, seconds)
    */
    void computeNetUser(
        Obj &obj,
        double tai
    );

}
