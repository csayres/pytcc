#pragma once

#include <string>
#include <iostream>
#include "coordConv/mathUtils.h"
#include "coordConv/pvt.h"

namespace tcc {

    /**
    Position, velocity, acceleration, jerk and time
    
    Compared to coordConv::PVT this object is somewhat restricted:
    the object is const and has no mathematical operators.
    
    Uses standard TCC conventions:
    * position is in degrees
    * velocity is in degrees/second
    * acceleration is in degrees/second^2
    * jerk is in degrees/second^3
    * t is TAI (MJD, seconds)
    */
    class PVAJT {
    public:
        double pos, vel, accel, jerk, t;

        /**
        Construct from specified position, velocity and time
        
        @param pos  position (deg)
        @param vel  velocity (deg/sec)
        @param accel  acceleration (deg/sec^2)
        @param jerk  jerk (deg/sec^3)
        @param t    TAI date (MJD, sec)
        */
        explicit PVAJT(double pos, double vel, double accel, double jerk, double t);
        
        /**
        Construct a PVAJT from a coordConv::PVT
        
        Assumes PVT units are degrees, deg/sec and sec.
        */
        explicit PVAJT(coordConv::PVT const &pvt);
        
        /**
        Construct a PVAJT with all fields NaN
        */
        explicit PVAJT();
        
        /// Extrapolate to a specified time, assuming a path of constant jerk
        PVAJT extrapolate(double t) const {
            double dt = t - this->t;
            return PVAJT(
                pos   + dt*(vel   + dt*(accel/2.0 + dt*jerk/6.0)),
                vel   + dt*(accel + dt*jerk/2.0),
                accel + dt*jerk,
                jerk,
                t);
        }
        
        /// Are all values finite? (Does it have finite pos, vel and t)?
        bool isfinite() const {
            return std::isfinite(pos)
                && std::isfinite(vel)
                && std::isfinite(accel)
                && std::isfinite(jerk)
                && std::isfinite(t);
        }
        
        bool operator==(PVAJT const &rhs) {
            return (this->pos == rhs.pos)
                && (this->vel == rhs.vel)
                && (this->accel == rhs.accel)
                && (this->jerk == rhs.jerk)
                && (this->t == rhs.t);
        }

        bool operator!=(PVAJT const &rhs) {
            return !operator==(rhs);
        }
       
        /**
        Return a string representation
        */
        std::string __repr__() const;
    };

    std::ostream &operator<<(std::ostream &os, PVAJT const &pvajt);

}
