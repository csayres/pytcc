#pragma once

#include <iostream>
#include <string>
#include <boost/tr1/array.hpp> // so array works with old and new compilers
#include "tcs.h"
#include "tcc/earth.h"
#include "tcc/weath.h"

namespace tcc {

    /**
    A wrapper around TCSpk's TIMEO (time) and SITE structures and auxiliary data array
    
    @note radio observatories may wish to specify a special refraction computation function
    in the implementation of the update method.
    
    @todo: compare the results of this function to updating teo and tsite using tcsSlow and tcsIsite
    */
    class TcsSite {
    public:
        TcsSite();

        /**
        Update teo and site; equivalent to running tcsSlow and preliminaries
        
        @note: wavelen is used as a starting point for refraction correction
        and is primarily intended to differentiate between optical and radio.
        Radio observatories should also change the refraction computation
        inside the implementation to one that is more accurate for radio wavelengths.
        */
        void update(
            double tai,                     ///< [in] TAI date at which to compute position (MJD seconds)
            coordConv::Site const &site     ///< [in] coordConv site information
        );
        
        double timestamp;   ///< tai of last update; 0 if never updated
        TIMEO teo;          ///< time information
        SITE tsite;         ///< site information
        std::tr1::array<double, MAXAUX> aux;  ///< auxiliary data for pointing model terms
        
        /**
        Return a detailed string representation (especially for Python)
        */
        std::string __repr__() const;
    };

    std::ostream &operator<<(std::ostream &os, TcsSite const &tcsSite);

}
