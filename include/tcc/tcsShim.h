#pragma once
/*
Definitions for the shim subroutines to the TCSpk library.

Items defined here should only be needed to call TCSpk code; never in the TCC's public API.

Use standard TCC units:
- degrees
- seconds (absolute time in MJD seconds)
- C
- etc.

Input velocities as well as positions; this code performs the extrapolation.
This code must also handle the great circle offset for ObjOff.

Input normal earth orientation block. Let this code interpolate.

Pass in TAI for which a position is wanted.

Handle wrap externally, as well as all recognition that "this is a new target".
*/

/*
Run basic pointing calculation
    
Would it make any sense to add parameters to allow coordinate conversion?
Or is that a different subroutine?

Coordinate conversions would include:
- given a guide star position in CCD coordinates, compute the sky coordinate of the star;
    the Inst block has guide probe info, so this might work already
- given a guide star position in CCD coordinates and the known sky coordinates of that guide star
    compute pointing error. This could be done via a separate obj field for guide stars or an additional block,
    or just write a different subroutine. In fact this routine could call a more general coordinate converter.
- test a new position: already supported; just call it and see if it's in range
- given a position, predict the rotator angle.
- 
*/
#include <iostream>
#include <string>
#include "tcs.h"
#include "coordConv/coordConv.h"
#include "tcc/basics.h"
#include "tcc/earth.h"
#include "tcc/instPos.h"
#include "tcc/weath.h"

namespace tcc {
namespace tcspk {

    double const FL = 1.0;      ///< focal length: 1.0 means focal plane coordinates are in radians
    int const JBP = 0;          ///< false: the telescope never goes below the horizon or beyond 90 alt
    double const RNoGo = 0.0;   ///< zone of automatic pole avoidances; 0.0 to not use

    /**
    * Convert a TCC PVT into tcs units; if the PVT is invalid then use 0, 0
    *
    * This is a best-effort approach intended to feed the command and achieved
    * inputs for mount position to TCSSpk.
    *
    * @param[out] pos  position in rad
    * @param[out] vel  velocity in rad/day
    * @param[in]  pvt: PVT to convert
    * @param[in]  tai: TAI at which to compute position (MJD, seconds)
    * @param[in]  isAz: the angle is azimuth; switch conventions (TCC is S to E, TCSpk is N to E)
    *
    * @return isfinite: true if pvt is finite
    */
    bool tcsFromPVT(double &pos, double &vel, coordConv::PVT const &pvt, double tai, bool isAz=false);
    
    /**
    * Compute a PVT given a pair of angles (in rad)
    *
    * @param[out] pvt  the pvt to be set
    * @param[in]  posArr: a pair of angles (rad): computed at tai and tai + DeltaTForVel
    * @param[in]  tai: TAI date of posArr[0] (MJD seconds)
    * @param[in]  deltaTAI: time of posArr[1] = tai + deltaTAI
    * @param[in]  isAz: the angle is azimuth; switch conventions (TCC is S to E, TCSpk is N to E)
    */
    void pvtFromTCS(coordConv::PVT &pvt, double posArr[2], double tai, double deltaTAI, bool isAz=false);

    /**
    Convert rotator angle from TCC physical (degrees) to Tcs mechanical (radians)
    */
    inline double rotTcsFromPhys(double rotPhys, tcc::InstPosition const &instPos) {
        return coordConv::wrapCtr(180.0 + instPos.getRotAzMechConst() - rotPhys) * coordConv::RadPerDeg;
    }

    /**
    Convert rotator angle from Tcs mechanical (radians) to TCC physical (degrees)
    */
    inline double rotPhysFromTcs(double rotTcs, tcc::InstPosition const &instPos) {
        return coordConv::wrapCtr(180.0 + instPos.getRotAzMechConst() - (rotTcs / coordConv::RadPerDeg));
    }

}
    std::ostream &operator<<(std::ostream &os, ASTROM const &astrom);

    std::ostream &operator<<(std::ostream &os, FLDOR const &fldor);

    std::ostream &operator<<(std::ostream &os, PORIG const &porig);

    std::ostream &operator<<(std::ostream &os, SITE const &site);

    std::ostream &operator<<(std::ostream &os, TARG const &targ);

    std::ostream &operator<<(std::ostream &os, TIMEO const &timeo);

    std::ostream &operator<<(std::ostream &os, TSCOPE const &tscope);
    
    /**
    Format N elements of an C array as a string
    */
    template <typename T>
    std::string formatCArr(T const arr[], int n);

    /**
    Format a C style matrix (array of pointers) as a string
    
    @tparam ROWS: number of rows in the matrix (first index)
    @tparam COLS: number of columns in the matrix (second index)
    @param[in] mat  a T[ROWS][COLS] C style matrix (array of pointers to arrays)
    */
    template <typename T, int ROWS, int COLS>
    std::string formatCMat(T const mat[ROWS][COLS]);

}
