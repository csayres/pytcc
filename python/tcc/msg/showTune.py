from __future__ import division, absolute_import

__all__ = ["showTune"]

from .formatDisabledProc import formatDisabledProc

def showTune(tccActor, cmd=None):
    """Display contents of tccActor.tune using standard keywords (not dumping the block)
    """
    disabledCode, disabledProcMsg = formatDisabledProc(tccActor.tune)
    tccActor.writeToUsers(disabledCode, disabledProcMsg, cmd=cmd)

    miscMsg = "; ".join((
        "StInterval=%0.2f, %0.2f, %0.2f" % tuple(tccActor.tune.statusInterval),
        "MaxUsers=%s" % (tccActor.tune.maxUsers,),
    ))
    tccActor.writeToUsers("i", miscMsg, cmd=cmd)
