from __future__ import division, absolute_import
"""!Format the values of data types found in the tcc.base.Obj, for key=value messages
"""
import collections
import coordConv
from RO.StringUtil import quoteStr
import tcc.base
from .blockToKW import WriterFromBlock, FieldKW

__all__ = ["CoordSysNameKeyDict", "RotTypeEnumKeyDict",
    "formatCoordSys", "formatMag", "formatPVT", "formatPVTList", "formatPVTCoord",
    "formatPxPMRadVel", "formatRotType", "formatWrapPref", "formatChebyCoeff", "formatUseCheby",
    "moveItemKW", "writerFromObjBlock", "computedFields", "objFieldKWDict"]

# dict of coordConv coordSys name (which are all lowercase): objSys keyword coordSys name
CoordSysNameKeyDict = {
    "icrs": "ICRS",
    "fk5": "FK5",
    "fk4": "FK4",
    "gal": "Gal",
    "appgeo": "Geo",
    "apptopo": "Topo",
    "obs": "Obs",
    "mount": "Mount",
    "none": "None",
}

# dict of rotType enum: rotType keyword string value
RotTypeEnumKeyDict = {
    tcc.base.RotType_None: "None",
    tcc.base.RotType_Object: "Obj",
    tcc.base.RotType_Horizon: "Horiz",
    tcc.base.RotType_Mount: "Mount",
}

def formatChebyCoeff(chebyCoeff):
    """!Format cheby coeffs

    @param[in] chebyCoeff a list of floats
    """
    return ",".join(["%.6f"%x for x in chebyCoeff.params])

def formatCoordSys(coordSys):
    """!Format a coordinate system

    @param[in] coordSys  a coordConv.CoordSys
    """
    coordSysName = coordSys.getName()
    if coordSys.isCurrent():
        date = 0
        nDig = 0
    else:
        date = coordSys.getDate()
        nDig = {
            coordConv.DateType_Julian: 2,
            coordConv.DateType_Besselian: 2,
            coordConv.DateType_TAI: 6,
            coordConv.DateType_None: 0,
        }.get(coordSys.getDateType(), 2)
    # translate names, as needed
    objSysName = CoordSysNameKeyDict.get(coordSysName.lower(), "?")
    return '%s, %.*f' % (objSysName, nDig, date)

def formatMag(mag):
    """!Format a magnitude
    """
    return "%.4f" % mag

def formatPVT(pvt):
    """!Format a single PVT

    The format is:
    > _pos_, _vel_, _time_
    where all values are shown in decimal format (rather than exponential notation):
    - pos and vel are shown to 7 digits after the decimal point (0.0003 arcsec resolution)
    - time is shown to 6 digits (a bit more than needed at sidereal tracking rates,
        but time is often known to the nearest microsecond)

    Example: "25.123456, -3.7654321, 2314.123456"

    @param[in] pvt  a coordConv.PVT
    """
    return "%0.6f, %0.6f, %0.7f" % (pvt.pos, pvt.vel, pvt.t)

def formatPVTList(pvtList):
    """!Format a sequence of PVTs

    The format is:
        PVT(_pos_, _vel_, _time_)
    where all values are shown in decimal format (rather than exponential notation):
    - _pos_ and _vel_ are shown to 7 digits after the decimal point (0.0003 arcsec resolution)
    - _time_ is shown to 6 digits (because time is commonly given to the nearest microsecond)

    @param[in] pvtList  sequence of coordConv.PVT
    """
    return ",  ".join(formatPVT(pvt) for pvt in pvtList)

def formatPVTCoord(pvtCoord):
    """!Format a PVTCoord

    @param[in] pvtCoord     a coordConv.PVTCoord
    """
    PVT1, PVT2 = coordConv.PVT(), coordConv.PVT()
    pvtCoord.getSphPVT(PVT1, PVT2)
    return ", ".join(formatPVT(pvt) for pvt in (PVT1, PVT2))

def formatPxPMRadVel(pxPMRadVel):
    """!Format a pxPMRadVel

    @param[in] pxPMRadVel   a tcc.base.PxPMRadVel
    """
    return "%0.4f, %0.4f, %0.4f, %0.4f" % \
        (pxPMRadVel.equatPM, pxPMRadVel.polarPM, pxPMRadVel.parallax, pxPMRadVel.radVel)

def formatRotType(rotType):
    """!Format rot type as a name; return "?" for unknown values

    @param[in] rotType  a rotation type (a tcc.base.RotTypeEnum)
    """
    return RotTypeEnumKeyDict.get(rotType, "?")

def formatWrapPref(wrapType):
    """!Format wrap preference as a name; return "?" for unknown values

    @param[in] wrapType     a wrap preference type (a tcc.base.WrapTypeEnum)
    """
    return tcc.base.WrapTypeValDict.get(wrapType, "?")

def formatUseCheby(useCheby):
    """!Format boolean use cheby, either T or F.
    @param[in] useCheby obj (field on obj block)
    """
    if useCheby:
        return "T"
    else:
        return "F"

# computedFields will not be shown unless show obj/full is specified.
computedFields = set(["ObjZPMPos", "ObjNetPos", "ObjInstAng", "SpiderInstAng"])

objFieldKWDict = collections.OrderedDict((
    ("ObjName",FieldKW("ObjName", "name", quoteStr)),
    ("ObjSys", FieldKW("ObjSys", "userSys", formatCoordSys)),
    ("ObjPos", FieldKW("ObjPos", "userPos", formatPVTList)),
    ("ObjPM", FieldKW("ObjPM", "userPxPMRadVel", formatPxPMRadVel)),
    ("ObjMag", FieldKW("ObjMag", "mag", formatMag)),
    ("RotType", FieldKW("RotType", "rotType", formatRotType)),
    ("RotPos", FieldKW("RotPos", "rotUser", formatPVT)),
    ("ObjArcOff", FieldKW("ObjArcOff", "userArcOff", formatPVTList)),
    ("Boresight", FieldKW("Boresight", "objInstXY", formatPVTList)),
    ("GuideOff", FieldKW("GuideOff", "guideOff", formatPVTList)),
    ("CalibOff", FieldKW("CalibOff", "calibOff", formatPVTList)),
    ("AzWrapPref", FieldKW("AzWrapPref", "azWrapPref", formatWrapPref)),
    ("RotWrapPref", FieldKW("RotWrapPref", "rotWrapPref", formatWrapPref)),
    ("UseCheby", FieldKW("UseCheby", "useCheby", formatUseCheby)),
    ("ChebyCoeffsUser1", FieldKW("ChebyCoeffsUser1", "chebyUser1", formatChebyCoeff)),
    ("ChebyCoeffsUser2", FieldKW("ChebyCoeffsUser2", "chebyUser2", formatChebyCoeff)),
    ("ChebyCoeffsDist", FieldKW("ChebyCoeffsDist", "chebyDist", formatChebyCoeff)),
    ### keywords below are strictly computed fields
    ("ObjZPMPos", FieldKW("ObjZPMPos", "zpmUserPos", formatPVTCoord)),
    ("ObjNetPos", FieldKW("ObjNetPos", "netUserPos", formatPVTCoord)),
    ("ObjInstAng", FieldKW("ObjInstAng", "objUserInstAng", formatPVT)),
    ("SpiderInstAng", FieldKW("SpiderInstAng", "spiderInstAng", formatPVT)),

))

class MoveItemKW(FieldKW):
    """The MoveItems keyword

    MoveItems indicates which user-set position attributes have been changed for a move. This keyword
    always appears with Moved or SlewBeg, and never appears any other time.

    str contains 9 characters, each of which is either "Y" (yes, this item changed) or "N"
    (no, this item did not change):

    1 object name
    2 any of coordinate system name or date, object position, parallax, proper motion, or radial velocity
        (i.e. any of obj.userSys or obj.userPos)
    3 object magnitude
    4 object offset (always N for the new TCC, since it does not support object offsets)
    5 arc offset
    6 boresight position
    7 rotator angle or type of rotation
    8 guide offset
    9 calibration offset
    """
    moveItemKWs = [["ObjName"], ["ObjPos", "ObjSys", "ObjPM"], ["ObjMag"], ["ObjOff"], ["ObjArcOff"],
        ["Boresight"], ["RotPos", "RotType"], ["GuideOff"], ["CalibOff"]]
    def __init__(self):
        self.block = tcc.base.Obj()

    def __call__(self, newBlock):
        """Compare newBlock with self.block, output differences as a keyword.
        @param[in] newBlock, the new object block.
        """
        newBlock = tcc.base.Obj(newBlock) # copy safely
        newBlock.userSys = newBlock.userSys.clone()
        updateStr = ""
        for kwList in self.moveItemKWs:
            if "ObjOff" in kwList:
                # ObjOff doesn't exist anymore
                updateStr+="N"
            else:
                anyUpdates = [objFieldKWDict[kw].wasUpdated(self.block, newBlock) for kw in kwList]
                if True in anyUpdates:
                    updateStr += "Y"
                else:
                    updateStr += "N"

        self.block = newBlock
        return "MoveItems=%s"%updateStr

def makeObjBlockWriter():
    writerFromObjBlock = WriterFromBlock(tcc.base.Obj, objFieldKWDict)
    # the ObjPM keyword needs special handling for output
    def pmSuppressionRule():
        coordSys = writerFromObjBlock.block.userSys.getName().strip().lower()
        if coordSys not in ("fk5", "fk4", "gal"):
            # system isn't a mean coord sys, suppress proper motion output
            return True
        else:
            return False

    def chebyOnRule():
        return writerFromObjBlock.block.useCheby

    def chebyOffRule():
        return not writerFromObjBlock.block.useCheby

    writerFromObjBlock.addSuppression("ObjPM", pmSuppressionRule)
    writerFromObjBlock.addAddition("ChebyCoeffsUser1", chebyOnRule) # other members in cheby group will be displayed automatically
    chebyGroup = ["ChebyCoeffsUser2", "ChebyCoeffsUser1", "ChebyCoeffsDist"]
    objGroup = ["ObjName", "ObjSys", "ObjPos", "ObjPM"]
    for kw in chebyGroup:
        writerFromObjBlock.addSuppression(kw, chebyOffRule)
    for kw in ["ObjPos", "ObjPM"]:
        writerFromObjBlock.addSuppression(kw, chebyOnRule)
    # groups represent keywords which always appear together (if one is output they are all output)
    writerFromObjBlock.addGroup(["RotType", "RotPos"])
    writerFromObjBlock.addGroup(chebyGroup)
    writerFromObjBlock.addGroup(objGroup) # note ObjPM may be suppressed depending on ObjSys
    return writerFromObjBlock

# a stand along function (with state),
# for outputting the moveitems keyword
# call it with an new instance of the object block
moveItemKW = MoveItemKW()
writerFromObjBlock = makeObjBlockWriter()
