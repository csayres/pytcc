from __future__ import division, absolute_import
"""Some tools for managing TCC keyword output from a block
"""
import itertools
import math

from coordConv import PVT
from RO.SeqUtil import isSequence

__all__ = ["WriterFromBlock", "FieldKW"]

class WriterFromBlock(object):
    """!A class containing mapping information between fields on a block and a TCC keyword
    """
    def __init__(self, objClass, fieldKWDict):
        """!Construct a WriterFromBlock

        @param[in] objClass  class of block
        @param[in] fieldKWDict  an ordered dictionary of FieldKW objects keyed by TCC keyword.
            Keywords are output in the order specified by this dict.
        """
        # for fkw in fieldKWDict.itervalues():
        #     if not hasattr(block, fkw.blockAttr):
        #         raise RuntimeError("Block has no attribute: %s"%fkw.blockAttr)
        self.objClass = objClass # keep around for making copies
        self.block = objClass() # an empty block
        self.fieldKWDict = fieldKWDict # ordered dict
        self.suppressionRules = {} # added by addSuppression method
        self.additionRules = {} # added by addAddition method
        self.groups = [] # added by addGroup method

    def writeKWs(self, writeToUsers, cmd=None, keywords=None):
        """!Write a list of keyword=value messages to users

        @param[in] writeToUsers  provided from the tccActor.writeToUsers method
        @param[in] cmd  command whose ID is used for output (a twistedActor.BaseCmd)
        @param[in] keywords  a list of keywords or None;
            if None then use self.fieldKWDict.keys()
        """
        if keywords:
            if not isSequence(keywords):
                raise RuntimeError("keywords=%r; not a sequence" % (keywords,))
            keywords = set(keywords)
            # should any be added based on addition rules?
            for key, logic in self.additionRules.iteritems():
                if logic():
                    keywords.update([key])
            # be sure that group output is satisfied.
            for group in self.groups: # self.groups is a list of sets
                # if any member of this group is in keywords
                if len(group & keywords):
                    keywords.update(group)

        else:
            keywords = self.fieldKWDict.keys() # write all
        for kw in self.fieldKWDict.iterkeys(): # iterate over fieldKWDict, because that defines the order
            if kw not in keywords:
                # don't write this one!
                continue
            self.writeSingleKW(kw, writeToUsers, cmd) # suppression rules checked here

    def addSuppression(self, keyword, logic):
        """!Add a suppression rule

        @param[in] keyword  the keyword to be suppressed (must be a keyword in self.fieldKWDict)
        @param[in] logic  a callable that evaluates to True or False. If True keyword will
            not be output, else keyword will be output
        """
        if not keyword in self.fieldKWDict:
            raise RuntimeError("Keyword :%s, not found in dict"%keyword)
        if not callable(logic):
            raise RuntimeError("logic")
        if keyword in self.suppressionRules:
            # a suppression rule already exists, add another
            self.suppressionRules[keyword].append(logic)
        else:
            # add the first one
            self.suppressionRules[keyword]=[logic]

    def addAddition(self, keyword, logic):
        """!Add an addition rule

        @param[in] keyword  the keyword to be added (must be a keyword in self.fieldKWDict)
        @param[in] logic  a callable that evaluates to True or False. If True keyword will
            be output
        """
        if not keyword in self.fieldKWDict:
            raise RuntimeError("Keyword :%s, not found in dict"%keyword)
        if not callable(logic):
            raise RuntimeError("logic")
        self.additionRules[keyword]=logic

    def addGroup(self, keywords):
        """!Define a keyword group

        @param[in] keywords  a list of keywords.
            If one is specified, then all are output
        """
        self.groups.append(set(keywords))


    def writeSingleKW(self, keyword, writeToUsers, cmd=None):
        """!Write a single keyword to users; if a suppression rule for this keyword exists, test it

        @param[in] keyword  the keyword to be suppressed (must be a keyword in self.fieldKWDict)
        @param[in] writeToUsers  output function (a twistedActor.BaseActor.writeToUsers)
        @param[in] cmd  command whose ID is used for output (a twistedActor.BaseCmd)
        """
        if keyword in self.suppressionRules:
            for logic in self.suppressionRules[keyword]:
                if logic():
                    # a suppression exists
                    return
        outStr = self.fieldKWDict[keyword].getKeyValStr(self.block)
        writeToUsers('i', outStr, cmd)

    def updateBlock(self, newBlock):
        """!Compare attributes on the newBlock, vs. the cached block

        @param[in] newBlock  the new block with possibly (likely) new values
        @return a list of keywords which have been updated
        """
        newBlock = self.objClass(newBlock)
        if "userSys" in dir(newBlock): # this is an obj block, explicitly clone usersys for safety
            newBlock.userSys = newBlock.userSys.clone()
        updatedKWs = [kw for kw, val in self.fieldKWDict.iteritems() if val.wasUpdated(self.block, newBlock)]
        # start with an empty object (cannot deep copy a swig item)
        self.block = newBlock #CopyBlock(newBlock)
        # self.block = newBlock
        return updatedKWs

def isEqualOrNan(a, b):
    """Return True if a==b or False otherwise, with special handling for float and coordConv.PVT

    Handle these types specially (and assumes a and b are the same type)
    - float: return True if both are nan
    - coordConv.PVT: return True if neither is finite
    """
    if a==b:
        return True
    if isinstance(a, float):
        if math.isnan(a) and math.isnan(b):
            return True
    elif isinstance(a, PVT):
        if not a.isfinite() and not b.isfinite():
            return True
    return False

class FieldKW(object):
    """!Object for holding an attribute of a block, and a formatting function for that attribute

    A block is a struct-like object, such as tcc.base.Obj
    """
    def __init__(self, tccKW, blockAttr, stringCast):
        """!Construct a FieldKW

        @param[in] tccKW  keyword used to output the value
        @param[in] blockAttr  name of attribute on the block
        @param[in] stringCast  function that formats the specified attribute;
            called with the Block.blockAttr as the only argument.
        """
        self.tccKW = tccKW
        self.blockAttr = blockAttr
        self.stringCast = stringCast

    def wasUpdated(self, oldBlock, newBlock):
        """!Return True if the attribute has changed

        @param[in] oldBlock  old value of the block
        @param[in] newBlock  new value of the block
        """
        attr1 = getattr(oldBlock, self.blockAttr)
        attr2 = getattr(newBlock, self.blockAttr)
        wasUpdated = False
        if isSequence(attr1):
            for x, y in itertools.izip(attr1, attr2):
                if not isEqualOrNan(x, y):
                    wasUpdated = True
                    break
        else:
            # this attribute isn't iterable
            # directly compare
            if not isEqualOrNan(attr1, attr2):
                wasUpdated = True
        return wasUpdated

    def getKeyValStr(self, block):
        """!Format the attribute as a key=value string

        @param[in] block  block whose attribute is to be formatted
        """
        return "%s=%s" % (self.tccKW, self.stringCast(getattr(block, self.blockAttr)))
