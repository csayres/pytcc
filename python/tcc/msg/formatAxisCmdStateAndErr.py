from __future__ import division, absolute_import

import itertools

from tcc.base import AxisStateValDict, AxisErrValDict, Obj

__all__ = ["formatAxisCmdStateAndErr"]

class _FormatAxisCmdStateAndErr(object):
    """!Format AxisCmdState, AxisErrCode and AxisNoTrack based on what has changed
    """
    def __init__(self):
        self.obj = Obj()

    def __call__(self, obj, showAll=False):
        """!Return a message code and formatted string

        @param[in] obj  current object block
        @param[in] showAll  if True show data even if not changed
        """
        msgCode = "i"
        msgList = []
        if showAll or self.obj.axisCmdState != obj.axisCmdState:
            msgList.append("AxisCmdState=%s" % (", ".join(AxisStateValDict.get(cmdState, "?") for cmdState in obj.axisCmdState),))

        if showAll or self.obj.axisErrCode != obj.axisErrCode:
            msgList.append("AxisErrCode=%s" % (", ".join(AxisErrValDict.get(errCode, "?") for errCode in obj.axisErrCode),))

        oldHaltedList = [errCode != 0 for errCode in self.obj.axisErrCode]
        newHaltedList = [errCode != 0 for errCode in obj.axisErrCode]
        if showAll or oldHaltedList != newHaltedList:
            msgList.append("AxisNoTrack=%s" % ("".join("T" if val else "F" for val in newHaltedList),))
        if any(newHalt and not oldHalt for oldHalt, newHalt in itertools.izip(oldHaltedList, newHaltedList)):
            msgCode = "w"

        self.obj = Obj(obj) # deep copy

        return msgCode, "; ".join(msgList)

formatAxisCmdStateAndErr = _FormatAxisCmdStateAndErr()
