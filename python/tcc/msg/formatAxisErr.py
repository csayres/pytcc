from __future__ import division, absolute_import

from RO.StringUtil import quoteStr
from tcc.base import AxisErrValDict

__all__ = ["formatAxisErr"]

def formatAxisErr(textMsg, badAxisKeyword, badAxisList, errCodeKeyword, errCodeList):
    """!Format an axis error based on axis configuration

    @param[in] textMsg  text message (not including Text keyword); none or "" if none
    @param[in] badAxisKeyword  keyword for badAxisList flags;
                usually AxisNoSlew or AxisNoTrack; None or "" if none
    @param[in] badAxisList  bad axis flags (T for bad axis, F for good axis)
    @param[in] errCodeKeyword  keyword for error codes;
                usually AxisErrCode or RejectedAxisErrCode; None or "" if none
    @param[in] errCodeList  axis error codes
    """
    itemList = []

    if textMsg:
        itemList.append(
            "Text=%s" % (quoteStr(textMsg),)
        )

    # handle badAxisList flags, if any
    if badAxisKeyword:
        itemList.append(
            "%s=%s" % (badAxisKeyword, "".join("T" if val else "F" for val in badAxisList))
        )

    if errCodeKeyword:
        itemList.append(
            "%s=%s" % (errCodeKeyword, ", ".join(AxisErrValDict.get(code, "?") for code in errCodeList))
        )

    return "; ".join(itemList)
