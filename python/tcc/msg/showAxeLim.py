from __future__ import division, absolute_import

__all__ = ["showAxeLim"]

def showAxeLim(tccActor, cmd=None):
    """Display contents of tccActor.axeLim using standard keywords (not dumping the block)
    """
    for ind, axisName in enumerate(("Az", "Alt", "Rot")):
        axisLim = tccActor.axeLim[ind]
        if ind == 2 and not tccActor.inst.hasRotator():
            valStr = "NaN, NaN, NaN, NaN, NaN"
        else:
            valStr = "%0.2f, %0.2f, %0.2f, %0.2f, %0.2f" % (axisLim.minPos, axisLim.maxPos, axisLim.vel, axisLim.accel, axisLim.jerk)
        tccActor.writeToUsers("i", "%sLim=%s" % (axisName, valStr), cmd=cmd)

    statusMsg = "AxisBadStatusMask=0x%X; AxisWarnStatusMask=0x%X; AxisMaxDTime=%0.3f" % \
        (tccActor.axeLim.badStatusMask, tccActor.axeLim.warnStatusMask, tccActor.axeLim.maxDTime)
    tccActor.writeToUsers("i", statusMsg, cmd=cmd)
