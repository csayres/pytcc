from __future__ import division, absolute_import

__all__ = ["formatDisabledProc"]

_AttrProcList = (
    ("doBrdTelPos", "BrdTelPos"),
    ("doCollimate", "Collimate"),
    ("doStatus", "Status"),
)
def formatDisabledProc(tune):
    """!Format the DisabledProc keyword

    @param[in] tune  current value of tune block
    @return two values:
    - message code ("w" if any processes are disabled, else "i")
    - formatted string
    """
    procList = [proc for attr, proc in _AttrProcList if not getattr(tune, attr)]

    if procList:
        return "w", "DisabledProc=%s" % (", ".join(procList),)
    else:
        return "i", "DisabledProc"
