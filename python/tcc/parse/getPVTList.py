from __future__ import division, absolute_import

import itertools

import coordConv
from twistedActor import CommandError

__all__ = ["getPVTList"]

def getPVTList(pvtParam, numAxes, defTAI):
    """!Obtain a list of PVTs from a PVT list parameter
    
    @param[in] pvtParam  the PVT list parameter or qualifier
    @param[in] numAxes  number of axes of PVT data required
    @param[in] defTAI  TAI date (MJD, seconds) to use if time is omitted
    @return two items:
    - pvtList: a list of numAxes coordConv.PVT
    - numValues: the number of values explicitly specified by the user
        (positions, velocities and TAI, as appropriate)

    Data must be in one of these forms, determined by the number of values the user provides:
    num values      description
    0               no values
    2               two positions, if numAxes>2
    numAxes           position only: pos1, pos2, ... posNAxes
    2*numAxes         position and velocity: pos1, pos2, ... posNAxes, vel1, vel2, ... velNAxes
    1 + 2*numAxes     position, velocity and time: pos1, pos2, ... posNAxes, vel1, vel2, ... velNAxes, tai
    position defaults to 0, velocity defaults to 0 and tai defaults to current TAI

    @throw twistedActor.CommandError under the following circumstances:
    - An invalid number of values is seen
    """
    numValues = len(pvtParam.valueList)

    numPos = numAxes
    numPosVel = 2*numAxes
    numPosVelT = 1 + numPosVel

    if numAxes > 2:
        # allow 2 positions
        validNumVals = (0, 2, numPos, numPosVel, numPosVelT)
    else:
        validNumVals = (0, numPos, numPosVel, numPosVelT)

    if numValues not in validNumVals:
        raise CommandError("Invalid %s: must specify one of: %s values" % (pvtParam.name, validNumVals))

    # usually velocity and time are defaulted
    posList = [0.0]*numAxes
    velList = [0.0]*numAxes
    tai = defTAI

    if numValues >= numPos:
        posList = pvtParam.valueList[0:numPosVel]
        if numValues >= numPosVel:
            velList = pvtParam.valueList[numPos:numPosVel]
            if numValues == numPosVelT:
                tai = pvtParam.valueList[numPosVelT-1]
    elif numAxes > 2 and numValues == 2:
        posList[0:2] = pvtParam.valueList[0:2]

    pvtList = [coordConv.PVT(pos, vel, tai) for pos, vel in itertools.izip(posList, velList)]
    return pvtList, numValues
