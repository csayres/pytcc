from __future__ import division, absolute_import

from twistedActor import CommandError

__all__ = ["getRestart"]

_TranslateDict = {
    "tel1": "azimuth",
    "tel2": "altitude",
}
for key, val in _TranslateDict.items():
    _TranslateDict["no" + key] = "no" + val
_AllAxesList = ("azimuth", "altitude", "rotator")

def getRestart(qual, rotExists):
    """!Get a list of axes from the /Restart qualifier

    @todo: tweak this once we figure out how the parser handles /Restart vs /Restart=();
    for now assume that the values are [] if present with no values and treat this as "all"

    Useful because the parser returns the data as named axes and the names are negatable.

    @param[in] qual  the parsed /Restart qualifier
    @param[in] rotExists  does the current instrument have a rotator?
        If False, the returned restartRot is always false (even if the user requested restarting the rotator)
    @return a tuple of 3 booleans: (restartAz, restartAlt, restartRot).
        If /Restart is omitted or specified with no values then return the default for the qualifier
            (but set restartRot false if rotExists false)

    @throw twistedActor.CommandError if an axis is explicitly specified and negated, e.g. /Restart=(Az, NoAz);
        the test ignores "all" and "noall" (they are not explicit), so this is permitted: /Restart=(All, NoAz)
        Also raise a CommandError if a /NoRestart qualifier was used with any axis.
    """
    AxisExists = (True, True, rotExists)
    if not qual.boolValue:
        # handle /NoRestart
        # don't allow any axis specification:
        if not qual.valueListDefaulted:
            raise CommandError("No axis specification allowed when using /NoRestart")
        return (False, False, False)

    # parse axis names; the default is /All if /Restart specified with no values
    allSpecified = qual.valueListDefaulted and not qual.boolValueDefaulted
    axisSet = set() # names of axes explicitly requested (e.g. "az" or "rot" but not "all")
    noAxisSet = set() # names of axes explicitly negated (e.g. "noaz" or "norot", but not "noall")
    for val in qual.valueList:
        val = val.lower()
        val = _TranslateDict.get(val, val)
        if val == "all":
            allSpecified = True
        elif val == "noall":
            # noall has no useful effect, so ignore it
            continue
        elif val.startswith("no"):
            noAxisSet.add(val[2:])
        else:
            axisSet.add(val)

    # reject explicit name conflicts such as /Restart=(Az, NoAz), but don't worry about All and NoAll
    conflictSet = axisSet.intersection(noAxisSet)
    if conflictSet:
        raise CommandError("Conflicting values in /Restart: %s" % (qual.valueList,))
    if allSpecified:
        # "all" axes requested, so restart all axes except those explicitly negated
        axisSet = set(_AllAxesList) - noAxisSet
    elif not axisSet and noAxisSet:
        # only recieved negated axes, all other axes should be restarted
        axisSet = set(_AllAxesList) - noAxisSet
    return tuple((axisName in axisSet) and AxisExists[i] for i, axisName in enumerate(_AllAxesList))
