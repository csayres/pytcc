from __future__ import division, absolute_import

import coordConv
from twistedActor import CommandError

from .getPVTList import getPVTList
import tcc.base

__all__ = ["getPVTPairPxPMRadVel"]

def getPVTPairPxPMRadVel(parsedCmd, coordName, defTAI):
    """!Obtain object data from a parsed command, and perform some sanity checks
    
    Read one parameter that contains pos1, pos2, [vel1, [vel2, [tai]]]
    and the following qualifiers: /Distance /Px /PM /RV.
    
    @param[in] parsedCmd  the parsed command
    @param[in] coordName  name of coord parameter
    @param[in] defTAI  TAI date to use if time not specified (MJD, seconds)
    @return:
    - pvtPair: a pair of PVTs (tcc.base.ArrayPVT2), or None if the pvt parameter is absent
    - pxPMRadVel: parallax, proper motion and radial velocity, as a tcc.base.PxPMRadVel
    
    @throw twistedActor.CommandError under the following circumstances:
    - /Distance and /Px are both specified
    - Any of /Distance /Px /PM /RV is specified and no position is provided
    """
    pvtParam = parsedCmd.paramDict[coordName]
    if pvtParam.defaulted:
        # no position specified; complain if any qualifiers specified and return None
        if not parsedCmd.qualDict["distance"].boolValueDefaulted:
            raise CommandError("Must specify new position to specify /Distance")
        if not parsedCmd.qualDict["px"].boolValueDefaulted:
            raise CommandError("Must specify new position to specify /Px")
        if not parsedCmd.qualDict["pm"].boolValueDefaulted:
            raise CommandError("Must specify new position to specify /PM")
        if not parsedCmd.qualDict["rv"].boolValueDefaulted:
            raise CommandError("Must specify new position to specify /RV")

        return None, tcc.base.PxPMRadVel()

    if not parsedCmd.qualDict["distance"].boolValueDefaulted and not parsedCmd.qualDict["px"].boolValueDefaulted:
        raise CommandError("Cannot specify both /Distance and /Px")

    numVals = len(pvtParam.valueList)
    if numVals not in (2, 4, 5):
        raise CommandError("Must specify one of pos, pos + vel or pos + vel + tai")
    
    pvtPair = tcc.base.ArrayPVT2()
    (equatPVT, polarPVT) = getPVTList(pvtParam, numAxes=2, defTAI=defTAI)[0]
    pvtPair[:] = equatPVT, polarPVT
    if parsedCmd.qualDict["distance"].boolValueDefaulted:
        parallax = parsedCmd.qualDict["px"].valueList[0]
    else:
        distList = parsedCmd.qualDict["distance"].valueList
        if len(distList) == 0:
            parallax = 0
        else:
            parallax = coordConv.parallaxFromDistance(distList[0])
    equatPM, polarPM = parsedCmd.qualDict["pm"].valueList[0:2]
    radVel = parsedCmd.qualDict["rv"].valueList[0]
    pxPMRadVel = tcc.base.PxPMRadVel(parallax, equatPM, polarPM, radVel)
    
    return pvtPair, pxPMRadVel
