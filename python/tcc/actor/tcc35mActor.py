from __future__ import division, absolute_import
"""The TCC (telescope control code) for the Apache Point Observatory 3.5m telescope
"""
import syslog
from .tccActor import TCCActor
from tcc.mir import MirrorDevice
from tcc.axis import AxisDevice

# list of axis controller device name, host, port
AxisNameHostPortList = (
    ("az",   "tccserv35m-p", 2300),
    ("alt",  "tccserv35m-p", 2400),
    ("rot1", "tccserv35m-p", 2500), # NA2 rotator
    ("rot2", "tccserv35m-p", 3900), # Agile rotator
)
# list of mirror controller device name, host, port
MirrorNameHostPortList = (
    ("sec",  "localhost", 3532),
    ("tert", "localhost", 3533),
)

class TCC35mActor(TCCActor):
    """!TCC actor for the APO 3.5m telescope
    """
    UserPort = 3500
    UDPPort = 1235  # from http://www.apo.nmsu.edu/Telescopes/TCC/UDPPackets.html
    Facility = syslog.LOG_LOCAL1
    def __init__(self):
        """!Construct a TCC35mActor
        """
        TCCActor.__init__(self,
            name = "tcc35m",
            axisDict = dict((name, AxisDevice(name=name, host=host, port=port, lineTerminator="\r"))
                for name, host, port in AxisNameHostPortList),
            mirrorDict = dict((name, MirrorDevice(name=name, host=host, port=port))
                for name, host, port in MirrorNameHostPortList),
            userPort = self.UserPort,
            udpPort = self.UDPPort,
        )
