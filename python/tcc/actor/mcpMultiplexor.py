from __future__ import absolute_import, division

from collections import deque, OrderedDict
import string
import syslog

from twistedActor import log

import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from RO.Comm.TCPConnection import TCPConnection
from RO.Comm.Generic import TCPServer

__all__ = ["MCPMultiplexor"]

LocalDebug = False # if true, talk to the azimuth controller started by emulate35m.py

AxisPortList = (
    ( "AZ", 2521),
    ("ALT", 2522),
    ("ROT", 2523),
)

# unprintable chars we see from the MCP (plus a few extras)
CharsToStrip = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\xfa\xfb\xfc\xfd\xfe\xff'

class AxisServer(object):
    """A server for axis commands

    Axis commands are prefixed with axis name (and a space) if necessary
    """
    AxisList = (val[0] for val in AxisPortList)
    def __init__(self, axis, port, queueFunc):
        """Construct an AxisServer

        For each command received, the server:
        - prepends the axis name and a space, if not already present
        - queues the command by calling queueFunc

        @param[in] axis  axis name
        @param[in] port  port on which to listen to commands for this axis
        @param[in] queueFunc  function to call when a command is to be queued;
            must take three arguments: cmdStr, socket, prefixStr
        """
        if axis not in self.AxisList:
            raise RuntimeError("axis=%r; must be one of %s" % (axis, self.AxisList))
        self.axisPrefix = axis + " "
        self.server = TCPServer(
            sockReadCallback = self._readCallback,
            stateCallback = self._stateCallback,
            port = port,
        )
        self.queueFunc = queueFunc

    def _readCallback(self, sock):
        """Called when a command is received
        """
        cmdStr = sock.readLine()
        if not cmdStr:
            return
        if not cmdStr.startswith(self.axisPrefix):
            prefixStr = self.axisPrefix
            cmdStr = self.axisPrefix + cmdStr
        else:
            prefixStr = ""
        self.queueFunc(cmdStr, sock, prefixStr)

    def _stateCallback(self, conn):
        log.info("%s conn state=%s" % (self.axisPrefix, conn.state,))

    def close(self):
        self.server.close()


class MCPMultiplexor(object):
    """Multiplex axis controller ports into one MCP port

    Axis commands are prefixed with axis name (and a space) if necessary
    """
    Facility = syslog.LOG_LOCAL4
    AxisPortDict = OrderedDict(AxisPortList)
    def __init__(self, mcpHost, mcpPort):
        """!Construct an MCPMultiplexor

        @param[in] mcpHost  MCP host
        @param[in] mcpPort  MCP port
        """
        self._currCmdSockPrefix = None
        self._cmdQueue = deque()

        # dict of axis name: server socket
        self._axisServerDict = None

        log.info("starting MCP connection to %s, %s" % (mcpHost, mcpPort))
        self._mcpConn = TCPConnection(
            name = "MCP",
            host = mcpHost,
            port = mcpPort,
            readLines = True,
            stateCallback = self._mcpConnStateCallback,
            readCallback = self._mcpReadCallback,
        )
        self._mcpConn.connect()

    def _logQueue(self):
        """Show the current state of the queue
        """
        # write prefix and cmdString
        log.info("currently waiting on axis: %r"%(self._currCmdSockPrefix[0] if self._currCmdSockPrefix is not None else "None"))
        log.info("queue=[%s] <--next to execute"%(", ".join([cmdTuple[-1] + " " + cmdTuple[0] for cmdTuple in self._cmdQueue])))


    def _mcpConnStateCallback(self, conn):
        log.info("MCP conn state=%s" % (conn.state,))
        if conn.isConnected and not self._axisServerDict:
            log.info("MCP connection made; starting up axis servers")
            self._cmdQueue = deque()
            self._axisServerDict = dict((axis, AxisServer(axis=axis, port=axisPort, queueFunc=self.queueCmd))
                for axis, axisPort in self.AxisPortDict.iteritems())
        elif conn.isDisconnected:
            log.info("MCP connection done; killing axis servers")
            self._cmdQueue = deque()
            if self._axisServerDict:
                for server in self._axisServerDict.itervalues():
                    server.close()
                self._axisServerDict = None

    def queueCmd(self, cmdStr, sock, prefixStr):
        """Add a command to the command queue

        @param[in] cmdStr  command to queue
        @param[in] sock  socket to which to write replies
        @param[in] prefixStr  prefix prepended to cmdStr
        """
        log.info("queue %r for %s" % (cmdStr, sock))
        self._cmdQueue.appendleft((cmdStr, sock, prefixStr))
        self._logQueue()
        self._runQueue()

    def _mcpReadCallback(self, sock, replyStr):
        """Called when data is read from the MCP
        """
        if self._currCmdSockPrefix:
            log.info("read %r from MCP for %r" % (replyStr, self._currCmdSockPrefix[0]))
            sock = self._currCmdSockPrefix[1]

            # strip unprintable chars
            replyStr = replyStr.translate(string.maketrans("",""), CharsToStrip)

            # strip prefix string
            prefixStr = self._currCmdSockPrefix[2]
            if replyStr and prefixStr and replyStr.startswith(prefixStr):
                replyStr = replyStr[len(prefixStr):]

            if not replyStr:
                return

            try:
                log.info("sending %r to %r for %r" % (replyStr, sock, self._currCmdSockPrefix[0]))
                sock.writeLine(replyStr)
            except Exception, e:
                log.warn("Could not write to %s: %s" % (sock, e))
            if replyStr == "OK" or replyStr.endswith(" OK"):
                # command is done; clear current command and start a new one
                self._currCmdSockPrefix = None
                self._runQueue()
        else:
            log.warn("read %r from MCP, unsolicited" % (replyStr,))

    def _runQueue(self):
        """Start a new command, if ready to do so, else do nothing
        """
        if self._currCmdSockPrefix or not self._cmdQueue:
            return
        cmdStr, sock, prefixStr = self._cmdQueue.pop()
        self._currCmdSockPrefix = (cmdStr, sock, prefixStr)
        log.info("sending %r to MCP" % (cmdStr,))
        self._logQueue()
        self._mcpConn.writeLine(cmdStr)
