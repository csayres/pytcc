from __future__ import division, absolute_import
"""The TCC (telescope control code) for the Apache Point Observatory 2.5m telescope
"""
import syslog
from .tccActor import TCCActor
from tcc.mir import MirrorDevice
from tcc.axis import SDSSAxisDevice, MCPMultiplexor

# list of axis controller device name, host, port;
# these are ports on the MCP multiplexor
# AxisNameHostPortList = (
#     ("az",   "localhost", 2521),
#     ("alt",  "localhost", 2522),
#     ("rot1", "localhost", 2523),
# )

AxisNameList = ("alt", "az", "rot1")
MCPHost = "t-g-sdss-2"
MCPPort = 3900
mcpMultiplexor = MCPMultiplexor(name="mcpMultiplexor", host=MCPHost, port=MCPPort)


# list of mirror controller device name, host, port
MirrorNameHostPortList = (
    ("prim",  "localhost", 2531),
    ("sec",   "localhost", 2532),
)

class TCC25mActor(TCCActor):
    """!TCC actor for the APO 2.5m telescope
    """
    UserPort = 2500
    UDPPort = 1200  # from http://www.apo.nmsu.edu/Telescopes/TCC/UDPPackets.html
    Facility = syslog.LOG_LOCAL1
    def __init__(self):
        """!Construct a TCC25mActor
        """
        TCCActor.__init__(self,
            name = "tcc25m",
            axisDict = dict((name, SDSSAxisDevice(name=name, mcpMultiplexor=mcpMultiplexor)) for name in AxisNameList),
            mirrorDict = dict((name, MirrorDevice(name=name, host=host, port=port))
                for name, host, port in MirrorNameHostPortList),
            userPort = self.UserPort,
            udpPort = self.UDPPort,
            connectRot = True,
        )
