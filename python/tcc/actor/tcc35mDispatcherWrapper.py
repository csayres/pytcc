from __future__ import division, absolute_import

from twistedActor import DispatcherWrapper
from .tcc35mActorWrapper import TCC35mActorWrapper

__all__ = ["TCC35mDispatcherWrapper"]

class TCC35mDispatcherWrapper(DispatcherWrapper):
    """!Wrapper for an ActorDispatcher talking to a mock 3.5m TCC talking to mock controllers

    The wrapper manages everything: starting up fake axis and mirror controllers
    on automatically chosen ports, constructing devices that talk to them, constructing
    a TCC actor the specified port, and constructing and connecting the dispatcher.
    """
    def __init__(self, userPort=0):
        """!Construct a TCC35mDispatcherWrapper

        @param[in] userPort  port for mock 3.5m controller; 0 to chose a free port
        """
        actorWrapper = TCC35mActorWrapper(
            name = "mockTCC35m",
            userPort = userPort,
        )
        DispatcherWrapper.__init__(self,
            name = "tcc35mClient",
            actorWrapper = actorWrapper,
            dictName = "tcc",
        )
