from __future__ import division, absolute_import

from twistedActor import ActorWrapper
from mirrorCtrl.mirrors import mir35mSec, mir35mTert

from tcc.actor import TCCActor
from tcc.axis import AxisDeviceWrapper
from tcc.mir import MirrorDeviceWrapper

__all__ = ["TCC35mActorWrapper"]

class TCC35mActorWrapper(ActorWrapper):
    """!Unit test wrapper for a mock 3.5m TCC actor
    """
    def __init__(self,
        name = "mockTCC35m",
        userPort = 0,
        udpPort = 0,
        debug = False,
    ):
        """!Construct a TCC35mActorWrapper

        @param[in] name  a name to use for messages
        @param[in] userPort  port for actor server
        @param[in] udpPort  port for udp broadcasts, if 0 twisted will automatically select an open one
        @param[in] debug  print debug messages?
        """
        axisNameList = ("az", "alt", "rot1", "rot2")
        mirrorList = (
            ("sec", mir35mSec),
            ("tert", mir35mTert)
        )
        self.axisWrapperDict = dict((name, AxisDeviceWrapper(name=name, debug=debug)) for name in axisNameList)
        self.mirrorWrapperDict = dict((name, MirrorDeviceWrapper(name=name, mirror=mirror, debug=debug)) for name, mirror in mirrorList)
        deviceWrapperList = self.axisWrapperDict.values() + self.mirrorWrapperDict.values()
        ActorWrapper.__init__(self,
            deviceWrapperList = deviceWrapperList,
            name = name,
            userPort = userPort,
            debug = debug,
        )

    def _makeActor(self):
        self.debugMsg("_makeActor()")
        axisDict = dict((name, wrapper.device) for name, wrapper in self.axisWrapperDict.iteritems())
        mirrorDict = dict((name, wrapper.device) for name, wrapper in self.mirrorWrapperDict.iteritems())
        self.actor = TCCActor(
            name = self.name,
            axisDict = axisDict,
            mirrorDict = mirrorDict,
            userPort = self._userPort,
        )
