from __future__ import division, absolute_import

from .tccActor import TCCActor
from tcc.mir import MirrorDevice
from tcc.axis import AxisDevice

__all__ = ["FakeTCC35mActor"]

UserPort = 3500
# dict of axis name: host, port
AxisNameHostPortList = (
    ("az",   "localhost", 3534),
    ("alt",  "localhost", 3535),
    ("rot1", "localhost", 3536),
)
# dict of mirror name: host, port, modelName
MirrorNameHostPortList = (
    ("sec",  "localhost", 3532),
    ("tert", "localhost", 3533),
)

class FakeTCC35mActor(TCCActor):
    """!Emulator for the TCC for the APO 3.5m telescope
    """
    def __init__(self, userPort=UserPort):
        """!Construct a FakeTCC35mActor
        """
        TCCActor.__init__(self,
            axisDict = dict((name, AxisDevice(name=name, host=host, port=port))
                    for name, host, port in AxisNameHostPortList),
            mirrorDict = dict((name, MirrorDevice(name=name, host=host, port=port)) 
                for name, host, port in MirrorNameHostPortList),
            userPort = userPort,
        )