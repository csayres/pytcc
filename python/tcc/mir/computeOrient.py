from __future__ import division, absolute_import
"""Routine for constructing mirror orientations from tcc blocks for various mirrors
"""
import numpy

import coordConv

def computeOrient(mirName, alt, inst, weath):
    """!Compute the orientation of one mirror
    
    @param[in] mirName  mirror name: one of 'prim', 'sec', or 'tert' (case sensitive)
    @param[in] alt  telescope altitude (deg)
    @param[in] inst  instrument block
    @param[in] weath  weather block
    
    @return orient: desired mirror orientation, a list containing:
    - piston (um)
    - x tilt (arcsec)
    - y tilt (arcsec)
    - x piston (um)
    - y piston (um)
    """
    sinAlt = coordConv.sind(alt)
    cosAlt = coordConv.cosd(alt)

    try:
        orientCoeffs = [
            getattr(inst, mirName+'PistCoef'),
            getattr(inst, mirName+'XTiltCoef'),
            getattr(inst, mirName+'YTiltCoef'),
            getattr(inst, mirName+'XTransCoef'),
            getattr(inst, mirName+'YTransCoef'),
        ] # note: no rotation coeff   
    except AttributeError:
        raise RuntimeError("Unrecognized mirror for collimation: %s" % (mirName,))

    orient = [coeff[0] + (coeff[1] * sinAlt) + (coeff[2] * cosAlt) for coeff in orientCoeffs]

    if mirName == 'prim':
        # include scale coeff
        orient[0] += inst.primPistScaleCoef * (inst.scaleFac - 1.0)
    elif mirName == 'sec':
        # for secondary piston include focus offset, scale factor,
	    # and temperature coefficients
        addThese = [
            inst.inst_foc, 
            inst.secUserFocus,
            inst.secPistScaleCoef * (inst.scaleFac - 1.0),
	        inst.secPistTempCoef[0] * weath.secTrussTemp,
	        inst.secPistTempCoef[1] * weath.primFrontTemp, # old tcc: primF_BFTemp[0]
	        inst.secPistTempCoef[2] * weath.primBackFrontTemp, # old tcc: primF_BFTemp[1]
	        inst.secPistTempCoef[3] * weath.secFrontTemp,
	        inst.secPistTempCoef[4] * weath.secBackFrontTemp,
	    ]            
        orient[0] = orient[0] + numpy.sum(addThese)      
    return orient  
