from __future__ import division, absolute_import

from twistedActor import DeviceWrapper
from mirrorCtrl import MirrorCtrlWrapper
from mirrorCtrl.fakeGalil import FakeGalil

from .mirrorDevice import MirrorDevice

__all__ = ["MirrorDeviceWrapper"]

class MirrorDeviceWrapper(DeviceWrapper):
    """!A wrapper for an MirrorDevice talking to a fake mirror controller
    """
    def __init__(self,
        name,
        mirror,
        galilClass = FakeGalil,
        stateCallback = None,
        port = 0,
        modelName = "mirror",
        debug = False,
    ):
        """!Construct a MirrorDeviceWrapper that manages its fake mirror controller

        @param[in] name  keyword dictionary name (typically "mirror")
        @param[in] mirror  Mirror object from mirrorCtrl.mirrors
        @param[in] galilClass  fake Galil class; normally FakeGalil.
        @param[in] stateCallback  function to call when connection state of hardware controller or device changes;
            receives one argument: this device wrapper
        @param[in] port  port on which mirror controller listens
        @param[in] modelName  name of mirror controller keyword dictionary; normally "mirror"
        @param[in] debug  print debug messages to stdout?
        """
        self.modelName = modelName
        controllerWrapper = MirrorCtrlWrapper(
            mirror=mirror,
            galilClass = galilClass,
        )
        DeviceWrapper.__init__(self, name=name, stateCallback=stateCallback, controllerWrapper=controllerWrapper, debug=debug)

    def _makeDevice(self):
        port = self.port
        if port is None:
            raise RuntimeError("Controller port is unknown")
        self.debugMsg("_makeDevice, port=%s" % (port,))
        self.device = MirrorDevice(
            name=self.name,
            host="localhost",
            port=port,
            modelName=self.modelName,
        )


