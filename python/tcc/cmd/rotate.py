from __future__ import division, absolute_import

from twistedActor import CommandError

import tcc.base
from tcc.parse import getPVTList, getRestart
from .checkTargetForSlew import checkTargetForSlew
from .startSlew import startSlew
from .trackStop import trackStop

__all__ = ["rotate"]

def rotate(tccActor, userCmd):
    """Implement the rotate command
    
    @todo: Handle these qualifiers:
    - ScanVelocity
    - PtErr (raises a not implemented error)
    @todo: for the position check, improve the error and warning reporting; right now it is much too minimal
    
    @param[in,out] tccActor  tcc actor;
        modifies obj and slewCmd
    @param[in,out] userCmd  rotate command
    """
    parsedCmd = userCmd.parsedCmd
    if not tccActor.inst.hasRotator():
        raise CommandError("The current instrument has no rotator")

    qualDict = parsedCmd.qualDict
    if qualDict["stop"].boolValue:
        return trackStop(tccActor=tccActor, userCmd=userCmd, doObj=False, doRot=True)

    # modify a copy of obj; if all goes well then switch to the copy
    newObj = tcc.base.Obj(tccActor.obj)
    nextTAI = tcc.base.tai() + tccActor.tune.trackInterval + tccActor.tune.trackAdvTime
    
    doRestart = getRestart(
        qual = qualDict["restart"],
        rotExists = tccActor.inst.hasRotator(),
    )
    perfect = qualDict["perfect"].boolValue

    if qualDict["refcoefficients"].boolValue:
        tccActor.weath.updateSite(newObj.site)

    rotAngleParam = parsedCmd.paramDict["rotangle"]
    isNewRot = rotAngleParam.boolValue
    if isNewRot:
        # getPVTList returns (pvtList, rotOmitted), so we want the [0][0] returned value, a single PVT
        newObj.rotUser = getPVTList(rotAngleParam, numAxes=1, defTAI=nextTAI)[0][0]

    rotTypeParam = parsedCmd.paramDict["rottype"]
    if rotTypeParam.boolValue:
        rotTypeName = rotTypeParam.valueList[0].keyword
        newObj.rotType = tcc.base.RotTypeNameDict[rotTypeName.lower()]
        if not isNewRot:
            # I don't think this can happen, but just in case...
            raise CommandError("rotation angle required if rotation type specified")
    elif isNewRot:
        raise CommandError("rotation type required if rotation angle specified")

    if newObj.userSys.getName() == "mount":
        newObj.azWrapPref = tcc.base.WrapType_None
    else:
        # don't unwrap azimuth axis; we're just changing the rotator
        newObj.azWrapPref = tcc.base.WrapType_Nearest

    if newObj.rotType == tcc.base.RotType_Mount:
        newObj.rotWrapPref = tcc.base.WrapType_None
    else:
        newObj.rotWrapPref = tcc.base.WrapTypeNameDict[qualDict["rotwrap"].valueList[0].lower()]

    checkTargetForSlew(
        tccActor = tccActor,
        userCmd = userCmd,
        newObj = newObj,
        isNewObj = False,
        isNewRot = isNewRot,
        doWrap = True,
        doRestart = doRestart,
        perfect = perfect,
        tai = nextTAI,
    )
    if userCmd.isDone:
        return

    startSlew(
        tccActor = tccActor,
        obj = newObj,
        slewCmd = userCmd,
        doAbsRefCorr = qualDict["absrefcorrect"].boolValue,
        doCollimate = qualDict["collimate"].boolValue,
    )
