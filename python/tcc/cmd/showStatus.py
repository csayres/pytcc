from __future__ import division, absolute_import

__all__ = ["showStatus"]

def showStatus(tccActor, userCmd):
    """
    @param[in] tccActor  tcc actor
    @param[in,out] userCmd  user command; if not None then is set Done when finished
    """
    tccActor.showStatus(cmd=userCmd)