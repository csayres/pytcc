from __future__ import division, absolute_import

import functools

from twistedActor import CommandError

import tcc.base
from tcc.axis import computeObj
from .showInstrument import showInstrument
from .showObject import showObjectFields
from .startSlew import startSlew

__all__ = ["setInstrument"]

def setInstrument(tccActor, userCmd):
    """Implement the "set instrument" command

    @param[in,out] tccActor  tcc actor
        writes to tccActor.inst, and tccActor.obj
    @param[in,out] userCmd  user command
    """
    currSecUserFocus = tccActor.inst.secUserFocus
    try:
        instName = userCmd.parsedCmd.paramDict["instrument"].valueList[0].valueList[0]
    except Exception:
        # no name specified, re-use from current inst
        instName = tccActor.inst.instName
        if not instName:
            raise CommandError('No name avalable in current instrument block!')

    gcview = userCmd.parsedCmd.qualDict["gcview"].valueList[0] if userCmd.parsedCmd.qualDict["gcview"].boolValue else None
    newInst = tcc.base.loadInst(
        instDir = tcc.base.getInstDir(),
        instName = instName,
        gcViewName = gcview,
        writeToUsers = tccActor.writeToUsers,
        cmd = userCmd,
    )
    newInst.secUserFocus = currSecUserFocus

    if userCmd.parsedCmd.qualDict["keep"].boolValue:
        # keep certain values, note: no scaleFactor range checking...
        # could add it.
        for attr in userCmd.parsedCmd.qualDict["keep"].valueList:
            if attr.endswith('Focus'):
                # add 'User' before 'Focus' to match block attribute
                attr = attr.replace('Focus', 'UserFocus')
            setattr(newInst, attr, getattr(tccActor.inst, attr))

    # if rotator changes, then update tccActor.axisDevSet and initialize the old and new rotator
    if tccActor.inst.rotID != newInst.rotID:
        RotSwapper(tccActor=tccActor, userCmd=userCmd, newInst=newInst)
    else:
        isNewInst = newInst.instName != tccActor.inst.instName
        tccActor.inst = newInst
        finishSetInst(tccActor=tccActor, userCmd=userCmd, isNewInst=isNewInst)


class RotSwapper(object):
    """Swap instrument rotators.

    On success, update tccActor.inst and call finishSetInst
    """
    def __init__(self, tccActor, userCmd, newInst):
        """Construct a RotSwapper

        @param[in,out] tccActor  tcc actor
            writes to tccActor.inst, and tccActor.obj
        @param[in,out] userCmd  user command
        @param[in] newInst  new instrument data (a tcc.base.Inst)
        """
        self.userCmd = userCmd
        self.tccActor = tccActor
        self.newInst = newInst
        self.isNewInst = newInst.instName != tccActor.inst.instName
        if newInst.rotID <= 0:
            newDev = None
        else:
            newRotName = "rot%i" % (newInst.rotID,)
            try:
                newDev = self.tccActor.axisDict[newRotName]
            except Exception:
                raise CommandError("Unknown rotator ID %d" % (newInst.rotID,))
        swapCmd = self.tccActor.axisDevSet.replaceDev(
            "rot",
            newDev,
        )
        swapCmd.addCallback(functools.partial(self._swapCallback, newDev=newDev))

    def _swapCallback(self, cmd, newDev):
        if not cmd.isDone:
            return
        if cmd.didFail:
            self.userCmd.setState(self.userCmd.Failed,
                textMsg = "Failed to change to rotator %d: %s" % (self.newInst.rotID, cmd.getMsg()))
        else:
            self.tccActor.inst = self.newInst
            if self.tccActor.inst.rotID <= 0:
                # there is no rotator, update rotator axis state to not available
                self.tccActor.obj.axisCmdState[2] = tcc.base.AxisState_NotAvailable
            else:
                # there is a rotator
                self.tccActor.obj.axisCmdState[2] = tcc.base.AxisState_Halted
            finishSetInst(self.tccActor, self.userCmd, isNewInst=self.isNewInst)


def finishSetInst(tccActor, userCmd, isNewInst):
    """Finish the set instrument command by updating tccActor.axeLim and slewing to the updated target

    Halted axes are left halted. If the new instrument has no rotator than the old rotType is ditched.

    @param[in,out] tccActor  tcc actor
        reads and updates obj, setting azWrapPref and rotWrapPref and slewing
    @param[in,out] userCmd  set instrument command
        reads azwrap and rotwrap preferences from userCmd.parsedCmd.qualDict
    @param[in] isNewInst  set True if "set inst" changed to a new instrument,
        in which case set rotType to None
    """
    tccActor.axeLim.setRotLim(tccActor.inst)
    showInstrument(tccActor, userCmd, setDone=False, showFull=True)
    if isNewInst or not tccActor.inst.hasRotator():
        tccActor.obj.rotType = tcc.base.RotType_None
        tccActor.obj.rotUser.invalidate()
        showObjectFields(tccActor, userCmd, showFull=None)

    if tccActor.obj.isMoving():
        # apply wrap preference
        newObj = tcc.base.Obj(tccActor.obj)
        newObj.azWrapPref = tcc.base.WrapTypeNameDict[userCmd.parsedCmd.qualDict["azwrap"].valueList[0].lower()]
        newObj.rotWrapPref = tcc.base.WrapTypeNameDict[userCmd.parsedCmd.qualDict["rotwrap"].valueList[0].lower()]

        computeObj(
            obj = newObj,
            doWrap = True,
            doRestart = [False]*tcc.base.NAxes,
            reportLim = True,
            earth = tccActor.earth,
            inst = tccActor.inst,
            telMod = tccActor.telMod,
            axeLim = tccActor.axeLim,
            tai = tcc.base.tai(),
        )

        startSlew(
            tccActor = tccActor,
            obj = newObj,
            slewCmd = userCmd,
            doAbsRefCorr = True,
            doCollimate = True,
        )
    else:
        userCmd.setState(userCmd.Done)
