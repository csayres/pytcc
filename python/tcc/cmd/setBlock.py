from __future__ import division, absolute_import
"""SET Block command
"""
from copy import copy
import os.path

from RO.AddCallback import safeCall2
from twistedActor import CommandError

import tcc.base
from .showBlock import blockAttrDict
from tcc.msg import showAxeLim, showTune

__all__ = ["setBlock"]

exitChars = ("^z", "exit") # should be control z # add ^Y or ^C?

# pure python blocks must be copied differently than SWIGged C++ blocks
PurePythonBlocks = set(["Tune"])

blockNameShowFuncDict = dict(
    axelim = showAxeLim,
    tune = showTune,
)

def callShowFunc(blockName, tccActor, userCmd):
    """Call a function, if one exists, to show the contents of the specified block using standard keywords

    @param[in] blockName  block name (case ignored)
    @param[in] tccActor  tcc actor
    @param[in] userCmd  user command
    """
    showFunc = blockNameShowFuncDict.get(blockName.lower())
    if showFunc:
        safeCall2("show keywords for %s block" % (blockName,), showFunc, tccActor, userCmd)

def setBlock(tccActor, userCmd):
    """
    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  a twistedActor BaseCommand with parseCmd attribute

    from a userCmd set the appropriate block, an attribute on the tccActor.
    single fields may be changed, a complete block file may be read, or if no params
    are passed the block is reloaded
    """
    blockName = userCmd.parsedCmd.paramDict["blockname"].valueList[0].keyword
    inputPath = None
    if userCmd.parsedCmd.qualDict['input'].boolValue:
        dataDir = tcc.base.getDataDir()
        inputPath = userCmd.parsedCmd.qualDict['input'].valueList[0]
        if not os.path.splitext(inputPath)[1]:
            inputPath += ".dat"
        inputPath = os.path.join(dataDir, inputPath)
        if not os.path.isfile(inputPath):
            raise CommandError("No such file %r" % (inputPath,))
    noRead = userCmd.parsedCmd.qualDict['noread'].boolValue

    baseObj = getattr(tcc.base, blockName)
    if noRead:
        # get a copy of the block with null values, not the copy in the global database
        blockCopy = baseObj()
        setattr(tccActor, blockAttrDict[blockName], blockCopy)
        userCmd.setState(userCmd.Done)
        return
    else:
        currBlock = getattr(tccActor, blockAttrDict[blockName])
        if blockName in PurePythonBlocks:
            blockCopy = copy(currBlock)
        else:
            blockCopy = baseObj(currBlock)

    if inputPath:
        blockCopy.loadPath(inputPath)
        setattr(tccActor, blockAttrDict[blockName], blockCopy)
        callShowFunc(blockName, tccActor, userCmd)
        userCmd.setState(userCmd.Done)
    else:
        # raw input is expected, enter an input state
        # this intercepts incoming data at the socket's readLine level
        InteractiveSetBlock(tccActor, blockCopy, blockName, userCmd)

class InteractiveSetBlock(object):
    """update the block being modified directly from socket input
    """
    def __init__(self, tccActor, blockCopy, blockName, userCmd):
        self.tccActor = tccActor
        self.sock = self.tccActor.userDict[userCmd.userID]
        self.blockCopy = blockCopy
        self.blockName = blockName
        userCmd.addCallback(callFunc=self._revert)
        userCmd.setTimeLimit(0) # command won't terminate until exit lines are received
        self.userCmd = userCmd
        # short circuit the socket's read callback
        self.origCB = self.sock._readCallback
        self.sock.setReadCallback(self.readCallback)

    def readCallback(self, sock):
        try:
            line = sock.readLine()
            # dispatcher sends commands prepended with a number, strip it
            num1, num2, line = line.strip().lower().split(None, 2)
            # note some assumption of incoming line's structure here, may need to change
            # once implemented in the real system
            #int(num) # assertion
            if line in exitChars:
                setattr(self.tccActor, blockAttrDict[self.blockName], self.blockCopy)
                callShowFunc(self.blockName, self.tccActor, self.userCmd)
                # finish the cmd, will cause the tcc to revert to correct normal state
                self.userCmd.setState(self.userCmd.Done)
            else:
                self.blockCopy.load([line]) # load loops through lines, hence the list
        except:
            self.userCmd.setState(self.userCmd.Failed, textMsg="Failed to load %s into block: %s" % (line, self.blockName))

    def _revert(self, cmd):
        """callback for current userCmd to ensure that the correct parse method
        is replaced on the actor once the command is done
        """
        if cmd.isDone:
            self.sock.setReadCallback(self.origCB)
