from __future__ import division, absolute_import

__all__ = ["showPtErrProbe"]

def showPtErrProbe(tccActor, userCmd, setDone=True):
    """Implement the Show PtErrProbe command

    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  user command
    @param[in] setDone  set userCmd done when finished? (ignored if userCmd is already done)
    """
    msgStr = "ptErrProbe=%s" % (tccActor.inst.ptErrProbe,)
    tccActor.writeToUsers('i', msgStr, cmd=userCmd)
    if setDone and not userCmd.isDone:
        userCmd.setState(userCmd.Done)
