from __future__ import division, absolute_import

from twistedActor import CommandError

__all = ["mirror"]

def mirror(tccActor, userCmd):
    """Execute the mirror command

    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  mirror command
    """
    cmdVerb = userCmd.parsedCmd.paramDict["command"].valueList[0].keyword.lower()
    # match the parsed parameters to the device names defined in tccActor
    matchDict = {
        "primary": "prim",
        "secondary": "sec",
        "tertiary": "tert",
    }
    availableMirrors = []
    for mir in matchDict.values():
        if tccActor.mirDevSet[mir]!=None:
            availableMirrors.append(mir)

    cmdMirNameSet = set(mir.keyword.lower() for mir in userCmd.parsedCmd.paramDict["mirrors"].valueList)
    if "all" in cmdMirNameSet:
        # command all mirrors
        slotList = availableMirrors
    else:
        slotList = [matchDict[mir] for mir in cmdMirNameSet if matchDict[mir] in availableMirrors]
    if not slotList:
        raise CommandError("No existing mirror commanded. Received %s"%(str(cmdMirNameSet)))

    # sort out qualifiers
    kwargs = dict(
        slotList=slotList,
        userCmd=userCmd,
    )
    if userCmd.parsedCmd.qualDict["timelimit"].boolValue:
        kwargs["timeLim"] = userCmd.parsedCmd.qualDict['timelimit'].valueList[0]
    
    if cmdVerb == "connect":
        tccActor.mirDevSet.connect(**kwargs)
    elif cmdVerb == "disconnect":
        tccActor.mirDevSet.disconnect(**kwargs)
    elif cmdVerb == "initialize":
        tccActor.mirDevSet.connectOrInit(**kwargs)
    elif cmdVerb == "status":
        tccActor.mirDevSet.status(**kwargs)
    else:
        raise CommandError("Bug: unrecognized mirror command verb %r in %r" % (cmdVerb, userCmd.cmdStr))  
    return True
    
