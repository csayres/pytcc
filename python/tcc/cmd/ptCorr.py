from __future__ import division, absolute_import

import itertools

import coordConv
from twistedActor import CommandError

import tcc.base
from tcc.axis import computeObj, coordConvWithFP, obsFromRot
from tcc.parse import getPVTPairPxPMRadVel, getCoordSys

__all__ = ["ptCorr"]

_BadCoordSysNameList = ("mount", "none")
_BadCoordSysNameLCSet = set(csName.lower() for csName in _BadCoordSysNameList)
_BadCoordSysDescr = " or ".join(_BadCoordSysNameList)
del _BadCoordSysNameList # avoid accidental misuse when _BadCoordSysNameLCSet is wanted

def ptCorr(tccActor, userCmd):
    """Implement the ptCorr command
    
    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  user command
    """
    parsedCmd = userCmd.parsedCmd
    qualDict = parsedCmd.qualDict

    currTAI = tcc.base.tai()

    # use a copy of obj so we can safely update it at current time
    # and even swap obj.site if using GS wavelength
    obj = tcc.base.Obj(tccActor.obj)

    # it may seem tempting to make this automatic if toSys is guide (yes) or inst (no),
    # but the guider may be the inst for ptcorr, so I doubt it's wise
    useGSWavelen = qualDict["usegswavelength"].boolValue

    predPVTPair, predPxPMRadVel = getPVTPairPxPMRadVel(parsedCmd=parsedCmd, coordName="predcoords", defTAI=currTAI)
    if predPVTPair is None:
        raise CommandError("Must specify star's predicted position")
    predCoordSys = getCoordSys(param=parsedCmd.paramDict["predcoordsys"])
    if predCoordSys is None:
        raise CommandError("Must specify coordinate system of star's predicted position")

    measPVTPair, measPxPMRadVel = getPVTPairPxPMRadVel(parsedCmd=parsedCmd, coordName="meascoords", defTAI=currTAI)
    if measPVTPair is None:
        raise CommandError("Must specify star's measured position")
    measTAI = measPVTPair[0].t
    measCoordSys = getCoordSys(param=parsedCmd.paramDict["meascoordsys"])
    if measCoordSys is None:
        raise CommandError("Must specify coordinate system of star's measured position")

    if predCoordSys.getName().lower() in _BadCoordSysNameLCSet:
        raise CommandError("Invalid fromSys=%r: cannot convert from %s" % (predCoordSys.getName(), _BadCoordSysDescr))
    if measCoordSys.getName().lower() in _BadCoordSysNameLCSet:
        raise CommandError("Invalid toSys=%r: cannot convert to %s" % (measCoordSys.getName(), _BadCoordSysDescr))

    # update object block so focal-plane math is correct
    computeObj(
        obj,
        doWrap = False,
        doRestart = [True]*tcc.base.NAxes,
        reportLim = False,
        earth = tccActor.earth,
        inst = tccActor.inst,
        telMod = tccActor.telMod,
        axeLim = tccActor.axeLim,
        tai = measTAI,
    )

    # convert pred and meas to "PtCorr" coordinates (rotator, but aligned such that x = increasing azimuth)

    zeroPVT = coordConv.PVT(0, 0, currTAI)
    toSys = coordConv.OtherCoordSys("ptcorr", 0)
    predPtCorrXY = coordConvWithFP(
        fromCoordSys = predCoordSys,
        fromPVTPair = predPVTPair,
        fromPxPMRadVel = predPxPMRadVel,
        fromDir = zeroPVT,
        toCoordSys = toSys,
        obj = obj,
        inst = tccActor.inst,
        zeroPM = True,
        useGSWavelen = useGSWavelen,
    )[0]

    measPtCorrXY, dumPxPMRadVel, measToFromAng, dumScaleChange = coordConvWithFP(
        fromCoordSys = measCoordSys,
        fromPVTPair = measPVTPair,
        fromPxPMRadVel = measPxPMRadVel,
        fromDir = zeroPVT,
        toCoordSys = toSys,
        obj = obj,
        inst = tccActor.inst,
        zeroPM = True,
        useGSWavelen = useGSWavelen,
    )

    ptCorrXY = [pred - meas for pred, meas in itertools.izip(predPtCorrXY, measPtCorrXY)]
    
    # compute desired phys (obs at ctr of rot after correction)
    rotCtrXY = (coordConv.PVT(0, 0, currTAI), coordConv.PVT(0, 0, currTAI))
    currPhysCoord, dumAzRotAng = obsFromRot(rotCtrXY, obj, tccActor.inst, currTAI)
    currPhysXY = tcc.base.ArrayPVT2()
    currPhysCoord.getSphPVT(currPhysXY[0], currPhysXY[1])
    desPhysXY = [currPhys + corr for currPhys, corr in itertools.izip(currPhysXY, ptCorrXY)]
    currMountXY = obj.targetMount[0:2]
    
    msgStrList = [
        "ptCorr=%0.3f, %0.3f, %0.3f, %0.3f" % \
            tuple(pvt.getPos(currTAI) for pvt in itertools.chain(ptCorrXY, predPtCorrXY)),
        "ptData=%0.6f, %0.6f, %0.6f, %0.6f, %0.6f" % \
            tuple(pvt.getPos(currTAI) for pvt in itertools.chain(desPhysXY, currMountXY, (obj.rotPhys,)),
        )
    ]

    msgStr = "; ".join(msgStrList)
    userCmd.setState(userCmd.Done, hubMsg=msgStr)