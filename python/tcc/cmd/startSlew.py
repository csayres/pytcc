from __future__ import division, absolute_import

import itertools

from RO.StringUtil import strFromException

from tcc.axis import computeSlew
import tcc.base
from .showObject import showObjectFields
from tcc.msg import formatAxisCmdStateAndErr, moveItemKW

__all__ = ["startSlew"]

def startSlew(tccActor, obj, slewCmd, doAbsRefCorr, doCollimate, minEndTime=0):
    """Start a slew to the position specified in obj

    Before calling, be sure to update obj as you like, applying wrap preferences
    and doRestart such that obj.targetMount is as desired. StartSlew ignores wrap preferences
    and will not slew axes whose targetMount is invalid.
    Axes that exist and are not to be slewed are commanded to halt.

    This happens in three stages:
    - Send the DRIFT command the appropriate axis controllers
    - Compute the slew
    - Send the slew to the appropriate axis controllers

    @param[in,out] tccActor  tcc actor with old obj block
        obj block is replaced by obj
    @param[in,out] obj  new object block
    @param[in,out] slewCmd  new slew command
    @param[in] doAbsRefCorr  apply absolute fiducial corrections during this slew?
    @param[in] doCollimate  trigger a collimation update (based on destination altitude)?
    @param[in] minEndTime  minimum end time (TAI, MJD sec); 0 means no minimum
    """
    tccActor.collimateSlewEndTimer.cancel()
    if not tccActor.slewCmd.isDone:
        tccActor.slewCmd.setState(tccActor.slewCmd.Done, "SlewSuperseded")
    tccActor.obj = obj
    tccActor.slewCmd = slewCmd
    doDrift = [pvt.isfinite() or cmdState == tcc.base.AxisState_Halting \
        for pvt, cmdState in itertools.izip(obj.targetMount, obj.axisCmdState)]
    if not any(doDrift):
        tccActor.writeToUsers("i", "Text=\"Nothing to do: all axes halted\"", cmd=slewCmd)
        tccActor.axisDevSet.stop(userCmd=slewCmd)
        return

    def driftCallback(driftCmd):
        """Callback for drift command

        If drift succeeds for all axes then compute and start the slew,
        else initialize all axes (to make sure no drifting axes are left drifting).
        """
        if driftCmd.didFail:
            # note that AxisDeviceSet initializes all axes if DRIFT fails for any one axis, so just report the problem
            if not tccActor.slewCmd.isDone:
                tccActor.slewCmd.setState(tccActor.slewCmd.Failed,
                    textMsg = "Slew failed: %s" % (driftCmd.getMsg(),))
            return

        for i, doit in enumerate(doDrift):
            if doit:
                tccActor.obj.axisCmdState[i] = tcc.base.AxisState_Drifting
        msgCode, msgStr = formatAxisCmdStateAndErr(tccActor.obj)
        tccActor.writeToUsers(msgCode, msgStr, cmd=tccActor.slewCmd)
        _computeAndSendSlew(
            tccActor=tccActor,
            doAbsRefCorr=doAbsRefCorr,
            doCollimate=doCollimate,
            minEndTime=minEndTime,
        )

    # start the drift command
    tccActor.trackTimer.cancel()
    driftCmd = tccActor.axisDevSet.drift()
    driftCmd.addCallback(driftCallback)


def _computeAndSendSlew(tccActor, doAbsRefCorr, doCollimate, minEndTime=0):
    """Compute a slew and initiate it by sending PVTs to the axis controllers.

    Call after updating tccActor.obj as you like, and causing the axes you want slewed
    or halted to DRIFT (and updating obj.axisCmdState and obj.actMount accordingly).
    See tcc.axis.computeSlew for details on what you must do.
    Axes that exist and are not to be slewed are commanded to halt.

    @param[in,out] tccActor  tcc actor
        obj block is updated; see tcc.axis.computeSlew for details
        many other blocks are read
    @param[in] doAbsRefCorr  apply absolute fiducial corrections during this slew?
    @param[in] minEndTime  minimum end time (TAI, MJD sec); 0 means no minimum
    """
    try:
        slewCmd = tccActor.slewCmd

        slewData = computeSlew(
            minEndTime=minEndTime,
            obj=tccActor.obj,
            earth=tccActor.earth,
            inst=tccActor.inst,
            telMod=tccActor.telMod,
            axeLim=tccActor.axeLim,
            tune=tccActor.tune,
        )

        def axisSlewCallback(axisCmd, tccActor=tccActor, slewData=slewData):
            """Callback for axisDevSet.startSlew
            """
            msgCode, msgStr = formatAxisCmdStateAndErr(tccActor.obj)
            tccActor.writeToUsers(msgCode, msgStr, cmd=tccActor.slewCmd)

            if slewCmd.isDone:
                return

            if axisCmd.didFail:
                slewCmd.setState(slewCmd.Failed, textMsg = "Slew failed: %s" % (axisCmd.getMsg(),))
                return

            slewDuration = tccActor.obj.slewEndTime - tcc.base.tai()
            msgStr = "SlewBeg=%0.2f; SlewDuration=%0.2f; SlewNumIter=%s; %s" % \
                (tccActor.obj.slewEndTime, slewDuration, slewData.numIter, moveItemKW(tccActor.obj))
            tccActor.writeToUsers(">", msgStr, cmd=slewCmd)
            tccActor.slewDoneTimer.start(slewDuration, _reportSlewDone, tccActor, slewCmd)

            tccActor.trackTimer.start(0., callFunc=tccActor.updateTracking)
            if doCollimate:
                tccActor.updateCollimation()
                if tccActor.tune.collimateSlewEnd >= 0:
                    collWaitTime = slewDuration - tccActor.tune.collimateSlewEnd
                    if collWaitTime > 1: # gives some time for the main collimation update to finish
                        tccActor.collimateSlewEndTimer.start(collWaitTime, tccActor.updateCollimation)

        # start the slew
        axisCmd = tccActor.axisDevSet.startSlew(slewData.pathList, doAbsRefCorr=doAbsRefCorr)
        axisCmd.addCallback(axisSlewCallback)

    except Exception as e:
        # print some kind of warning or error, then...
        slewCmd.setState(slewCmd.Failed, textMsg="Could not start the slew: %s; initializing all axes" % \
            strFromException(e),)
        tccActor.axisDevSet.init()
        raise
    finally:
        showObjectFields(tccActor, slewCmd)


def _reportSlewDone(tccActor, slewCmd):
    """Report completion of slew (if slew command is still active)

    @param[in] slewCmd  user's track, offset or rotate command

    @warning: do not get slewCmd from tccActor, since it may change; it is crucial
    to have the original slew command.
    """
    if slewCmd.isDone:
        return

    stopSlotSet = []
    for axis in range(tcc.base.NAxes):
        if tccActor.obj.axisCmdState[axis] == tcc.base.AxisState_Slewing:
            tccActor.obj.axisCmdState[axis] = tcc.base.AxisState_Tracking
        elif tccActor.obj.axisCmdState[axis] == tcc.base.AxisState_Halting:
            tccActor.obj.axisCmdState[axis] = tcc.base.AxisState_Halted
            stopSlotSet.append(tccActor.axisDevSet.slotFromIndex(axis))

    msgCode, msgStr = formatAxisCmdStateAndErr(tccActor.obj)
    tccActor.writeToUsers(msgCode, msgStr, cmd=tccActor.slewCmd)

    slewCmd.setState(slewCmd.Done, hubMsg="SlewEnd")

    if stopSlotSet:
        # make sure the halting axes are truly halted;
        # do this after the slew command is reported finished, to avoid confusing output
        # use init instead of stop so the axis controller can be moved again without an init.
        tccActor.axisDevSet.init(slotList=stopSlotSet)
