from __future__ import division, absolute_import

from collections import OrderedDict
from RO.StringUtil import quoteStr
import RO.KeyVariable

__all__ = ["broadcast"]

MsgTypeCodeDict = OrderedDict((
    ("debug", "d"),
    ("information", "i"),
    ("warning", "w"),
    ("error", "e"),
    ("fatal", "f"),
    ("done", ":"),
    ("started", ">"),
    ("superseded", "f"),
))

def broadcast(tccActor, userCmd):
    """Implement the broadcast command

    @param[in] tccActor  tcc actor
    @param[in,out] userCmd  a twistedActor BaseCommand with parseCmd attribute
    """

    msgStr = userCmd.parsedCmd.paramDict["message"].valueList[0]
    msgType = userCmd.parsedCmd.qualDict["type"].valueList[0]
    msgCode = MsgTypeCodeDict[msgType]
    hubMsg = "Broadcast=%s" % (quoteStr(msgStr,))
    if msgCode in msgCode in RO.KeyVariable.DoneTypes:
        # don't report the command as done
        tccActor.writeToUsers(msgCode, hubMsg, userID=userCmd.userID)
    else:
        tccActor.writeToUsers(msgCode, hubMsg, cmd=userCmd)
    if not userCmd.isDone:
        userCmd.setState(userCmd.Done)
