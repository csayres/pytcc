from __future__ import division, absolute_import

__all__ = ["showMount"]

from tcc.msg.formatObjFields import formatPVT

def showMount(tccActor, userCmd):
    """Implement the "show mount" command

    @param[in,out] tccActor  tcc actor
        reads tccActor.obj.actMount
    @param[in,out] userCmd  user command
    """
    azAltStrList = [formatPVT(coord) for coord in tccActor.obj.actMount[0:2]]
    kwStr = 'TelMount=' + ', '.join(azAltStrList)
    tccActor.writeToUsers('i', kwStr, userCmd)
    
    kwStr = 'RotMount=' + formatPVT(tccActor.obj.actMount[2])
    tccActor.writeToUsers('i', kwStr, userCmd)
    userCmd.setState(userCmd.Done)
