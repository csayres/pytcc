from __future__ import division, absolute_import

import os
import StringIO

from coordConv import PVT, rot2D
import numpy
from twistedActor import CommandError

import tcc.base
from tcc.parse import getPVTPairPxPMRadVel, getPVTList, getCoordSys, getRestart
from tcc.msg import formatPVTList
from .checkTargetForSlew import checkTargetForSlew
from .startSlew import startSlew
from .trackStop import trackStop

__all__ = ["track", "loadChebyshevFile"]

_BadCoordSysNameList = ("Instrument", "GImage", "GProbe", "PtCorr", "Rotator")
_BadCoordSysNameLCSet = set(csName.lower() for csName in _BadCoordSysNameList)
_BadCoordSysDescr = ", ".join(_BadCoordSysNameList)
del _BadCoordSysNameList # avoid accidental misuse when _BadCoordSysNameLCSet is wanted

def track(tccActor, userCmd):
    """Implement the track command

    @todo: Handle these qualifiers:
    - ScanVelocity
    - PtErr (raises a not implemented error)
    @todo: for the position check, improve the error and warning reporting; right now it is much too minimal

    @param[in,out] tccActor  tcc actor;
        modifies obj and slewCmd
    @param[in,out] userCmd  track command
    """
    if tccActor.inst.instName in ("", "?"):
        raise CommandError("Cannot track: unknown instrument")
    if not tccActor.inst.instPos:
        raise CommandError("Cannot track: unknown instrument position")

    rotExists = tccActor.inst.hasRotator()
    parsedCmd = userCmd.parsedCmd
    qualDict = parsedCmd.qualDict
    if qualDict["stop"].boolValue:
        return trackStop(tccActor=tccActor, userCmd=userCmd, doObj=True, doRot=True)

    doRestoreObj = False
    doRefSlew = False
    doFindRef = False
    ptErrQual = qualDict["pterror"]
    if ptErrQual.boolValue:
        ptErrKeys = set(val.lower() for val in ptErrQual.valueList)
        if "objslew" in ptErrKeys:
            doRestoreObj = True
        else:
            if "nofindreference" not in ptErrKeys:
                doFindRef = True
            if "norefslew" not in ptErrKeys:
                doRefSlew = True

    # modify a copy of obj; if all goes well then switch to the copy
    if doRestoreObj:
        if tccActor.savedObj is None:
            raise CommandError("no saved object for Track/PtErr=ObjSlew")
        newObj = tcc.base.Obj(tccActor.savedObj)
    else:
        newObj = tcc.base.Obj(tccActor.obj)
    nextTAI = tcc.base.tai() + tccActor.tune.trackInterval + tccActor.tune.trackAdvTime
    zeroPVT = PVT(0, 0, nextTAI)

    doRestart = getRestart(
        qual = qualDict["restart"],
        rotExists = rotExists,
    )
    perfect = qualDict["perfect"].boolValue

    newObj.azWrapPref  = tcc.base.WrapTypeNameDict[qualDict[ "azwrap"].valueList[0].lower()]
    newObj.rotWrapPref = tcc.base.WrapTypeNameDict[qualDict["rotwrap"].valueList[0].lower()]

    if qualDict["refcoefficients"].boolValue:
        tccActor.weath.updateSite(newObj.site)

    isNewObj = False
    isNewArcOff = False
    if qualDict["chebyshev"].boolValue:
        isNewObj = True
        isNewRot = loadChebyshevFile(tccActor=tccActor, userCmd=userCmd, obj=newObj)
    else:
        newUserPos, newUserPxPMRadVel = getPVTPairPxPMRadVel(parsedCmd=parsedCmd, coordName="coordpair", defTAI=nextTAI)
        # default TAI for coordinate system and rotator angle is time specified for coordinates,
        # (which is nextTAI unless the user specified a date)
        if newUserPos:
            newObj.useCheby = False
            newObj.chebyUser1 = tcc.base.ChebyshevPolynomial()
            newObj.chebyUser2 = tcc.base.ChebyshevPolynomial()
            newObj.chebyDist = tcc.base.ChebyshevPolynomial()

            defTAI = newUserPos[0].t
            isNewObj = True
            newObj.userPos = newUserPos
            newObj.userPxPMRadVel = newUserPxPMRadVel
            newObj.mag = qualDict["magnitude"].valueList[0] # defaults to NaN
            if qualDict["name"].boolValue:
                newObj.name = qualDict["name"].valueList[0]
            else:
                newObj.name = ""
        else:
            defTAI = nextTAI

        if qualDict["scanvelocity"].boolValue:
            isNewArcOff = True
            scanVelList = qualDict["scanvelocity"].valueList[:]
            if len(scanVelList) == 3:
                scanTAI = scanVelList[2]
            else:
                scanTAI = defTAI
            for i in range(2):
                newObj.userArcOff[i] = PVT(0, scanVelList[i], scanTAI)

        newCoordSys = getCoordSys(
            param = parsedCmd.paramDict["coordsys"],
        )
        if newCoordSys:
            if newCoordSys.getName().lower() in _BadCoordSysNameLCSet:
                raise CommandError("Invalid coordSys=%r: cannot track any of %s" % (newCoordSys.getName(), _BadCoordSysDescr))
            if not newUserPos:
                raise CommandError("Must specify position if you specify /CoordSys")
            newObj.userSys = newCoordSys

        rotAngVals = qualDict["rotangle"].valueList[:]
        isNewRot = bool(rotAngVals)
        if isNewRot:
            # getPVTList returns (pvtList, rotOmitted), so we want the [0][0] returned value, a single PVT
            newObj.rotUser = getPVTList(qualDict["rotangle"], numAxes=1, defTAI=defTAI)[0][0]

        if qualDict["rottype"].valueList:
            newObj.rotType = tcc.base.RotTypeNameDict[qualDict["rottype"].valueList[0].lower()]
            if not isNewRot:
                if newObj.rotType != tcc.base.RotType_None:
                    raise CommandError("/RotType requires /RotAng")
                isNewRot = True

    if newObj.userSys.getName().lower() == "mount" \
        and newObj.rotType not in (tcc.base.RotType_None, tcc.base.RotType_Mount):
        # rotation type not compatible with az, alt mount coordinates;
        # if user specified rotType or rotAng then reject the command, else warn user and set rotType None
        rotTypeName = tcc.base.RotTypeValDict[newObj.rotType]
        if isNewRot:
            raise CommandError("RotType=%s not compatible with mount az, alt" % (rotTypeName,))
        else:
            newObj.rotType = tcc.base.RotType_None
            tccActor.writeToUsers("w",
                "Text=\"RotType=%s not compatible with mount az, alt; setting to None\"" % (rotTypeName,))

    if isNewRot and not rotExists and newObj.rotType != tcc.base.RotType_None:
        raise CommandError("Cannot rotate; instrument has no rotator")

    # for wrap = None for mount
    if newObj.userSys.getName() == "mount":
        newObj.azWrapPref = tcc.base.WrapType_None
    if newObj.rotType == tcc.base.RotType_Mount:
        newObj.rotWrapPref = tcc.base.WrapType_None

    keepQual = qualDict["keep"]
    doKeepSet = set(val.lower() for val in keepQual.valueList if not val.lower().startswith("no"))
    doNotKeepSet = set(val.lower()[2:] for val in keepQual.valueList if val.lower().startswith("no"))
    conflictingKeepSet = doKeepSet.intersection(doNotKeepSet)
    if conflictingKeepSet:
        raise CommandError("Conflicting /Keep demands: keep and do not keep %s" % \
            ", ".join(sorted(conflictingKeepSet)))
    if isNewObj:
        if "arcoffset" not in doKeepSet and not isNewArcOff:
            newObj.userArcOff[:] = [zeroPVT]*2
        if "boresight" not in doKeepSet:
            newObj.objInstXY[:] = [zeroPVT]*2
        if "gcorrection" not in doKeepSet:
            newObj.guideOff[:] = [zeroPVT]*3
    else:
        if "arcoffset" in doNotKeepSet and not isNewArcOff:
            newObj.userArcOff[:] = [zeroPVT]*2
        if "boresight" in doNotKeepSet:
            newObj.objInstXY[:] = [zeroPVT]*2
        if "gcorrection" in doNotKeepSet:
            newObj.guideOff[:] = [zeroPVT]*3
    if "calibration" in doNotKeepSet:
        newObj.calibOff[:] = [zeroPVT]*3
    elif "calibration" not in doKeepSet:
        # just zero the position, not the velocity
        newObj.calibOff[:] = [PVT(pvt.pos, 0, nextTAI) for pvt in newObj.calibOff]

    checkTargetForSlew(
        tccActor = tccActor,
        userCmd = userCmd,
        newObj = newObj,
        isNewObj = isNewObj,
        isNewRot = isNewRot,
        doWrap = True,
        doRestart = doRestart,
        perfect = perfect,
        tai = nextTAI,
    )

    if doRefSlew:
        tccActor.savedObj = tcc.base.Obj(newObj)
    else:
        tccActor.savedObj = None

    if doFindRef:
        if qualDict["magrange"].boolValue:
            magRange = qualDict["magrange"].valueList[:]
        elif numpy.isfinite(newObj.mag):
            magRange = (newObj.mag - 2, newObj.mag + 2)
        else:
            magRange = (3, 7)
        refStarData = tccActor.posRefCatalog.findStar(
            tccActor = tccActor,
            obj = newObj,
            magRange = magRange,
            tai = nextTAI,
            userCmd = userCmd,
        )
        newObj.name = "pt ref %s" % (refStarData.name,)
        newObj.userPos = refStarData.pvtPair
        newObj.userPxPMRadVel = refStarData.pxPMRadVel
        newObj.userSys = tccActor.posRefCatalog.coordSys.clone() # clone because need a pointer
        newObj.userMag = refStarData.mag

        # report position
        refPosMsg = "ptRefStar=%0.6f, %0.6f, %0.5f, %0.2f, %0.2f, %0.1f, %s, %0.1f, %0.2f; ptRefPos=%s" % \
            (refStarData.pvtPair[0].pos, refStarData.pvtPair[1].pos,
            refStarData.pxPMRadVel.parallax,
            refStarData.pxPMRadVel.equatPM, refStarData.pxPMRadVel.polarPM, refStarData.pxPMRadVel.radVel,
            tccActor.posRefCatalog.coordSys.getName(), tccActor.posRefCatalog.coordSys.getDate(), refStarData.mag,
            formatPVTList(refStarData.pvtPair))
        tccActor.writeToUsers(msgCode="i", msgStr=refPosMsg, cmd=userCmd)
        if not doRefSlew:
            userCmd.setState(userCmd.Done)
            return

    if doRefSlew:
        if tccActor.inst.ptErrProbe == 0:
            # no pointing error probe; center reference on instrument
            for axis in range(2):
                newObj.objInstXY[axis] = PVT(0, 0, nextTAI)
        else:
            try:
                # subtract 1 because probes are numbered 1-N
                ptErrProbe = tccActor.inst.gProbe[tccActor.inst.ptErrProbe - 1]
            except IndexError:
                raise CommandError("No data for PtErr probe %s; %s has %s guide probes" % \
                    (tccActor.inst.ptErrProbe, tccActor.inst.instName, len(tccActor.inst.gProbe)))

            if not ptErrProbe.exists:
                raise CommandError("PtErr probe %s is disabled" % (tccActor.inst.ptErrProbe,))

            gp_rot_xy = rot2D(-ptErrProbe.rot_gp_xy[0], -ptErrProbe.rot_gp_xy[1], -ptErrProbe.rot_gim_ang)
            gp_rot_inst_xy = rot2D(gp_rot_xy[0], gp_rot_xy[1], tccActor.inst.rot_inst_ang)
            for axis in range(2):
                newObj.objInstXY[axis] = PVT(gp_rot_inst_xy[axis] + tccActor.inst.rot_inst_xy[axis], 0, nextTAI)

        # use guide star wavelength for two reasons:
        # - position reference stars are normal stars, even if the current target has a strange color
        # - the guider may have a different filter than the instrument
        # note that guide star wavelength is used even if the current target is the position reference star,
        # because of the guider filter issue and because the behavior is easier to predict and explain
        # If this becomes a problem, add noGSWavelength as an option for /ptError
        newObj.site = newObj.gsSite

        # make sure reference star is available
        checkTargetForSlew(
            tccActor = tccActor,
            userCmd = userCmd,
            newObj = newObj,
            isNewObj = isNewObj,
            isNewRot = isNewRot,
            doWrap = False, # already wrapped for target
            doRestart = doRestart,
            perfect = True, # no point slewing unless all axes can make it
            tai = nextTAI,
        )

    if userCmd.isDone:
        # I don't think this can happen, but just in case...
        return

    startSlew(
        tccActor=tccActor,
        obj=newObj,
        slewCmd=userCmd,
        doAbsRefCorr=qualDict["absrefcorrect"].boolValue,
        doCollimate=qualDict["collimate"].boolValue,
    )


_ChebyDeprecatedKeyDict = dict(
    usecheby = "deprecated",
    wavelen = "specify with Set Wavelength",
    numchebycoeffs = "deprecated",
    coordsys = "use 'userSys name date' instead",
    az_wrappref = "deprecated?",
    chebybegendtime = "times now specified in cheby<user1/user2/dist>",
    chebycoeffsuser1 = "now use chebyuser1",
    chebycoeffsuser2 = "now use chebyuser2",
    chebycoeffsdist = "now user chebydist",
)
_ChebyRequiredKeySet = set(("chebyuser1", "chebyuser2", "chebydist"))
_ChebyValidKeySet = _ChebyRequiredKeySet \
    | set(("name", "mag", "usersys", "rottype", "rotuser"))

def loadChebyshevFile(tccActor, userCmd, obj):
    """Load chebyshev coefficients from a file

    @param[in] tccActor  tcc actor; used for writeToOneUser
    @param[in] userCmd  user command
    @param[in,out] obj  object block into which to load chebyshev data
    @return isNewRot
    """
    dataDir = tcc.base.getDataDir()
    if len(userCmd.parsedCmd.qualDict["chebyshev"].valueList) < 1:
        raise CommandError("Must specify Chebyshev file")
    fileName = userCmd.parsedCmd.qualDict["chebyshev"].valueList[0]
    filePath = os.path.join(dataDir, fileName)
    if not os.path.splitext(filePath)[1]:
        filePath += ".dat"
    cleanFile = StringIO.StringIO()
    neededKeys = set()
    obj.useCheby = True
    obj.chebyUser1 = tcc.base.ChebyshevPolynomial()
    obj.chebyUser2 = tcc.base.ChebyshevPolynomial()
    obj.chebyDist = tcc.base.ChebyshevPolynomial()
    obj.rotUser.invalidate()
    obj.rotType = tcc.base.RotType_None
    newRotType = False
    newRotUser = False
    with file(filePath, "rU") as chebFile:
        for line in chebFile:
            cleanLine = line.strip()
            if cleanLine and not cleanLine[0] in ("!", "#"):
                key = cleanLine.split()[0]
                keyLow = key.lower()
                warningMsg = _ChebyDeprecatedKeyDict.get(keyLow)
                if warningMsg is not None:
                     tccActor.writeToOneUser("w", 'Text="Ignoring %s; %s"' % (key, warningMsg), cmd=userCmd)
                     continue
                if keyLow not in _ChebyValidKeySet:
                    raise CommandError("Error in Chebyshev file %r: invalid key %s" % (filePath, key))
                if keyLow in _ChebyRequiredKeySet:
                    neededKeys.add(keyLow)
                if keyLow == "rottype":
                    newRotType = True
                if keyLow == "rotuser":
                    newRotUser = True
                cleanFile.write(cleanLine + '\n')

    missingKeySet = _ChebyRequiredKeySet - neededKeys
    if missingKeySet:
        missingKeyStr = ", ".join(sorted(missingKeySet))
        raise CommandError("Error in Chebyshev file %r: missing key(s): %s" % (filePath, missingKeyStr))
    if newRotUser != newRotType:
        raise CommandError("Error in Chebyshev file %r: must specify both or neither of rotUser and rotType" % (filePath,))
    isNewRot = newRotType

    cleanFile.seek(0)
    try:
        obj.load(cleanFile)
        tccActor.writeToUsers('i', "ChebyFile=%s"%filePath, userCmd)
    except Exception as e:
        raise CommandError("Error in Chebyshev file %r: %s" % (filePath, str(e)))

    minTAI = max(obj.chebyUser1.getMinX(), obj.chebyUser2.getMinX(), obj.chebyDist.getMinX())
    maxTAI = min(obj.chebyUser1.getMaxX(), obj.chebyUser2.getMaxX(), obj.chebyDist.getMaxX())
    currTAI = tcc.base.tai()
    if minTAI > currTAI:
        raise CommandError("At least one Chebyshev polynomial does not start for another %s second" % (minTAI - currTAI,))
    if maxTAI < currTAI:
        raise CommandError("At least one Chebyshev polynomial ended %s seconds ago" % (currTAI - maxTAI,))

    return isNewRot
