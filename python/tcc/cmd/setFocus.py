from __future__ import division, absolute_import

from .showFocus import showFocus

__all__ = ["setFocus"]

def setFocus(tccActor, userCmd):
    """Set the focus offset for the secondary mirror and trigger a collimation update

    If not offset is specified, simply trigger a collimation update

    @param[in,out] tccActor  tcc actor
        reads tccActor.inst
        writes tccActor.inst and triggers a collimation update
    @param[in,out] userCmd  user command
    """
    valueList = userCmd.parsedCmd.paramDict["focus"].valueList[0].valueList
    if valueList is not None:
        value = valueList[0]
        if userCmd.parsedCmd.qualDict['incremental'].boolValue:
            tccActor.inst.secUserFocus += value
        else:
            tccActor.inst.secUserFocus = value

    showFocus(tccActor, userCmd, setDone=False)
    tccActor.updateCollimation(cmd=userCmd)
