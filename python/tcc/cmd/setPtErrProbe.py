from __future__ import division, absolute_import

from .showPtErrProbe import showPtErrProbe

__all__ = ["setPtErrProbe"]

def setPtErrProbe(tccActor, userCmd, setDone=True):
    """Implement the Set PtErrProbe command

    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  a twistedActor BaseCommand with parseCmd attribute
    @param[in,out] isDone  set userCmd done when finished?
    """
    valueList = userCmd.parsedCmd.paramDict["pterrprobe"].valueList[0].valueList
    if valueList:
        value = valueList[0]
        maxValue = len(tccActor.inst.gProbe)
        if not (0 <= value <= maxValue):
            userCmd.setState(userCmd.Failed,
                textMsg="Cannot set PtErrProbe to %s; must be in range [0, %s] for this instrument" % (value, maxValue),
            )
            return

        tccActor.inst.ptErrProbe = value

    showPtErrProbe(tccActor, userCmd, setDone=True)
