from __future__ import division, absolute_import

__all__ = ["showStInterval"]

def showStInterval(tccActor, userCmd, setDone=True):
    """Implement the "show stinterval" command

    @param[in] tccActor  tcc actor
        reads tccActor.tune.statusInterval
    @param[in,out] userCmd  user command
    @param[in] setDone  set userCmd done when finished? (ignored if userCmd is already done)
    """   
    kwStr = "StInterval=%0.1f, %0.1f, %0.1f" % tuple(tccActor.tune.statusInterval)
    tccActor.writeToUsers('i', kwStr, userCmd)
    if setDone and not userCmd.isDone:
        userCmd.setState(userCmd.Done)
    
