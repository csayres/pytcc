from __future__ import division, absolute_import

import numpy
from twistedActor import CommandError

from tcc.base import Inst
from .startSlew import startSlew

__all__ = ["setImCenter"]

def setImCenter(tccActor, userCmd):
    """Implement the Set ImCenter command

    @param[in,out] tccActor  tcc actor
        writes tccActor.inst
    @param[in,out] userCmd  user command
    """  
    newImCtrPix = numpy.array(userCmd.parsedCmd.paramDict["imcenter"].valueList[0].valueList, dtype=float)
    dImCtrPix = [None, None]
    dImCtrDeg = [None, None]

    newInst = Inst(tccActor.inst)

    inst = tccActor.inst
    if numpy.any(newImCtrPix < newInst.iim_minXY) or numpy.any(newImCtrPix > newInst.iim_maxXY):
        raise CommandError("Desired image center %s out of range min=%s, max=%s" % (
            newImCtrPix, newInst.iim_minXY, newInst.iim_maxXY))

    dImCtrPix = newImCtrPix - newInst.iim_ctr
    dImCtrDeg = dImCtrPix / newInst.iim_scale

    newInst.rot_inst_xy[:] = newInst.rot_inst_xy - dImCtrDeg
    newInst.iim_ctr[:] = newImCtrPix

    tccActor.inst = newInst

    kwStr="IImCtr=%0.2f, %0.2f; RotInstXYAng=%0.6f, %0.6f, %0.6f" % (
        tccActor.inst.iim_ctr[0],
        tccActor.inst.iim_ctr[1],
        inst.rot_inst_xy[0],
        inst.rot_inst_xy[1],
        inst.rot_inst_ang,
    )
    tccActor.writeToUsers('i', kwStr, userCmd)

    startSlew(
        tccActor=tccActor,
        obj=tccActor.obj,
        slewCmd=userCmd,
        doAbsRefCorr=False,
        doCollimate=False,
    )
