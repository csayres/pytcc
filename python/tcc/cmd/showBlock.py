from __future__ import division, absolute_import
"""
Displays information in the global database. By default, it displays to the screen, 
but you may send output to a file using /Output
"""
import StringIO
from RO.StringUtil import quoteStr

__all__ = ["showBlock"]

blocks = ("AxeLim", "Earth",
        "Inst", "Obj", "TelMod", "Tune", "Weath")
attrs = ("axeLim", "earth", "inst", "obj", "telMod", "tune", "weath")
blockAttrDict = dict(zip(blocks, attrs))

def showBlock(tccActor, userCmd):   
    """ @param[in,out] tccActor  tcc actor
        @param[in,out] userCmd  mirror command
    """
    gotBlock = userCmd.parsedCmd.paramDict["blockname"].valueList[0].keyword
    currBlock = getattr(tccActor, blockAttrDict[gotBlock])

    if userCmd.parsedCmd.qualDict['output'].boolValue:
        filePath = userCmd.parsedCmd.qualDict['output'].valueList[0]
        with open(filePath, 'w') as f:
            currBlock.dump(f)
    else:
        # dump from block into a StringIO object as a temporary file
        f = StringIO.StringIO()
        currBlock.dump(f)
        listOfLines = f.getvalue().split("\n")
        for line in listOfLines:
            if not line:
                continue
            kwForm = 'Text=' + quoteStr(line)
            tccActor.writeToOneUser('i', kwForm, cmd=userCmd)
    userCmd.setState(userCmd.Done) 
