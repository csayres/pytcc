from __future__ import division, absolute_import

from .showStInterval import showStInterval
    
__all__ = ["setStInterval"]

def setStInterval(tccActor, userCmd):
    """ @param[in,out] tccActor  tcc actor
        @param[in,out] userCmd  a twistedActor BaseCommand with parseCmd attribute
        
        writes to tccActor.tune block
    """
    valueList = userCmd.parsedCmd.paramDict["stinterval"].valueList[0].valueList
    if valueList is not None:
        assert len(valueList) == 3
        tccActor.tune.statusInterval[:] = valueList
    showStInterval(tccActor, userCmd, setDone=True)
