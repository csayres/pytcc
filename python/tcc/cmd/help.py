from __future__ import division, absolute_import

from RO.StringUtil import quoteStr
from RO.Alg import MatchList
from twistedActor import CommandError

__all = ["help"]

_CmdMatchList = None

def getCmdMatchList(cmdParser):
    """Return an RO.Alg.MatchList with all command names

    Uses an internal cache, so only computes once
    """
    global _CmdMatchList
    if _CmdMatchList is None:
        cmdNameList = cmdParser.cmdDefDict.keys()
        _CmdMatchList = MatchList(
            valueList = cmdNameList,
            abbrevOK = True,
            ignoreCase = True,
        )
    return _CmdMatchList

def getSubCmdMatchList(cmdDef):
    """Return a sub command matchList and dict of sub-command name: sub-command definition
    """
    try:
        subCmdList = cmdDef.subCmdList
    except AttributeError:
        raise CommandError("The command %r has no sub-commands" % (cmdDef.name,))
    subCmdDict = dict((subcmd.subCommandName.lower(), subcmd) for subcmd in subCmdList)
    return MatchList(
            valueList = subCmdDict.keys(),
            abbrevOK = True,
            ignoreCase = True,
        ), subCmdDict

def help(tccActor, cmd):
    """Print help

    Use help cmdName for full info on the specified command
    Use help/full for full info on all commands
    """
    cmdName = None
    subCmdName = None
    subCmdDef = None
    if cmd.parsedCmd.paramDict['command'].valueList:
        cmdName = cmd.parsedCmd.paramDict['command'].valueList[0]
        cmdMatchList = getCmdMatchList(tccActor.cmdParser)
        try:
            fullCmdName = cmdMatchList.getUniqueMatch(cmdName)
        except ValueError as e:
            raise CommandError(e)
        cmdDef = tccActor.cmdParser.cmdDefDict[fullCmdName]
    if cmd.parsedCmd.paramDict['subcommand'].valueList:
        subCmdName = cmd.parsedCmd.paramDict['subcommand'].valueList[0]
        subCmdMatchList, subCmdDict = getSubCmdMatchList(cmdDef)

        # discover the index in subCommandList for which to print help
        try:
            fullSubCmdName = subCmdMatchList.getUniqueMatch(subCmdName)
        except ValueError as e:
            raise CommandError(e)
        else:
            subCmdDef = subCmdDict[fullSubCmdName]
    if cmdName is not None:
        # print full help for one command
        if subCmdDef is not None:
            dataList = subCmdDef.getFullHelp()
        else:
            dataList = cmdDef.getFullHelp()
        for outStr in dataList:
            for line in outStr.split("\n"):
                tccActor.writeToOneUser("i", "Text=%s" % (quoteStr(line),), cmd=cmd)
        return

    # print help for all commands
    cmdList = sorted(tccActor.cmdParser.cmdDefDict.keys())
    isFull = cmd.parsedCmd.qualDict["full"].boolValue
    if isFull:
        # print full help for all commands
        for cmdName in cmdList:
            cmdDef = tccActor.cmdParser.cmdDefDict[cmdName]
            dataList = cmdDef.getFullHelp()
            for outStr in dataList:
                # split on newlines
                for line in outStr.split("\n"):
                    tccActor.writeToOneUser("i", "Text=%s" % (quoteStr(line),), cmd=cmd)
            tccActor.writeToOneUser("i", "", cmd=cmd)
    else:
        # print brief help for all commands
        for cmdName in cmdList:
            cmdDef = tccActor.cmdParser.cmdDefDict[cmdName]
            dataList = cmdDef.getBriefHelp()
            for outStr in dataList:
                for line in outStr.split("\n"):
                    tccActor.writeToOneUser("i", "Text=%s" % (quoteStr(line),), cmd=cmd)
            tccActor.writeToOneUser("i", "", cmd=cmd)
