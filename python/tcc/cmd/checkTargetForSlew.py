from __future__ import division, absolute_import

import tcc.base
from tcc.axis import computeObj, checkAxesForSlew
from tcc.msg import formatAxisErr

__all__ = ["checkTargetForSlew"]

MaxVelFrac = 0.80 # fraction of maximum velocity allowed for a target

def checkTargetForSlew(
    tccActor,
    userCmd,
    newObj,
    isNewObj,
    isNewRot,
    doWrap,
    doRestart,
    perfect,
    tai,
):
    """Check a potential target for slewing or offsetting

    @param[in] tccActor  tcc actor
    @param[in,out] userCmd  user command (track, offset or rotate); state is set to Failed if the command is rejected
    @param[in,out] newObj  proposed new object block, updated at tai date
    @param[in] isNewObj  did the user request a new object position?
    @param[in] isNewRot  did the user request a new rotator position? (ignored if instrument has no rotator)
    @param[in] doWrap  apply wrap preferences (True for slewing to a new object, False for offsetting)
    @param[in] doRestart  restart each axis (a sequence of 3 bools)
    @param[in] perfect  if True then all axes must make it or the move is rejected
    @param[in] tai  TAI date at which to update the obj block
    """
    computeObj(
        newObj,
        doWrap=doWrap,
        doRestart=doRestart,
        reportLim=True,
        earth=tccActor.earth,
        inst=tccActor.inst,
        telMod=tccActor.telMod,
        axeLim=tccActor.axeLim,
        tai=tai,
    )

    # check velocity limits
    for axis in range(3):
        if newObj.axisErrCode[axis] == tcc.base.AxisErr_OK \
            and abs(newObj.targetMount[axis].vel) > tccActor.axeLim.vel[axis] * MaxVelFrac:
                newObj.axisErrCode[axis] = tcc.base.AxisErr_MaxVel

    doAxis, badAxis, sigBad = checkAxesForSlew(
        desObj=newObj,
        perfect=perfect,
        doRestart=doRestart,
        isHalted=[not mt.isfinite() for mt in tccActor.obj.targetMount],
        isNewObj=isNewObj,
        isNewRot=isNewRot,
        rotExists=tccActor.inst.hasRotator(),
        nullSysFlag=1,
    )

    if sigBad:
        # a significant axis cannot make it; reject the command and quit
        hubMsg = formatAxisErr(
            textMsg = "one or more critical axes cannot make it",
            badAxisKeyword = "AxisNoSlew",
            badAxisList = badAxis,
            errCodeKeyword = "RejectedAxisErrCode",
            errCodeList = newObj.axisErrCode,
        )
        userCmd.setState(userCmd.Failed, hubMsg=hubMsg)
        return

    if any(badAxis):
        # an insignificant axis cannot make it; warn users
        msgStr = formatAxisErr(
            textMsg = "one or more axes halting or left halted",
            badAxisKeyword = "AxisNoSlew",
            badAxisList = badAxis,
            errCodeKeyword = "AxisErrCode",
            errCodeList = newObj.axisErrCode,
        )
        tccActor.writeToUsers(msgCode="w", msgStr=msgStr, cmd=userCmd)
