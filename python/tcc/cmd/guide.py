from __future__ import division, absolute_import

# from astropy.time import Time

from twistedActor import expandCommand

import coordConv

import tcc.base
from .showFocus import showFocus
from .showScaleFactor import showScaleFactor
from .checkTargetForSlew import checkTargetForSlew
from .startSlew import startSlew

__all__ = ["guide"]


def setScaleFactor(tccActor, scaleMult, userCmd):
    """Implement Set ScaleFactor

    @param[in,out] tccActor  tcc actor
        writes tccActor.inst.scaleFac
    @param[in,out] scaleMult
        multiplicative scale factor to apply
    @param[in,out] userCmd  a twistedActor BaseCommand with parseCmd attribute
    """
    maxScaleFactor = tccActor.inst.maxScaleFac
    minScaleFactor = 1.0 / maxScaleFactor
    scaleFac = tccActor.inst.scaleFac * scaleMult
    if not (minScaleFactor <= scaleFac <= maxScaleFactor):
        userCmd.setState(userCmd.Failed,
            textMsg="Scale factor %0.6f invalid; must be in range [%0.6f, %0.6f]" % (scaleFac, minScaleFactor, maxScaleFactor),
        )
        return

    tccActor.inst.scaleFac = scaleFac
    showScaleFactor(tccActor, userCmd, setDone=True)

def setFocus(tccActor, focusOffset, userCmd):
    """Set the focus offset for the secondary mirror and trigger a collimation update

    If not offset is specified, simply trigger a collimation update

    @param[in,out] tccActor  tcc actor
        reads tccActor.inst
        writes tccActor.inst and triggers a collimation update
    @param[in] focusOffset
        amount to offset M2
    @param[in,out] userCmd  user command
    """

    tccActor.inst.secUserFocus += focusOffset
    showFocus(tccActor, userCmd, setDone=False)
    tccActor.updateCollimation(cmd=userCmd)

def axisOffset(tccActor, dRa, dDec, dRot, tai, userCmd):
    """dRA/dDec in arcseconds, dRot in degrees
    tai is time at which offsets were measured, if none use the time now

    # tai only relevant for az alt guide offset
    """
    if tai is None:
        tai = tcc.base.tai()
    newObj = tcc.base.Obj(tccActor.obj)

    arcOffsetField = newObj.userArcOff
    offPVTs = [
        coordConv.PVT(dRa, 0, tai),
        coordConv.PVT(dDec, 0, tai),
    ]
    for ii, offPVT in enumerate(offPVTs):
        pvt = arcOffsetField[ii]
        newPVT = pvt.copy(tai)
        newPVT.pos += offPVT.pos
        arcOffsetField[ii] = newPVT

    guideOffsetField = newObj.guideOff
    rotPVT = guideOffsetField[2]
    newPVT = rotPVT.copy(tai)
    newPVT.pos += coordConv.PVT(dRot, 0, tai).pos
    guideOffsetField[2] = newPVT

    currTAI = tcc.base.tai()
    checkTargetForSlew(
        tccActor=tccActor,
        userCmd=userCmd,
        newObj=newObj,
        isNewObj= True,
        isNewRot= True,
        doWrap=False,
        doRestart=[False,False,False],
        perfect=False,
        tai=currTAI, # use tai now rather than tai at time of measurement?
    )
    if userCmd.isDone:
        # slew didn't check out.
        return

    # computed offset, use a normal slew
    startSlew(
        tccActor=tccActor,
        obj=newObj,
        slewCmd=userCmd,
        doAbsRefCorr=False,
        doCollimate=False,
    )


def guide(tccActor, userCmd):
    """Implement the "show scalefactor" command

    @param[in] tccActor  tcc actor
        read tccActor.inst.maxScaleFac
    @param[in,out] userCmd  user command
    """

    print("tcc guide command")

    paramDict = userCmd.parsedCmd.paramDict
    mandatoryParams = ["dra", "ddec", "drot", "dfocus", "dscale"]
    for key in mandatoryParams:
        if not paramDict[key].valueList:
            userCmd.setState(userCmd.Failed, "failed to receive %s arg"%key)
            return

    dRA = paramDict['dra'].valueList[0] # arcsec
    dDec = paramDict['ddec'].valueList[0] #arcsec
    dRot = paramDict['drot'].valueList[0] #degrees
    dFocus = paramDict['dfocus'].valueList[0] #microns
    dScale = paramDict['dscale'].valueList[0] #mult factor
    # expStart is a datetime string via isoformat()
    # eg '2018-05-24T18:29:42.547522'

    # if paramDict['expstart'].valueList:
    #     expStart = paramDict['expstart'].valueList[0]
    #     expTAI = Time(expStart, scale="tai").mjd*24*60*60 # get tai mjd seconds
    #     expTime = paramDict['exptime'].valueList[0] #seconds
    #     # taiMeas only makes sense if commanding offset in a rotating coordinate system wrt north?
    #     taiMeas = expTAI + expTime/2.0 # midpoint of guide exposure (time of offset measurement)

    taiNow = tcc.base.tai() # for now just use the current tai

    subcmdList = []
    if sum([dRA, dDec, dRot]) != 0:
        axisCmd = expandCommand()
        axisOffset(tccActor, dRA, dDec, dRot, taiNow, axisCmd)
        subcmdList.append(axisCmd)
    if dScale != 0:
        scaleCmd = expandCommand()
        setScaleFactor(tccActor, dScale, scaleCmd)
        subcmdList.append(scaleCmd)
    if dFocus != 0:
        focusCmd = expandCommand()
        setFocus(tccActor, dFocus, focusCmd)
        subcmdList.append(focusCmd)

    userCmd.linkCommands(subcmdList)
