from __future__ import division, absolute_import
"""set weather command
"""
import itertools

import tcc.base
from .showWeather import showWeather

__all__ = ["setWeather"]


# dict of parameter name (lowercase): list of inst block attribute names
defAttrDict = {
    "airtemp": ["airTemp"],
    "sectrusstemp": ["secTrussTemp"],
    "primf_bftemp": ["primFrontTemp", "primBackFrontTemp"],
    "secf_bftemp": ["secFrontTemp", "secBackFrontTemp"],
    "pressure": ["press"],
    "humidity": ["humid"],
    "tlapse": ["tempLapseRate"],
    "wspeed": ["windSpeed"],
    "wdirection": ["windDir"],
}

def setWeather(tccActor, userCmd):
    """
    @param[in,out] tccActor  tcc actor
        reads tccActor.weath
        writes tccActor.weath and triggers a showWeather
    @param[in,out] userCmd  user command
    @param[in,out] isDone  set command done?
    """
    #get parameters = expect a valueList of keyword objects, each of which has a valueList
    try:
        kwList = userCmd.parsedCmd.paramDict["items"].valueList
    except IndexError:
        # no items were specified
        kwList = []
    for kw in kwList:
        attrs = defAttrDict[kw.keyword.lower()]
        for attr, value in itertools.izip(attrs, kw.valueList):
            setattr(tccActor.weath, attr, value)

    # set weath.timestamp using tcc.base.tai() only if something was set
    if len(kwList) > 0:
        tccActor.weath.timestamp = tcc.base.tai()

    # sort out qualifiers:
    # update collimation correction:
    showWeather(tccActor, userCmd, setDone=False)
    if userCmd.parsedCmd.qualDict['refcoefficients'].boolValue:
        tccActor.weath.updateSite(tccActor.obj.site)
        tccActor.weath.updateSite(tccActor.obj.gsSite)
    if userCmd.parsedCmd.qualDict['collimate'].boolValue:
        # update collimation anonymously so the user need not wait
        tccActor.updateCollimation()
    userCmd.setState(userCmd.Done)
