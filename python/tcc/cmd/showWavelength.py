from __future__ import division, absolute_import

__all__ = ["showWavelength"]

def showWavelength(tccActor, userCmd, setDone=True):
    """Implement the "show wavelength" command
    
    @param[in,out] tccActor  tcc actor
        reads tccActor.obj.site
    @param[in,out] userCmd  user command
    @param[in] setDone  set userCmd done when finished? (ignored if userCmd is already done)
    """    
    badWavelenList = []
    objWavelen = tccActor.obj.site.wavelen
    gsWavelen = tccActor.obj.gsSite.wavelen
    if objWavelen <= 0:
        badWavelenList.append("object")
    if gsWavelen <= 0:
        badWavelenList.append("guide star")

    msgStr = "ObjWavelength=%0.1f; GSWavelength=%0.1f" % (objWavelen, gsWavelen)
    if badWavelenList:
        msgCode = "w"
        msgStr += "; Text=\"%s wavelength(s) invalid (<=0)\"" % "and ".join(badWavelenList)
    else:
        msgCode = "i"

    tccActor.writeToUsers(msgCode, msgStr, userCmd)
    if setDone and not userCmd.isDone:
        userCmd.setState(userCmd.Done)
