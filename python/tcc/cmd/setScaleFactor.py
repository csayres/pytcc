from __future__ import division, absolute_import

from .showScaleFactor import showScaleFactor

__all__ = ["setScaleFactor"]

def setScaleFactor(tccActor, userCmd):
    """Implement Set ScaleFactor

    @param[in,out] tccActor  tcc actor
        writes tccActor.inst.scaleFac
    @param[in,out] userCmd  a twistedActor BaseCommand with parseCmd attribute
    """
    valueList = userCmd.parsedCmd.paramDict["scalefactor"].valueList[0].valueList
    if valueList:
        scaleFac = valueList[0]

        maxScaleFactor = tccActor.inst.maxScaleFac
        minScaleFactor = 1.0 / maxScaleFactor

        if userCmd.parsedCmd.qualDict['multiplicative'].boolValue:
            scaleFac *= tccActor.inst.scaleFac
        if not (minScaleFactor <= scaleFac <= maxScaleFactor):
            userCmd.setState(userCmd.Failed,
                textMsg="Scale factor %0.6f invalid; must be in range [%0.6f, %0.6f]" % (scaleFac, minScaleFactor, maxScaleFactor),
            )
            return

        tccActor.inst.scaleFac = scaleFac

    showScaleFactor(tccActor, userCmd, setDone=True)

