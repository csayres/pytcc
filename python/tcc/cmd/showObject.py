from __future__ import division, absolute_import

from tcc.msg import writerFromObjBlock, computedFields, objFieldKWDict

__all__ = ["showObject"]

def showObject(tccActor, userCmd):
    """!Implement the "show object" command

    @param[in,out] tccActor  tcc actor
        reads tccActor.obj
    @param[in,out] userCmd  user command; if showFull is None then parsedCmd attribute must be set
    """
    if userCmd.parsedCmd.qualDict['full'].boolValue:
        # show all
        showObjectFields(tccActor, userCmd, showFull=True)
    else:
        # show all (except computed)
        showObjectFields(tccActor, userCmd, showFull=False)
    userCmd.setState(userCmd.Done)

def showObjectFields(tccActor, userCmd, showFull=None):
    """!Write fields from the object block

    @param[in,out] tccActor  tcc actor
        reads tccActor.obj
    @param[in,out] userCmd  user command that this output should be associated with
    @param[in] showFull:
        If None, show only changed fields (and others that may be grouped with a changed field)
        If False, show user-set values
        If True, show all parameters
    """
    if showFull is None:
        # show only updated keywords
        updatedKWs = writerFromObjBlock.updateBlock(tccActor.obj)
        writerFromObjBlock.writeKWs(tccActor.writeToUsers, userCmd, updatedKWs)
    elif showFull:
        # show all keywords
        writerFromObjBlock.writeKWs(tccActor.writeToUsers, userCmd)
    else:
        # show all keywords except computed
        # list cast necessary because writeKWs checks is sequence
        # and apparanetly a set is not a sequence
        showKWs = list(set(objFieldKWDict.keys()) - computedFields)
        writerFromObjBlock.writeKWs(tccActor.writeToUsers, userCmd, showKWs)

