from __future__ import division, absolute_import

import coordConv

from .showWavelength import showWavelength
    
__all__ = ["setWavelength"]

def setWavelength(tccActor, userCmd):
    kwList = userCmd.parsedCmd.paramDict["items"].valueList
    # use a copy in case something goes wrong
    for kw in kwList:
        if kw.keyword == 'Obj':
            newSite = coordConv.Site(tccActor.obj.site)
            wavelen = kw.valueList[0]
            if wavelen <= 0:
                raise RuntimeError("Invalid object wavelength=%r" % (wavelen,))
            else:
                # set wavelength and update refraction coefficients
                newSite.wavelen = wavelen
                tccActor.weath.updateSite(newSite)
            tccActor.obj.site = newSite
        elif kw.keyword == "GStars":
            newSite = coordConv.Site(tccActor.obj.gsSite)
            wavelen = kw.valueList[0]
            if wavelen <= 0:
                raise RuntimeError("Invalid guide star wavelength=%r" % (wavelen,))
            else:
                # set wavelength and update refraction coefficients
                newSite.wavelen = wavelen
                tccActor.weath.updateSite(newSite)
            tccActor.obj.gsSite = newSite
        else:
            raise RuntimeError("Bug! Unknown keyword %s" % (kw.keyword,))
    showWavelength(tccActor, userCmd, setDone=True)
