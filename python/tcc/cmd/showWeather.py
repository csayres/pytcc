from __future__ import division, absolute_import

__all__ = ["showWeather"]

def showWeather(tccActor, userCmd, setDone = True):
    """Implement the "show weather" command

    @param[in,out] tccActor  tcc actor
        reads tccActor.weath
    @param[in,out] userCmd  user command
    @param[in,out] isDone  set command done?
    """
    allKwStr = []
    allKwStr.append('AirTemp=%.2f' % (tccActor.weath.airTemp,))
    allKwStr.append('SecTrussTemp=%.2f' % (tccActor.weath.secTrussTemp,))
    allKwStr.append('PrimF_BFTemp=%.2f,%.2f' % \
        (tccActor.weath.primFrontTemp, tccActor.weath.primBackFrontTemp,))
    allKwStr.append('SecF_BFTemp=%.2f,%.2f' % \
        (tccActor.weath.secFrontTemp, tccActor.weath.secBackFrontTemp,))
    allKwStr.append('Pressure=%.1f' % (tccActor.weath.press,))
    allKwStr.append('Humidity=%.2f' % (tccActor.weath.humid,))
    allKwStr.append('TLapse=%.2f' % (tccActor.weath.tempLapseRate,))
    allKwStr.append('WindSpeed=%.1f' % (tccActor.weath.windSpeed,))
    allKwStr.append('WindDir=%.1f' % (tccActor.weath.windDir,))
    # note documentation says TAI kw is output, showweather.for
    # outputs kw TimeStamp.
    allKwStr.append('TimeStamp=%.2f' % (tccActor.weath.timestamp,))
    for kwStr in allKwStr:
        tccActor.writeToUsers('i', kwStr.strip(), cmd=userCmd)
    if setDone:
        userCmd.setState(userCmd.Done)


