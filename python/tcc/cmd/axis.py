from __future__ import division, absolute_import
"""For TCC AXIS command

note: currently not checking for a axis command of the type rot, norot
in this case rot is ignored
"""
from twistedActor import CommandError

__all = ["axis"]

def axis(tccActor, userCmd):
    """Execute the axis command
    
    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  axis command
    """
    cmdVerb = userCmd.parsedCmd.paramDict['command'].valueList[0].keyword.lower()
    # dict of commanded axis name: axis name in tccActor.axisDevSet
    matchDict = dict(
        azimuth = "az",
        altitude = "alt",
        rotator = "rot",
    )
    checkExist = not userCmd.parsedCmd.qualDict['nocheck'].boolValue

    axisCmdNameList = [axis.keyword.lower() for axis in userCmd.parsedCmd.paramDict['axes'].valueList]
    yesAxisCmdNameSet = set()
    noAxisCmdNameSet = set()
    allSpecified = False
    for axisCmdName in axisCmdNameList:
        if axisCmdName.startswith("no"):
            if axisCmdName == "noall":
                continue # NoAll is a no-op
            noAxisCmdNameSet.add(axisCmdName[2:]) # skip the No prefix
        elif axisCmdName == "all":
            allSpecified = True
            # don't update yesAxisCmdNameSet until after checking for conflicts
        else:
            # handle explicit requests for invalid axes now, before handling all; later just skip nonexistent axes
            if checkExist:
                if tccActor.axisDevSet[matchDict[axisCmdName]] is None:
                    raise CommandError("Axis %s does not exist" % (axisCmdName,))
            yesAxisCmdNameSet.add(axisCmdName)

    # report conflicts (before applying All, since All means "all available axes that are not explicitly negated")
    if yesAxisCmdNameSet & noAxisCmdNameSet:
        raise CommandError("Conflicting axis name in %s" % axisCmdNameList)

    if allSpecified:
        yesAxisCmdNameSet = set(matchDict.keys())
    netAxisCmdNameSet = yesAxisCmdNameSet - noAxisCmdNameSet
    slotList = [matchDict[cmdName] for cmdName in netAxisCmdNameSet if tccActor.axisDevSet[matchDict[cmdName]]]
    if not slotList:
        raise CommandError("No axes to command")
    
    # sort out qualifiers
    kwargs = dict(
        slotList=slotList,
        userCmd=userCmd,
    )
    if userCmd.parsedCmd.qualDict['timelimit'].boolValue:
        kwargs["timeLim"] = userCmd.parsedCmd.qualDict['timelimit'].valueList[0]
    if cmdVerb == "initialize":
        tccActor.axisDevSet.connectOrInit(**kwargs)
    elif cmdVerb == "reset":
        tccActor.axisDevSet.stop(**kwargs)
    elif cmdVerb == "status":
        tccActor.axisDevSet.status(**kwargs)
    elif cmdVerb == "stop":
        tccActor.axisDevSet.stop(**kwargs)
    elif cmdVerb == "connect":
        tccActor.axisDevSet.connect(**kwargs)
    elif cmdVerb == "disconnect":
        tccActor.axisDevSet.disconnect(**kwargs)
    else:
        raise CommandError("Bug: unrecognized axis command verb %r in %r" % (cmdVerb, userCmd.cmdStr))
    return True
