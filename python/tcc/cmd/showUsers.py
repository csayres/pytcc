
__all__ = ["showUsers"]

def showUsers(tccActor, userCmd, setDone=True):
    """
    @param[in] tccActor  tcc actor
    @param[in,out] userCmd  user command
    @param[in] setDone  set userCmd done when finished? (ignored if userCmd is already done)
    """
    tccActor.showUserInfo(userCmd)
    if setDone and not userCmd.isDone:
        userCmd.setState(userCmd.Done)