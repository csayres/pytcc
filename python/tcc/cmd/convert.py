from __future__ import division, absolute_import

import coordConv

import tcc.base
from twistedActor import CommandError
from tcc.axis import computeObj, coordConvWithFP
from tcc.msg import formatPVT, formatPVTList, formatPxPMRadVel
from tcc.parse import getCoordSys, getPVTPairPxPMRadVel

__all__ = ["convert"]

_BadCoordSysNameList = ("mount", "none")
_BadCoordSysNameLCSet = set(csName.lower() for csName in _BadCoordSysNameList)
_BadCoordSysDescr = " or ".join(_BadCoordSysNameList)
del _BadCoordSysNameList # avoid accidental misuse when _BadCoordSysNameLCSet is wanted

def convert(tccActor, userCmd):
    """Implement the convert command
    
    @param[in] tccActor  tcc actor
    @param[in] userCmd  convert command
    """
    parsedCmd = userCmd.parsedCmd
    qualDict = parsedCmd.qualDict

    currTAI = tcc.base.tai()

    # use a copy of obj so we can safely update it at current time
    # and even swap obj.site if using GS wavelength
    obj = tcc.base.Obj(tccActor.obj)

    useGSWavelen = qualDict["usegswavelength"].boolValue
    zeroPM = qualDict["zpm"].boolValue

    fromPVTPair, fromPxPMRadVel = getPVTPairPxPMRadVel(parsedCmd=parsedCmd, coordName="fromcoords", defTAI=currTAI)
    if fromPVTPair is None:
        raise CommandError("Must specify from position")
    convTAI = fromPVTPair[0].t # TAI date at which to convert
    fromCoordSys = getCoordSys(param=parsedCmd.paramDict["fromsys"])
    if fromCoordSys is None:
        raise CommandError("Must specify from coordinate system")
    toCoordSys = getCoordSys(param=parsedCmd.paramDict["tosys"])
    if toCoordSys is None:
        toCoordSys = fromCoordSys

    if fromCoordSys.getName().lower() in _BadCoordSysNameLCSet:
        raise CommandError("Invalid fromSys=%r: cannot convert from %s" % (fromCoordSys.getName(), _BadCoordSysDescr))
    if toCoordSys.getName().lower() in _BadCoordSysNameLCSet:
        raise CommandError("Invalid toSys=%r: cannot convert to %s" % (toCoordSys.getName(), _BadCoordSysDescr))

    fromDir = coordConv.PVT(0, 0, convTAI) # might want to make this a user input (single value; vel!=0 is silly)

    # update object block so focal-plane math is correct
    computeObj(
        obj = obj,
        doWrap = False,
        doRestart = [True]*tcc.base.NAxes,
        reportLim = False,
        earth = tccActor.earth,
        inst = tccActor.inst,
        telMod = tccActor.telMod,
        axeLim = tccActor.axeLim,
        tai = convTAI,
    )

    toPVTPair, toPxPMRadVel, toDir, scaleChange = coordConvWithFP(
        fromCoordSys = fromCoordSys,
        fromPVTPair = fromPVTPair,
        fromPxPMRadVel = fromPxPMRadVel,
        fromDir = fromDir,
        toCoordSys = toCoordSys,
        obj = obj,
        inst = tccActor.inst,
        zeroPM = zeroPM,
        useGSWavelen = useGSWavelen,
    )
    toFromAng = toDir - fromDir

    msgStrList = [
        "ConvPos=" + formatPVTList(toPVTPair),
        "ConvAng=" + formatPVT(toFromAng),
        "ConvPM=" + formatPxPMRadVel(toPxPMRadVel),
    ]
    msgStr = "; ".join(msgStrList)
    userCmd.setState(userCmd.Done, hubMsg=msgStr)
