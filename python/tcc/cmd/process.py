from __future__ import division, absolute_import

from tcc.msg import formatDisabledProc

__all__ = ["process"]

# dict of process name: tune attribute name, tccActor timer attribute name, tccActor method
_ProcNameAttrsDict = {
    "brdtelpos": ("doBrdTelPos", "brdTelPosTimer", "brdTelPos"),
    "collimate": ("doCollimate", "collimateTimer", "updateCollimation"),
    "status": ("doStatus", "statusTimer", "showStatus"),
}

_DidCheck = False

def process(tccActor, userCmd):
    """Implement the process command
    
    @param[in,out] tccActor  tcc actor
        reads tccActor.inst
        writes tccActor.inst and triggers a collimation update
    @param[in,out] userCmd  user command
    """
    global _DidCheck
    if not _DidCheck:
        for tuneAttr, timerName, methodName in _ProcNameAttrsDict.itervalues():
            assert hasattr(tccActor.tune, tuneAttr)
            assert hasattr(tccActor, timerName)
            assert hasattr(tccActor, methodName)
        _DidCheck = True

    try:
        cmdStr = userCmd.parsedCmd.paramDict["command"].valueList[0].keyword.lower()
        if cmdStr != "status":
            procNameList = [val.keyword for val in userCmd.parsedCmd.paramDict["procnames"].valueList]
            doEnable = bool(cmdStr == "enable")
            for procName in procNameList:
                (tuneAttr, timerName, methodName) = _ProcNameAttrsDict.get(procName.lower())
                setattr(tccActor.tune, tuneAttr, doEnable)
                timer = getattr(tccActor, timerName)
                if doEnable:
                    method = getattr(tccActor, methodName)
                    timer.start(0, method)
                else:
                    timer.cancel()
                    if procName == "collimate":
                        # cancel the extra collimate timer
                        tccActor.collimateSlewEndTimer.cancel()
    finally:
        msgCode, msgStr = formatDisabledProc(tccActor.tune)
        tccActor.writeToUsers(msgCode, msgStr, cmd=userCmd)

    userCmd.setState(userCmd.Done)