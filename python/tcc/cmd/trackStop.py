from __future__ import division, absolute_import

import coordConv

import tcc.base
from .startSlew import startSlew

__all__ = ["trackStop"]

def trackStop(tccActor, userCmd, doObj, doRot):
    """Implement the track/stop or rotate/stop command

    @param[in,out] tccActor  tcc actor;
        modifies obj and slewCmd
    @param[in,out] userCmd  track command
    @param[in] doObj  halt az/alt?
    @param[in] doRot  halt rotator?
    """
    haltAxis = [False]*3
    if doObj:
        haltAxis[0:2] = [True, True]
        tccActor.obj.userSys = coordConv.makeCoordSys("none", 0)
    if doRot:
        haltAxis[2] = tccActor.inst.hasRotator()
        tccActor.obj.rotType = tcc.base.RotType_None
    for i in range(3):
        if haltAxis[i] and tccActor.obj.targetMount[i].isfinite():
            tccActor.obj.axisCmdState[i] = tcc.base.AxisState_Halting
            tccActor.obj.axisErrCode[i] = tcc.base.AxisErr_HaltRequested
    startSlew(tccActor=tccActor, obj=tccActor.obj, slewCmd=userCmd, doAbsRefCorr=False, doCollimate=False)
