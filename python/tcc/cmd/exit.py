from __future__ import division, absolute_import

__all__ = ["exit"]

def exit(tccActor, cmd=None):
    """Disconnect yourself
    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  exit command
    """
    sock = tccActor.userDict[cmd.userID]
    sock.close()
