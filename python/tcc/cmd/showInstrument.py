from __future__ import division, absolute_import
"""
    notes:
    I do not-- 
    worry about name lengths & truncation as old tcc behaves
    show filters not on inst blk
    show guide probe info
"""
from RO.StringUtil import quoteStr

from .showFocus import showFocus
from tcc.msg import showAxeLim

__all__ = ["showInstrument"]

def showInstrument(tccActor, userCmd, setDone=True, showFull=None):
    """
    @param[in] tccActor  tcc actor
        reads tccActor.inst
    @param[in,out] userCmd  user command; if showFull is None then parsedCmd attribute must be set
    @param[in] setDone  set userCmd done when finished? (ignored if userCmd is already done)
    @param[in] showFull  if True then show all data; if False then show an abbreviated version;
        if None then use the "full" qualifier in userCmd, if present, else use True
    """
    if showFull is None:
        if "full" in userCmd.parsedCmd.qualDict:
            showFull = userCmd.parsedCmd.qualDict["full"].boolValue
        else:
            showFull = True
    kwStrs = []
    inst = tccActor.inst
    instPos = inst.instPos.getName()
    if instPos == "?":
        instPos = "non"
    kwStrs.append("Inst=%s; InstPos=%s; GCView=%s" % (
        quoteStr(tccActor.inst.instName),
        instPos,
        quoteStr(inst.gcViewName),
    ))
    if showFull:
        ipConfig = "".join((
            "T" if (inst.rotID > 0) else "F", 
            "T" if (inst.gcamID > 0) else "F", 
            "T" if (inst.gmechID > 0) else "F"
        ))
        kwStrs.append("IPConfig=%s; RotID=%i, 0; GCamID=%i, 0; GMechID=%i" % \
            (ipConfig, inst.rotID, inst.gcamID, inst.gmechID))
        kwStrs.append("IImScale=%0.4f, %0.4f; IImCtr=%0.2f, %0.2f; IImLim=%0.2f, %0.2f, %0.2f, %0.2f" % (
            inst.iim_scale[0], inst.iim_scale[1],
            inst.iim_ctr[0],   inst.iim_ctr[1],
            inst.iim_minXY[0], inst.iim_minXY[1],
            inst.iim_maxXY[0], inst.iim_maxXY[1],
        ))
        kwStrs.append("InstFocus=%0.2f" % (inst.inst_foc,))
        kwStrs.append("RotInstXYAng=%0.6f, %0.6f, %0.6f" % (
            inst.rot_inst_xy[0],
            inst.rot_inst_xy[1],
            inst.rot_inst_ang,
        ))
        if (inst.gcamID > 0):
            kwStrs.append("GImScale=%0.4f, %0.4f; GImCtr=%0.2f, %0.2f; GImLim=%0.2f, %0.2f, %0.2f, %0.2f" % (
                inst.gim_scale[0], inst.gim_scale[1],
                inst.gim_ctr[0], inst.gim_ctr[1],
                inst.gim_minXY[0], inst.gim_minXY[1],
                inst.gim_maxXY[0], inst.gim_maxXY[1],    
            ))
            kwStrs.append("GCNomFocus=%0.2f" % (inst.gcNomFocus,))
        else:
            kwStrs.append("GImScale=NaN, NaN; GImCtr=NaN, NaN; GImLim=NaN, NaN, NaN, NaN")
            kwStrs.append("GCNomFocus=NaN")       
        kwStrs.append("PtErrProbe=%d" % (inst.ptErrProbe),)
        for i, probe in enumerate(inst.gProbe):
            probeNum = i + 1
            kwStrs.append("GProbeInfo=%d, %s, %0.2f, %0.2f, %0.2f, %0.2f, %0.2f, %0.2f, %0.6f, %0.6f, %0.5f" % (
                probeNum,
                "T" if probe.exists else "F",
                probe.ctr[0], probe.ctr[1],
                probe.minXY[0], probe.minXY[1],
                probe.maxXY[0], probe.maxXY[1],
                probe.rot_gp_xy[0], probe.rot_gp_xy[1],
                probe.rot_gim_ang,
            ))
            # the following is a deprecated version of gProbeInfo; remove when it is safe to do so
            kwStrs.append("GProbe=%d; GPCtr=%0.2f, %0.2f; GPLim=%0.2f, %0.2f, %0.2f, %0.2f" % (
                probeNum,
                probe.ctr[0], probe.ctr[1],
                probe.minXY[0], probe.minXY[1],
                probe.maxXY[0], probe.maxXY[1],
            ))

    for kwStr in kwStrs:
        tccActor.writeToUsers("i", kwStr, userCmd)
    if showFull:
        showAxeLim(tccActor, userCmd)
    showFocus(tccActor, userCmd, setDone=False)
    if setDone and not userCmd.isDone:
        userCmd.setState(userCmd.Done)
