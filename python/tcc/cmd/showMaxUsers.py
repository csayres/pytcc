from __future__ import division, absolute_import

__all__ = ["showMaxUsers"]

def showMaxUsers(tccActor, userCmd, setDone=True):
    """
    @param[in,out] tccActor  tcc actor
        reads tccActor.tune
    @param[in,out] userCmd  user command
    @param[in] setDone  set userCmd done when finished? (ignored if userCmd is already done)
    """
    kwStr='Maxusers=%i' % (tccActor.tune.maxUsers,)
    tccActor.writeToUsers('i', kwStr, userCmd)
    if setDone and not userCmd.isDone:
        userCmd.setState(userCmd.Done)
