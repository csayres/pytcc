from __future__ import division, absolute_import

from .showTime import showTime

__all__ = ["setTime"]

def setTime(tccActor, userCmd):
    """Load earth orientation predictions from the current earth orientation data. 

    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  a twistedActor BaseCommand with parseCmd attribute
    """
    # no qualifiers
    # no parameters
    
    tccActor.updateEarth()
    showTime(tccActor, userCmd, setDone=True)
