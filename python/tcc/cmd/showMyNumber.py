from __future__ import division, absolute_import

__all__ = ["showMyNumber"]

def showMyNumber(tccActor, userCmd):
    """
        @param[in,out] tccActor  tcc actor
        @param[in,out] userCmd  user command
    """
    kwStr = 'YourUserNum=%i'% (userCmd.userID,)
    tccActor.writeToOneUser('i', kwStr, userCmd)
    userCmd.setState(userCmd.Done)
