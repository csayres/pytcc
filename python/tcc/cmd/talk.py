from __future__ import division, absolute_import

from twistedActor import CommandError

__all__ = ["talk"]

def talk(tccActor, userCmd):
    """Implement the talk command

    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  user command
    """
    devName = userCmd.parsedCmd.paramDict["device"].valueList[0]
    cmdStr = userCmd.parsedCmd.paramDict["command"].valueList[0]
    argDict = dict()
    timeLimQual = userCmd.parsedCmd.qualDict["timelimit"]
    if timeLimQual.boolValue:
        argDict["timeLim"] = timeLimQual.valueList[0]

    lowDevName = devName.lower()
    if lowDevName in tccActor.axisDevSet:
        # dev is an axis device
        axisDev = tccActor.axisDevSet[lowDevName]
        axisDev.startCmd(cmdStr, userCmd=userCmd, showReplies=True, **argDict)
    elif lowDevName in tccActor.mirDevSet:
        # dev is a mirror device
        mirDev = tccActor.mirDevSet[lowDevName]
        mirDev.startCmd(cmdStr, userCmd=userCmd, showReplies=True, **argDict)
    else:
        raise CommandError("Unknown device %r" % (devName,))
