from __future__ import division, absolute_import

from tcc.msg.formatObjFields import formatPVT

__all__ = ["showPhysical"]

def showPhysical(tccActor, userCmd):
    """Implement the "show physical" command

    @param[in] tccActor  tcc actor
        reads tccActor.obj.rotPhys
    @param[in,out] userCmd  user command
    """
    kwStr = 'RotPhys=' + formatPVT(tccActor.obj.rotPhys)
    tccActor.writeToUsers('i', kwStr, userCmd)
    userCmd.setState(userCmd.Done)
    
    
