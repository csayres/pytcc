from __future__ import division, absolute_import
"""
From TCC documentation:
Returns the current time in several time systems. The time returned is the 
time the command was parsed. There is not attempt to compensate for delays 
in transmitting the time string. However, the various kinds of time returned 
are consistent with each other. Note that there is a SET TIME command, but it 
calibrates the TCC's clock from the observatory's time standard. 

Returned keywords: TAI, UT1, LST, UTC_TAI.
"""
import tcc.base
import coordConv

__all__ = ["showTime"]

def showTime(tccActor, userCmd, setDone=True):
    """Implement the Show Time command

    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  user command
    @param[in] setDone  set userCmd done? (ignored if userCmd is already done)
    """    
    currTAI = tcc.base.tai()
    
    # make a copy of site and update that at the current TAI
    # making a copy is probably silly, but avoids the possibility that the "show time" command
    # could influence tracking (except by delaying it)
    site = coordConv.Site(tccActor.obj.site)
    tccActor.earth.updateSite(site, currTAI)
    
    currLAST = coordConv.lastFromTAI(currTAI, site)
    currUT1 = site.ut1_tai + currTAI
    
    msgStrList = [
        "TAI=%0.3f" % (currTAI,),
        "LAST=%0.4f" % (currLAST,),
        "UT1=%0.3f" % (currUT1,),
        "UTC_TAI=%0.0f" % (site.utc_tai,),
        "UT1_TAI=%0.3f" % (site.ut1_tai,),    
    ]
    msgStr = "; ".join(msgStrList)
    tccActor.writeToUsers('i', msgStr, cmd=userCmd)        
    if setDone and not userCmd.isDone:
        userCmd.setState(userCmd.Done)
