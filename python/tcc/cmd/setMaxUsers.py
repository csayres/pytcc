from __future__ import division, absolute_import

from .showMaxUsers import showMaxUsers
    
__all__ = ["setMaxUsers"]

def setMaxUsers(tccActor, userCmd):
    """Implement the Set MaxUsers command

    @param[in,out] tccActor  tcc actor
        writes tccActor.tune.maxUsers
    @param[in,out] userCmd  user command
    """
    valueList = userCmd.parsedCmd.paramDict["maxusers"].valueList[0].valueList
    if valueList:
        value = valueList[0]
        if value < 1:
            userCmd.setState(userCmd.Failed, textMsg="Cannot set MaxUsers to %s; must be > 0" % (value,))
            return

        tccActor.tune.maxUsers = value

    showMaxUsers(tccActor, userCmd, setDone=True)
