from __future__ import division, absolute_import

__all__ = ["showAxisConfig"]

from tcc.msg import showAxeLim

def showAxisConfig(tccActor, userCmd, setDone=True):
    """Implement the "show axisconfig" command

    @param[in,out] tccActor  tcc actor
    @param[in,out] userCmd  user command
    @param[in] setDone  set userCmd done when finished? (ignored if userCmd is already done)
    """
    showAxeLim(tccActor, userCmd)

    if setDone and not userCmd.isDone:
        userCmd.setState(userCmd.Done)
