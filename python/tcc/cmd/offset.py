from __future__ import division, absolute_import

from twistedActor import CommandError

import tcc.base
from tcc.axis import computeObj
from tcc.parse import getPVTList, getRestart
from .checkTargetForSlew import checkTargetForSlew
from .startSlew import startSlew
from .showObject import showObjectFields
from tcc.msg import moveItemKW

__all__ = ["offset"]

# dict of offset name (lowercase): number of axes, obj block attribute name (case-sensitive):
OffsetNumAxesAttrDict = dict(
    arc         = (2, "userArcOff"),
    boresight   = (2, "objInstXY"),
    instplane   = (2, "objInstXY"),
    rotator     = (1, "rotUser"),
    guidecorrection = (3, "guideOff"),
    gcorrection = (3, "guideOff"),
    calibration = (3, "calibOff"),
)

def setObjOffset(obj, parsedCmd, rotExists, tai, writeToUsers=None):
    """Update user-set attributes of object block based on an offset command

    @param[in,out] obj  object block to update
    @param[in] parsedCmd  parsed offset command
    @param[in] rotExists  True if there is an instrument rotator
    @param[in] tai  TAI date for PVT coordinates

    @return three flags:
    - isNewObj: True if new object position requested
    - isNewRot: True if new rotator position requested

    @throw twistedActor.CommandError if offset type is rotator and there is no rotator
    """
    #print 'offsetType', parsedCmd.paramDict["type"].valueList[0].keyword.lower()
    offsetType = parsedCmd.paramDict["type"].valueList[0].keyword.lower()
    if offsetType == "rotator" and not rotExists:
        raise CommandError("Cannot command rotator offset, current instrument has no rotator")
    numAxes, attrName = OffsetNumAxesAttrDict[offsetType]
    coordSetParam = parsedCmd.paramDict["coordset"]
    pvtList, numValues = getPVTList(coordSetParam, numAxes, defTAI=tai)

    pAbs = parsedCmd.qualDict["pabsolute"].boolValue
    vAbs = not parsedCmd.qualDict["vincremental"].boolValue

    objField = getattr(obj, attrName)
    if numAxes == 1:
        objField = [objField]

    numAxesToSet = numAxes
    if numAxes == 3:
        if numValues == 0 and not rotExists:
            # user did not specify any values; skip rotator
            numAxesToSet = 2
        elif numValues == 2:
            # user specified 2 values, and so explicitly excluded the rotator
            numAxesToSet = 2
    rotOmitted = numAxesToSet != numAxes

    for ind in range(numAxesToSet):
        pvt = objField[ind]
        offPVT = pvtList[ind]
        # get a copy of the current offset(s) at the TAI date of the new offset,
        # since that is the desired final TAI of the offset and one can simply add .pos
        newPVT = pvt.copy(tai)
        if pAbs:
            newPVT.pos = offPVT.pos
        else:
            newPVT.pos += offPVT.pos

        if vAbs:
            newPVT.vel = offPVT.vel
        else:
            newPVT.vel += offPVT.vel

        if numAxes == 1:
            # scalar field; setting objField[ind] will not change obj, so set the field directly
            setattr(obj, attrName, newPVT)
        else:
            objField[ind] = newPVT

    isNewObj = False
    isNewRot = False
    if numAxes == 1: # only the rotator is changed
        isNewRot = True
    elif numAxes == 2: # az, alt or user1, user2
        isNewObj = True
    else: # az, alt [, rot]
        isNewObj = True
        isNewRot = not rotOmitted

    return isNewObj, isNewRot


def offset(tccActor, userCmd):
    """Implement the offset command
    """
    rotExists = tccActor.inst.hasRotator()
    parsedCmd = userCmd.parsedCmd

    currTAI = tcc.base.tai()
    newObj = tcc.base.Obj(tccActor.obj)
    isNewObj, isNewRot = setObjOffset(obj=newObj, parsedCmd=parsedCmd, rotExists=rotExists, tai=currTAI, writeToUsers=tccActor.writeToUsers)
    if isNewRot and not rotExists:
        # rotator offset (with no rotator) as part of offsetting other axes as well (if offset type is "rotator",
        # then the command was already rejected by setObjOffset); warn but continue
        tccActor.writeToUsers("w", "Text=\"No instrument rotator: rotator component of offset recorded but not applied\"", cmd=userCmd)

    if isNewObj and tccActor.obj.userSys.getName().lower() == "mount":
        tccActor.obj = newObj
        tccActor.writeToUsers("w", "Text=\"Tracking in mount coordinates; offset recorded but not applied\"", cmd=userCmd)
        userCmd.setState(userCmd.Done)
        return

    qualDict = parsedCmd.qualDict
    # note: collimate, refCoefficients handled in startSlew()?
    doRestart = getRestart(qual=qualDict["restart"], rotExists=rotExists)
    doComputed = qualDict["computed"].boolValue
    doAbsRefCorr = qualDict["absrefcorrect"].boolValue
    if any(doRestart):
        if qualDict["computed"].boolValueDefaulted:
            doComputed = True # if restart is specified, computed must be the default action
        elif not doComputed:
            # user specifically commanded /nocomputed along with /restart, this isn't allowed
            raise CommandError("May not specify /NoComputed and /Restart simultaneously")
    perfect = qualDict["perfect"].boolValue
    if qualDict["refcoefficients"].boolValue:
        tccActor.weath.updateSite(newObj.site)

    checkTargetForSlew(
        tccActor=tccActor,
        userCmd=userCmd,
        newObj=newObj,
        isNewObj=isNewObj,
        isNewRot=isNewRot,
        doWrap=False,
        doRestart=doRestart,
        perfect=perfect,
        tai=currTAI,
    )
    if userCmd.isDone:
        return

    doCollimate = qualDict["collimate"].boolValue

    if doComputed:
        # computed offset, use a normal slew
        startSlew(
            tccActor=tccActor,
            obj=newObj,
            slewCmd=userCmd,
            doAbsRefCorr=doAbsRefCorr,
            doCollimate=doCollimate,
        )
    else:
        # uncomputed offset; use +MOVE P V

        # compute delta-mount at currTAI
        # (note that newObj has already been computed at that date by checkTargetForSlew)
        oldObj = tcc.base.Obj(tccActor.obj)
        computeObj(
            oldObj,
            doWrap=False,
            doRestart=doRestart,
            reportLim=True,
            earth=tccActor.earth,
            inst=tccActor.inst,
            telMod=tccActor.telMod,
            axeLim=tccActor.axeLim,
            tai=currTAI,
        )

        offsetPVTList = [newObj.targetMount[axis] - oldObj.targetMount[axis] for axis in range(tcc.base.NAxes)]
        # preserve old MOVE P V T update time to allow testing for calling updateTracking late
        newObj.updateTime = tccActor.obj.updateTime
        tccActor.obj = newObj
        tccActor.axisDevSet.plusMovePV(offsetPVTList, userCmd=userCmd)

        showObjectFields(tccActor, userCmd)
        kwString = "Moved; %s"%moveItemKW(tccActor.obj)
        tccActor.writeToUsers("i", kwString)
        if doCollimate:
            tccActor.updateCollimation()



