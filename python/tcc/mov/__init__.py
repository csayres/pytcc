from __future__ import absolute_import
"""Basic motion-related computations for a single axis
"""
from .checkSegment import *
from .checkPos import *
from .pathSegment import *
from .fullSlew import *
from .slew import *
from .trapSlew import *
