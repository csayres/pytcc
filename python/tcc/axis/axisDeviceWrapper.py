from __future__ import division, absolute_import

from twistedActor import DeviceWrapper

from .axisDevice import AxisDevice
from .fakeAxisCtrl import FakeAxisCtrl

__all__ = ["AxisDeviceWrapper"]

class AxisDeviceWrapper(DeviceWrapper):
    """!A wrapper for an AxisDevice talking to a fake axis controller
    """
    def __init__(self,
        name,
        stateCallback = None,
        port = 0,
        debug = False,
        logReplies = False,
    ):
        """!Construct a GalilDeviceWrapper that manages its fake Galil

        @param[in] name  name of axis: one of "az", "alt", "rotN"
        @param[in] stateCallback  function to call when connection state of hardware controller or device changes;
            receives one argument: this device wrapper
        @param[in] port  port for device; 0 to assign a free port
        @param[in] debug  if True, print debug messages
        @param[in] logReplies  should the FakeAxisCtrl print replies to stdout?
        """
        controller = FakeAxisCtrl(
            name = name,
            port = port,
            logReplies = logReplies,
        )
        DeviceWrapper.__init__(self, name=name, stateCallback=stateCallback, controller=controller, debug=debug)
    
    def _makeDevice(self):
        port = self.port
        if port is None:
            raise RuntimeError("Controller port is unknown")
        self.device = AxisDevice(
            name=self.name,
            host="localhost",
            port=port,
        )
