from __future__ import division, absolute_import

import sys

import tcc.base
import tcc.mov

__all__ = ["oneSlewIter"]

MinPVTSep = 0.05 # minimum separation between PVTs, in sec
    # Warning: this only controls whether a final PVT is added to the slew;
    # for other PVTs the separation is controlled by other factors, such as the jerk limit.
    # The value should be significantly less than the track advance time.

def oneSlewIter(doAxis, oldMount, newMount, begTime, minTime, axeLim):
    """!Compute a slew for all axes, given the starting and target paths.
    
    @param[in] doAxis   slew this axis? A sequence of NAxes bools
    @param[in] oldMount initial position, as a list of NAxes PVTs
    @param[in] newMount target coordinates, as a list of NAxes PVTs
    @param[in] begTime	start time (TAI, MJD sec)
    @param[in] minTime  minimum slew duration (sec)
    @param[in] axeLim   axes limits, as an tcc.base.AxeLim
    where NAxes = tcc.base.NAxes
    
    @return two items:
    - pathList: a list of NAxes pvtLists, where each pvtList is a list of PVTs describing the motion of that axis;
        if doAxis[axis] is false then pathList[axis] is an empty list
    - endTime: end time of slew (TAI, MJD sec)

    @throw RuntimeError if the slew ends too soon or cannot be computed
    
    If no axes are to be slewed (slew not requested, or beginning or ending
    position is unknown), then endTime = begTime + minTime. No warning is given.
    
    History:
    2013-12-06 ROwen    Converted from doSlew.for
    """
    def formatArgs():
        return "tcc.mov.oneSlewIter(doAxis=%s, oldMount=%s, newMount=%s, begTime=%r, minTime=%r)" % \
            (doAxis, oldMount, newMount, begTime, minTime)

    def reportBug(msgStr):
        """!Write a detailed message to stderr and raise RuntimeError"""
        sys.stderr.write("%s failed:\n%s\n" % (formatArgs(), msgStr))
        raise RuntimeError(msgStr)

    # begin
    # set endTime and nNodes to the values they'll have
    # if no axes are to be slewed.
    pathList = [[]]*tcc.base.NAxes

    # Compute a slew for each axis (skip those with unknown beginning
    # or ending positions), and find out when the longest slew ends
    endTime = 0
    for axis in range(tcc.base.NAxes):
        oldPVT = oldMount[axis]
        newPVT = newMount[axis]
        if doAxis[axis] and oldPVT.isfinite() and newPVT.isfinite():
            # compute the slew
            # note that tcc.mov.slew wants both pA and pB at the start time,
            # and it returns a path whose times are shifted so the start time is 0
            try:
                pvtList = tcc.mov.slew(
                    pA = oldPVT.getPos(begTime),
                    vA = oldPVT.vel,
                    pB = newPVT.getPos(begTime),
                    vB = newPVT.vel,
                    tMin = minTime,
                    axisLim = axeLim[axis],
                )
            except Exception:
                sys.stderr.write("%s failed on axis %s when calling tcc.mov.slew\n" % (formatArgs(), axis))
                raise

            # shift the times of the path PVTs to start at begTime instead of 0
            for pvt in pvtList:
                pvt.t += begTime

            endTime = max(endTime, pvtList[-1].t)
            pathList[axis] = list(pvtList)

    # Add a new node to the slew for each axis except the longest,
    # so that all axes end their slew at the same time (endTime).
    for axis in range(tcc.base.NAxes):
        pvtList = pathList[axis]
        if len(pvtList) > 0:
            lastPVT = pvtList[-1]
            if lastPVT.t < endTime - MinPVTSep:
                endPVT = newMount[axis].copy(endTime)
                pvtList.append(endPVT)

    # if any axis is being slewed, make sure the slew takes long enough
    if (endTime > 0.0) and (endTime < begTime + minTime):
        reportBug("Bug! Computed slew ended too soon; endTime=%s" % (endTime,))

    return pathList, endTime
