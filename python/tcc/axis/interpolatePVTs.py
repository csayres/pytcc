from __future__ import division, absolute_import

from tcc.base import PVAJT

__all__ = ["interpolatePVTs"]

def interpolatePVTs(pvti, pvtf):
    """!Compute a path of constant jerk by interpolating between two PVTs
    
    @param[in] pvti  initial PVT (a coordConv.PVT)
    @param[in] pvtf  final PVT (a coordConv.PVT)

    @return interpolated path of constant jerk, as a tcc.base.PVAJT at time pvti.t
    """
    pvajti = PVAJT(pvti)
    dt = pvtf.t - pvti.t
    vprime = (pvtf.pos - pvti.pos) / dt
    pvajti.accel = (2 / dt) * (3*vprime - 2*pvti.vel - pvtf.vel)
    pvajti.jerk  = (6 / dt**2)* (pvti.vel + pvtf.vel - 2*vprime)
    return pvajti
