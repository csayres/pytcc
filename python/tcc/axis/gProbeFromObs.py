from __future__ import division, absolute_import

from coordConv import rot2D

from tcc.base import ArrayPVT2
from .rotFromObs import rotFromObs

__all__ = ["gProbeFromObs"]

def gProbeFromObs(targObs, gProbeNum, obj, inst, taiDate):
    """!Compute guide probe position from observed coordinates

    @param[in] targObs  position of target in observed coordinates (a coordConv.PVTCoord)
    @param[in] gProbeNum  guide probe number
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] inst  the instrument block; the following fields are read:
    - gProbe
    - rot_inst_xy
    - rot_inst_ang
    @param[in] taiDate  TAI date at which to compute the result (MJD, sec)

    @return a tuple:
    - targGProbeXY: position of target in guide probe coordinates (a tcc.base.ArrayPVT2) (deg)
    - targAzGProbeAng: angle of direction of increasing azimuth at target in guide probe coordinates (a coordConv.PVT)
    """
    try:
        gProbeInfo = inst.gProbe[gProbeNum-1]
        assert gProbeInfo.exists
    except Exception:
        raise RuntimeError("Guide probe %d does not exist" % (gProbeNum,))
    
    # compute position of target in rotator frame
    targRotXY, targAzRotAng = rotFromObs(targObs, obj, inst, taiDate)
    
    # compute targGProbeXY in two stages:
    # compute targGProbe_rotXY = vector from guide probe to target, in rotator frame
    # rotate that vector into the guide probe frame
    gp_rot_xy = rot2D(-gProbeInfo.rot_gp_xy[0], -gProbeInfo.rot_gp_xy[1], -gProbeInfo.rot_gim_ang)
    gp_rot_ang = -gProbeInfo.rot_gim_ang
    targGProbe_rotXY = ArrayPVT2()
    targGProbe_rotXY[:] = [targRotXY[i] - gp_rot_xy[i] for i in range(2)]
    targGProbeXY = ArrayPVT2()
    rot2D(targGProbeXY[0], targGProbeXY[1], targGProbe_rotXY[0], targGProbe_rotXY[1], -gp_rot_ang, taiDate)
    
    targAzGProbeAng = targAzRotAng - gp_rot_ang
    return targGProbeXY, targAzGProbeAng
