from __future__ import division, absolute_import

import tcc.base
from .gProbeFromObs import gProbeFromObs

__all__ = ["gImageFromObs"]

def gImageFromObs(targObs, gProbeNum, obj, inst, taiDate):
    """!Compute guide probe position from observed coordinates

    @param[in] targObs  position of target in observed coordinates (a coordConv.PVTCoord)
    @param[in] gProbeNum  guide probe number
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - gProbe
    - obsPos
    - objAzInstAng
    @param[in] inst  the instrument block; the following fields are read:
    - gim_scale
    - rot_inst_xy
    - rot_inst_ang
    @param[in] taiDate  TAI date at which to compute the result (MJD, sec)

    @return a tuple:
    - targGImageXY: position of target in guide image coordinates (a tcc.base.ArrayPVT2) (x,y unbinned pixels)
    - targAzGProbeAng: angle of direction of increasing azimuth at target in guide probe coordinates (a coordConv.PVT)
    """
    try:
        gProbeInfo = inst.gProbe[gProbeNum-1]
        assert gProbeInfo.exists
    except Exception:
        raise RuntimeError("Guide probe %d does not exist" % (gProbeNum,))

    targGProbeXY, targAzGProbeAng = gProbeFromObs(targObs, gProbeNum, obj, inst, taiDate)

    targGImageXY = tcc.base.ArrayPVT2()
    for i in range(2):
        targGImageXY[i] = (targGProbeXY[i] * inst.gim_scale[i]) + gProbeInfo.ctr[i]

    return targGImageXY, targAzGProbeAng
