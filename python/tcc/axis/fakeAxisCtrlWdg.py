from __future__ import division, absolute_import

import Tkinter

from twisted.internet import defer
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
import RO.Wdg

from .fakeAxisCtrl import FakeAxisCtrl

__all__ = ["FakeAxisCtrlWdg", "FakeAxisCtrlSetWdg"]
                   
sampleMSDump ="""time                position    velocity   error    edge  correction
    1266860307.600     74.814029    0.241533   0.000954   1    0.000524
    1266816131.180     59.597493    1.485286  -0.003319   0    0.000000
    1266816131.330     59.819334    1.485286  -0.000797   1   -0.002057
    1266839360.530     59.810348   -1.376835   0.006240   0    0.000000
    1266839360.680     59.590300   -1.376835   0.001925   1    0.004082
    1266839369.990     44.817815   -1.607564   0.012479   0    0.000000
    1266839370.120     44.606302   -1.605523  -0.007002   1    0.002738
    1266860253.490     44.615251    0.252483  -0.014001   0    0.000000
    1266860254.160     44.825963    0.252483   0.006281   1   -0.003860
    1266860268.360     59.597959    1.462901  -0.003784   0    0.000000
    1266860268.510     59.821265    1.462901  -0.002728   1   -0.003256
    1266860306.680     74.595496    0.336560   0.000096   0    0.000000
    1266860307.600     74.814029    0.241533   0.000954   1    0.000524"""

sampleID = "axis code: 2009-10-16 379M, axis.ini version $Revision: 1.41 $"

class FakeAxisCtrlWithWdg(FakeAxisCtrl):
    """!Variant of FakeAxisCtrl that sets some widgets
    """
    def __init__(self, name, port, logReplies, logWdg, cmdPVTWdg, advTimeStatsWdg):
        """!Construct a FakeAxisCtrlWithWdg
        
        @param[in] name  name of axis controller
        @param[in] port  port on which to command axis controller
        @param[in] logReplies  log all replies? Set False for less chatter
        @param[in] logWdg  an RO.Wdg.LogWdg in which to log replies
        @param[in] cmdPVTWdg  a RO.Wdg.StrLabel in which to display commanded position, velocity and time
        @param[in] advTimeStatsWdg  a RO.Wdg.StrLabel in which  to display advanced time statistics
        """
        self.logWdg = logWdg
        self.cmdPVTWdg = cmdPVTWdg
        self.advTimeStatsWdg = advTimeStatsWdg
        FakeAxisCtrl.__init__(self, name=name, port=port, logReplies=logReplies)
    
    def logMsg(self, msgStr):
        self.logWdg.addMsg(msgStr)

    @FakeAxisCtrl.cmdPVT.setter
    def cmdPVT(self, pvt):
        self._cmdPVT = pvt
        self.cmdPVTWdg.set("%0.3f %0.3f %0.3f" % (self.cmdPVT.pos, self.cmdPVT.vel, self.cmdPVT.t))
    
    def addAdvTime(self, advTime):
        FakeAxisCtrl.addAdvTime(self, advTime)
        advTimeStatsDict = self.advTimeStats.getStats()
        self.advTimeStatsWdg.set("min=%(min)0.2f, median=%(median)0.2f stdDev=%(stdDev)0.2f" % advTimeStatsDict)


class FakeAxisCtrlWdg(Tkinter.Frame):
    """!A widget that emulates an axis controller and displays information as a summary and log
    """
    def __init__(self, master, name, port, borderwidth=0, logReplies=True):
        """!Construct a fake axis controller widget

        @param[in] master  master widget
        @param[in] name  name of axis controller
        @param[in] port  port on which to command axis controller
        @param[in] borderwidth  border width of enclosing frame
        @param[in] logReplies  log all replies? Set False for a very slow connection
        """
        self.name = name
        self.logReplies = bool(logReplies)
        Tkinter.Frame.__init__(self, master, borderwidth=borderwidth)

        statusFrame = Tkinter.Frame(self)
        row = 0
        RO.Wdg.StrLabel(master=statusFrame, text="%s Last PVT" % (self.name,)).grid(row=row, column=0, sticky="e")
        self.cmdPVTWdg = RO.Wdg.StrLabel(master=statusFrame)
        self.cmdPVTWdg.grid(row=row, column=1, sticky="w")
        row += 1
        RO.Wdg.Button(master=statusFrame, text="Adv Time", callFunc=self.resetAdvTime).grid(row=row, column=0, sticky="e")
        self.advTimeStatsWdg = RO.Wdg.StrLabel(master=statusFrame)
        self.advTimeStatsWdg.grid(row=row, column=1, sticky="w")
        row += 1
        statusFrame.grid(row=0, column=0, sticky="w")

        self.logWdg = RO.Wdg.LogWdg(master=self, height=10)
        self.logWdg.grid(row=1, column=0, sticky="nsew")
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        
        self.fakeAxisCtrl = FakeAxisCtrlWithWdg(
            name=name,
            port=port,
            logReplies=logReplies,
            logWdg=self.logWdg,
            cmdPVTWdg=self.cmdPVTWdg,
            advTimeStatsWdg=self.advTimeStatsWdg,
        )

    def resetAdvTime(self, wdg=None):
        self.advTimeStats.clear()
        self.advTimeStatsWdg.set("")


class FakeAxisCtrlSetWdg(Tkinter.Frame):
    def __init__(self, master, namePortList, logReplies=True):
        """!Create a FakeAxisCtrlSetWdg

        @param[in] master  master widget
        @param[in] namePortList  list of (dev name, port)
        @param[in] logReplies  log all replies? Set False for a very slow connection
        """
        Tkinter.Frame.__init__(self, master)
        rdList = []
        for axisName, port in namePortList:
            wdg = FakeAxisCtrlWdg(
                master=self,
                name=axisName,
                port=port,
                logReplies=logReplies,
                borderwidth=1,
            )
            rdList.append(wdg.fakeAxisCtrl.readyDeferred)
            wdg.pack(side="top", fill="both", expand=True)
        self.readyDeferred = defer.gatherResults(rdList, consumeErrors=True)
