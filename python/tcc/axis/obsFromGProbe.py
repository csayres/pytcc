from __future__ import division, absolute_import

from coordConv import rot2D

from tcc.base import ArrayPVT2
from .obsFromRot import obsFromRot

__all__ = ["obsFromGProbe"]

def obsFromGProbe(targGProbeXY, gProbeNum, obj, inst, taiDate):
    """!Compute observed coordinates from guide probe coordinates

    @param[in] targGProbeXY  position of target in guide probe coordinates (a tcc.base.ArrayPVT2) (deg)
    @param[in] gProbeNum  guide probe number
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] inst  the instrument block; the following fields are read:
    - gProbe
    - rot_inst_xy
    - rot_inst_ang
    @param[in] taiDate  TAI date at which to compute the result (MJD, sec)

    @return a tuple:
    - targObs: position of target in observed coordinates (a coordConv.PVTCoord)
    - targAzGProbeAng: angle of direction of increasing azimuth at target in guide probe coordinates (a coordConv.PVT)
    """
    try:
        gProbeInfo = inst.gProbe[gProbeNum-1]
        assert gProbeInfo.exists
    except Exception:
        raise RuntimeError("Guide probe %d does not exist" % (gProbeNum,))
    
    # compute targGProbe_rotXY = targGProbeXY rotated into the rotator frame
    gp_rot_xy = rot2D(-gProbeInfo.rot_gp_xy[0], -gProbeInfo.rot_gp_xy[1], -gProbeInfo.rot_gim_ang)
    gp_rot_ang = -gProbeInfo.rot_gim_ang
    targGProbe_rotXY = ArrayPVT2()
    rot2D(targGProbe_rotXY[0], targGProbe_rotXY[1], targGProbeXY[0], targGProbeXY[1], gp_rot_ang, taiDate)

    # compute targRotXY = gpRotXY + targGProbe_rotXY
    targRotXY = ArrayPVT2()
    targRotXY[:] = [targGProbe_rotXY[i] + gp_rot_xy[i] for i in range(2)]

    # compute observed position of target
    targInstPVTCoord, targAzRotAng = obsFromRot(targRotXY, obj, inst, taiDate)
    targAzGProbeAng = targAzRotAng - gp_rot_ang
    return targInstPVTCoord, targAzGProbeAng
