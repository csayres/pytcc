from __future__ import division, absolute_import

import coordConv

import tcc.base

__all__ = ["obsFromInst"]

def obsFromInst(targInstXY, obj, taiDate):
    """!Compute observed coordinates from instrument coordinates

    @param[in] targInstXY  position of target in instrument coordinates (a tcc.base.ArrayPVT2) (deg)
        i.e. a position in instrument X,Y relative to inst.iim_ctr, the center of the instrument
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] taiDate  TAI date at which to compute the result (MJD, sec)

    @return a tuple:
    - targObs: position of target in observed coordinates (a coordConv.PVTCoord)
    - targAzInstAng: angle of direction of increasing azimuth at target in instrument coordinates (a coordConv.PVT)
    """
    # compute the inst-plane offset from boresight to target in r, theta
    targObjXY = tcc.base.ArrayPVT2()
    for i in range(2):
        targObjXY[i] = targInstXY[i] - obj.objInstXY[i]
    targObjDist = coordConv.PVT()
    targObjInstAng = coordConv.PVT() # angle of arc from obj/boresight to x at obj w.r.t inst axis
    # note: if targObjXY is (0,0) then targObjInstAng will be 0, and that works fine for computing
    # the final values, so there's no need for special case code
    coordConv.polarFromXY(targObjDist, targObjInstAng, targObjXY[0], targObjXY[1], taiDate)

    # use obsPos.offset to compute the observed position at the target
    targObjAzAng = targObjInstAng - obj.objAzInstAng
    toDir = coordConv.PVT() # angle of arc from obj to x at x w.r.t inst axis
    targObs = obj.obsPos.offset(toDir, targObjAzAng, targObjDist)
    targAzInstAng = obj.objAzInstAng + toDir - targObjAzAng
    return targObs, targAzInstAng
