from __future__ import division, absolute_import

import coordConv

import tcc.base
from .obsFromRot import obsFromRot

__all__ = ["obsFromPtCorr"]

def obsFromPtCorr(targPtCorrXY, obj, inst, taiDate):
    """!Compute observed coordinates from pointing correction coordinates

    @param[in] targPtCorrXY  position of target in pointing correction frame (a tcc.base.ArrayPVT2) (deg)
        i.e. a position in rotator coordinates where X is the direction of increasing azimuth
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] inst  the instrument block; the following fields are read:
    - rot_inst_xy
    - rot_inst_ang
    @param[in] taiDate  TAI date at which to compute the result (MJD, sec)

    @return a tuple:
    - targObs: position of target in observed coordinates (a coordConv.PVTCoord)
    - targAzPtCorrAng: angle of direction of increasing azimuth at target in rotator coordinates (a coordConv.PVT)
    """
    targRotXY = tcc.base.ArrayPVT2()
    coordConv.rot2D(targRotXY[0], targRotXY[1], targPtCorrXY[0], targPtCorrXY[1], obj.rotAzRotAng.getPos(taiDate), taiDate)

    targObs, targAzRotAng = obsFromRot(targRotXY, obj, inst, taiDate)

    targAzPtCorrAng = targAzRotAng.copy(taiDate) - obj.rotAzRotAng

    return targObs, targAzPtCorrAng
