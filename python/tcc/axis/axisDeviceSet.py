from __future__ import division, absolute_import

import itertools

from twistedActor import DeviceSet, log, LinkCommands, UserCmd, expandUserCmd

import tcc.base
from tcc.util import secInDayFromDate
from tcc.msg import formatAxisCmdStateAndErr
from .axisDevice import AxisStatus, AxisDevice

__all__ = ["AxisDeviceSet"]

StatusDelay = 0.1 # delay of status after MOVE P V T or other commands that change status

class AxisDeviceSet(DeviceSet):
    """!Axis devices
    """
    DefaultTimeLim = AxisDevice.DefaultTimeLim
    DefaultInitTimeLim = AxisDevice.DefaultInitTimeLim
    SlotList = ("az", "alt", "rot")
    def __init__(self, actor, devList):
        """!Construct an AxisDeviceSet

        @param[in] actor  actor (instance of twistedActor.BaseActor);
            items used include writeToUsers, queueStatus, obj, inst and axeLim
        @param[in] devList  az, alt and rot device; any can be None if it does not exist

        @throw RuntimeError if len(devList) != 3
        """
        DeviceSet.__init__(self,
            actor = actor,
            slotList = self.SlotList,
            devList = devList,
            connStateKeyword = "axisConnState",
        )
        self._currStatusCmd = None
        self._prevReportedStatusList = [AxisStatus() for slot in self.slotList]

    def connect(self, slotList=None, userCmd=None, timeLim=DefaultInitTimeLim):
        """Connect the specified axis controllers

        Overridden because connecting can be slow, presumably due to the multiplexor
        """
        return DeviceSet.connect(self, slotList=slotList, userCmd=userCmd, timeLim=timeLim)

    def connectOrInit(self, slotList=None, userCmd=None, timeLim=DefaultInitTimeLim):
        """!Connect and initialize axis controllers

        Specified axis controllers that are already connected are simply initialized.

        @param[in] slotList  collection of slot names, or None for all filled slots
        @param[in] userCmd  user command whose set state is set to Done or Failed when all device commands are done;
            if None a new UserCmd is created and returned
        @param[in] timeLim  time limit for init and connect command (sec)
            uses a longer than default time limit because connecting can be slow due to the multiplexor
        @return userCmd: the supplied userCmd or a newly created UserCmd
        """
        # print "connectOrInit(slotList=%r, userCmd=%r, timeLim=%s)" % (slotList, userCmd, timeLim)
        expSlotList = self.expandSlotList(slotList)
        connSlotList = [slot for slot in expSlotList if self.get(slot).isConnected]
        notConnSlotList = list(set(expSlotList) - set(connSlotList))
        initUserCmd = self.init(connSlotList, timeLim=timeLim)
        connUserCmd = self.connect(notConnSlotList, timeLim=timeLim)
        if userCmd is None:
            userCmd = UserCmd()
        LinkCommands(userCmd, (initUserCmd, connUserCmd))
        return userCmd

    def disconnect(self, slotList=None, userCmd=None, timeLim=DefaultInitTimeLim):
        """Disconnect the specified axis controllers

        Overridden because disconnecting can be slow
        """
        return DeviceSet.disconnect(self, slotList=slotList, userCmd=userCmd, timeLim=timeLim)

    def drift(self, slotList=None, userCmd=None, timeLim=DefaultTimeLim):
        """!Execute a DRIFT command and update self.actor.obj

        If drift fails for any axes then initialize all axes and update self.actor.obj

        @param[in] slotList  collection of slot names, or None for all filled slots
        @param[in] userCmd  user command whose set state is set to Done or Failed when all device commands are done;
            if None a new UserCmd is created and returned
        @param[in] timeLim  time limit for command (sec)
        @return userCmd: the supplied userCmd or a newly created UserCmd

        @throw RuntimeError if slotList has empty or non-existent slots
        """
        return self.startCmd(cmdStrOrList="DRIFT", slotList=slotList, userCmd=userCmd, callFunc=self._initAllOnFailure, timeLim=timeLim)

    def init(self, slotList=None, userCmd=None, timeLim=DefaultInitTimeLim):
        """!Initialize the axes controllers

        @param[in] slotList  collection of slot names, or None for all filled slots
        @param[in] userCmd  user command whose set state is set to Done or Failed when all device commands are done;
            if None a new UserCmd is created and returned
        @param[in] timeLim  time limit for init and connect command (sec)
        @return userCmd: the supplied userCmd or a newly created UserCmd
        """
        # print "%s.init(slotList=%s)"%(self, slotList)
        slotList = self.expandSlotList(slotList)
        userCmd = expandUserCmd(userCmd)
        subCmdList = []
        # update axes state to halting
        self._setObjForInit(slotList=slotList, halted=False)
        for slot in slotList:
            dev = self.get(slot)
            subCmdList.append(dev.init(timeLim=timeLim))
        LinkCommands(userCmd, subCmdList)
        return userCmd

    def movePVT(self, pvtList, userCmd=None, timeLim=DefaultTimeLim):
        """!Execute a MOVE Pos Vel Time or MOVE command for each axis

        If any axis fails, INIT that axis and update self.actor.obj

        As soon as the MOVE Pos Vel Time command is sent, schedule a status command.

        @param[in] pvtList  list of PVTs or PVAJTs, one per axis; isfinite() must be false for nonexistent axes
            and any axis you don't want moved
        @param[in] userCmd  user command whose set state is set to Done or Failed when all device commands are done;
            if None a new UserCmd is created and returned
        @param[in] timeLim  time limit for command (sec)
        @return userCmd: the supplied userCmd or a newly created UserCmd
        """
        if len(pvtList) != len(self):
            raise RuntimeError("pvtList must have %d items; pvtList=%s" % (len(self), pvtList))

        cmdDict = dict()
        for pvt, (slot, dev) in itertools.izip(pvtList, self._slotDevDict.iteritems()):
            if dev:
                if pvt.isfinite():
                    cmdDict[slot] = "MOVE %0.7f %0.7f %0.5f" % (pvt.pos, pvt.vel, secInDayFromDate(pvt.t))
                else:
                    cmdDict[slot] = "MOVE"
        self.actor.queueStatus(StatusDelay)
        return self.startCmdDict(cmdDict=cmdDict, userCmd=userCmd, timeLim=timeLim)

    def plusMovePV(self, pvtList, userCmd=None, timeLim=DefaultTimeLim):
        """!Execute a +MOVE Pos Vel or MOVE command for each axis

        If any axis fails, INIT that axis and update self.actor.obj

        As soon as the +MOVE Pos Vel command is sent, schedule a status command.

        @param[in] pvtList  list of PVTs or PVAJTs, one per axis; isfinite() must be false for nonexistent axes
            and any axis you don't want moved
        @param[in] userCmd  user command whose set state is set to Done or Failed when all device commands are done;
            if None a new UserCmd is created and returned
        @param[in] timeLim  time limit for command (sec)
        @return userCmd: the supplied userCmd or a newly created UserCmd
        """
        if len(pvtList) != len(self):
            raise RuntimeError("pvtList must have %d items; pvtList=%s" % (len(self), pvtList))

        cmdDict = dict()
        for pvt, (slot, dev) in itertools.izip(pvtList, self._slotDevDict.iteritems()):
            if dev:
                if pvt.isfinite():
                    cmdDict[slot] = "+MOVE %0.7f %0.7f" % (pvt.pos, pvt.vel)
                else:
                    cmdDict[slot] = "MOVE"
        self.actor.queueStatus(StatusDelay)
        return self.startCmdDict(cmdDict=cmdDict, userCmd=userCmd, timeLim=timeLim)

    def startSlew(self, pathList, doAbsRefCorr, userCmd=None, timeLim=DefaultTimeLim):
        """!Execute a set if MOVE Pos Vel Time commands or one MOVE command for each axis

        If any axis fails, INIT that axis and update self.actor.obj

        As soon as the commands are sent, schedule a status command.

        @param[in] pathList  list of pvtLists, one per slot;
            if pathList[slot] is an empty list, then that device is halted immediately (if it exists);
            pathList[slot] must be an empty list if that slot has no device
        @param[in] doAbsRefCorr  apply absolute fiducial corrections during this slew?
        @param[in] userCmd  user command whose set state is set to Done or Failed when all device commands are done;
            if None a new UserCmd is created and returned
        @param[in] timeLim  time limit for command (sec)
        @return userCmd: the supplied userCmd or a newly created UserCmd
        """
        if len(pathList) != len(self):
            raise RuntimeError("pathList must have %d items; pathList=%s" % (len(self), pathList))

        cmdDict = dict()
        for pvtList, (slot, dev) in itertools.izip(pathList, self._slotDevDict.iteritems()):
            if dev:
                if len(pvtList) > 0:
                    cmdDict[slot] = ["MOVE %0.7f %0.7f %0.5f" % (pvt.pos, pvt.vel, secInDayFromDate(pvt.t)) for pvt in pvtList]
                    if doAbsRefCorr:
                        cmdDict[slot] += ["MS.ON", "MS.OFF %0.5f" % (secInDayFromDate(pvtList[-1].t),)]
                else:
                    cmdDict[slot] = ["MOVE"]
        self.actor.queueStatus(StatusDelay)
        return self.startCmdDict(cmdDict=cmdDict, userCmd=userCmd, timeLim=timeLim)

    def status(self, slotList=None, userCmd=None, timeLim=DefaultTimeLim):
        """!Execute a STATUS command and update self.actor.obj

        @param[in] slotList  collection of slot names, or None for all filled and connected slots
        @param[in] userCmd  user command whose set state is set to Done or Failed when all device commands are done;
            if None a new UserCmd is created and returned
        @param[in] timeLim  time limit for command (sec)
        @return userCmd: the supplied userCmd or a newly created UserCmd

        @throw RuntimeError if slotList has empty or non-existent slots
        """
        self.showConnState(userCmd=userCmd)
        slotList = self.expandSlotList(slotList, connOnly=True)
        self.actor.queueStatus()
        self._currStatusCmd = userCmd
        return self.startCmd(cmdStrOrList="STATUS", slotList=slotList, userCmd=userCmd, timeLim=timeLim)

    def stop(self, slotList=None, userCmd=None, timeLim=DefaultTimeLim):
        """!Halt one or more axes and udpate self.actor.obj

        If any axis fails, INIT that axis and update self.actor.obj

        @param[in] slotList  collection of slot names, or None for all filled slots
        @param[in] userCmd  user command whose set state is set to Done or Failed when all device commands are done;
            if None a new UserCmd is created and returned
        @param[in] timeLim  time limit for command (sec)
        @return userCmd: the supplied userCmd or a newly created UserCmd

        @throw RuntimeError if slotList has empty or non-existent slots
        """
        # slotList = self.expandSlotList(slotList)
        # update axes state to halting
        self._setObjForInit(slotList=self.expandSlotList(slotList), halted=False)
        return self.startCmd(cmdStrOrList="MOVE", slotList=slotList, userCmd=userCmd, timeLim=timeLim)

    def _addDevCallbacks(self, dev, slot):
        """!Called when adding a device

        Called after device is registered in slot dictionary, but possibly before it is connected.
        Use to add callbacks to the device.

        @param[in] dev  device that has been removed
        @param[in] slot  slot the device occupied
        """
        DeviceSet._addDevCallbacks(self, dev, slot)
        dev.driftCallFunc = self._driftCallback
        dev.initCallFunc = self._initCallback
        dev.statusCallFunc = self._statusCallback

    def _removeDevCallbacks(self, dev, slot):
        """!Called when removing a device

        Called after the device is deregistered from the slot dictionarhy, but before it is disconnected.
        Use this to remove all callbacks from the device and clear information about it.

        @param[in] dev  device that has been removed
        @param[in] slot  slot the device occupied
        """
        DeviceSet._removeDevCallbacks(self, dev, slot)
        dev.driftCallFunc = None
        dev.initCallFunc = None
        dev.statusCallFunc = None

        ind = self.getIndex(slot)
        currTAI = tcc.base.tai()
        self.actor.obj.actMount[ind].invalidate(currTAI)
        self.actor.obj.axisStatusWord[ind] = 0
        self.actor.obj.axisStatusTime[ind] = currTAI

    def _initAllOnFailure(self, cmdInfo):
        """!Callback for a command that, if it fails, all axes should be initialized

        This will work with any command, though I suspect only the DRIFT command is sufficiently dangerous
        to justify initializing all axes if DRIFT fails for one axis. One might argument that MOVE P V T
        also qualifies, but at least the axis controller will halt an axis once time runs outo n a MOVE P V T,
        wheras it won't halt for DRIFT until the axis reaches a limit.

        @param[in] cmdInfo  info for device command, an instance of twistedActor.DevCmdInfo
        """
        if cmdInfo.devCmd.didFail:
            if not cmdInfo.userCmd.isDone:
                cmdInfo.userCmd.setState(
                    cmdInfo.userCmd.Failed,
                    textMsg = "%s %s failed, initializing ALL axes: %s" % \
                        (cmdInfo.devCmd.dev.name, cmdInfo.devCmd.cmdStr, cmdInfo.devCmd.getMsg()),
                )
            log.error("%s %s failed; initializing ALL axes" % (self, cmdInfo.devCmd))
            self.init()

    def _driftCallback(self, dev):
        """!Callback when device parses reply from DRIFT

        This callback does nothing unless this device is currently in use;
        that is why this function is in AxisDeviceSet instead of AxisDevice.
        """
        # print "%s._driftCallback(dev=%r)" % (self, dev)
        slot = self.slotFromDevName(dev.name)
        if slot is None:
            return

        ind = self.getIndex(slot)
        self.actor.obj.actMount[ind] = dev.status.pvt
        log.info("%s._driftCallback set self.actor.obj.actMount[%s] = %s" % (self, ind, self.actor.obj.actMount[ind]))

    def _setObjForInit(self, slotList, halted=True):
        """!Set command state, error code, and target mount for each axis
        in devList.

        @param[in] slotList: list of device slots for which to update state
        @param[in] halted: bool, if True set axes to halted, else set to Halting
        """
        # print("_setObjForInit", slotList, str(halted))
        axisState = tcc.base.AxisState_Halted if halted else tcc.base.AxisState_Halting
        for slot in slotList:
            ind = self.getIndex(slot)
            # print("setting axisState", ind, "to", axisState, "from", self.actor.obj.axisCmdState[ind])
            # print("setting axisErrCode", ind, "to", tcc.base.AxisErr_HaltRequested, "from", self.actor.obj.axisErrCode[ind])
            if self.actor.obj.axisCmdState[ind] != tcc.base.AxisState_Halted:
                # if the obj block is already halted, don't set the axis state.
                self.actor.obj.axisCmdState[ind] = axisState
            self.actor.obj.axisErrCode[ind] = tcc.base.AxisErr_HaltRequested
            self.actor.obj.targetMount[ind].invalidate(tcc.base.tai())
        msgCode, msgStr = formatAxisCmdStateAndErr(self.actor.obj)
        if msgStr:
            self.actor.writeToUsers(msgCode=msgCode, msgStr=msgStr)

    def _initCallback(self, dev):
        """!Callback when a device sends INIT or MOVE

        This callback does nothing unless this device is currently in use;
        that is why this function is in AxisDeviceSet instead of AxisDevice.
        """
        # print "%s._initCallback(dev=%r)" % (self, dev)
        slot = self.slotFromDevName(dev.name)
        if slot is None:
            return
        self._setObjForInit(slotList=[slot], halted=True)
        self.actor.queueStatus(1)

    def _statusCallback(self, dev):
        """!Callback when device parses new status

        This callback does nothing unless this device is currently in use;
        that is why this function is in AxisDeviceSet instead of AxisDevice.
        """
        # print "%s._statusCallback(dev=%r)" % (self, dev)
        slot = self.slotFromDevName(dev.name)
        if slot is None:
            return

        ind = self.getIndex(slot)
        self.actor.obj.actMount[ind] = dev.status.pvt
        self.actor.obj.axisStatusWord[ind] = dev.status.statusWord
        self.actor.obj.axisStatusTime[ind] = tcc.base.tai() # TCC's idea of time, not axis controllers
        prevRepStatus = self._prevReportedStatusList[ind]
        if self._currStatusCmd and not self._currStatusCmd.isDone:
            userCmd = self._currStatusCmd
        else:
            userCmd = None

        if userCmd or prevRepStatus.wordOrDTimeChanged(dev.status):
            msgCode, msgStr = dev.status.formatStatus(name=slot, axeLim=self.actor.axeLim)
            self._prevReportedStatusList[ind] = dev.status.copy()
            self.actor.writeToUsers(msgCode, msgStr, cmd=userCmd)
