from __future__ import division, absolute_import
"""
The SDSS axis controllers are implemented by the MCP, which accepts commands for all axes on a single port
(rather than one port per axis controller). You must wait until one command is done before sending the next.
This code handles the serialization of axis controller commands to the MCP.
"""
import string

from twistedActor import log, TCPDevice, expandUserCmd, LinkCommands
from RO.Comm.TwistedTimer import Timer

from tcc.axis import AxisDevice

__all__ = ["SDSSAxisDevice", "MCPMultiplexor"]

CharsToStrip = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\xfa\xfb\xfc\xfd\xfe\xff'

class MCPMultiplexor(TCPDevice):
    """!A TCPDevice that serializes commands from a set of SDSSAxisDevices
    """
    def __init__(self, name, host, port, callFunc=None):
        """!Construct an MCPMultiplexor

        @param[in] name  name of device
        @param[in] host  host address of Galil controller
        @param[in] port  port of Galil controller
        @param[in] callFunc  function to call when state of device changes;
                note that it is NOT called when the connection state changes;
                register a callback with "conn" for that task.
        """
        TCPDevice.__init__(self,
            name = name,
            host = host,
            port = port,
            callFunc = callFunc,
            cmdInfo = (),
        )
        self._initialSetup()

    def _initialSetup(self):
        """!Device-specific constructor code
        """
        self.sdssAxisDeviceList = []
        # index of current axis: the axis currently being commanded
        # initialize to -1 so that when cycleAxis is called for the first time
        # currentAxisIndex will be equal to 0
        self.currentAxisIndex = -1
        self.isBusy = False # if true we are awaiting replies

    def init(self, userCmd=None, timeLim=AxisDevice.DefaultInitTimeLim, getStatus=True):
        """!Initialize the multiplexor

        This is a no-op that immediately reports done, because the individual SDSSDevices handle init
        """
        # each axis device handles initializatio
        userCmd = expandUserCmd(userCmd)
        userCmd.setState(userCmd.Done)
        return userCmd

    @property
    def nAxes(self):
        """!The number of axes (SDSSAxisDevices)
        """
        return len(self.sdssAxisDeviceList)

    @property
    def axisNames(self):
        """!A list of axis names, in the order registered
        """
        return [axis.name for axis in self.sdssAxisDeviceList]

    def registerAxisDevice(self, sdssAxisDevice):
        """!Register an SDSSAxisDevice with the MCPDevice.

        This allows the MCP access to the AxisDevice's methods

        @param[in] sdssAxisDevice an instance of SDSSAxisDevice
        """
        if not isinstance(sdssAxisDevice, SDSSAxisDevice):
            raise RuntimeError("You may only register instances of SDSSAxisDevice with MCPDevice")
        self.sdssAxisDeviceList.append(sdssAxisDevice)

    def sendNextCmd(self):
        """!Look through the currentCmdDict for a command ready to go.
        """
        if self.isBusy:
            # do nothing, currently waiting on a command
            return
        for i in range(self.nAxes):
            # check each axis, starting with the current axis, for a command ready to run
            self.cycleAxis()
            if not self.currCmd.isDone:
                # note: currCmd will always be Running (the command queue in AxisDevice does that
                # automatically); we're just looking for commands that are not yet done
                self.isBusy = True
                # notify the axis device that it should listen to replies
                self.currCmd.addCallback(self.mcpCmdCallback)
                self.writeToMCP(self.currCmd.cmdStr)
                # Timer(0, self.currentAxisDevice.sendCmdSuper, self.currCmd)
                log.info("multiplexor starting with %s for axis: %s"%(self.currCmd, self.currentAxisName))
                break

    def writeToMCP(self, line, prependAxis=True):
        """!Send a command to the MCP

        @param[in] line  command to send (not including a trailing newline)
        @param[in] prependAxis  if True then prepend the axis name and a space (and strip it from the command
            echo, later)
        """
        # prepend the axis name
        if prependAxis:
            line = self.currentAxisDevice.prefixStr + line
        log.info("%s writing %r" % (self, line))
        self.conn.writeLine(line)

    def handleReply(self, reply):
        """!Handle a reply from the MCP
        """
        reply = reply.translate(string.maketrans("",""), CharsToStrip)
        # strip axis name prefix, if present
        if reply and reply.startswith(self.currentAxisDevice.prefixStr):
            reply = reply[len(self.currentAxisDevice.prefixStr):]
        self.currentAxisDevice.handleReply(reply)

    def mcpCmdCallback(self, cmd):
        """!Callback for the currently executing command

        When the command finishes (whether successfully or not) put the multiplexor and current axis device
        in an appropriate state and trigger the next command (if one is ready)

        @param[in] cmd  currently executing command
        """
        if cmd.isDone:
            self.isBusy = False
            log.info("multiplexor done with %s for axis: %s"%(cmd, self.currentAxisName))
            # Use a timer to allow all callbacks from this command to finish before starting the next command
            Timer(0, self.sendNextCmd)

    @property
    def currCmd(self):
        """!Return the currently executing command (if it is done then no command is executing)
        """
        return self.currentAxisDevice.currExeCmd.cmd

    @property
    def currentAxisDevice(self):
        """!The current SDSSAxisDevice
        """
        return self.sdssAxisDeviceList[self.currentAxisIndex]

    @property
    def currentAxisName(self):
        """!The name of the current axis
        """
        return self.currentAxisDevice.name

    def cycleAxis(self):
        """!Increment the current axis index, in order 0-->1-->2-->0....
        """
        self.currentAxisIndex = (self.currentAxisIndex+1)%self.nAxes
        log.info("new current axis: %s"%self.currentAxisName)


class SDSSAxisDevice(AxisDevice):
    def __init__(self, name, mcpMultiplexor):
        """!Construct an AxisDevice

        Inputs:
        @param[in] name  name of device
        @param[in] mcpMultiplexor  an instance of MCPMultiplexor.
            This device adopts the connection behavior from the mcpMultiplexor
        """
        AxisDevice.__init__(self, name, host=None, port=None)
        if type(mcpMultiplexor) != MCPMultiplexor:
            raise RuntimeError("SDSSAxisDevice must be initialized with an MCPMultiplexor instance")
        mcpMultiplexor.registerAxisDevice(self)
        self.mcpMultiplexor = mcpMultiplexor
        self.conn = self.mcpMultiplexor.conn
        self.conn.addStateCallback(self._connCallback, callNow=True)
        if self.name.lower().startswith("rot"):
            self.prefixStr = "ROT "
        else:
            self.prefixStr = self.name.upper() + " "


    def init(self, userCmd=None, timeLim=AxisDevice.DefaultInitTimeLim, getStatus=True):
        """!Initialize the device and cancel all pending commands

        @param[in] userCmd  user command that tracks this command, if any
        @param[in] timeLim  maximum time before command expires, in sec; None for no limit
        @param[in] getStatus  if true then get status after init
        @return userCmd (a new one if the provided userCmd is None)

        Note: overridden from base class to include SET.TIME (mcp-specific requirement)
        """
        userCmd = expandUserCmd(userCmd)
        log.info("%s.init(userCmd=%s, timeLim=%s, getStatus=%s)" % (self, userCmd, timeLim, getStatus))
        print("%s.init(userCmd=%s, timeLim=%s, getStatus=%s)" % (self, userCmd, timeLim, getStatus))
        if getStatus:
            # put SET.TIME and STATUS on queue after init
            # SET.TIME is requried for the MCP (the 3.5m axes ignore the command)
            # SET.TIME is a vistigial command from when the TCC had to provide the
            # axes with the correct time.  Now it is sent without any argument
            # because the MCP has the ability to get its own time
            # but it will only do so if you tell it to.
            # The better fix would be for the MCP to get its time automatically
            # with every init...

            # put a status on the queue after the init. Use StatusInitVerb
            # to run the status at high priority, so it cannot be interrupted by a MOVE
            # of similar command, which would make the returned userCmd fail
            devCmd0 = self.startCmd("INIT", timeLim=timeLim)
            devCmd1 = self.startCmd("SET.TIME", timeLim=timeLim)
            devCmd2 = self.startCmd("STATUS", timeLim=timeLim, devCmdVerb=self.StatusInitVerb)
            LinkCommands(mainCmd=userCmd, subCmdList=[devCmd0, devCmd1, devCmd2])
        else:
            self.startCmd("INIT", userCmd=userCmd, timeLim=timeLim)
        return userCmd


    @property
    def currExeCmd(self):
        """Convenience, grab the command queue's currently executing command.
        multiplexor looks here for commands to send to the mcp, and judge completion
        """
        return self.cmdQueue.currExeCmd

    def writeToDev(self, line):
        """! now handled by multiplexor, do nothing here
        """
        self.mcpMultiplexor.sendNextCmd()
        # pass

