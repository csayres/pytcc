from __future__ import absolute_import # division fails with SWIG 2

import tcc.base
from .obsFromGProbe import obsFromGProbe

__all__ = ["obsFromGImage"]

def obsFromGImage(targGImageXY, gProbeNum, obj, inst, taiDate):
    """!Compute observed coordinates from guide probe coordinates

    @param[in] targGImageXY  position of target in guide image coordinates as two PVTs (unbinned x,y pixels)
    @param[in] gProbeNum  guide probe number
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] inst  the instrument block; the following fields are read:
    - gim_scale
    - gProbe
    - rot_inst_xy
    - rot_inst_ang
    @param[in] taiDate  TAI date at which to compute the result (MJD, sec)

    @return a tuple:
    - targObs: position of target in observed coordinates (a coordConv.PVTCoord)
    - targAzGProbeAng: angle of direction of increasing azimuth at target in guide probe coordinates (a coordConv.PVT)
    """
    try:
        gProbeInfo = inst.gProbe[gProbeNum-1]
        assert gProbeInfo.exists
    except Exception:
        raise RuntimeError("Guide probe %d does not exist" % (gProbeNum,))
    
    targGProbeXY = tcc.base.ArrayPVT2()
    for i in range(2):
        targGProbeXY[i] = (targGImageXY[i] - gProbeInfo.ctr[i]) / inst.gim_scale[i]

    return obsFromGProbe(targGProbeXY, gProbeNum, obj, inst, taiDate)
