from __future__ import division, absolute_import

import sys

import numpy

import tcc.base
from tcc.mov import checkPos

__all__ = ["computeObj"]

def computeObj(
    obj,
    doWrap,
    doRestart,
    reportLim,
    earth,
    inst,
    telMod,
    axeLim,
    tai,
):
    """!Compute all computed obj fields from user-specified fields except path generator outputs

    @param[in,out] obj  object data
        Input fields include all user inputs and current state
        Ouput fields include all computed fields
    @param[in] doWrap  if True, obey ojb.wrapPref else force nearest wrap
    @param[in] doRestart (3 bools) compute new position for (az, alt, rot) axis, if halted?
    @param[in] reportLim  report hitting a position limit? WARNING: IGNORED AND LIKELY TO GO AWAY
    @param[in] earth  earth orientation predictions (tcc.base.Earth)
    @param[in] inst  instrument block (tcc.base.Inst)
    @param[in] telMod  telescope model (tcc.base.TelMod)
    @param[in] axeLim  axes limits (tcc.base.AxeLim)
    @param[in] tai  TAI date at which to compute netUserPos and other values (MJD, seconds)
    """
    oldMountList = tuple(pvt.copy() for pvt in obj.targetMount)
    
    earth.updateSite(obj.site, tai)
    earth.updateSite(obj.gsSite, tai)

    tcc.base.computeNetUser(obj, tai)
    
    tcc.base.obsFromNetUser(obj, tai)
    
    tcc.base.mountFromObs(obj, inst, telMod, tai)
    
    if doWrap:
        wrapPrefList = (obj.azWrapPref, tcc.base.WrapType_None, obj.rotWrapPref)
    else:
        wrapPrefList = (tcc.base.WrapType_Nearest, tcc.base.WrapType_None, tcc.base.WrapType_Nearest)
        
    if inst.hasRotator():
        axisIndList = range(3)
    else:
        obj.targetMount[2].invalidate(tai)
        obj.axisCmdState[2] = tcc.base.AxisState_NotAvailable
        obj.axisErrCode[2] = tcc.base.AxisErr_NotAvailable
        axisIndList = range(2)

    for i in axisIndList:    
        oldMount = oldMountList[i]

        # keep halted axes halted, unless doRestart
        if not oldMount.isfinite():
            if obj.axisCmdState[i] == tcc.base.AxisState_Halting:
                # oldMount is NaN while halting; don't worry about it
                pass
            elif not doRestart[i]:
                # halt new axis and make sure errCode set
                obj.targetMount[i].invalidate(tai)
                obj.axisCmdState[i] = tcc.base.AxisState_Halted
                if obj.axisErrCode[i] == 0:
                    sys.stderr.write("Warning: old axis %d halted, but error code = 0; setting to NoRestart\n" % (i,))
                    obj.axisErrCode[i] = tcc.base.AxisErr_NoRestart
          
        # apply wrap and position limits to moving axes
        if obj.targetMount[i].isfinite():
            wrapPref = wrapPrefList[i]
            nearPos = oldMount.getPos(tai)
            if not numpy.isfinite(nearPos) and wrapPref == tcc.base.WrapType_Nearest:
                # set nearPos to last known position; if that is also unknown then use middle wrap
                nearPos = obj.actMount[i].getPos(tai)
                if not numpy.isfinite(nearPos):
                    sys.stderr.write("Warning: near wrap wanted but current position unknown; using middle\n")
                    wrapPref = tcc.base.WrapType_Middle
                
            obj.targetMount[i].pos, obj.axisErrCode[i] = checkPos(
                pos = obj.targetMount[i].pos,
                minPos = axeLim.minPos[i],
                maxPos = axeLim.maxPos[i],
                nearPos = nearPos,
                wrapPref = wrapPref,
                doWrap = doWrap,
            )

    obj.updateTime = tai
