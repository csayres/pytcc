from __future__ import division, absolute_import

from twistedActor import log

import tcc.base
from tcc.mov import checkSegment

__all__ = ["checkSlewPath"]

def checkSlewPath(errCode, doAxis, pathList, doPos, doVel, doAccel, doJerk, axeLim, inst):
    """!Check a slew to make sure each segment is in bounds

    @param[in,out] errCode  error code for each axis:
        if errCode = tcc.base.AxisErr_OK and doAxis True, then check path and set errCode accordingly;
        otherwise leave errCode alone and ignore the other inputs for this axis
    @param[in] doAxis   check axis? (a list of 3 bools, one per axis)
    @param[in] pathList list of 3 pvtLists (paths), one per axis
    @param[in] doPos    test position?
    @param[in] doVel    test velocity?
    @param[in] doAccel  test acceleration?
    @param[in] doJerk   test jerk?
    @param[in] axeLim   axes limits block (a tcc.base.AxeLim)
    @param[in] inst     instrument-position information block (a tcc.base.Inst)

    History:
    2013-12-09 ROwen    Converted from axe_checkPath.for
    """
    # check each node for each axis
    for axis in range(tcc.base.NAxes):
        if errCode[axis] != tcc.base.AxisErr_OK or not doAxis[axis]:
            continue

        pvtList = pathList[axis]
        if len(pvtList) < 2:
            # not enough data points; give up
            logMsg = "Not enough data points for axis %i\ncheckSlewPath(errCode=%s, doAxis=%s, pathList=%s, doPos=%s, doVel=%s, doAccel=%s, doJerk=%s, axeLim=%s, inst)\nDumping inst block:\n%s"%(axis, str(errCode), str(doAxis), str(pathList), str(doPos), str(doVel), str(doAccel), str(doJerk), str(axeLim), str(inst))
            log.error(logMsg)
            errCode[axis] = tcc.base.AxisErr_CannotCompute
        elif any(not pvt.isfinite() for pvt in pvtList):
            logMsg = "In infinite pvt for axis %i\ncheckSlewPath(errCode=%s, doAxis=%s, pathList=%s, doPos=%s, doVel=%s, doAccel=%s, doJerk=%s, axeLim=%s, inst)\nDumping inst block:\n%s"%(axis, str(errCode), str(doAxis), str(pathList), str(doPos), str(doVel), str(doAccel), str(doJerk), str(axeLim), str(inst))
            log.error(logMsg)
            errCode[axis] = tcc.base.AxisErr_CannotCompute
        else:
            for i in range(len(pvtList) - 1):
                pvt0 = pvtList[i]
                pvt1 = pvtList[i+1]
                errCode[axis] = checkSegment(
                    pA=pvt0.pos,
                    vA=pvt0.vel,
                    pB=pvt1.pos,
                    vB=pvt1.vel,
                    dt=pvt1.t - pvt0.t,
                    doPos=doPos,
                    doVel=doVel,
                    doAccel=doAccel,
                    doJerk=doJerk,
                    axisLim=axeLim[axis],
                )
            if errCode[axis] != tcc.base.AxisErr_OK:
                break
