from __future__ import division, absolute_import

import coordConv

import tcc.base
from .instFromObs import instFromObs
from .gProbeFromObs import gProbeFromObs
from .gImageFromObs import gImageFromObs
from .ptCorrFromObs import ptCorrFromObs
from .rotFromObs import rotFromObs
from .obsFromInst import obsFromInst
from .obsFromGProbe import obsFromGProbe
from .obsFromGImage import obsFromGImage
from .obsFromPtCorr import obsFromPtCorr
from .obsFromRot import obsFromRot

__all__ = ["coordConvWithFP"]

FPNames = set(("instrument", "gprobe", "gimage", "ptcorr", "rotator"))
DeltaT = 0.01

def coordConvWithFP(fromCoordSys, fromPVTPair, fromPxPMRadVel, fromDir, toCoordSys, obj, inst, zeroPM, useGSWavelen):
    """!Convert a coord from one coordinate system to another
    
    The conversion is performed at obj.updateTime; you may wish to call computeObj
    before calling this subroutine to set that to your desired date.

    @param[in] fromCoordSys  from coordinate system; a coordConv.CoordSys
        if one matches, else an object containing getName() and getDate()
        (for gimage and gprobe coordinates the date is the guide probe number)
    @param[in] fromPVTPair  from position (equatorial or focal plane x,y, depending on fromSys)
    @param[in] fromPxPMRadVel  from parallax, proper motion and radial velocity
    @param[in] fromDir  from direction as a coordConv.PVT
    @param[in] toCoordSys  to coordinate system (a coordConv.CoordSys)
    @param[in] obj  object block computed at desired desired TAI date of conversion
    @param[in] inst  instrument block
    @param[in] zeroPM  zero proper motion and parallax to the current date?
        Ignored unless fromCoordSys and toCoordSys are both mean, since otherwise proper motion is always zeroed.
    @param[in] useGSWavelen  use guide star wavelength? else use object wavelength

    @return a tuple:
    - toPVTPair: resulting pair of PVTs (equatorial or x,y, depending on toSys)
    - toPxPMRadVel: resulting parallax, proper motion and radial velocity
    - toDir: angle of fromDir in the "to" system, as a coordConv.PVT
    - scaleChange: change in scale (a float): output delta sky/input delta sky, measured along the specified direction
    """
    taiDate = obj.updateTime
    # print "coordConvWithFP(fromCoordSys=%s, fromPVTPair=%s,\n  fromPxPMRadVel=%s,\n  fromDir=%s, toCoordSys=%s, zeroPM=%s, useGSWavelen=%s), obj.updateTime=%0.7f" % \
    #     (fromCoordSys, fromPVTPair, fromPxPMRadVel, fromDir, toCoordSys, zeroPM, useGSWavelen, obj.updateTime)
    obj = tcc.base.Obj(obj) # make a copy so I can change obj.site if useGSWavelen

    if fromCoordSys.isMean() and toCoordSys.isMean() and not zeroPM:
        # convert directly to avoid zeroing proper motion
        # safe to do before handling useGSWavelen because wavelength is not used for mean->mean conversion
        fromPVTCoord = tcc.base.pvtCoordFromPVTPair(fromPVTPair, fromPxPMRadVel)
        toDir = coordConv.PVT()
        toPVTCoord, scaleChange = toCoordSys.convertFrom(toDir, fromCoordSys, fromPVTCoord, fromDir, obj.site)
        toPVTPair, toPxPMRadVel = tcc.base.pvtPairFromPVTCoord(toPVTCoord)

        return toPVTPair, toPxPMRadVel, toDir, scaleChange

    if useGSWavelen:
        obj.site = obj.gsSite

    # compute "intermediate from" position and coordinate system:
    # - observed if fromSys is a focal-plane coordinate system (instrument, rotator or gprobe)
    # - the original fromPVTCoord and fromCoordSys otherwise
    fromSysName = fromCoordSys.getName().lower()
    if fromSysName in FPNames:
        intFromSys = obj.obsSysPtr

        if fromSysName == "instrument":
            intFromPVTCoord, fromAzIntAng = obsFromInst(fromPVTPair, obj, taiDate)
        elif fromSysName == "gimage":
            gProbeNum = int(fromCoordSys.getDate())
            intFromPVTCoord, fromAzIntAng = obsFromGImage(fromPVTPair, gProbeNum, obj, inst, taiDate)
        elif fromSysName == "gprobe":
            gProbeNum = int(fromCoordSys.getDate())
            intFromPVTCoord, fromAzIntAng = obsFromGProbe(fromPVTPair, gProbeNum, obj, inst, taiDate)
        elif fromSysName == "ptcorr":
            intFromPVTCoord, fromAzIntAng = obsFromPtCorr(fromPVTPair, obj, inst, taiDate)
        elif fromSysName == "rotator":
            intFromPVTCoord, fromAzIntAng = obsFromRot(fromPVTPair, obj, inst, taiDate)
        else:
            raise RuntimeError("Bug: unknown from focal plane coordinate system %s" % (fromCoordSys,))
        intFromDir = fromDir - fromAzIntAng

    else:
        # intFrom = from (null conversion)
        intFromSys = fromCoordSys
        intFromPVTCoord = tcc.base.pvtCoordFromPVTPair(fromPVTPair, fromPxPMRadVel)
        intFromDir = fromDir

    # compute final position and coordinate system
    toSysName = toCoordSys.getName().lower()    
    if toSysName in FPNames:
        # compute "intermediate to" position in observed coordinates
        intToSys = obj.obsSysPtr
        intToDir = coordConv.PVT()
        intToPVTCoord, scaleChange = intToSys.convertFrom(intToDir, intFromSys, intFromPVTCoord, intFromDir, obj.site)

        # compute final position in focal plane coordinates
        if toSysName == "instrument":
            toPVTPair, targAzToAng = instFromObs(intToPVTCoord, obj, taiDate)
        elif toSysName == "gprobe":
            gProbeNum = int(toCoordSys.getDate())
            toPVTPair, targAzToAng = gProbeFromObs(intToPVTCoord, gProbeNum, obj, inst, taiDate)
        elif toSysName == "gimage":
            gProbeNum = int(toCoordSys.getDate())
            toPVTPair, targAzToAng = gImageFromObs(intToPVTCoord, gProbeNum, obj, inst, taiDate)
        elif toSysName == "ptcorr":
            toPVTPair, targAzToAng = ptCorrFromObs(intToPVTCoord, obj, inst, taiDate)
        elif toSysName == "rotator":
            toPVTPair, targAzToAng = rotFromObs(intToPVTCoord, obj, inst, taiDate)
        else:
            raise RuntimeError("Bug: unknown to focal plane coordinate system %s" % (toCoordSys,))

        toPxPMRadVel = tcc.base.PxPMRadVel()
        toDir = intToDir + targAzToAng

    else:
        # convert directly from intermediate from system to final to system
        toDir = coordConv.PVT()
        toPVTCoord, scaleChange = toCoordSys.convertFrom(toDir, intFromSys, intFromPVTCoord, intFromDir, obj.site)
        toPVTPair, toPxPMRadVel = tcc.base.pvtPairFromPVTCoord(toPVTCoord)

    return toPVTPair, toPxPMRadVel, toDir, scaleChange
