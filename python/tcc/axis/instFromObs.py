from __future__ import division, absolute_import

import coordConv

import tcc.base

__all__ = ["instFromObs"]

def instFromObs(targObs, obj, taiDate):
    """!Compute instrument position from observed coordinates

    @param[in] targObs  position of target in observed coordinates (a coordConv.PVTCoord)
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] taiDate  TAI date at which to compute (MJD, sec)

    @return a tuple:
    - targInstXY: position of target in instrument coordinates (a tcc.base.ArrayPVT2) (deg)
        i.e. a position in instrument X,Y relative to inst.iim_ctr, the center of the instrument
    - targAzInstAng: angle of direction of increasing azimuth at target in instrument coordinates (a coordConv.PVT)
    """
    # compute the instrument-plane offset from boresight to target
    targBoreDist = obj.obsPos.angularSeparation(targObs)
    boreToTargAzAng = obj.obsPos.orientationTo(targObs)
    boreToTargInstAng = boreToTargAzAng + obj.objAzInstAng
    if not boreToTargInstAng.isfinite() and abs(targBoreDist.pos) < 1:
        # target is fixed at boresight (dist < 1 avoids the case dist=180)
        targObjXY = tcc.base.ArrayPVT2()
        for i in range(2):
            targObjXY[i] = obj.objInstXY[i].copy(taiDate)
        return targObjXY, obj.objAzInstAng.copy(taiDate)
    else:
        targObjXY = tcc.base.ArrayPVT2()
        coordConv.xyFromPolar(targObjXY[0], targObjXY[1], targBoreDist, boreToTargInstAng, taiDate)

        targInstXY = tcc.base.ArrayPVT2()
        for i in range(2):
            targInstXY[i] = targObjXY[i] + obj.objInstXY[i]

        # now compute targAzInstAng, e.g. by using orientationTo from target to boresight
        targToBoreAzAng = targObs.orientationTo(obj.obsPos)
        targAzInstAng = coordConv.wrapCtr(boreToTargAzAng + 180.0 - targToBoreAzAng + obj.objAzInstAng)
        return targInstXY, targAzInstAng
