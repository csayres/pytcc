from __future__ import division, absolute_import

import coordConv

import tcc.base
from .instFromObs import instFromObs

__all__ = ["rotFromObs"]

def rotFromObs(targObs, obj, inst, taiDate):
    """!Compute instrument position from observed coordinates

    @param[in] targObs  position of target in observed coordinates (a coordConv.PVTCoord)
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] inst  the instrument block; the following fields are read:
    - rot_inst_xy
    - rot_inst_ang
    @param[in] taiDate  TAI date at which to compute the result (MJD, sec)

    @return a tuple:
    - targRotXY: position of target in rotator coordinates (a tcc.base.ArrayPVT2) (deg)
    - targAzRotAng: angle of direction of increasing azimuth at target in rotator coordinates (a coordConv.PVT)
    """
    # compute position of target in instrument frame
    targInstXY, targAzInstAng = instFromObs(targObs, obj, taiDate)
    
    # compute targRotXY in two stages:
    # - compute targRot_instXY: vector from rotator to target, in instrument frame
    # - rotate that vector into the guide probe frame
    targRot_instXY = tcc.base.ArrayPVT2()
    targRot_instXY[:] = [targInstXY[i] - inst.rot_inst_xy[i] for i in range(2)]
    targRotXY = tcc.base.ArrayPVT2()
    coordConv.rot2D(targRotXY[0], targRotXY[1], targRot_instXY[0], targRot_instXY[1], -inst.rot_inst_ang, taiDate)

    targAzRotAng = targAzInstAng - inst.rot_inst_ang
    return targRotXY, targAzRotAng
