from __future__ import division, absolute_import

import re

from coordConv import PVT
from twisted.internet import defer
from twisted.python import failure
from RO.Comm.TwistedSocket import TCPServer

from tcc.base import tai
from tcc.util import daySecFromDate, dateFromSecInDay, RunningStats

__all__ = ["FakeAxisCtrl",]

sampleMSDump ="""time                position    velocity   error    edge  correction
    1266860307.600     74.814029    0.241533   0.000954   1    0.000524
    1266816131.180     59.597493    1.485286  -0.003319   0    0.000000
    1266816131.330     59.819334    1.485286  -0.000797   1   -0.002057
    1266839360.530     59.810348   -1.376835   0.006240   0    0.000000
    1266839360.680     59.590300   -1.376835   0.001925   1    0.004082
    1266839369.990     44.817815   -1.607564   0.012479   0    0.000000
    1266839370.120     44.606302   -1.605523  -0.007002   1    0.002738
    1266860253.490     44.615251    0.252483  -0.014001   0    0.000000
    1266860254.160     44.825963    0.252483   0.006281   1   -0.003860
    1266860268.360     59.597959    1.462901  -0.003784   0    0.000000
    1266860268.510     59.821265    1.462901  -0.002728   1   -0.003256
    1266860306.680     74.595496    0.336560   0.000096   0    0.000000
    1266860307.600     74.814029    0.241533   0.000954   1    0.000524"""

sampleID = "axis code: 2009-10-16 379M, axis.ini version $Revision: 1.41 $"

class FakeAxisCtrl(TCPServer):
    """!A server that emulates an axis controller
    """
    def __init__(self, name, port, logReplies=True):
        """!Construct a fake axis controller

        @param[in] name  name of axis controller
        @param[in] port  port on which to command axis controller
        @param[in] logReplies  log all replies? Set False for less chatter
        """
        self.name = name
        self.logReplies = bool(logReplies)
        self.readyDeferred = defer.Deferred()

        self.statusWord = 0
        self.cmdPVT = PVT(10, 0, tai()) # last commanded PVT; start with a reasonable value
        self.lastTAI = None
        self.advTimeStats = RunningStats(bufferSize=1000)

        self.driftRE = re.compile("(?:[A-Z]+ +)?DRIFT")
        self.movePVTRE   = re.compile("(?:[A-Z]+ +)?MOVE +([0-9.+-]+) +([0-9.+-]+) +([0-9.+-]+)")
        self.statusRE = re.compile("(?:[A-Z]+ +)?STATUS")
        self.stopRE   = re.compile("(?:[A-Z]+ +)?MOVE")
        self.msDumpRE = re.compile("(?:[A-Z]+ +)?MS\.DUMP")
        self.idRE = re.compile("(?:[A-Z]+ +)?ID")
        self.isDead = False
        self.isSlow = False

        TCPServer.__init__(self,
            port=port,
            stateCallback=self.stateCallback,
            sockReadCallback = self.sockReadCallback,
            sockStateCallback = self.sockStateCallback,
        )

    def logMsg(self, msgStr):
        """!Log a message
        """
        print msgStr

    @property
    def cmdPVT(self):
        return self._cmdPVT

    @cmdPVT.setter
    def cmdPVT(self, pvt):
        """!Update self.cmdPVT

        Use a setter so that the widget version can override this method and display the new PVT
        """
        self._cmdPVT = pvt

    def addAdvTime(self, advTime):
        """!Add an advanced time to the advanced time stats

        @param[in] advTime  advanced time of a MOVE P V T command (sec)
        """
        self.advTimeStats.addValue(advTime)

    def parseCmd(self, cmdStr):
        """!Parse a command and return the reply, if any

        @param[in] cmdStr  command string
        @return reply, as a string, or None if no reply
        """
        cmdStr = cmdStr.upper()
        moveMatch = self.movePVTRE.match(cmdStr)
        if moveMatch:
            if self.lastTAI is not None:
                advTime = self.lastTAI - tai()
                # print "%4s advTime=%0.2f" % (self.name, advTime)
                self.addAdvTime(advTime)
            # else:
            #     print self.name, "%4s advTime=? (first MOVE PVT of a slew)" % (self.name,)
            valStrList = moveMatch.groups()
            pos, vel, secInDay = [float(val) for val in valStrList]
            time = dateFromSecInDay(secInDay)
            self.cmdPVT = PVT(pos, vel, time)
            self.lastTAI = time
            return None

        statusMatch = self.statusRE.match(cmdStr)
        if statusMatch:
            currTAI = tai()
            currDay, currSecInDay = daySecFromDate(currTAI)
            currPos = self.cmdPVT.getPos(currTAI)
            return "%0.7f %0.7f %0.7f %d %0.7f" % (
                currPos,
                self.cmdPVT.vel,
                currSecInDay,
                self.statusWord,
                0,
            )

        stopMatch = self.stopRE.match(cmdStr)
        if stopMatch:
            self.stop()
            return None

        driftMatch = self.driftRE.match(cmdStr)
        if driftMatch:
            self.lastTAI = None
            currTAI = tai()
            currDay, currSecInDay = daySecFromDate(currTAI)
            currPos = self.cmdPVT.getPos(currTAI)
            return "%0.7f %0.7f %0.7f" % (
                currPos,
                self.cmdPVT.vel,
                currSecInDay,
            )

        msDumpMatch = self.msDumpRE.match(cmdStr)
        if msDumpMatch:
            return sampleMSDump

        idMatch = self.idRE.match(cmdStr)
        if idMatch:
            return sampleID

        if "DIE" in cmdStr:
            self.isDead = True

        if "LIVE" in cmdStr:
            self.isDead = False

        if "SLOW" in cmdStr:
            self.isSlow = True

        if "FAST" in cmdStr:
            self.isSlow = False

    def stop(self):
        currTAI = tai()
        stoppedPVT = self.cmdPVT.copy(currTAI)
        stoppedPVT.vel = 0
        self.cmdPVT = stoppedPVT
        self.lastTAI = None

    def sockReadCallback(self, sock):
        cmdStr = sock.readLine()
        if cmdStr is None:
            sock.close()
            return
        outMsgList = [cmdStr]
        outMsg = self.parseCmd(cmdStr)
        if outMsg:
            outMsgList.append(outMsg)
        outMsgList[-1] = outMsgList[-1] + " OK"
        for outMsg in outMsgList:
            if self.logReplies:
                self.logMsg(outMsg)
            sock.writeLine("\x00" + outMsg)
            if self.isDead or self.isSlow:
                # break after first reply (the reply from the terminal server)
                break

        # slowly send back remaining replies on a timer if "slow"
        if self.isSlow:
            # waste time before next reply
            from twisted.internet import reactor
            for outMsg in outMsgList[1:]:
                # first reply was sent above
                reactor.callLater(8, sock.writeLine, "\x00" + outMsg)


    def sockStateCallback(self, sock):
        if sock.state == sock.Connected:
            self.logMsg("Client at %s connected" % (sock.host))
        elif sock.state == sock.Closed:
            self.logMsg("Client at %s disconnected" % (sock.host))
            # in real life the buffer would run out and the axis would stop
            # an instant stop is simpler for this fake controller
            # and allows a more realistic measurement of track advance time
            # while disconnecting the client to work on it
            self.stop()

    def stateCallback(self, server=None):
        if self.isReady:
            self.readyDeferred.callback(None)
            self.logMsg("Fake axis controller %s running on port %s" % (self.name, self.port))
        elif self.didFail and not self.readyDeferred.called:
            errMsg = "Fake axis controller %s failed to start on port %s" % (self.name, self.port)
            self.logMsg(errMsg)
            self.readyDeferred.errback(failure.Failure(RuntimeError(errMsg)))
