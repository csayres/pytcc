from __future__ import division, absolute_import
"""Position Reference Catalog
"""
import bisect
import sys
import traceback

import numpy
from RO.StringUtil import strFromException
from coordConv import makeCoordSys, PVT

import tcc.base
from .computeObj import computeObj

__all__ = ["PosRefCatalog"]

class StarData(object):
    def __init__(self, dataList, tai):
        """!Construct a StarData from a position reference star catalog entry

        @param[in] dataList  list of string data from a position reference star catalog:
            equatAng    polarAng    parallax  equatPM  polarPM  Vrad   mag   starName
            (deg)        (deg)        (")     ("/cy)   ("/cy)  (km/s)
        @param[in] tai  TAI date for PVTs
        """
        self.name = dataList[7]
        dataList = [float(val) for val in dataList[0:-1]]
        self.mag = dataList[6]
        self.pvtPair = tcc.base.ArrayPVT2()
        self.pvtPair[0] = PVT(dataList[0], 0, tai)
        self.pvtPair[1] = PVT(dataList[1], 0, tai)
        self.pxPMRadVel = tcc.base.PxPMRadVel(*dataList[2:6])

    @property
    def polarAng(self):
        return self.pvtPair[1].pos

    def __repr__(self):
        return "StarData(name=%s, pos=%0.6f, %0.6f)" % (self.name, self.pvtPair[0].pos, self.pvtPair[1].pos)


class PosRefCatalog(object):
    """!Position reference catalog

    Reads a catalog file whose format is as follows:
    Blank lines and lines starting with # are ignored
    First line of data: coordSysName date of observation
    All remaining lines of data:
    equatAng    polarAng    parallax  equatPM  polarPM  Vrad   mag   starName
    (deg)        (deg)        (")     ("/cy)   ("/cy)  (km/s)

    where:
    - equatPM is d(equatAng)/dt (i.e. it gets large near the poles)
    - Vrad is positive receding

    The data is stored in order of increasing polar angle.
    """
    def __init__(self, catPath):
        """!Construct an PosRefCatalog

        @param[in] catPath  path to catalog file
        """
        self.coordSys = None
        self._starDataList = [] # list of (polarAngle, starData)
        constrTAI = tcc.base.tai()

        with open(catPath, "rU") as catFile:
            for line in catFile:
                line = line.strip()
                if not line or line[0] == "#":
                    continue
                coordSysNameDate = line.split()
                if len(coordSysNameDate) != 2:
                    raise RuntimeError("Could not parse %r as coordSys date" % (line,))
                self.coordSys = makeCoordSys(coordSysNameDate[0].lower(), float(coordSysNameDate[1]))
                break

            for line in catFile:
                line = line.strip()
                if not line or line[0] == "#":
                    continue
                data = line.split()
                try:
                    starData = StarData(data, constrTAI)
                except Exception:
                    sys.stderr.write("PosRefCatalog: cannot parse %r in %r; skipping" % (line, catPath))
                self._starDataList.append(starData)

        if len(self._starDataList) == 0:
            raise RuntimeError("No stars in catalog")

        self._starDataList.sort(key=lambda starData: starData.polarAng)
        self._polarAngList = [sd.polarAng for sd in self._starDataList]

    def __len__(self):
        return len(self._starDataList)

    def __getitem__(self, ind):
        """!Return the item at the specified index

        @param[in] ind  integer index
        @return (polarAngle, mag, coord)
        """
        return self._starDataList[ind]

    def findStar(self, tccActor, magRange, tai, obj=None, userCmd=None):
        """!Find the nearest star whose brightness is in the given range

        @param[in] tccActor  an instance of TCCActor
        @param[in] magRange  minimum and maximum magnitude; order doesn't matter,
            to avoid confusion between minimum meaning numerically smaller or fainter.
        @param[in] tai  TAI date at which to compute (typically nearly the current date) (MJD, seconds)
        @param[in] obj  object block of target; if None then tccActor.obj is used.
            obj.userPos, obj.userPxPMRadVel and userSys must be set to the target position and coordSys
            and the obj block must be updated for the target position.
        @param[in] userCmd  user command (for warning messages) or None
        @return a StarData for the found star
        Note that PosRefCatalog.coordSys gives the coordinate system of the returned star.

        The algorithm is to search backwards and forwards in the list of stars,
        starting from the star nearest in polar angle to the target.
        When delta declination > closest found star, the search ends.
        """
        minDist = 360.0 # minimum az/alt distance found so far, in degrees
        minMag, maxMag = sorted(magRange)
        bestInd = None
        if obj is None:
            obj = tcc.base.Obj(tccActor.obj)
        else:
            obj = tcc.base.Obj(obj)
        targetMount = tuple(pvt.copy() for pvt in obj.targetMount)
        obj.azWrapPref = tcc.base.WrapType_Nearest
        obj.rotWrapPref = tcc.base.WrapType_Nearest
        azAltInd = range(2)
        userPVTCoord = tcc.base.pvtCoordFromPVTPair(obj.userPos, obj.userPxPMRadVel)
        targetPos = self.coordSys.convertFrom(obj.userSys, userPVTCoord, obj.site)
        targetPolarAng = targetPos.getCoord().getSphPos()[-1]
        # print "targetPos=", targetPos.getCoord(), "(ICRS)"
        # print "targetMount=", targetMount

        for axis in azAltInd:
            if not obj.targetMount[axis].isfinite():
                raise RuntimeError("Target mount position axis %s unknown" % (axis,))
        targetMountPos = numpy.array([obj.targetMount[axis].getPos(tai) for axis in azAltInd])

        obj.userSys = self.coordSys
        for ind in self.indexIter(targetPolarAng):
            starData = self._starDataList[ind]
            if not (minMag <= starData.mag <= maxMag):
                continue
            if abs(starData.polarAng - targetPolarAng) > minDist:
                break

            obj.userPos = starData.pvtPair
            obj.userPxPMRadVel = starData.pxPMRadVel
            obj.targetMount[:] = targetMount[:]
            try:
                self._computeObj(obj, tccActor, tai)
            except Exception as e:
                tccActor.writeToUsers(
                    msgCode = "w",
                    msgStr = "Ignoring reference star %s: could not compute mount: %s" % (starData, strFromException(e)),
                    cmd = userCmd,
                )
                traceback.print_exc(file=sys.stderr)
                continue
            if not all(obj.targetMount[axis].isfinite() for axis in azAltInd):
                continue
            starMountPos = numpy.array([obj.targetMount[axis].getPos(tai) for axis in azAltInd])

            dist = numpy.max(numpy.abs(starMountPos - targetMountPos))
            if dist < minDist:
                bestInd = ind
                minDist = dist

        if bestInd is None:
            raise RuntimeError("No valid star found")
        return self._starDataList[bestInd]

    def indexIter(self, polarAng):
        """!Index iterator starting from a specified polar angle

        @param[in] polarAng  polar angle (deg)

        Returns indices in order of increasing delta-polar angle
        """
        numStars = len(self)
        highInd = bisect.bisect_left(self._polarAngList, polarAng)
        lowInd = highInd - 1

        while True:
            if lowInd < 0:
                for ind in range(highInd, numStars):
                    yield ind
                return
            elif highInd >= numStars:
                for ind in range(lowInd, -1, -1):
                    yield ind
                return
            elif abs(self._polarAngList[lowInd] - polarAng) < \
                abs(self._polarAngList[highInd] - polarAng):
                yield lowInd
                lowInd -= 1
            else:
                yield highInd
                highInd += 1

    def _computeObj(self, obj, tccActor, tai):
        """!Update the obj block at the specified date

        @param[in,out] obj  object block to update
        @param[in] tccActor  an instance of TCCActor
        @param[in] tai  TAI date at which to compute
        """
        computeObj(
            obj = obj,
            doWrap = True, # unwrap if in a limit
            doRestart = [True]*3,
            reportLim = False,
            earth = tccActor.earth,
            inst = tccActor.inst,
            telMod = tccActor.telMod,
            axeLim = tccActor.axeLim,
            tai = tai,
        )
        for axis in range(3):
            if obj.axisErrCode[axis] != tcc.base.AxisErr_OK:
                obj.targetMount[axis].invalidate(tai)
