from __future__ import division, absolute_import

import coordConv

import tcc.base
from .rotFromObs import rotFromObs

__all__ = ["ptCorrFromObs"]

def ptCorrFromObs(targObs, obj, inst, taiDate):
    """!Compute pointing correction position from observed coordinates

    @param[in] targObs  position of target in observed coordinates (a coordConv.PVTCoord)
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] inst  the instrument block; the following fields are read:
    - rot_inst_xy
    - rot_inst_ang
    @param[in] taiDate  TAI date at which to compute (MJD, sec)

    @return a tuple:
    - targPtCorrXY: position of target in pointing correction coordinates (a tcc.base.ArrayPVT2) (deg)
        i.e. a position in rotator coordinates where X is the direction of increasing azimuth
    - targAzPtCorrAng: angle of direction of increasing azimuth at target in ptCorr coordinates (a coordConv.PVT).
        Warning: do not use this angle to adjust the direction of your pointing correction;
        pointing correction should be applied using the orientation at the center of the ptCorr frame.
    """
    targRotXY, targAzRotAng = rotFromObs(targObs, obj, inst, taiDate)

    targPtCorrXY = tcc.base.ArrayPVT2()
    coordConv.rot2D(targPtCorrXY[0], targPtCorrXY[1], targRotXY[0], targRotXY[1], -obj.rotAzRotAng.getPos(taiDate), taiDate)

    targAzPtCorrAng = targAzRotAng.copy(taiDate) - obj.rotAzRotAng

    return targPtCorrXY, targAzPtCorrAng