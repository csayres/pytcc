from __future__ import division, absolute_import

import coordConv

import tcc.base
from .obsFromInst import obsFromInst

__all__ = ["obsFromRot"]

def obsFromRot(targRotXY, obj, inst, taiDate):
    """!Compute observed coordinates from rotator coordinates

    @param[in] targRotXY  position of target in rotator frame (a tcc.base.ArrayPVT2) (deg)
    @param[in] obj  the object block; the following fields are read:
    - objInstXY
    - obsPos
    - objAzInstAng
    @param[in] inst  the instrument block; the following fields are read:
    - rot_inst_xy
    - rot_inst_ang
    @param[in] taiDate  TAI date at which to compute the result (MJD, sec)

    @return a tuple:
    - targObs: position of target in observed coordinates (a coordConv.PVTCoord)
    - targAzRotAng: angle of direction of increasing azimuth at target in rotator coordinates (a coordConv.PVT)
    """
    # compute targRot_instXY = targRotXY rotated into the instrument frame
    targRot_instXY = tcc.base.ArrayPVT2()
    coordConv.rot2D(targRot_instXY[0], targRot_instXY[1], targRotXY[0], targRotXY[1], inst.rot_inst_ang, taiDate)

    # compute targInstXY = rotInstXY + targRot_instXY
    targInstXY = tcc.base.ArrayPVT2()
    targInstXY[:] = [targRot_instXY[i] + inst.rot_inst_xy[i] for i in range(2)]

    # compute observed coordinates at target
    targInstPVTCoord, targAzInstAng = obsFromInst(targInstXY, obj, taiDate)
    targAzRotAng = targAzInstAng - inst.rot_inst_ang
    return targInstPVTCoord, targAzRotAng
