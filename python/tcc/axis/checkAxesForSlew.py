from __future__ import division, absolute_import

import numpy

from twistedActor import log

import tcc.base

__all__ = ["checkAxesForSlew"]

def checkAxesForSlew(desObj, perfect, doRestart, isHalted, isNewObj, isNewRot, rotExists, nullSysFlag):
    """!Apply wrap preference and verify that position is in bounds, preparatory to a slew

    @param[in] desObj  object block; information about the desired object
        in: userSys, rotType, targetMount, axisErrCode
        out: axisErrCode
    @param[in] perfect  if True, all existing axes must be able to move, else badAxis and sigBad are set true
    @param[in] doRestart (3 bools) restart axis, if possible? (ignored for rotator if no rotator)
    @param[in] isHalted  (3 bools) true if axis is presently halted
    @param[in] isNewObj  are you acquiring a new object?
    @param[in] isNewRot  are you setting a new rotator angle; if True and rotExists then the rot axis is "significant"
    @param[in] rotExists  is there a rotator axis?
    @param[in] nullSysFlag  how to set badAxis if coordSys (for az, alt) or rotType (for rot) is none:
        -1: reject all such cases (badAxis=true); use if you cannot handle this situation
         0: accept only if halted or halting (isHalted or desObj.targetMount and desObj.axisErrCode are both OK)
         1: accept for all such cases; useful for a preliminary check, if you are going to handle halting later
    @return these items:
    - doAxis[3]: move or halt this axis?
    - badAxis[3]: error code is nonzero, the axis exists, and a move is wanted**
        Normally set True if desObj.axisErrCode not OK. These are the exceptions:
        - The rotator does not exist and move not wanted (desObj.rotType none)
        - The axis exists, a halt is wanted*, "perfect" is false and:
          - nullSysFlag > 0 (accept all halts)
          - nullSysFlag = 0 (accept valid halts) and the halt is valid, i.e.
            isHalted true (already halted, no need to do anything)
            or desObj.targetMount is valid and desObj.axisErrCode=tcc.base.AxisErr_HaltRequested (a valid halt)
    - sigBad: set True if badAxis is true for a "significant" axis. A significant axis is so important
        that if it cannot be moved as requested then the request should be rejected, specifically:
        - If isNewObj or perfect then azimuth and altitude are significant.
        - If rotExists and (isNewRot or perfect) then the rotator is significant.

    Sets desObj.axisErrCode as follows; the first appropriate condition listed is used:
    - If not rotExists: sets axisErrCode[3] to tcc.base.AxisErr_NotAvailable
    - If halt wanted* and nullSysFlag=0 but the halt cannot happen (axisErrCode not tcc.base.AxisErr_HaltRequested,
        or mount pos. unknown): leaves axisErrCode alone if nonzero, else sets to tcc.base.AxisErr_CannotCompute
    - If halt wanted* and nullSysFlag != 0: sets axisErrCode to tcc.base.AxisErr_HaltRequested
    - If move wanted**, doRestart false and isHalted true: sets axisErrCode to tcc.base.AxisErr_NoRestart
    - If move wanted** but mount position unknown: leaves axisErrCode alone if nonzero,
        else sets axisErrCode to tcc.base.AxisErr_CannotCompute
    If none of these conditions apply then axisErrCode is left alone.

    *halt wanted:
        - for az and alt: associated coordSys is none (coordConv.NoneCoordSys)
        - for rot: rot exists and rotType is none (tcc.base.RotType_None)
    **move wanted:
        - for az and alt: associated coordSys is not none (coordConv.NoneCoordSys)
        - for rot: rot exists and rotType is not none (tcc.base.RotType_None)
    """
    badAxis = [False]*3
    axisExists = (True, True, rotExists)

    # compute doAxis -- all existing axes that are now moving or will be moving
    doAxis = tuple(axisExists[axis] and ((not isHalted[axis]) or desObj.targetMount[axis].isfinite()) for axis in range(3))

    # compute wantToMove: all axes we want to move (csys not "none")
    # as opposed to those we want to halt (csys = "none")
    wantToMove = (
        desObj.userSys.getName() != "none",
        desObj.userSys.getName() != "none",
        rotExists and desObj.rotType != tcc.base.RotType_None,
    )

    # set desObj.axisErrCode and badAxis appropriately
    for axis in range(3):
        if axisExists[axis]:
            if wantToMove[axis]:
                # the axis exists and motion wanted

                if isHalted[axis] and not doRestart[axis]:
                    # axis is being left halted
                    desObj.axisErrCode[axis] = tcc.base.AxisErr_NoRestart
                elif (not desObj.targetMount[axis].isfinite()) and (desObj.axisErrCode[axis] == tcc.base.AxisErr_OK):
                    # axis halted, but no error code to explain why;
                    # this case should never occur
                    desObj.axisErrCode[axis] = tcc.base.AxisErr_CannotCompute
                badAxis[axis] = (desObj.axisErrCode[axis] != tcc.base.AxisErr_OK)
            else:
                # axis exists and halt wanted
                desObj.axisErrCode[axis] = tcc.base.AxisErr_HaltRequested
                if not isHalted[axis]:
                    desObj.axisCmdState[axis] = tcc.base.AxisState_Halting
                if (nullSysFlag < 0) or perfect:
                    # any halt request is bad
                    badAxis[axis] = True
                elif nullSysFlag == 0:
                    # a halt request is OK only if it can be satisfied:
                    # if halted or halting (isHalted or desObj.targetMount and desObj.axisErrCode are both OK)
                    if not isHalted[axis] and not desObj.targetMount[axis].isfinite():
                        # halt slew computation failed
                        badAxis[axis] = True
                        logMsg = "Cannot compute mount axis %i\ncheckAxesForSlew(desObj, perfect=%s, doRestart=%s isHalted=%s isNewObj=%s isNewRot=%s rotExists=%s nullSysFlag)\nDumping desObj block:\n%s"%(axis, str(isHalted), str(isNewObj), str(isNewRot), str(rotExists), str(nullSysFlag), str(desObj))
                        log.error(logMsg)
                        desObj.axisErrCode[axis] = tcc.base.AxisErr_CannotCompute
        else:
            # axis does not exist
            # mark axis bad only if we want to move it
            desObj.axisErrCode[axis] = tcc.base.AxisErr_NotAvailable
            badAxis[axis] = wantToMove[axis]

    desObj.axisIsSignificant[0] = isNewObj or perfect
    desObj.axisIsSignificant[1] = isNewObj or perfect
    # user demanded rotation (even if rotator does not exist, though this should be caught earlier)
    # or user demanded perfection and a rotator exists
    desObj.axisIsSignificant[2] = rotExists and (isNewRot or perfect)

    sigBad = numpy.any(numpy.logical_and(badAxis, desObj.axisIsSignificant))

    return doAxis, badAxis, sigBad
