%copyctor tcc::Earth;

%include "tcc/earth.h"

%extend tcc::Earth {
    %pythoncode {
        _SecPerDay  = 24.0 * 3600.0  # seconds per day

        @property
        def _WrapperList(self):
            """Create WrapperList cache if it does not already exist
            """
            if hasattr(self, "__WLCache"):
                return self.__WLCache
            
            wrapperList = WrapperList(name="Earth", obj=self,
                wrappers=(
                    ScalarWrapper("utcDay", dtype=intFromStr, 
                        help="UTC day of the beginning of the data (MJD, days)"),
                    ArrayWrapper("startTAI", dtype=float, numElts=3,
                        help="TAI date of start of UTC_day and the next two UTC days (MJD, sec)"),
                    ArrayWrapper("ut1_taiIntercept", dtype=float, numElts=2,
                        help="UT1-TAI intercept at TAI[i] (sec)"),
                    ArrayWrapper("poleInterceptX", dtype=float, numElts=2,
                        help="Pole x offset intercept at TAI[i] (deg)"),
                    ArrayWrapper("poleInterceptY", dtype=float, numElts=2,
                        help="Pole y offset intercept at TAI[i] (deg)"),
                    ArrayWrapper("ut1_taiSlope", dtype=float, numElts=2,
                        help="UT1-TAI slope over range TAI[i] <= date < TAI[i+1] (sec/sec)"),
                    ArrayWrapper("poleSlopeX", dtype=float, numElts=2,
                        help="Pole x offset slope over range TAI[i] <= date < TAI[i+1] (deg/sec)"),
                    ArrayWrapper("poleSlopeY", dtype=float, numElts=2,
                        help="Pole y offset slope over range TAI[i] <= date < TAI[i+1] (deg/sec)"),
                ),
            )
                
            self.__WLCache = wrapperList
            return self.__WLCache

        def load(self, f):
            """Load data from a file-like object
            """
            self._WrapperList.load(f)
        
        def loadPath(self, filePath):
            """Load data from a file specified by path
            """
            self._WrapperList.loadPath(filePath)
        
        def dump(self, f):
            """Dump data to a file-like object
            """
            self._WrapperList.dump(f)            
        
        def __repr__(self):
            return self._WrapperList.dumpStr()

        def loadPredictions(self, stream, forTAI=None):
            """Load a set of earth orientation predictions
            
            @param[in] stream  a string generator, such as an open file
            @param[in] forTAI  TAI for which to load data; if None then the current TAI is used
            """
            if forTAI is None:
                forTAI = tai()
            earthPred = numpy.loadtxt(
                stream,
                comments = "!",
                dtype = [("utc", int), ("utcMinTAI", float), ("ut1MinTAI", float), ("dX", float), ("dY", float)],
            )
            taiArr = (earthPred["utc"] * self._SecPerDay) - earthPred["utcMinTAI"]
            # convert pole wander from arcseconds to degrees
            earthPred["dX"] /= 3600.0
            earthPred["dY"] /= 3600.0

            predInd = bisect.bisect_right(taiArr, forTAI)
            # need access to data for predInd-1, predInd and predInd+1
            if predInd < 1:
                ageError = (taiArr[0] - forTAI) / self._SecPerDay
                raise RuntimeError("Data is too new by %s days" % (ageError,))
            if predInd + 1 >= len(taiArr):
                ageError = (forTAI - taiArr[-1]) / self._SecPerDay
                raise RuntimeError("Data is too old by %s days" % (ageError,))
            
            self.utcDay = int(earthPred["utc"][predInd-1])
            self.startTAI[:] = taiArr[predInd-1:predInd+2]
    
            for ind in (0, 1):
                bigInd = predInd + ind - 1
                dSec = self.startTAI[ind+1] - self.startTAI[ind]
                self.ut1_taiIntercept[ind] = earthPred["ut1MinTAI"][bigInd]
                self.ut1_taiSlope[ind] = (earthPred["ut1MinTAI"][bigInd + 1] - earthPred["ut1MinTAI"][bigInd]) / dSec
    
                self.poleInterceptX[ind] = earthPred["dX"][bigInd]
                self.poleSlopeX[ind] = (earthPred["dX"][bigInd + 1] - earthPred["dX"][bigInd]) / dSec
    
                self.poleInterceptY[ind] = earthPred["dY"][bigInd]
                self.poleSlopeY[ind] = (earthPred["dY"][bigInd + 1] - earthPred["dY"][bigInd]) / dSec
    }
}
