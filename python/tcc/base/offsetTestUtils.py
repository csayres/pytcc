from __future__ import division, absolute_import

from twisted.internet.defer import gatherResults

from tcc.actor import TCC35mDispatcherWrapper
from tcc.base.wrapperTestCase import WrapperTestCase

class Tests(WrapperTestCase):
    def setUp(self):
        """!Set up the test case
        """
        self.dw = TCC35mDispatcherWrapper()
        self.dw.readyDeferred.addCallback(self._initCmds)
        self.dw.readyDeferred.addCallback(self._increaseSpeed)
        return self.dw.readyDeferred

    def _initCmds(self, dum=None):
        """!Execute tcc commands as soon as the dispatcher wrapper is ready, before setUp is done

        The default is to set inst=dis

        @param[in] dum  ignored
        """
        d0, cmdVar0 = self.dw.queueCmd("set inst=dis")
        d1, cmdVar1 = self.dw.queueCmd("show status")
        return gatherResults((d0, d1))

    def _increaseSpeed(self, dum=None):
        """!increase axes speed to speed up tests

        @param[in] dum  ignored
        """
        upFactor = 80.
        for x in range(3):
            self.actor.axeLim.vel[x] = self.actor.axeLim.vel[x]*upFactor
            self.actor.axeLim.accel[x] = self.actor.axeLim.accel[x]*upFactor
            self.actor.axeLim.jerk[x] = self.actor.axeLim.jerk[x]*upFactor
        # self.actor.tune.slewFudge = 0. #self.actor.tune.slewFudge/100.
        # self.actor.tune.slewAdvTime = self.actor.tune.slewAdvTime*.55

