from __future__ import division, absolute_import

from StringIO import StringIO

from coordConv import rot2D

from .tccLib import Inst
from .fieldWrapper import tokenize

__all__ = ["loadOldInstData"]

InstPosCodeNameDict = {
     "1": "PF1",
     "2": "CA1",
     "3": "BC1",
     "4": "BC2",
     "5": "NA1",
     "6": "NA2",
     "7": "TR1",
     "8": "TR2",
     "9": "TR3",
    "10": "TR4",
    "11": "SK1",
    "12": "SK2",
    "13": "SK3",
    "14": "SK4",
    "15": "MB1",
    "16": "MB2",
    "17": "MB3",
    "18": "MB4",
}

def loadOldInstData(filePath):
    """!Read old format Inst data file into a new Inst

    @param[in] filePath  path to data file
    @return inst: an Inst block with most fields filled in
    """
    
    strFile = StringIO()

    # set of keys that are IDs
    idSet = set(val.lower() for val in (
        "RotID",
        "GCamID",
        "GMechID",
    ))

    # set of names to ignore
    obsoletes = set(val.lower() for val in (
        "expTime",
        "gBoxSize",
        "gcCurrFiltInd",
        "gScanSize",
        "gcviewnum",
        "gim_binfac",
        "gim_maxcount",
        "minStarsForRot",
        "ptScanSize",
    ))

    with file(filePath, "rU") as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            if line[0] in ("#", "!"):
                continue
            dataList = tokenize(line)
            key = dataList[0].lower()
            if key in obsoletes:
                continue
            
            if key == "instpos":
                # change instPos code to string
                dataList[1] = InstPosCodeNameDict[dataList[1]]
            elif key in idSet:
                dataList = dataList[0:2] # strip 3rd value
            newLine = " ".join(repr(val) for val in dataList)
            strFile.writelines((newLine, "\n"))
    strFile.pos = 0
    inst = Inst()
    inst.load(strFile)

    # old guide probe data had gp_rot_xy instead of rot_gp_xy, so turn those around
    for probe in inst.gProbe:
        gp_rot_xy = probe.rot_gp_xy[:]
        probe.rot_gp_xy[:] = rot2D(-gp_rot_xy[0], -gp_rot_xy[1], probe.rot_gim_ang)
    
    return inst
