from __future__ import division, absolute_import
"""Utilities to aid unit tests
"""
import os
import shutil
import stat
import tempfile
import time

import eups
from RO.StringUtil import strFromException
from twistedActor import testUtils

from .tccLib import tai

_MaxEarthAgeSec = 3600
_ModeRWRWR = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH
_DaysPerSec = 1.0 / (24.0 * 3600.0)

def getTestDir():
    """!Return path to unit tests
    """
    return os.path.join(eups.productDir("tcc"), "tests")

def setEnviron():
    """!Set TCC environment variables appropriately for running unit tests
    """
    testDir = getTestDir()
    os.environ["TCC_DATA_DIR"] = os.path.join(testDir, "data")

def makeEarthPred():
    """!Create a new earth prediction file, so the TCC can be started

    The file is: <getTestDir()>/data/earthpred.dat and any existing copy is overwritten.

    Note that this intentionally does not use os.environ["TCC_DATA_DIR"], for fear of
    accidentally overwriting the real earth orientation prediction file.
    """
    testDir = getTestDir()
    tempEarthPath = None
    earthPath = os.path.join(testDir, "data", "earthpred.dat")
    try:
        mtime = os.path.getmtime(earthPath)
        if time.time() - mtime < _MaxEarthAgeSec:
            return
    except OSError:
        pass # file does not exist; create it

    currTAI = tai()
    startMJD = int(currTAI * _DaysPerSec) - 1
    print "Creating new %r" % (earthPath,)
    try:
        with tempfile.NamedTemporaryFile(mode="w", dir=testDir, delete=False) as tempEarthFile:
            tempEarthPath = tempEarthFile.name
            print "Created temporary earthpred file %r" % (tempEarthPath,)
            tempEarthFile.write("""! Earth Orientation Predictions (in TCC format)

! UTC   UTC-TAI   UT1-TAI     dX       dY
! days    sec       sec     arcsec   arcsec
""")
            for i in range(7):
                utcDays = startMJD + i - 1
                tempEarthFile.write("%d    -35   -34.8   0.1   0.3\n" % (utcDays,))
    except Exception as e:
        print "Failed to create %s: %s; trying to remove temp file %r" % \
            (earthPath, strFromException(e), tempEarthPath)
        os.remove(tempEarthPath)
        return
    os.chmod(tempEarthPath, _ModeRWRWR)

    # check file date again in case another process already replaced it
    try:
        mtime = os.path.getmtime(earthPath)
        if time.time() - mtime < _MaxEarthAgeSec:
            print "Another thread already replaced %r; removing temp file %r" % (earthPath, tempEarthPath)
            os.remove(tempEarthPath)
            return
    except OSError:
        pass # file does not exist; create it
    shutil.move(tempEarthPath, earthPath)
    print "Updated %r" % (earthPath,)

def init(filePath=None):
    """!Prepare for a unit test to run that starts an actor

    @param[in] filePath  path of file being tested (e.g. __file__), or None:
        - If supplied must be in subdir tests of your package (NOT deeper in the hierarchy)
            in order to determine where the log file should go.
        - If None, no log file is created.
    """
    testUtils.startLogging(filePath)
    setEnviron()
    makeEarthPred()
