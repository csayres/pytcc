%copyctor tcc::AxeLim;

%include "tcc/axeLim.h"

%extend tcc::AxeLim {
    %pythoncode {
        @property
        def _WrapperList(self):
            """Create WrapperList cache if it does not already exist
            """
            if hasattr(self, "__WLCache"):
                return self.__WLCache
            
            wrapperList = WrapperList(name="AxeLim", obj=self,
                wrappers = (
                    ArrayWrapper("minPos", dtype=float, numElts=3, help="Minimum position of Az, Alt, Rot (deg)"),
                    ArrayWrapper("maxPos", dtype=float, numElts=3, help="Maximum position of Az, Alt, Rot (deg)"),
                    ArrayWrapper("vel", dtype=float, numElts=3, help="Maximum velocity of Az, Alt, Rot (deg/sec)"),
                    ArrayWrapper("accel", dtype=float, numElts=3, help="Maximum acceleration of Az, Alt, Rot (deg/sec^2)"),
                    ArrayWrapper("jerk", dtype=float, numElts=3, help="Maximum jerk of Az, Alt, Rot (deg/sec^3)"),
                    ScalarWrapper("badStatusMask", dtype=intFromStr, fmt="%#X",
                        help="Fail if any of these bits are high in an axis controller's status word"),
                    ScalarWrapper("warnStatusMask", dtype=intFromStr, fmt="%#X",
                        help="Warn if any of these bits are high in an axis controller's status word and no badStatusMask bits are high"),
                    ScalarWrapper("maxDTime", dtype=float, help="Warn if axis controller and TCC clock disagree by this much (sec)"),
                )
            )
            
            self.__WLCache = wrapperList
            return self.__WLCache
        
        def __getitem__(self, axis):
            """Get an AxisLim for the specified axis (0=az, 1=alt, 2=rot)
            """
            return AxisLim(
                minPos=self.minPos[axis],
                maxPos=self.maxPos[axis],
                vel=self.vel[axis],
                accel=self.accel[axis],
                jerk=self.jerk[axis],
            )
        
        def __len__(self):
            return len(self.minPos)
                
        def load(self, f):
            """Load data from a file-like object
            """
            self._WrapperList.load(f)
        
        def loadPath(self, filePath):
            """Load data from a file specified by path
            """
            self._WrapperList.loadPath(filePath)
        
        def dump(self, f):
            """Dump data to a file-like object
            """
            self._WrapperList.dump(f)            
        
        def __repr__(self):
            return self._WrapperList.dumpStr()
        
        def setRotLim(self, inst):
            """Update rotator limits from an instrument block
        
            @param[in] inst  instrument data; only the instPosLim and rotLim fields are read
            """
            self.minPos[2] = max(inst.rotLim[0], inst.instPosLim[0])
            self.maxPos[2] = min(inst.rotLim[1], inst.instPosLim[1])
            self.vel[2]    = inst.rotLim[2]
            self.accel[2]  = inst.rotLim[3]
            self.jerk[2]   = inst.rotLim[4]
    }
}
