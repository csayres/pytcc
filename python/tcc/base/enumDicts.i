
%pythoncode {
    RotTypeValDict = {
        RotType_None: "None",
        RotType_Object: "Object",
        RotType_Horizon: "Horizon",
        RotType_Mount: "Mount",
    }

    RotTypeNameDict = dict((name.lower(), val) for val, name in RotTypeValDict.iteritems())

    WrapTypeValDict = {
        WrapType_None: "None",
        WrapType_Nearest: "Nearest",
        WrapType_Negative: "Negative",
        WrapType_Middle: "Middle",
        WrapType_Positive: "Positive",
        WrapType_NoUnwrap: "NoUnwrap",
    }

    WrapTypeNameDict = dict((name.lower(), val) for val, name in WrapTypeValDict.iteritems())

    AxisStateValDict = {
        AxisState_NotAvailable: "NotAvailable",
        AxisState_Halted: "Halted",
        AxisState_Drifting: "Drifting",
        AxisState_Slewing: "Slewing",
        AxisState_Halting: "Halting",
        AxisState_Tracking: "Tracking",
        AxisState_BadCode: "BadCode",
    }

    AxisStateNameDict = dict((name.lower(), val) for val, name in AxisStateValDict.iteritems())

    AxisErrValDict = {
        AxisErr_HaltRequested: "HaltRequested",
        AxisErr_NoRestart: "NoRestart",
        AxisErr_NotAvailable: "NotAvailable",
        AxisErr_OK: "OK",
        AxisErr_MinPos: "MinPos",
        AxisErr_MaxPos: "MaxPos",
        AxisErr_MaxVel: "MaxVel",
        AxisErr_MaxAccel: "MaxAccel",
        AxisErr_MaxJerk: "MaxJerk",
        AxisErr_CannotCompute: "CannotCompute",
        AxisErr_ControllerErr: "ControllerErr",
        AxisErr_TCCBug: "TCCBug",
        AxisErr_BadCode: "BadCode",
    }

    AxisErrNameDict = dict((name.lower(), val) for val, name in AxisErrValDict.iteritems())
}
