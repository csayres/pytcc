%copyctor tcc::Inst;

%include "tcc/inst.h"

%extend tcc::Inst {
    %pythoncode {
        @property
        def _WrapperList(self):
            """Create WrapperList cache if it does not already exist
            """
            if hasattr(self, "__WLCache"):
                return self.__WLCache
            
            wrapperList = WrapperList(name="Inst", obj=self,
                wrappers = (
                    DocWrapper(help="""
                                Data encoded in the NAME of the instrument data files.
                                Values specified in the contents of such files will be ignored by the "set inst" command
                                (but will be used by the "set block inst" command).
                                """),
                    InstPositionWrapper("instPos", help="Instrument position"),
                    ScalarWrapper("instName", dtype=str, help="Instrument name"),
                    ScalarWrapper("gcViewName", dtype=str, help="Name of current guide camera view"),
            
                    DocWrapper(help="""
                                User-set values
                                These values are held fixed or reset during an instrument change;
                                values specified in instrument data files will be ignored.
                                """),
                    ScalarWrapper("secUserFocus", dtype=float, help="User-set secondary mirror focus offset (um)"),
                    ScalarWrapper("gcUserFocus", dtype=float, help="User-set guide camera focus offset (um)"),
                    ScalarWrapper("scaleFac", dtype=float,
                        help="""Scale factor = actual focal plane scale / nominal scale (no units)
                                larger scale gives higher resolution (e.g. more pixels/arcsec)"""),
                    
                    DocWrapper(help="""
                                Mirror collimation coefficients
                                value = coeff[0] + coeff[1] sin(alt) + coeff[2] cos(alt)
                                """),
                    ArrayWrapper("primPistCoef", dtype=float, numElts=3,
                        help="""Primary piston collimation coefficients (um)"""),
                    ArrayWrapper("primXTiltCoef", dtype=float, numElts=3,
                        help="""Primary x tilt collimation coefficients (arcsec)"""),
                    ArrayWrapper("primYTiltCoef", dtype=float, numElts=3,
                        help="""Primary y tilt collimation coefficients (arcsec)"""),
                    ArrayWrapper("primXTransCoef", dtype=float, numElts=3,
                        help="""Primary x translation collimation coefficients (um)"""),
                    ArrayWrapper("primYTransCoef", dtype=float, numElts=3,
                        help="""Primary y translation collimation coefficients (um)"""),
                    
                    ArrayWrapper("secPistCoef", dtype=float, numElts=3,
                        help="""Secondary piston collimation coefficients (um)"""),
                    ArrayWrapper("secXTiltCoef", dtype=float, numElts=3,
                        help="""Secondary x tilt collimation coefficients (arcsec)"""),
                    ArrayWrapper("secYTiltCoef", dtype=float, numElts=3,
                        help="""Secondary y tilt collimation coefficients (arcsec)"""),
                    ArrayWrapper("secXTransCoef", dtype=float, numElts=3,
                        help="""Secondary x translation collimation coefficients (um)"""),
                    ArrayWrapper("secYTransCoef", dtype=float, numElts=3,
                        help="""Secondary y translation collimation coefficients (um)"""),
                    
                    ArrayWrapper("tertPistCoef", dtype=float, numElts=3,
                        help="""Tertiary piston collimation coefficients (um)"""),
                    ArrayWrapper("tertXTiltCoef", dtype=float, numElts=3,
                        help="""Tertiary x tilt collimation coefficients (arctert)"""),
                    ArrayWrapper("tertYTiltCoef", dtype=float, numElts=3,
                        help="""Tertiary y tilt collimation coefficients (arctert)"""),
                    ArrayWrapper("tertXTransCoef", dtype=float, numElts=3,
                        help="""Tertiary x translation collimation coefficients (um)"""),
                    ArrayWrapper("tertYTransCoef", dtype=float, numElts=3,
                        help="""Tertiary y translation collimation coefficients (um)"""),

                    DocWrapper(help="""
                                Other mirror-related data
                                """),
                    ArrayWrapper("secPistTempCoef", dtype=float, numElts=5,
                        help="""Secondary piston (focus) temperature coefficients (um/C)
                                offset = coeff[0] * secondary truss temp
                                        + coef[1] * primary front temp   + coef[2] * primary back-front delta-t
                                        + coef[3] * secondary front temp + coef[4] * secondary back-front delta-t"""),

                    ScalarWrapper("maxScaleFac", dtype=float,
                        help="""Maximum allowed scale factor; minimum is 1/maxScaleFac;
                                set to 1.0 if scale factor is not adjustable"""),
                    ScalarWrapper("primPistScaleCoef", dtype=float,
                        help="""Primary scale coefficient (um): delta piston = coeff * (scaleFac - 1)"""),
                    ScalarWrapper("secPistScaleCoef", dtype=float,
                        help="""Secondary scale coefficient (um): delta piston = coeff * (scaleFac - 1)"""),

                    DocWrapper(help="""
                                Instrument data
                                """),
                    ArrayWrapper("iim_ctr", dtype=float, numElts=2,
                        help="""Desired center of instrument image (unbinned pixels)"""),
                    ArrayWrapper("iim_scale", dtype=float, numElts=2,
                        help="""Instrument image scale (unbinned pixels/deg on the sky)"""),
                    ArrayWrapper("iim_minXY", dtype=float, numElts=2,
                        help="""Minimum position on instrument image (unbinned pixels)"""),
                    ArrayWrapper("iim_maxXY", dtype=float, numElts=2,
                        help="""Maximum position on instrument image (unbinned pixels)"""),

                    ScalarWrapper("inst_foc", dtype=float,
                        help="""Focus (secondary piston) offset due to instrument (um)"""),
                    ArrayWrapper("rot_inst_xy", dtype=float, numElts=2,
                        help="""Position of the center of the rotator in instrument frame (x,y deg)"""),
                    ScalarWrapper("rot_inst_ang", dtype=float,
                        help="""Angle from the instrument x axis to the rotator x axis (deg)"""),
                    ArrayWrapper("instPosLim", dtype=float, numElts=2,
                        help="""Rotator position limits due to the instrument: min pos (deg), max pos (deg)
                                Note: the actual rotator position limits are the intersection of this
                                with the position components of rotLim
                            """),

                    DocWrapper(help="""
                                Instrument rotator data that are independent of the instrument
                                """),
                    ScalarWrapper("rotID", dtype=int, help="Rotator ID, or 0 if none"),
                    ScalarWrapper("rot_fixed_phys", dtype=float,
                        help="""Physical position used for pointing error measurements,
                                and the fixed physical rotation if there is no rotator
                                mount pos = offet + (scale * physical pos)"""),
                    ScalarWrapper("rot_offset", dtype=float, help="Rotator mount offset (mount units)"),
                    ScalarWrapper("rot_scale", dtype=float, help="Rotator mount scale (mount units/degree)"),
                    ArrayWrapper("rotLim", dtype=float, numElts=5,
                        help="""Rotator limits due to the rotator: min pos(deg), max pos (deg), vel (deg/sec), accel (deg/sec^2) and jerk (deg/sec^3)
                                note: the true position limits are the intersection of this with instPosLim"""),
                    
                    DocWrapper(help="""
                                Guider data
                                """),
                    ScalarWrapper("gcamID", dtype=int, help="Guide camera ID, or 0 if none"),
                    ArrayWrapper("gim_ctr", dtype=float, numElts=2, help="Desired center of guide camera image (unbinned pixels)"),
                    ArrayWrapper("gim_scale", dtype=float, numElts=2, help="Guide image scale (unbinned pixels/deg on the sky)"),
                    ArrayWrapper("gim_minXY", dtype=float, numElts=2, help="Minimum position on guide image (unbinned pixels)"),
                    ArrayWrapper("gim_maxXY", dtype=float, numElts=2, help="Maximum position on guide image (unbinned pixels)"),
                    
                    GuideProbeVectorWrapper("gProbe", help="Guide probe data"),
                    ScalarWrapper("ptErrProbe", dtype=int, help="Number of guide probe to use for pointing error measurements"),

                    ScalarWrapper("gmechID", dtype=int, help="Guider mechanical controller ID, or 0 if none"),
                    ScalarWrapper("gcNomFocus", dtype=float, help="Nominal focus for guide camera (um)"),
                ),
                obsoletes = (
                    ("expTime", 1),
                    ("gBoxSize", 1),
                    ("gcCurrFiltInd", 1),
                    ("gScanSize", 1),
                    ("gcviewnum", 1),
                    ("gcfoclim", 1),
                    ("gim_binfac", 1),
                    ("gim_maxcount", 1),
                    ("minStarsForRot", 1),
                    ("ptScanSize", 1),
                ),
            )
            
            self.__WLCache = wrapperList
            return self.__WLCache
                
        def load(self, f):
            """Load data from a file-like object
            """
            self._WrapperList.load(f)
        
        def loadPath(self, filePath):
            """Load data from a file specified by path
            """
            self._WrapperList.loadPath(filePath)
        
        def dump(self, f):
            """Dump data to a file-like object
            """
            self._WrapperList.dump(f)            
        
        def __repr__(self):
            return self._WrapperList.dumpStr()
    }
}
