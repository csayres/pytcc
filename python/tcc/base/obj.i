%import(module="coordConv.coordConvLib") "coordConv/site.h"
%import(module="coordConv.coordConvLib") "coordConv/coordSys.i"
%import(module="coordConv.coordConvLib") "coordConv/coord.h"
%import(module="coordConv.coordConvLib") "coordConv/pvtCoord.h"

%copyctor tcc::Obj;

%include "tcc/obj.h"

%extend tcc::Obj {
    %pythoncode {
        @property
        def _WrapperList(self):
            """Create WrapperList cache if it does not already exist
            """
            if hasattr(self, "__WLCache"):
                return self.__WLCache

            wrapperList = WrapperList(name="Obj", obj=self,
                wrappers = (
                    ScalarWrapper("name", dtype=str, help="Name of object"),
                    CoordSysWrapper("userSys", help="Coordinate system code and date of object"),
                    PVTArrayWrapper("userPos", numElts=2, help="User-set object position"),
                    PxPMRadVelWrapper("userPxPMRadVel", help="""Object parallax, proper motion and radial velocity:
                                - parallax (arcsec)
                                - equatorial proper motion (e.g. dRA/dt, larger at pole) arcsec/century
                                - polar proper motion (e.g. dDec/dt) arcsec/century
                                - radial velocity (km/sec, positive receding)"""),
                    ScalarWrapper("useCheby", dtype=boolFromStr,
                        help="""Is the object position obtained from Chebyshev polynomials?
                                If true: position and distance are obtained from cheby; userPos and meanSys are ignored
                                and the position is only valid if tai is in range [cheby.startTime, cheby.endTime]
                                If false: position and parallax are obtained from userPos and meanSys; cheby is ignored."""),
                    EnumWrapper("rotType", valDict=RotTypeValDict, help="Type of rotation (e.g. with object or horizon)"),
                    PVTWrapper("rotUser",
                        help="""Rotator angle. The exact meaning depends on rot_type:
                                - object: the angle of the user coordinate system w.r.t. the instrument.
                                - horizon: the angle of az/alt w.r.t the instrument
                                - mount: the angle sent to the axis controller
                                - none: no rotation"""),
                    EnumWrapper("azWrapPref",  valDict = WrapTypeValDict, help="Azimuth wrap preference"),
                    EnumWrapper("rotWrapPref", valDict = WrapTypeValDict, help="Rotator wrap preference; see basics.h for codes"),
                    ScalarWrapper("mag", dtype=float, help="Predicted magnitude of object"),
                    # If one wants a wrapper for obj.site (a LocSite) then put it here
                    DocWrapper("""Chebyshev polynomial coefficients for track/chebyshev
                                The format is minTAI  maxTAI  coeff0  coeff1...
                                where:
                                - minTAI and maxTAI are the minimum and maximum allowed TAI date, in MJD, seconds;
                                  (tracking will fail if you use these polynomials outside this date range)
                                - The polynomial is evaluated with x ranging from -1 to 1 over the specified date range"""),
                    ChebyshevWrapper("chebyUser1", help="Chebyshev coefficients for object position, equatorial axis (deg)"),
                    ChebyshevWrapper("chebyUser2", help="Chebyshev coefficients object position, polar axis (deg)"),
                    ChebyshevWrapper("chebyDist", help="Chebyshev coefficients for object distance (au)"),

                    PVTArrayWrapper("userArcOff", numElts=2,
                        help="""Offset of object position along a great circle;
                                note: the actual velocity is adjusted for refraction
                                so that a fixed velocity gives proper drift-scanning"""),
                    PVTArrayWrapper("objInstXY", numElts=2, help="User-set boresight pos. (aka instrument-plane offset (inst x,y)"),
                    PVTArrayWrapper("guideOff", numElts=NAxes, help="Net guiding correction (mount)"),
                    PVTArrayWrapper("calibOff", numElts=NAxes, help="Calibration offset (mount)"),

                    ScalarWrapper("updateTime", dtype=float,
                        fmt="%0.7f", help="Time of last tracking update; TAI, MJD sec; 0 if none"),
                    ScalarWrapper("slewEndTime", dtype=float, fmt="%0.7f",
                        help="End time of slew (or slew to halt); TAI, MJD sec; 0 or stale if not slewing"),

                    PVTCoordWrapper("zpmUserPos",
                        help="""UserPos with proper motion removed to the current date; in "coordSys" coordinates:
                                - equatorial position (e.g. RA) (deg)
                                - polar position (e.g. Dec) (deg)
                                - equatorial velocity (e.g. dRA/dt) (deg)
                                - polar velocity (e.g. dDec/dt) (deg)
                                - TAI date (MJD, seconds)
                                - distance (AU)
                                - rate of change of distance (AU/sec)
                                  the following will be zero except for FK4 coordinates, which have fictitious proper motion:
                                - equatorial proper motion (e.g. dRA/dt) arcsec/century
                                - polar proper motion (e.g. dDec/dt) arcsec/century
                                - radial velocity (km/sec, positive receding)"""),
                    PVTCoordWrapper("netUserPos",
                        help="""Net user position, including all offsets; in "coordSys" coordinates:
                                - equatorial position (e.g. RA) (deg)
                                - polar position (e.g. Dec) (deg)
                                - equatorial velocity (e.g. dRA/dt) (deg)
                                - polar velocity (e.g. dDec/dt) (deg)
                                - TAI date (MJD, seconds)
                                - distance (AU)
                                - rate of change of distance (AU/sec)
                                  the following will be zero except for FK4 coordinates, which have fictitious proper motion:
                                - equatorial proper motion (e.g. dRA/dt) arcsec/century
                                - polar proper motion (e.g. dDec/dt) arcsec/century
                                - radial velocity (km/sec, positive receding)"""),
                    PVTWrapper("arcUserNoArcUserAng",
                        help="""Angle of arc (of arc offset) in user coordinates
                                at the object minus the same angle at the non-arc-offset position"""),
                    ObsCoordSysWrapper("obsSysPtr", help="Date at which observed coordinates measured"),
                    FixedPVTCoordWrapper("obsPos",
                        help="""Observed (refracted apparent topocentric) position and velocity of netUserPos:
                                - azimuth (deg)
                                - altitude (deg)
                                - azimuth velocity (deg/sec)
                                - altitude velocity (deg/sec)
                                - TAI date (MJD, seconds)
                                - distance (AU)
                                - rate of change of distance (AU/sec)"""),
                    PVTWrapper("objUserObjAzAng", "Angle from instrument x axis to object user coordsys axis 1"),
                    PVTWrapper("objAzInstAng", "Angle from instrument x axis to object azimuth axis"),
                    PVTWrapper("objUserInstAng", "Angle from instrument x axis to object user coordsys axis 1"),
                    PVTWrapper("rotAzObjAzAng", help="Direction of increasing azimuth at center of rotator - direction of increasing azimuth at boresight"),
                    PVTWrapper("rotAzRotAng", help="Direction of increasing azimuth at center of rotator in rotator frame"),
                    PVTWrapper("spiderInstAng",
                        help="""Direction of increasing azimuth at optical axis (center of rotator) in the instrument frame;
                                spiderInstAng = rotAzInstAng = rotAzRotAng - inst.inst_rot_ang"""),
                    PVTWrapper("rotPhys",
                        help="""Rotator physical angle: the desired mount angle before applying inst.rot_scale and inst.rot_offset;
                                if there is no rotator then rotPhys = inst.rot_fixed_phys"""),

                    PVTArrayWrapper("targetMount", numElts=NAxes,
                        help="Target mount position of each axis (az, alt and rot)"),
#                     PVAJTArrayWrapper("pathMount", numElts=NAxes,
#                         help="Slew path mount position of each axis (az, alt and rot); matches targetMount if tracking"),
                    EnumArrayWrapper("axisCmdState", valDict=AxisStateValDict, numElts=NAxes,
                        help="Commanded state of each axis"),
                    EnumArrayWrapper("axisErrCode",  valDict=AxisErrValDict,   numElts=NAxes,
                        help="Error code of each axis; if the TCC has commanded a halt, this explains why"),
                    ArrayWrapper("axisIsSignificant",  dtype=bool, numElts=NAxes,
                        help="Is axis significant for the current slew command?"),

                    PVTArrayWrapper("actMount", numElts=NAxes,
                        help="Actual mount position of each axis, as read from the axis controller"),
                    ArrayWrapper("axisStatusWord", dtype=int, numElts=NAxes,
                        help="Status word last read from each axis controller"),
                    ArrayWrapper("axisStatusTime", dtype=float, numElts=NAxes,
                        help="Time at which status was last read from each axis controller; TAI, MJD sec"),
                ),
                obsoletes = (
                    ("slewEndTime", 1),
                    ("pathMount1", 1),
                    ("pathMount2", 1),
                    ("pathMount3", 1),
                ),
            )
                
            self.__WLCache = wrapperList
            return self.__WLCache

        def load(self, f):
            """Load data from a file-like object
            """
            self._WrapperList.load(f)
        
        def loadPath(self, filePath):
            """Load data from a file specified by path
            """
            self._WrapperList.loadPath(filePath)
        
        def dump(self, f):
            """Dump data to a file-like object
            """
            self._WrapperList.dump(f)            
        
        def __repr__(self):
            return self._WrapperList.dumpStr()
    }
}
