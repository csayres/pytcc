%define ModuleDocStr
"Python interface to coordinate conversion library (which is based on coordConv, tcspk and slalib)."
%enddef

%feature("autodoc", "1");
%module(package="tcc", docstring=ModuleDocStr) tccLib

%{
#include <sstream>
#include <stdexcept>
#include "tcc/testSwig.h"
#include "tcc/basics.h"
#include "tcc/pvajt.h"
#include "tcc/cheby.h"
#include "tcc/axeLim.h"
#include "tcc/earth.h"
#include "tcc/inst.h"
#include "tcc/computeNetUser.h"
#include "tcc/obsFromNetUser.h"
#include "tcc/mountFromObs.h"
#include "tcc/pxPMRadVel.h"
#include "tcc/obj.h"
#include "tcc/tcsSite.h"
#include "tcc/telConst.h"
#include "tcc/telMod.h"
#include "tcc/weath.h"
%}

%inline %{
namespace tcc {}
using namespace tcc;
%}

%init %{
%}

%import "coordConv/pvt.h" // allows obj.rotUser's fields to be set (i.e. obj.rotUser.pos = newValue);
    // without this import, attemps to set obj.rotUser's fields are silenly ignored (a terrible failure mode)
// testSwig.obsSys is still an opaque object in Python; I had hoped this would fix it, but it does not:
// %import "coordConv/coordSys.h"

// any imports needed by python code (e.g. in included .i files)
%pythoncode {
    import bisect
    import numpy
    from .axisLim import *
    from .fieldWrapper import *
}

%include "std_vector.i"
%include "std_except.i"
%include "std_string.i"
// %include "std_pair.i"

%template(VectorInt) std::vector<int>;
%template(VectorFloat) std::vector<float>;
%template(VectorDouble) std::vector<double>;
%template(VectorString) std::vector<std::string>;
%template(VectorGuideProbe) std::vector<tcc::GuideProbe>;

// Specifies the default C++ to python exception handling interface
%exception {
    try {
        $action
    } catch (std::domain_error & e) {
        PyErr_SetString(PyExc_ArithmeticError, e.what());
        SWIG_fail;
    } catch (std::invalid_argument & e) {
        PyErr_SetString(PyExc_ValueError, e.what());
        SWIG_fail;
    } catch (std::length_error & e) {
        PyErr_SetString(PyExc_IndexError, e.what());
        SWIG_fail;
    } catch (std::out_of_range & e) {
        PyErr_SetString(PyExc_ValueError, e.what());
        SWIG_fail;
    } catch (std::logic_error & e) {
        PyErr_SetString(PyExc_RuntimeError, e.what());
        SWIG_fail;
    } catch (std::range_error & e) {
        PyErr_SetString(PyExc_ValueError, e.what());
        SWIG_fail;
    } catch (std::underflow_error & e) {
        PyErr_SetString(PyExc_ArithmeticError, e.what());
        SWIG_fail;
    } catch (std::overflow_error & e) {
        PyErr_SetString(PyExc_OverflowError, e.what());
        SWIG_fail;
    } catch (std::runtime_error & e) {
        PyErr_SetString(PyExc_RuntimeError, e.what());
        SWIG_fail;
    } catch (std::exception & e) {
        PyErr_SetString(PyExc_StandardError, e.what());
        SWIG_fail;
    } catch (...) {
        SWIG_fail;
    }
}

// %template(strIntPair) std::pair<std::string, int>;

%include "tcc/testSwig.h"
%include "tcc/basics.h"
%include "tcc/pvajt.h"
%include "tcc/cheby.h"
%include "tcc/instPos.h"
%include "tcc/tcsSite.h"
%include "tcc/computeNetUser.h"
%include "tcc/obsFromNetUser.h"
%include "tcc/mountFromObs.h"
%include "pxPMRadVel.i"
%include "enumDicts.i"
%include "axeLim.i"
%include "telMod.i"
%include "earth.i"
%include "inst.i"
%include "obj.i"
%include "weath.i"

// put this after any structs that contain std::tr1::array
%include "array.i"
