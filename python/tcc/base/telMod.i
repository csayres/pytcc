%copyctor tcc::TelMod;

%include "tcc/telMod.h"

%extend tcc::TelMod {
    %pythoncode {
        def dump(self, f):
            """Dump data to a file
            
            Inputs:
            - f: file or file-like object that supports write
            """
            f.write("%r" % (self,))

        def load(self, f):
            """Load data from a file-like object, replacing all existing data
            """
            for i, line in enumerate(f):
                if i < 2:
                    continue
                line = line.strip()
                if line == "END":
                    return
                termName = line[2:11].replace("&", "").replace("=","").strip()
                termVal = float(line[11:20])
                self.addTerm(termName, termVal)
    }
}
