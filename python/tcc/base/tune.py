from __future__ import division, absolute_import

from .fieldWrapper import WrapperList, DocWrapper, ScalarWrapper, ArrayWrapper

__all__ = ["Tune"]

class Tune(object):
    """!Tuning Constants
    """
    def __init__(self):
        self.doBrdTelPos = True
        self.doCollimate = True
        self.doStatus = True
        self.maxUsers = 25
        self.collimateInterval = 15
        self.collimateSlewEnd = 2
        self.statusInterval = [10, 2, 10]
        self.slewAdvTime = 0.5
        self.slewMinDuration = 0.5
        self.trackInterval = 5.0
        self.trackAdvTime = 1.0
        self.slewConvTime = 0.15
        self.slewMaxIter = 30
        self.slewFudge = 1.0

    @property
    def _WrapperList(self):
        """!Create WrapperList cache if it does not already exist
        """
        if hasattr(self, "__WLCache"):
            return self.__WLCache
        
        wrapperList = WrapperList(name="Tune", obj=self,
            wrappers = (
                DocWrapper("\nMiscellaneous Parameters\n"),

                ScalarWrapper("doBrdTelPos", dtype=bool, help="Broadcast telescope position as UDP packets?"),
                ScalarWrapper("doCollimate", dtype=bool, help="Update collimation?"),
                ScalarWrapper("doStatus",    dtype=bool, help="Print status at regular intervals?"),

                ScalarWrapper("maxUsers", dtype=int, help="maximum number of simultaneous users"),

                ScalarWrapper("collimateInterval", dtype=float,
                    help="interval between collimation corrections (sec; 0 for none)",
                ),
                ScalarWrapper("collimateSlewEnd", dtype=float,
                    help="""Time before slew end for an extra collimation update (sec);
                            <0 for no extra update (0 simply starts it as the slew ends)""",
                ),
                ArrayWrapper("statusInterval", numElts=3, dtype=float,
                    help="""Interval between status updates (sec; 0 for none):
                            while tracking (ignored), during a slew, and other""",
                ),

                DocWrapper("\nTracking and Slewing Parameters\n"),

                ScalarWrapper("slewAdvTime", dtype=float,
                    help="How far in advance to send the first PVT of a slew (sec)",
                ),
                ScalarWrapper("slewMinDuration", dtype=float,
                    help="""Minimum slew duration after the first PVT is sent (sec);
                            provides time to send the remaining slew PVTs after the first""",
                ),

                ScalarWrapper("trackInterval", dtype=float,
                    help="The interval between tracking updates (sec)",
                ),
                ScalarWrapper("trackAdvTime",  dtype=float,
                    help="How far in advance to send a tracking update (sec)",
                ),

                ScalarWrapper("slewConvTime",  dtype=float,
                    help="""Slew convergence criterion (sec);
                        successive slews must have durations that match to within this time interval""",
                ),
                ScalarWrapper("slewMaxIter",  dtype=int,
                    help="Max iterations of the slew computation",
                ),
                ScalarWrapper("slewFudge",  dtype=float,
                    help="Time for axis controllers to settle after a slew (sec)",
                ),
            ),
        )
            
        self.__WLCache = wrapperList
        return self.__WLCache

    def load(self, f):
        """!Load data from a file-like object
        """
        self._WrapperList.load(f)
    
    def loadPath(self, filePath):
        """!Load data from a file specified by path
        """
        self._WrapperList.loadPath(filePath)
    
    def dump(self, f):
        """!Dump data to a file-like object
        """
        self._WrapperList.dump(f)            
    
    def __repr__(self):
        return self._WrapperList.dumpStr()
