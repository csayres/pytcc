from __future__ import division, absolute_import
"""Support for loading standard fields in a struct-like object from a file
"""
import cStringIO
import collections
import RO.CnvUtil
import coordConv
import tcc.base

# NOTE: if you add a wrapper type then update tests/base/testFieldWrapper.py
__all__ = ["boolFromStr", "intFromStr", "ArrayWrapper", "ChebyshevWrapper", "CoordSysWrapper", "DocWrapper",
    "EnumArrayWrapper", "EnumWrapper",
    "GuideProbeVectorWrapper", "ObsCoordSysWrapper", "InstPositionWrapper",
    "PVAJTArrayWrapper", "PVTArrayWrapper",
    "PVTCoordWrapper", "PxPMRadVelWrapper", "FixedPVTCoordWrapper", "PVTWrapper", "ScalarWrapper", "WrapperList"]

boolFromStr = RO.CnvUtil.asBool


def intFromStr(strVal):
    """!Convert string to int: `0xN` is hex, `N` is decimal, where N is a string of 1 or more digits

    Intended for fields that are likely to contain hex data (e.g. bit masks)
    """
    return int(strVal, 0)


class BaseFieldWrapper(object):
    """!A class that sets the values of a struct-like field
    """
    def __init__(self, name, help, numLines=1):
        """!Construct a BaseFieldWrapper

        @param[in] name  name of field; used for messages and by self.getKey()
        @param[in] help  help text; may be many lines of text; leading spaces on each line are ignored
        @param[in] numLines  the number of lines of (non-comment) data; used by WrapperList.load
            if the number of lines is variable then specify the maximum number of lines
            and have "set" return the number of lines actually used
        """
        self.name = name
        self.help = help
        self.numLines = int(numLines)
        self.helpStrList = ["# %s" % helpStr.strip() for helpStr in help.split("\n")]

    def getField(self, obj):
        """!Return the wrapped field from obj

        @param[in] obj  the struct-like object that this wrapper wraps
        """
        try:
            return getattr(obj, self.name)
        except Exception:
            raise RuntimeError("Field %s does not exist in object %s" % (self.name, obj.__class__.__name__))

    def setField(self, obj, val):
        """!Set the wrapped field to the specified value

        @warning: do not use:
        field = self.getField(obj)
        field = val
        because that just changes your local variable named "field"

        @param[in] obj  the struct-like object that this wrapper wraps
        @param[in] val  the new value for the field
        """
        setattr(obj, self.name, val)

    def getKeyList(self):
        """!Return a list of keys by which this field wrapper is stored in WrapperList

        Most wrappers have a single key (and return a list containing one element),
        but some arrays have one key per element.

        The key should be lowercase so WrapperList.load is not case-sensitive
        """
        return [self.name.lower()]

    def set(self, obj, rawDataList):
        """!Set the field based on raw data.

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of up to numLines of data (less if at the end of the file),
            where data = line.split(). You must check that enough data was supplied.
            The first element of rawDataList[0] is the field name, and is typically ignored
            Trailing comments must have been stripped (and WrapperList.load does this)
            so each element of the list should contain exactly  the correct number of elements.
        @return number of fields used from rawDataList; if None then self.numLines is assumed
        """
        raise NotImplementedError()

    def getStrList(self, obj):
        """!Return help comment and field data as a list of formatted strings
        """
        return self.helpStrList + self.getDataStrList(obj)

    def getDataStrList(self, obj):
        """!Return the field data as a list of formatted strings

        Note that the help data is handled separately.

        This is wrapper-specific, and so must be defined by each subclass
        """
        raise NotImplementedError()

    def _checkRawDataListLen(self, rawDataList):
        """!Assert that len(rawDataList) == self.numLines. Typically called by set.

        @param[in] rawDataList  a list of numLines of data; where data = line.split()
        """
        if len(rawDataList) != self.numLines:
            raise RuntimeError("need %s lines of data; got %s" % (self.numLines, len(rawDataList)))


class ArrayWrapper(BaseFieldWrapper):
    """!Wrapper for a fixed-length array of values, all of the same type
    """
    def __init__(self, name, dtype, numElts, fmt="%r", help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)
        self.dtype = dtype
        if numElts < 1:
            raise RuntimeError("numElts = %s; must be > 0" % (numElts,))
        self.numElts = int(numElts)
        self.fmt = str(fmt)

    def set(self, obj, rawDataList):
        """!Set an array from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[name, val0, val1, val2...., valN]]
        """
        field = self.getField(obj)
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != self.numElts:
            raise RuntimeError("need %d values but got %s" % (self.numElts, rawData))
        field[:] = tuple(self.dtype(val) for val in rawData)
        self.setField(obj, field)
        #!!! previously BUG? no self.setField(obj, ...) figure this out

    def getDataStrList(self, obj):
        field = self.getField(obj)
        strList = [self.fmt % (val,) for val in field[:]]
        return ["%s %s" % (self.name, " ".join(strList))]


class ChebyshevWrapper(BaseFieldWrapper):
    """!Wrapper for a Chebyshev Polynomial
    """
    def __init__(self, name, help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)

    def set(self, obj, rawDataList):
        """!Set a Chebyshev polynomial from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings, containing:
        - name
        - min: minimum allowed x
        - max: maximum allowed x
        - ... one or more Chebyshev polynomial parameters
        """
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) < 3:
            raise RuntimeError("need minX maxX param0 [param1 [param2 [...]]] but got %s" % (rawData,))
        valList = [float(val) for val in rawData]
        minX = valList[0]
        maxX = valList[1]
        params = valList[2:]
        newValue = tcc.base.ChebyshevPolynomial(params, minX, maxX)
        self.setField(obj, newValue)

    def getDataStrList(self, obj):
        cheby = self.getField(obj)
        minX = cheby.getMinX()
        maxX = cheby.getMaxX()
        paramStrList = ["%0.7f" % (val,) for val in cheby.params]
        return ["%s %0.7f %0.7f %s" % (self.name, minX, maxX, " ".join(paramStrList))]


class CoordSysWrapper(BaseFieldWrapper):
    """!Wrapper for coordConv::CoordSys
    """
    def __init__(self, name, help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)

    def set(self, obj, rawDataList):
        """!Set a coordConv::CoordSys from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[fieldName, coordSysName, dateStr]]
        """
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 2:
            raise RuntimeError("need name data but got %s" % (rawData,))
        name = rawData[0]
        date = float(rawData[1])
        newValue = coordConv.makeCoordSys(name, date)
        self.setField(obj, newValue)

    def getDataStrList(self, obj):
        coordSys = self.getField(obj)
        csysName = coordSys.getName()
        date = coordSys.getDate()
        if csysName in ("obs", "apptopo"):
            dateStr = "%0.7f" % (date,)
        else:
            dateStr = "%0.4f" % (date,)
        return ["%s %s %s" % (self.name, csysName, dateStr)]


class DocWrapper(BaseFieldWrapper):
    """!Wrapper that only prints documentation
    """
    def __init__(self, help=""):
        BaseFieldWrapper.__init__(self, name=None, help=help, numLines=0)

    def getDataStrList(self, obj):
        return []


class EnumArrayWrapper(BaseFieldWrapper):
    """!Wrapper for a fixed-length array of enums that receives and prints strings"""
    def __init__(self, name, valDict, numElts, help=""):
        """!Create an EnumArrayWrapper

        @param[in] name  name of field
        @param[in] valDict  dict of integer value: name (case of name is ignored)
        @param[in] numElts  required number of elements
        @param[in] help  string to print before the value (do not include leading comment chars)
        """
        BaseFieldWrapper.__init__(self, name=name, help=help)
        self.valDict = dict((int(item[0]), item[1].lower()) for item in valDict.iteritems())
        nameDict = dict()
        for val, valName in valDict.iteritems():
            nameDict[valName.lower()] = val
        self.nameDict = nameDict # dict of value name: value
        self.numElts = int(numElts)

    def set(self, obj, rawDataList):
        """!Set an array of enums from names or integer values in raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[name, val0, val1, val2...., valN]]
            where each value is a name or the associated integer value
        @return converted value: self.dtype(value)
        """
        field = self.getField(obj)
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != self.numElts:
            raise RuntimeError("need %d values but got %s" % (self.numElts, rawData))
        intList = []
        for valNameOrInt in rawData:
            try:
                intValue = self.nameDict[valNameOrInt.lower()]
            except KeyError:
                try:
                    intValue = intFromStr(valNameOrInt)
                    self.valDict[intValue] # check that the value exists
                except Exception:
                    raise RuntimeError("unknown value %s" % (valNameOrInt,))
            intList.append(intValue)
        field[:] = intList

    def getDataStrList(self, obj):
        field = self.getField(obj)
        strList = [self.valDict[val] for val in field]
        return ["%s %s" % (self.name, " ".join(strList))]


class EnumWrapper(BaseFieldWrapper):
    """!Wrapper for enums that receives and prints strings"""
    def __init__(self, name, valDict, help=""):
        """!Create an EnumWrapper

        @param[in] name  name of field
        @param[in] valDict  dict of integer value: name (case of name is ignored)
        @param[in] help  string to print before the value (do not include leading comment chars)
        """
        BaseFieldWrapper.__init__(self, name=name, help=help)
        self.valDict = dict((int(item[0]), item[1].lower()) for item in valDict.iteritems())
        nameDict = dict()
        for val, valName in valDict.iteritems():
            nameDict[valName.lower()] = val
        self.nameDict = nameDict # dict of value name: value

    def set(self, obj, rawDataList):
        """!Set an enum from its name or integer value in raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[name, value_name_or_int]]
        @return converted value: self.dtype(value)
        """
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 1:
            raise RuntimeError("need 1 value but got %s" % (rawData,))
        valNameOrInt = rawData[0]
        try:
            intValue = self.nameDict[valNameOrInt.lower()]
        except KeyError:
            try:
                intValue = intFromStr(valNameOrInt)
                self.valDict[intValue] # check that the value exists
            except Exception:
                raise RuntimeError("invalid value %s" % (valNameOrInt,))
        self.setField(obj, intValue)

    def getDataStrList(self, obj):
        field = self.getField(obj)
        valName = self.valDict[field]
        return ["%s %s" % (self.name, valName)]


class GuideProbeVectorWrapper(BaseFieldWrapper):
    """!Wrapper for inst.gProbe vector

    Guide probes are numbered starting at one.
    On input, the probe number must be for the next available slot in the vector.
    """
    def __init__(self, name, help=""):
        if help:
            help += "\n"
        help += """probe number (starting from 1 and strictly sequential), does probe exist?
            if and only if the probe exists then this data must follow in order:
            center of guide probe (guide image x,y unbinned pix)
            lower left corner of the smallest box containing the probe (guide image x,y unbinned pix)
            upper right corner of the smallest box containing the probe (guide image x,y unbinned pix)
            position of rotator with respect to probe center (x,y deg on sky)
            angle of rotator x axis with respect to guide image (deg)"""

        BaseFieldWrapper.__init__(self, name=name, help=help, numLines=6)

    def set(self, obj, rawDataList):
        """!Set guide probe data from raw string data

        This will consume 1 line of data if the probe does not exist, 6 lines of data if the probe exists

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of lists of strings; the format depends if the probe exists:
            if probe exists: [
                [fieldName, probeNum, T],
                [xCtr, yCtr],
                [xMin, yMin],
                [xMax, yMax],
                [gp_rot_x, gp_rot_y],
                [rot_gim_ang],
            ]
            if probe does not exist: [[fieldName, probeNum, F]]
        @return the number of lines consumed

        @note: valid values for exists include 0, F, False, 1, T, True
        """
        field = self.getField(obj)
        numProbes = len(field)
        firstRawData = rawDataList[0][1:]
        if len(firstRawData) != 2:
            raise RuntimeError("need probe number and exists in first line, but got %s" % (firstRawData,))
        probeNum = int(firstRawData[0])
        if probeNum < 1:
            raise RuntimeError("guide probe numbers start at 1, but got %d" % (probeNum,))
        gProbe = tcc.base.GuideProbe()
        gProbe.exists = boolFromStr(firstRawData[1])

        if not gProbe.exists:
            numLines = 1
        else:
            if len(rawDataList) != 6:
                raise RuntimeError("need 1 or 6 lines of data, but got %s" % (len(rawDataList),))
            numLines = 6
            gProbe.ctr[:]       = getNFloats(2, rawDataList[1])
            gProbe.minXY[:]     = getNFloats(2, rawDataList[2])
            gProbe.maxXY[:]     = getNFloats(2, rawDataList[3])
            gProbe.rot_gp_xy[:] = getNFloats(2, rawDataList[4])
            gProbe.rot_gim_ang  = getNFloats(1, rawDataList[5])[0]

        if probeNum > numProbes:
            if probeNum > numProbes + 1:
                raise RuntimeError("have %d probes; next probe number must be %d but got %d" % (numProbes, numProbes + 1, probeNum))
            field.append(gProbe)
        else:
            field[probeNum - 1] = gProbe
        return numLines

    def getDataStrList(self, obj):
        gProbeList = self.getField(obj)
        strList = []
        if gProbeList:
            for ind, gProbe in enumerate(gProbeList):
                probeNum = ind + 1
                strList.append("%s  %3d  %5s  # probe number, exists?" % (self.name, probeNum, gProbe.exists))
                if gProbe.exists:
                    strList += [
                        "%8.1f  %8.1f  # center of probe (guide image x,y unbinned pix)" % tuple(gProbe.ctr),
                        "%8.1f  %8.1f  # lower left corner of probe (guide image x,y unbinned pix)" % tuple(gProbe.minXY),
                        "%8.1f  %8.1f  # upper right corner of probe (guide image x,y unbinned pix)" % tuple(gProbe.maxXY),
                        "%8.5f  %8.5f  # position of rotator with respect to probe center (x,y deg on sky)" % tuple(gProbe.rot_gp_xy),
                        "%8.1f            # angle of rotator with respect to guide image (deg)" % (gProbe.rot_gim_ang,),
                    ]
        else:
            strList += [
                "# Note: this instrument has no guide probes",
                "",
            ]
        return strList


class InstPositionWrapper(BaseFieldWrapper):
    """!Wrapper for InstPosition

    For input both the new names (e.g. "?", "NA2") and old names (e.g. "0", "6") are accepted.
    The new names are always output.
    """
    # translate old TCC instrument position number strings to new names
    _OldNewDict = {
	    "0": "?",
        "1": "PF1",
        "2": "CA1",
        "3": "BC1",
        "4": "BC2",
        "5": "NA1",
        "6": "NA2",
        "7": "TR1",
        "8": "TR2",
        "9": "TR3",
        "10": "TR4",
        "11": "SK1",
        "12": "SK2",
        "13": "SK3",
        "14": "SK4",
        "15": "MB1",
        "16": "MB2",
        "17": "MB3",
        "18": "MB4",
    }
    def __init__(self, name, help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)

    def set(self, obj, rawDataList):
        """!Set instrument position name from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[field name, instrument position name]]
        """
        field = self.getField(obj)
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 1:
            raise RuntimeError("need instrument position name, but got %s" % (rawData,))
        instPosName = str(rawData[0]).upper()
        instPosName = self._OldNewDict.get(instPosName, instPosName)
        field.setName(instPosName)

    def getDataStrList(self, obj):
        field = self.getField(obj)
        return ["%s %s" % (self.name, field.getName())]


class ObsCoordSysWrapper(BaseFieldWrapper):
    """!Wrapper for coordConv::ObsCoordSys (user cannot change the system)
    """
    def __init__(self, name, help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)

    def set(self, obj, rawDataList):
        """!Set the date of an ObsCoordSys field from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[name, taiDate]], where taiDate is in MJD, seconds
        """
        field = self.getField(obj)
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 1:
            raise RuntimeError("needs TAI date (MJD, sec), but got %s" % (rawData,))
        taiDate = float(rawData[0])
        field.setDate(taiDate)

    def getDataStrList(self, obj):
        coordSys = self.getField(obj)
        return ["%s %s" % (self.name, coordSys.getDate(),)]

class PVAJTArrayWrapper(BaseFieldWrapper):
    """!Wrapper for a fixed-length array of PVAJT
    """
    def __init__(self, name, numElts, help):
        BaseFieldWrapper.__init__(self, name=name, help=help, numLines=numElts)

    def set(self, obj, rawDataList):
        """!Set an array of PVAJT from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of numElts lists of strings: [
            [<i>field name</i>1, position, velocity, acceleration, jerk, tai date],
            [<i>field name</i>2, position, velocity, acceleration, jerk, tai date],
            ...
        ]
        """
        field = self.getField(obj)
        self._checkRawDataListLen(rawDataList)
        for ind, nameRawData in enumerate(rawDataList):
            name = nameRawData[0]
            rawData = nameRawData[1:]
            expectedName = "%s%d" % (self.name, ind+1)
            if name.lower() != expectedName.lower():
                raise RuntimeError("Expected name %r but got %r" % (expectedName, name))
            if len(rawData) != 5:
                raise RuntimeError("%s needs pos vel accel jerk time, but got %s" % (expectedName, rawData))
            field[ind].pos, field[ind].vel, field[ind].accel, field[ind].jerk, field[ind].t = tuple(float(val) for val in rawData[0:5])

    def getDataStrList(self, obj):
        field = self.getField(obj)
        return ["%s%d %0.6f %0.6f %0.6f %0.6f %0.7f" % \
            (self.name, ind+1, field[ind].pos, field[ind].vel, field[ind].accel, field[ind].jerk, field[ind].t) \
                for ind in range(self.numLines)]

    def getKeyList(self):
        return tuple("%s%d" % (self.name.lower(), ind+1) for ind in range(self.numLines))

class PVTArrayWrapper(BaseFieldWrapper):
    """!Wrapper for a fixed-length array of coordConv.PVT
    """
    def __init__(self, name, numElts, help):
        BaseFieldWrapper.__init__(self, name=name, help=help, numLines=numElts)

    def set(self, obj, rawDataList):
        """!Set an array of coordConv.PVT from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of numElts lists of strings: [
            [<i>field name</i>1, position, velocity, tai date],
            [<i>field name</i>2, position, velocity, tai date],
            ...
        ]
        """
        field = self.getField(obj)
        self._checkRawDataListLen(rawDataList)
        for ind, nameRawData in enumerate(rawDataList):
            name = nameRawData[0]
            rawData = nameRawData[1:]
            expectedName = "%s%d" % (self.name, ind+1)
            if name.lower() != expectedName.lower():
                raise RuntimeError("Expected name %r but got %r" % (expectedName, name))
            if len(rawData) != 3:
                raise RuntimeError("%s needs pos vel time, but got %s" % (expectedName, rawData))
            field[ind].pos, field[ind].vel, field[ind].t = tuple(float(val) for val in rawData[0:3])

    def getDataStrList(self, obj):
        field = self.getField(obj)
        return ["%s%d %0.6f %0.6f %0.7f" % (self.name, ind+1, field[ind].pos, field[ind].vel, field[ind].t) \
                for ind in range(self.numLines)]

    def getKeyList(self):
        return tuple("%s%d" % (self.name.lower(), ind+1) for ind in range(self.numLines))


class PVTCoordWrapper(BaseFieldWrapper):
    """!Wrapper for coordConv.PVTCoord
    """
    def __init__(self, name, help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)

    def set(self, obj, rawDataList):
        """!Set a coordConv.PVTCoord from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings:
            [[name, equatAng, polarAng, equatVel, polarVel, taiDate, dist, distVel, equatPM, polarPM, radVel]]
        """
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 10:
            raise RuntimeError("need equatAng, polarAng, equatVel, polarVel, taiDate, dist, distVel, equatPM, polarPM, radVel, but got %s" % (rawData,))
        equatAng, polarAng, equatVel, polarVel, tai, dist, distVel, equatPM, polarPM, radVel = tuple(float(v) for v in rawData)
        equatPVT = coordConv.PVT(equatAng, equatVel, tai)
        polarPVT = coordConv.PVT(polarAng, polarVel, tai)
        distPVT  = coordConv.PVT(dist, distVel, tai)
        newValue = coordConv.PVTCoord(equatPVT, polarPVT, distPVT, equatPM, polarPM, radVel)
        self.setField(obj, newValue)

    def getDataStrList(self, obj):
        pvtCoord = self.getField(obj)

        equatPVT = coordConv.PVT()
        polarPVT = coordConv.PVT()
        pvtCoord.getSphPVT(equatPVT, polarPVT)
        distPVT = pvtCoord.getDistance()

        coord = pvtCoord.getCoord()
        atPole, equatPM, polarPM = coord.getPM()
        radVel = coord.getRadVel()
        return ["%s %0.7f %0.7f %0.7f %0.7f %0.6f %0.3f %0.3f %0.5f %0.5f %0.5f" % \
            (self.name, equatPVT.pos, polarPVT.pos,  equatPVT.vel, polarPVT.vel, equatPVT.t,
             distPVT.pos, distPVT.vel, equatPM, polarPM, radVel)]

class PxPMRadVelWrapper(BaseFieldWrapper):
    def __init__(self, name, help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)

    def set(self, obj, rawDataList):
        """!Set the date of an ObsCoordSys field from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[parallax, equatPM, polarPM, radVel]]
        """
        field = self.getField(obj)
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 4:
            raise RuntimeError("needs parallax, equatPM, polarPM, radVel but got %s" % (rawData,))
        field.parallax = float(rawData[0])
        field.equatPM = float(rawData[1])
        field.polarPM = float(rawData[2])
        field.radVel = float(rawData[3])

    def getDataStrList(self, obj):
        pxPMRadVel = self.getField(obj)
        return ["%s %0.6f %0.3f %0.3f %0.3f" % (self.name, pxPMRadVel.parallax, pxPMRadVel.equatPM, pxPMRadVel.polarPM, pxPMRadVel.radVel)]


class FixedPVTCoordWrapper(BaseFieldWrapper):
    """!Wrapper for coordConv.PVTCoord with no proper motion or radial velocity
    """
    def __init__(self, name, help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)

    def set(self, obj, rawDataList):
        """!Set a coordConv.PVTCoord from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings:
            [[name, equatAng, polarAng, equatVel, polarVel, taiDate, dist, distVel]]
        """
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 7:
            raise RuntimeError("need equatAng, polarAng, equatVel, polarVel, taiDate, dist, distVel, but got %s" % (rawData,))
        equatAng, polarAng, equatVel, polarVel, tai, dist, distVel = tuple(float(v) for v in rawData)
        equatPVT = coordConv.PVT(equatAng, equatVel, tai)
        polarPVT = coordConv.PVT(polarAng, polarVel, tai)
        distPVT  = coordConv.PVT(dist, distVel, tai)
        newValue = coordConv.PVTCoord(equatPVT, polarPVT, distPVT)
        self.setField(obj, newValue)

    def getDataStrList(self, obj):
        pvtCoord = self.getField(obj)

        equatPVT = coordConv.PVT()
        polarPVT = coordConv.PVT()
        pvtCoord.getSphPVT(equatPVT, polarPVT)
        distPVT = pvtCoord.getDistance()
        return ["%s %0.7f %0.7f %0.7f %0.7f %0.6f %0.3f %0.3f" % \
            (self.name, equatPVT.pos, polarPVT.pos, equatPVT.vel, polarPVT.vel, equatPVT.t, distPVT.pos, distPVT.vel)]


class PVTWrapper(BaseFieldWrapper):
    """!Wrapper for a single coordConv.PVT
    """
    def getDataStrList(self, obj):
        field = self.getField(obj)
        return ["%s %0.6f %0.6f %0.7f" % (self.name, field.pos, field.vel, field.t)]

    def set(self, obj, rawDataList):
        """!Set a coordConv.PVT from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[name, position, velocity, time]]
        """
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 3:
            raise RuntimeError("need pos vel time but got %s" % (rawData,))
        newValue = coordConv.PVT(*(float(val) for val in rawData))
        self.setField(obj, newValue)


class ScalarWrapper(BaseFieldWrapper):
    """!Wrap a single value
    """
    def __init__(self, name, dtype, fmt="%r", help=""):
        BaseFieldWrapper.__init__(self, name=name, help=help)
        self.dtype = dtype
        self.fmt = str(fmt)

    def set(self, obj, rawDataList):
        """!Set a scalar from raw string data

        @param[in] obj  the struct-like object whose field is to be set;
            use self.getField(obj) to retrieve the field
        @param[in] rawDataList  a list of one list of strings: [[name, value]]
        @return converted value: self.dtype(value)
        """
        self._checkRawDataListLen(rawDataList)
        rawData = rawDataList[0][1:]
        if len(rawData) != 1:
            raise RuntimeError("need 1 value but got %s" % (rawData,))
        newValue = self.dtype(rawData[0])
        self.setField(obj, newValue)

    def getDataStrList(self, obj):
        field = self.getField(obj)
        valStr = self.fmt % (field,)
        return ["%s %s" % (self.name, valStr)]


class WrapperList(object):
    """!An ordered collection of field wrappers
    """
    def __init__(self, name, obj, wrappers=(), aliases=(), obsoletes=()):
        """!Construct a WrapperList

        Inputs:
        - name: name of struct-like object whose fields are being wrapped
        - obj: struct-like object being wrapped
        - wrappers: an ordered collection of field wrappers
        - aliases: an ordered collection of (alias, name) pairs; see addAlias
        - obsoletes: an ordered collection of (name, numLines); see addObsolete
        """
        self.name = name
        self.obj = obj
        self.wrapperDict = collections.OrderedDict() # dict of key: field wrapper
        self.wrapperList = [] # list of all wrappers, including documentation-only wrappers
        self.aliasDict = dict()
        self.obsoleteDict = dict()
        for fieldWrapper in wrappers:
            self.add(fieldWrapper)
        for oldName, newName in aliases:
            self.addAlias(oldName, newName)
        for oldName, numLines in obsoletes:
            self.addObsolete(oldName, numLines)

    def add(self, wrapper):
        """!Add a field wrapper
        """
        if wrapper.name:
            wrapper.getField(self.obj) # test that field exists in obj
            keyList = wrapper.getKeyList()
            for key in keyList:
                if key in self.wrapperDict:
                    raise KeyError("Field wrapper %r already exists" % (key,))
            if wrapper.numLines <= 0:
                raise RuntimeError("wrapper %s numLines = %s <= 0" % (key, wrapper.numLines,))
            for key in keyList:
                self.wrapperDict[key] = wrapper
        self.wrapperList.append(wrapper)

    def addAlias(self, oldName, newName):
        """!Add an oldName for another field or an obsolete newName to ignore (if name==None)

        Inputs:
        - oldName: old name in data file (case ignored)
        - newName: current name in data file
        """
        if newName.lower() not in self.wrapperDict:
            raise KeyError("No field named %s" % (newName,))
        self.aliasDict[oldName.lower()] = newName

    def addObsolete(self, oldName, numLines):
        """!Add an obsolete item to ignore

        Inputs:
        - oldName: name in data file (case ignored)
        - numLines: number of lines of data for that field
        """
        self.obsoleteDict[oldName.lower()] = int(numLines)

    def load(self, f):
        """!Load data from a file (or array of strings). Only fields specified in the data file are changed.

        @param[in] f  a file or file-like object that supports line-based iteration
        """
        rawDataList = [tokenize(line.strip()) for line in f]
        rawDataList = [rd for rd in rawDataList if rd] # strip lines that contain no data

        numLinesInFile = len(rawDataList)
        ind = 0
        while ind < numLinesInFile:
            name = rawDataList[ind][0]
            key = name.lower()
            if key in self.obsoleteDict:
                numLinesToSkip = self.obsoleteDict[key]
                ind += numLinesToSkip
                continue

            if key in self.aliasDict:
                newName = self.aliasDict[key]
                print "Warning: the new name for %s is %s" % (name, newName)
                key = newName.lower()

            try:
                wrapper = self.wrapperDict[key]
            except KeyError:
                raise KeyError("No field named %s" % (name,))

            maxLinesForField = wrapper.numLines
            subList = rawDataList[ind:ind+maxLinesForField]
            try:
                numLinesUsed = wrapper.set(self.obj, subList)
            except Exception as e:
                raise RuntimeError("Failed to set %s: %s" % (name, e))
            if numLinesUsed is None:
                numLinesUsed = maxLinesForField
            if numLinesUsed <= 0:
                raise RuntimeError("numLinesUsed = %s <= 0" % (numLinesUsed,))
            ind += numLinesUsed

    def loadPath(self, filePath):
        """!Load data from a file specified by path. Only fields specified in the data file are changed.

        @param[in] filePath  path of data file
        """
        with file(filePath, "rU") as f:
            self.load(f)

    def dump(self, f):
        """!Dump data to a file

        Inputs:
        @param[in] f  a file or file-like object that supports writelines
        """
        f.write("# %s data\n" % (self.name,))
        for wrapper in self.wrapperList:
            for outStr in wrapper.getStrList(self.obj):
                f.writelines((outStr, "\n"))

    def dumpStr(self):
        """!Dump the data to a string
        """
        strFile = cStringIO.StringIO()
        self.dump(strFile)
        return strFile.getvalue()


def getNInts(numVals, strDataList):
    """!Return numVals integers

    @param[in] numVals  the number of values required
    @param[in] strDataList  list of string values to be converted
    @return a tuple of numVals ints
    @throw RuntimeError if the wrong number of values are provided or if they are the wrong type
    """
    try:
        if len(strDataList) != numVals:
            raise RuntimeError()
        return tuple(intFromStr(val) for val in strDataList)
    except Exception:
        raise RuntimeError("need %s integers but got %s" % (strDataList,))


def getNFloats(numVals, strDataList):
    """!Return numVals floats

    @param[in] numVals  the number of values required
    @param[in] strDataList  list of string values to be converted
    @return a tuple of numVals ints
    @throw RuntimeError if the wrong number of values are provided or if they are the wrong type
    """
    try:
        if len(strDataList) != numVals:
            raise RuntimeError()
        return tuple(float(val) for val in strDataList)
    except Exception:
        raise RuntimeError("need %s floats but got %s" % (strDataList,))


def tokenize(line):
    """!Break a line of whitespace-separated data into individual tokens

    Strings are one token: strings are delimited with " or ',
        but a string can contain its delimiter char if followed by \
    Comments are stripped: all text following # or ! (except within a string)
    Escaped chars are not treated as delimiters: text followed by \ is retained
    Values are separated by one or more spaces, tabs or commas (yes, even multiple commas)
    """
    dataList = []
    tokenChars = [] # list of chars in current token
    inToken = False # set True if processing a token
    strDel = None # set to delim if in string
    inEsc = False # set to True if \ escape char seen, set false for next char
    for i, c in enumerate(line):
        if inToken:
            if inEsc:
                inEsc = False
                tokenChars.append(c)
            elif c == strDel:
                # end of string; omit final string delimiter
                dataList.append("".join(tokenChars))
                tokenChars = []
                strDel = None
                inToken = False
                inEsc = False
            elif c == "\\":
                if strDel:
                    # start escape pair
                    inEsc = True
                else:
                    raise RuntimeError("unexpected \\ in %r at index %d" % (line, i))
            elif c in (" ", "\t", ",") and not strDel:
                if not strDel:
                    # end of token
                    dataList.append("".join(tokenChars))
                    tokenChars = []
                    inToken = False
                    inEsc = False
            else:
                tokenChars.append(c)
        else:
            if c in ("#", "!"):
                # start a comment, which extends to the end of the line
                break
            if c in (" ", "\t", ","):
                continue
            elif c in ("\"", "'"):
                # start a string; omit starting delimiter
                strDel = c
                tokenChars = []
                inToken = True
            elif c == "\\":
                # escape character: an error, since these can only occur in delimited strings
                raise RuntimeError("unexpected \\ in %r at index %d" % (line, i))
            else:
                tokenChars = [c]
                inToken = True

    if inToken:
        # final token ends at end of line
        dataList.append("".join(tokenChars))

    return dataList
