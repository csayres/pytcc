from __future__ import division, absolute_import

__all__ = ["AxisLim"]

class AxisLim(object):
    """Motion limits for one axis
    
    See also AxeLim: motion limits for all axes
    """
    def __init__(self, minPos, maxPos, vel, accel, jerk):
        """Construct an AxisLim
        
        @param[in] minPos  minimum position (deg)
        @param[in] maxPos  maximum position (deg)
        @param[in] vel     maximum velocity (deg/sec)
        @param[in] accel   maximum acceleration (deg/sec^2)
        @param[in] jerk    maximum jerk (deg/sec^3)
        """
        self.minPos = minPos
        self.maxPos = maxPos
        self.vel = vel
        self.accel = accel
        self.jerk = jerk

    def __repr__(self):
        return "AxisLim(minPos=%s, maxPos=%s, vel=%s, accel=%s, jerk=%s)" % \
            (self.minPos, self.maxPos, self.vel, self.accel, self.jerk)
