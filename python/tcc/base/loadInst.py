from __future__ import division, absolute_import

import glob
import os.path

from twistedActor import BaseActor, CommandError

import tcc.base

__all__ = ["loadInst"]

def loadInst(instDir, instName, gcViewName=None, writeToUsers=None, cmd=None):
    """!Create an instrument block for the specified instrument
    
    @param[in] instDir  path to instrument dir (typically tcc.base.getInstDir())
    @param[in] instName  name of instrument (e.g. "DIS"); case is ignored; may be an abbreviation if unique
    @param[in] gcViewName  name of guide camera view; unlike instName, this may not be an abbreviation;
                if "" or None then no guide camera view is loaded
    @param[in] writeToUsers  tccActor.writeToUsers or a similar function; if None, write to stdout
    @param[in] cmd  command (twistedActor.BaseCmd) associated with this request, or None
    @return an Inst

    @throw CommandError if instrument not found or name not unique

    @warning: does not update the AxeLim block (you must do that separately)
    """
    if writeToUsers is None:
        writeToUsers = BaseActor.writeToStdOut
    gcViewName = gcViewName or "" # convert None to ""

    # make sure instName is a unique abbreviation and find the associated instrument position
    instGlobExpr = "i_[a-z][a-z][0-9]_%s*.dat" % (instName.lower(),)
    instFilePathList = glob.glob(os.path.join(instDir, instGlobExpr))
    if not instFilePathList:
        raise CommandError("Could not find instrument %r in %r" % (instName, instDir))
    if len(instFilePathList) > 1:
        raise CommandError("Found more than one instrument named %r in %r" % (instName, instDir))
    instFilePath = instFilePathList[0]
    instFileName = os.path.basename(instFilePath)
    instPosName, fullInstName = instFileName.split("_", 2)[1:3]
    fullInstName = fullInstName[:-4] # strip .dat
    
    def fileExists(fileName):
        return os.path.isfile(os.path.join(instDir, fileName))
    
    # generate names of data files to load
    dataFileNameList = []

    defFileName = "default.dat"
    if not fileExists("default.dat"):
        raise RuntimeError("Could not find default file %r in %r" % (defFileName, instDir))
    dataFileNameList.append(defFileName)

    instPosFileName = "ip_%s.dat" % (instPosName,)
    if fileExists(instPosFileName):
        dataFileNameList.append(instPosFileName)    
    
    if gcViewName:
        gcViewFileName = "v_%s_%s.dat" % (instPosName, gcViewName)
        if not fileExists(gcViewFileName):
            raise RuntimeError("Could not find guide camera view file %r in %r" % (gcViewFileName, instDir))
        dataFileNameList.append(gcViewFileName)
    
    dataFileNameList.append(instFileName)
    
    # load data files
    inst = tcc.base.Inst()
    for fileName in dataFileNameList:
        filePath = os.path.join(instDir, fileName)
        inst.loadPath(filePath)
    writeToUsers("i", "InstFiles=%s" % (", ".join('"%s"' % (fileName,) for fileName in dataFileNameList),), cmd=cmd)
    
    # override the few values that are set based on file name (instead of file contents)
    inst.instName = fullInstName
    inst.gcViewName = gcViewName
    inst.instPos.setName(instPosName.upper())
    return inst
