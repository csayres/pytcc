from __future__ import division, absolute_import

import functools
import re

from twisted.internet.defer import gatherResults
import coordConv
import RO.Alg

from tcc.actor import TCC35mDispatcherWrapper
from tcc.base import WrapTypeValDict, tai, RotTypeNameDict #,  AxisStateValDict
from tcc.parse import CoordCmdNameDict

from tcc.base.wrapperTestCase import WrapperTestCase
"""General classes and tests that various track testing files may inherit from
"""

class GenerateChebyFile(object):
    userSys = "obs"
    userDate = 0

    def __call__(self, filePath):
        """!Generate a chebyshev file for tracking

        The range of times extends 30 minutes before and after the current date.

        @param[in] filePath directory where to write the file
        @return a tempory file object
        """
        startTime = tai() - 1800
        endTime = startTime + 3600

        text="""! name of object
Name "testCheby"

userSys %s %i

! Chebyshev  coefficients for user position 1 (deg)
ChebyUser1 %.1f %.1f 12 0.375093

! Chebyshev coefficients for user position  2 (deg)
ChebyUser2 %.1f %.1f 12 0.190697

! Chebyshev coefficients for distance (au)
ChebyDist %.1f %.1f 0.002576 4.9833e-09""" % (self.userSys, self.userDate, startTime, endTime, startTime, endTime, startTime, endTime)
        with open(filePath, "w") as f:
            f.write(text)

# now a function with attributes userSys and userDate
generateChebyFile = GenerateChebyFile()

CoordSysMatchList = RO.Alg.MatchList(valueList = CoordCmdNameDict.keys(), abbrevOK=True, ignoreCase=True)
RotTypeMatchList = RO.Alg.MatchList(valueList = RotTypeNameDict.keys(), abbrevOK=True, ignoreCase=True)

class LessTests(object):
    # more or less a short number of tests to be inherited by a TestCase

    def checkTrackCmd(self, trackCmd, shouldRotate=False):
        """!Run the track command and see if it has the expected results.

        The details (including the desired initial state) depends on the unit test, so this must be
        defined by each unit test.
        """
        raise NotImplementedError()

    def testObs(self):
        trackCmd = "track 12,12 observed"
        return self.checkTrackCmd(trackCmd)

    def testMount(self):
        trackCmd = "track 12,12 mount"
        return self.checkTrackCmd(trackCmd)

    def testFK51(self):
        trackCmd = "track %.2f, %.2f FK5" % tuple(self._getGoodFK5Coord())
        return self.checkTrackCmd(trackCmd)

    def testTrackRot1(self):
        """!specify /rottype and /rotangle
        """
        trackCmd = "track 12,12 mount /rottype=mount /rotangle=20" # this works
        return self.checkTrackCmd(trackCmd, shouldRotate=True)

    def testTrackStop(self):
        """!Start tracking then stop it
        """
        d1 = self.checkTrackCmd("track 12,12 observed")
        def checkTrackCmd(cmdVar):
            if cmdVar.lastCode == ">":
                self.checkAxisCmdState(cmdVar,
                    axisCmdState = ["Halting", "Halting", "Halted"],
                    checkActor = False,
                )
            elif cmdVar.isDone:
                self.checkTrackResult(cmdVar,
                    axisCmdState = ["Halted", "Halted", "Halted"],
                    coordSysName = "none",
                    coordSysDate = 0,
                )
        d2, cmd = self.dw.queueCmd(
            "track/stop",
            callFunc = checkTrackCmd,
            callCodes = ">:",
        )
        return gatherResults([d1, d2])

class AllTests(LessTests):
    # define the rest of the tests
    def testTopo(self):
        trackCmd = "track 12,12 topocentric"
        return self.checkTrackCmd(trackCmd)

    def testFK52(self):
        trackCmd = "track %.2f, %.2f FK5=2000" % tuple(self._getGoodFK5Coord())
        return self.checkTrackCmd(trackCmd)

    def testFK53(self):
        trackCmd = "track %.2f, %.2f FK5=1950" % tuple(self._getGoodFK5Coord())
        return self.checkTrackCmd(trackCmd)

    def testTrackPM1(self):
        """!use the PM qualifier for FK5 tracking
        """
        trackCmd = "track %.2f, %.2f FK5 /PM=(52, 26)" % tuple(self._getGoodFK5Coord())
        return self.checkTrackCmd(trackCmd)

    def testTrackRot2(self):
        """!specify /rottype and /rotangle
        """
        trackCmd = "track 12,12 observed /rottype=object /rotangle=20" # could not compute roll and pitch
        return self.checkTrackCmd(trackCmd, shouldRotate=True)

    def testTrackBadRot(self):
        """!specify a nonsense /rottype
        """
        trackCmd = "track 12,12 mount /rottype=object /rotangle=20"
        def shouldFail(cmdVar):
            self.assertTrue(cmdVar.didFail)
        d, cmd = self.dw.queueCmd(
            trackCmd,
            callCodes = "F:",
            callFunc = shouldFail
        )
        return d

    def testTrackRot3(self):
        """!specify /rottype and /rotangle
        """
        trackCmd = "track 12,12 observed /rottype=horizon /rotangle=20" # could not compute roll and pitch
        return self.checkTrackCmd(trackCmd, shouldRotate=True)

    def testTrackPM2(self):
        """!use the PM qualifier for FK5 tracking
        """
        trackCmd = "track %.2f, %.2f FK5 /PM=(-52, 26)" % tuple(self._getGoodFK5Coord())
        return self.checkTrackCmd(trackCmd)

    def testTrackWrap(self):
        """!use the azwrap and rotwrap qualifiers
        """
        def checkWrap(foo="bar"):
            self.assertEqual(WrapTypeValDict[self.actor.obj.azWrapPref].lower(), "negative")
            self.assertEqual(WrapTypeValDict[self.actor.obj.rotWrapPref].lower(), "positive")
        trackCmd = "track 10,10 observed /Azwrap=negative /rotwrap=positive"
        d1 = self.checkTrackCmd(trackCmd)#, callFunc=checkWrap)
        d1.addCallback(checkWrap)
        return d1

    def testTrackDist(self):
        """!use distance qualifier
        """
        dist = 10 * 206264 # 10 parsecs in au
        trackCmd = "track %.2f, %.2f FK5 /distance=%.2f" % (self._getGoodFK5Coord()[0], self._getGoodFK5Coord()[1], dist)
        return self.checkTrackCmd(trackCmd)

    def testTrackPx(self):
        """!user parallax
        """
        dist = 1./(10.) # 1/10 parsecs
        coord = self._getGoodFK5Coord()
        trackCmd = "track %.2f, %.2f FK5 /Px=%.2f" % (coord[0], coord[1], dist)
        return self.checkTrackCmd(trackCmd)

    def testTrackRv(self):
        """!user rv
        """
        vel = 200 # km/s
        coord = self._getGoodFK5Coord()
        trackCmd = "track %.2f, %.2f FK5 /RV=%.2f" % (coord[0], coord[1], vel)
        return self.checkTrackCmd(trackCmd)

    def testTrackName(self):
        """!user name qual
        """
        trackCmd = "track 12,12 observed /Name=Conor"
        return self.checkTrackCmd(trackCmd)

    def testTrackMag(self):
        """!user name qual
        """
        trackCmd = "track 12,12 observed /Magnitude=18"
        return self.checkTrackCmd(trackCmd)

    def testTrackNoAbsRefCorr(self):
        """!use the noabsrefcorr qualifier
        """
        trackCmd = "track 12,12 observed /NoAbsRefCorrect"
        return self.checkTrackCmd(trackCmd)

    def testTrackNoRefCoeffs(self):
        """!use the norefcoeffs qualifier
        """
        trackCmd = "track 12,12 observed /NoRefCoefficients"
        return self.checkTrackCmd(trackCmd)

    def testTrackNoCollimate(self):
        """!use the nocollimate qualifier
        """
        trackCmd = "track 12,12 observed /NoCollimate"
        return self.checkTrackCmd(trackCmd)

    def testTrackPerfect(self):
        """!use the perfect qualifier
        """
        trackCmd = "track 12,12 observed /Perfect/rottype=mount/rotangle=15"
        return self.checkTrackCmd(trackCmd, shouldRotate=True)

    def testTrackOffset(self):
        """!use the /keep offset qualifier
        """
        self.actor.obj.userArcOff[0] = coordConv.PVT(3, 0, tai())
        self.actor.obj.objInstXY[0] = coordConv.PVT(3, 0, tai())
        trackCmd = "track 12,12 observed /Keep=(ArcOffset, noboresight)"
        def checkOffsets(cmdVar=None):
            self.assertEqual(self.actor.obj.userArcOff[0].pos, 3)
            self.assertEqual(self.actor.obj.objInstXY[0].pos, 0)
        d1 = self.checkTrackCmd(trackCmd)#, callFunc=checkOffsets)
        d1.addCallback(checkOffsets)
        return d1

    def testTrackRestart(self):
        """!use the restart qualifier
        """
        axesHalted = ["Halted"]*3
        d1, cmd1 = self.dw.queueCmd(
            "track /stop", # enforce all axes halted
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = axesHalted),
        )

        def checkTrackCmd(cmdVar):
            if cmdVar.lastCode == ">":
                self.checkAxisCmdState(cmdVar, axisCmdState = ["Slewing"]*2 + ["Halted"])
            elif cmdVar.isDone:
                self.checkAxisCmdState(cmdVar, axisCmdState = ["Tracking"]*2 + ["Halted"])

        trackCmd = "track 12,12 observed /Restart=noRot"
        d2, cmd2 = self.dw.queueCmd(
            trackCmd,
            callFunc = checkTrackCmd,
            callCodes = ">:"
        )
        return gatherResults([d1, d2])

    def testTrackRestart2(self):
        """!use the restart qualifier
        """
        def shouldFail(cmdVar):
            self.assertTrue(cmdVar.didFail)
        axesHalted = ["Halted"]*3
        d1, cmd1 = self.dw.queueCmd(
            "track /stop", # enforce all axes halted
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = axesHalted),
        )
        d2, cmd2 = self.dw.queueCmd(
            "track 12,12 observed /NoRestart", # nothing should restart
            callCodes = "F:",
            callFunc = shouldFail,
            # callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = axesHalted),
        )
        return gatherResults([d1, d2])

    def testTrackRestart3(self):
        """!use the restart qualifier
        """
        axesHalted = ["Halted"]*3
        d1, cmd1 = self.dw.queueCmd(
            "track /stop", # enforce all axes halted
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState=axesHalted),
        )

        def checkTrackCmd(cmdVar):
            if cmdVar.lastCode == ">":
                self.checkAxisCmdState(cmdVar, axisCmdState = ["Slewing"]*2 + ["Halted"])
            elif cmdVar.isDone:
                self.checkAxisCmdState(cmdVar, axisCmdState = ["Tracking"]*2 + ["Halted"])

        trackCmd = "track 12,12 observed /Restart"
        d2, cmd2 = self.dw.queueCmd(
            trackCmd,
            callFunc = checkTrackCmd,
            callCodes = ">:"
        )
        return gatherResults([d1, d2])

    def testTrackRestart4(self):
        """!use the restart qualifier
        """
        axesHalted = ["Halted"]*3
        d1, cmd1 = self.dw.queueCmd(
            "track /stop", # enforce all axes halted
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = axesHalted),
        )

        def checkTrackCmd(cmdVar):
            if cmdVar.lastCode == ">":
                self.checkAxisCmdState(cmdVar, axisCmdState = ["Slewing"]*2 + ["Halted"])
            elif cmdVar.isDone:
                self.checkAxisCmdState(cmdVar, axisCmdState = ["Tracking"]*2 + ["Halted"])

        trackCmd = "track 12,12 observed /Restart=(All, noRot)"
        d2, cmd2 = self.dw.queueCmd(
            trackCmd,
            callFunc = checkTrackCmd,
            callCodes = ">:"
        )
        return gatherResults([d1, d2])

class TrackCommonMethods(WrapperTestCase):
    def setUp(self):
        self.dw = TCC35mDispatcherWrapper()
        self.dw.readyDeferred.addCallback(self._initCmds)
        self.dw.readyDeferred.addCallback(self._increaseSpeed)
        return self.dw.readyDeferred

    def _initCmds(self, dum=None):
        """!Execute tcc commands as soon as the dispatcher wrapper is ready, before setUp is done

        The default is to set inst=dis
        """
        d0, cmdVar0 = self.dw.queueCmd("set inst=dis")
        d1, cmdVar1 = self.dw.queueCmd("show status")
        return gatherResults((d0, d1))

    def _increaseSpeed(self, foo=None):
        """!increase axes speed to speed up tests
        """
        upFactor = 80.
        for x in range(3):
            self.actor.axeLim.vel[x] = self.actor.axeLim.vel[x]*upFactor
            self.actor.axeLim.accel[x] = self.actor.axeLim.accel[x]*upFactor
            self.actor.axeLim.jerk[x] = self.actor.axeLim.jerk[x]*upFactor
        # self.actor.tune.slewFudge = self.actor.tune.slewFudge/100.
        # self.actor.tune.slewAdvTime = self.actor.tune.slewAdvTime*.75

    def _getGoodFK5Coord(self):
        """@return good coordinates for fk5Tracking
        """
        trackCoord = coordConv.Coord(12,12)
        obsSys = coordConv.ObsCoordSys(tai())
        fk5Coord = obsSys.toFK5J2000(trackCoord, self.actor.obj.site)
        return fk5Coord.getSphPos()[1:]

    def checkATrackCmd(self, cmdStr, begAxisCmdState=None, endAxisCmdState=None):
        """!Initiate a track or track/stop command and check the result

        Does not handle track/pterr or track/cheby

        The idea is to allow you to initiate and test a slew with a minimum of fuss and bother.
        However, the parser is very simplistic and easily fooled.
        The command string must appear in a particular order, for it to be parsed:
            track _pos_ _coordsys_ [= _date_]
        followed by any qualifiers.

        @param[in] cmdStr  the track command
        @param[in] begAxisCmdState  expected axisCmdState when slew begins (">" message), or None to not check
        @param[in] endAxisCmdState  expected axisCmdState at end of slew, or None to not check
        @return a deferred
        """
        trackStopMatch = re.match(
            r" *track */stop", cmdStr, re.I)
        trackChebMatch = re.match(
            r" *track */cheb.*$", cmdStr, re.I)
        if trackStopMatch:
            dataDict = dict(
                coordSysName = "None",
                coordSysDate = 0,
            )
        elif trackChebMatch:
            dataDict = dict(
                coordSysName = CoordSysMatchList.getUniqueMatch(generateChebyFile.userSys),
                coordSysDate = generateChebyFile.userDate,
            )
        else:
            trackMatch = re.match(
                r" *track (?P<pos>[-+0-9][-+0-9., ]+)(?P<coordSysName>[a-z][a-z0-9]+)(?: *= *(?P<coordSysDate>[0-9.]+))?", cmdStr, re.I)
            if trackMatch is None:
                raise RuntimeError("cannot parse %r as a track command" % (cmdStr,))
            dataDict = trackMatch.groupdict()
            # Note: for the following I would prefer to enclose the search expression in (...)? and allow zero matches,
            # but when I try that it doesn't match at all
            rotTypeMatch = re.search(r"/rott[a-z]* *= *(?P<rotType>[a-z]+)", cmdStr, re.I)
            if rotTypeMatch:
                dataDict.update(rotTypeMatch.groupdict())
            rotAngleMatch = re.search(r"/rota[a-z]* *= *(?P<rotAngle>[-0-9.]+)", cmdStr, re.I)
            if rotAngleMatch:
                dataDict.update(rotAngleMatch.groupdict())

            # expand coordSysName and rotType
            dataDict["coordSysName"] = CoordSysMatchList.getUniqueMatch(dataDict["coordSysName"])
            if "rotType" in dataDict:
                dataDict["rotType"] = RotTypeMatchList.getUniqueMatch(dataDict["rotType"])

            if dataDict["coordSysDate"] is not None:
                dataDict["coordSysDate"] = float(dataDict["coordSysDate"])

        def checkTrackCmdState(cmdVar):
            if cmdVar.lastCode == ">":
                self.checkTrackResult(cmdVar, axisCmdState = begAxisCmdState, checkActor=False, **dataDict)
            elif cmdVar.isDone:
                self.checkTrackResult(cmdVar, axisCmdState = endAxisCmdState, checkActor=True, **dataDict)
        d1, cmd1 = self.dw.queueCmd(
            cmdStr,
            callFunc = checkTrackCmdState,
            callCodes = ":>",
        )
        return d1
