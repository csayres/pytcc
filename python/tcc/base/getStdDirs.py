from __future__ import division, absolute_import
"""!Get the paths to various standard directories for the TCC
"""
import os

__all__ = ["getDataDir", "getInstDir", "getJobsDir"]

def getDataDir():
    """!Get path to data directory (value of environment variable TCC_DATA_DIR))
    """
    return os.environ["TCC_DATA_DIR"]

def getInstDir():
    """!Get path to instrument data directory
    """
    return os.path.join(getDataDir(), "inst")

def getJobsDir():
    """!Get path to batch job directory
    """
    return os.path.join(getDataDir(), "jobs")
