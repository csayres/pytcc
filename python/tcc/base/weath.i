%copyctor tcc::Weath;

%include "tcc/weath.h"

%extend tcc::Weath {
    %pythoncode {
        @property
        def _WrapperList(self):
            """Create WrapperList cache if it does not already exist
            """
            if hasattr(self, "__WLCache"):
                return self.__WLCache
            
            wrapperList = WrapperList(name="Weath", obj=self,
                wrappers = (
                    ScalarWrapper("airTemp", dtype=float, help="Outside air temperature (C)"),
                    ScalarWrapper("secTrussTemp", dtype=float, help="Secondary truss temperature (C)"),
                    ScalarWrapper("primFrontTemp", dtype=float, help="Primary mirror front temperature (C)"),
                    ScalarWrapper("primBackFrontTemp", dtype=float, help="Primary mirror back - front temperature difference (C)"),
                    ScalarWrapper("secFrontTemp", dtype=float, help="Secondary mirror front temperature (C)"),
                    ScalarWrapper("secBackFrontTemp", dtype=float, help="Secondary mirror back - front temperature difference (C)"),
                    ScalarWrapper("press", dtype=float, help="Pressure (Pascal)"),
                    ScalarWrapper("humid", dtype=float, help="Humidity (fraction)"),
                    ScalarWrapper("tempLapseRate", dtype=float, help="Temperature lapse rate (C/km)"),
                    ScalarWrapper("windSpeed", dtype=float, help="Wind speed (m/sec)"),
                    ScalarWrapper("windDir", dtype=float, help="Wind direction (degrees, from the south = 0, from the east = 90)"),
                    ScalarWrapper("timestamp", dtype=float, help="Time any field last set (TAI, MJD seconds)"),
                ),
            )
                
            self.__WLCache = wrapperList
            return self.__WLCache

        def load(self, f):
            """Load data from a file-like object
            """
            self._WrapperList.load(f)
        
        def loadPath(self, filePath):
            """Load data from a file specified by path
            """
            self._WrapperList.loadPath(filePath)
        
        def dump(self, f):
            """Dump data to a file-like object
            """
            self._WrapperList.dump(f)            
        
        def __repr__(self):
            return self._WrapperList.dumpStr()
    }
}
