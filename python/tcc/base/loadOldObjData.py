from __future__ import division, absolute_import

import collections
from StringIO import StringIO

import coordConv

from tcc.base import Obj, ChebyshevPolynomial, PxPMRadVel
from .fieldWrapper import boolFromStr, tokenize

__all__ = ["loadOldObjData"]

def _getPVTCoord(meanSysName, posName, dataDict):
    """!Retrieve PVTCoord data from a data dict from an old Obj data file
    """
    pvtX = coordConv.PVT(*(float(val) for val in dataDict[posName + "1"]))
    pvtY = coordConv.PVT(*(float(val) for val in dataDict[posName + "2"]))
    taiDate = pvtX.t
    if meanSysName is not None:
        pm1, pm2, px, rv = (float(val) for val in dataDict[meanSysName])
        distPVT = coordConv.PVT(coordConv.distanceFromParallax(px), 0, taiDate)
        meanArgs = (distPVT, pm1, pm2, rv)
    else:
        meanArgs = ()
    return coordConv.PVTCoord(pvtX, pvtY, *meanArgs)

def loadOldObjData(filePath):
    """!Read old format Obj data file into a new Obj

    @param[in] filePath  path to data file
    @return obj: an Obj block with most fields filled in
    """
    
    # read what lines we can directly (renaming fields in a few cases that obj.i cannot handle)
    # put other data into dataDict: key: tokenized data from rest of line
    dataDict = collections.OrderedDict()
    strFile = StringIO()
    # names that can be processed "as is"
    validNames = set(val.lower() for val in (
        "name",
        "useCheby",
        "rotUser",
        "userArcOff1", "userArcOff2",
        "guideoff1", "guideoff2", "guideoff3", 
        "caliboff1", "caliboff2", "caliboff3", 
    ))
    # dict of old name: new name
    # start with items that have been renamed, then add valid names mapped to themselves, to simplify the code
    oldNewNameDict = dict((val[0].lower(), val[1].lower()) for val in (
        ("rot_type", "rotType"),
        ("az_wrapPref", "azWrapPref"),
        ("rot_wrapPref", "rotWrapPref"),
        ("pvtEndTime", "updateTime"),
        ("obj_inst_xy1", "objInstXY1"),
        ("obj_inst_xy2", "objInstXY2"),
        ("arcuser_noarcuser_ang", "arcUserNoArcUserAng"),
        ("objuser_objaz_ang", "objUserObjAzAng"),
        ("spider_inst_ang", "spiderInstAng"),
        ("objaz_inst_ang", "objAzInstAng"),
        ("objuser_inst_ang", "objUserInstAng"),
        ("rotaz_rot_ang", "rotAzRotAng"),
        ("spider_inst_ang", "spiderInstAng"),
        ("rot_phys", "rotPhys"),
        ("azalt_mt1", "targetMount1"),
        ("azalt_mt2", "targetMount2"),
        ("rot_mt",    "targetMount3"),
        ("mountCmdState", "axisCmdState"),
        ("mountErrCode", "axisErrCode"),
    ))
    oldNewNameDict.update(dict((name, name) for name in validNames))

    # set of names to ignore
    obsoletes = set(val.lower() for val in (
        "doArcOffTDICorr",
        "offsetEndTime",
#         "NumChebyCoeffs",
#         "ChebyBegEndTime",
        "arcVelCorr",
        "prevArcVelCorr",
        "Mech_Obj_xy1",
        "Mech_Obj_xy2",
        "Mech_Inst_ang",
        "RotAz_Mech_ang",
        "Mech_Inst_ang",
        "AzAlt_Phys1",
        "AzAlt_Phys2",
        "SlewMsgTag",
        "timestamp",
    ))
    # old names that are PVTs; if t = 0 then convert to nan
    pvtNames = set(val.lower() for val in (
        "userPos1",
        "userPos2",
        "userOff1",
        "userOff2",
        "userArcOff1",
        "userArcOff2",
        "obj_inst_xy1",
        "obj_inst_xy2",
        "rotuser",
        "guideoff1",
        "guideoff2",
        "guideoff3",
        "caliboff1",
        "caliboff2",
        "caliboff3",
        "zpmUser1",
        "zpmUser2",
        "arcVelCorr",
        "prevArcVelCorr",
        "netUserPos1",
        "netUserPos2",
        "obs1",
        "obs2",
        "objUser_objAz_ang",
        "objAz_inst_ang",
        "objUser_Inst_ang",
        "mech_obj_xy1",
        "mech_obj_xy2",
        "mech_inst_ang",
        "rotaz_mech_ang",
        "rotAz_Rot_ang",
        "Spider_inst_ang",
        "azalt_phys1",
        "azalt_phys2",
        "rot_phys",
        "azAlt_mt1",
        "azAlt_mt2",
    ))
    usedNames = set() # valid names that were processed
    with file(filePath, "rU") as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            if line[0] in ("#", "!"):
                continue
            dataList = tokenize(line)
            key = dataList[0].lower()
            if key in obsoletes:
                continue
            
            if key in pvtNames:
                # PVT: if time <= 0 then set time = NaN
                if len(dataList) != 4:
                    raise RuntimeError("Expected 3 values for %s but got %s" % (key, line))
                if float(dataList[3]) <= 0:
                    dataList[3] = "NaN"
            if key in oldNewNameDict:
                dataList[0] = oldNewNameDict[key]
                usedNames.add(key)
                newLine = " ".join(repr(val) for val in dataList)
                strFile.writelines((newLine, "\n"))
            else:
                dataDict[key] = dataList[1:]
    strFile.pos = 0
    obj = Obj()
    obj.load(strFile)
    
    missingKeys = sorted(set(oldNewNameDict.keys()) - usedNames)
    if missingKeys:
        print "Warning: did not see the following keys:", sorted(missingKeys)
    
    sysNum, date = dataDict["coordsys"]
    sysNum = int(sysNum)
    userSysName = {
        -5: "mount",
        -3: "obs",
        -2: "apptopo",
        -1: "appgeo",
         0: "none",
         1: "fk4",
         2: "fk5",
         3: "gal",
         4: "icrs",
    }.get(sysNum, "none")
    if userSysName in ("obs", "apptopo", "appgeo"):
        date = 0
    else:
        date = float(date)
    obj.userSys = coordConv.makeCoordSys(userSysName, date)

    for axis in range(2):
        obj.userPos[axis] = coordConv.PVT(*[float(val) for val in dataDict["userpos" + str(axis + 1)]])

    pm1, pm2, px, rv = (float(val) for val in dataDict["meansys"])
    obj.userPxPMRadVel = PxPMRadVel(px, pm1, pm2, rv)
    obj.zpmUserPos = _getPVTCoord("zpmmeansys", "zpmuser", dataDict)
    obj.netUserPos = _getPVTCoord("zpmmeansys", "netuserpos", dataDict)
    obj.obsPos = _getPVTCoord(None, "obs", dataDict)

    mag, magIsValid = dataDict["mag"]
    obj.mag = float(mag) if boolFromStr(magIsValid) else float("nan")
    
    obj.site.wavelen = float(dataDict["wavelen"][0])
    obj.site.refCoA, obj.site.refCoB = tuple(float(val) for val in dataDict["refco"])
    
    if obj.useCheby:
        numChebyCoeffsList = tuple(int(val) for val in dataDict["numchebycoeffs"])
        chebyBegTime, chebyEndTime = tuple(float(val) for val in dataDict["chebybegendtime"])
        for i, suffix in enumerate(("User1", "User2", "Dist")):
            oldName = "chebycoeffs" + suffix.lower()
            newName = "cheby" + suffix.title()
            numChebyCoeffs = numChebyCoeffsList[i]
            if numChebyCoeffs > 0:
                chebyCoeffs = tuple(float(val) for val in dataDict[oldName])
            else:
                chebyCoeffs = (0,)
            setattr(obj, newName, ChebyshevPolynomial(chebyCoeffs, chebyBegTime, chebyEndTime))

    obj.obsSysPtr.setDate(0)
    
    return obj
