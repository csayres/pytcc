from __future__ import division, absolute_import

import math

import coordConv
import numpy

from .tccLib import ArrayPVT2, PxPMRadVel

__all__ = ("pvtCoordFromPVTPair", "pvtPairFromPVTCoord")

def pvtCoordFromPVTPair(pvtPair, pxPMRadVel):
    """!Make a PVTCoord from a pair of PVTs and a pxPMRadVel

    @param[in] pvtPair  pair of coordConv.PVT: equatorial, polar
    @param[in] pxPMRadVel  parallax, proper motion and radial velocity (a PxPMRadVel)
    @return a coordConv.PVTCoord
    """
    tai = pvtPair[0].t
    if (pvtPair[0].t != pvtPair[1].t):
        raise RuntimeError("pvtPair[0].t = %r != %r = pvtPair[1].t" % (pvtPair[0].t, pvtPair[1].t))
    distPVT = coordConv.PVT(coordConv.distanceFromParallax(pxPMRadVel.parallax), 0, tai)
    return coordConv.PVTCoord(pvtPair[0], pvtPair[1], distPVT,
            pxPMRadVel.equatPM, pxPMRadVel.polarPM, pxPMRadVel.radVel)

def pvtPairFromPVTCoord(pvtCoord):
    """!Make a PVTPair and PxPMRadVel from a PVTCoord

    @param[in] pvtCoord  a coordConv.PVTCoord
    @return two items:
    - pvtPair: spherical PVTs, as a tcc.base.ArrayPVT2
    - pxPMRadVel: parallax, proper motion and radial velocity as a PxPMRadVel
    """
    pvtPair = ArrayPVT2()
    pvtCoord.getSphPVT(pvtPair[0], pvtPair[1])
    coord = pvtCoord.getCoord()
    atPole, equatPM, polarPM = coord.getPM()
    if atPole and math.hypot(equatPM, polarPM) > 1e-6:
        equatPM = numpy.nan()
        polarPM = numpy.nan()
    pxPMRadVel = PxPMRadVel(coord.getParallax(), equatPM, polarPM, coord.getRadVel())
    return pvtPair, pxPMRadVel
