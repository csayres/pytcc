from __future__ import division, absolute_import
"""!A base class that trial's unittest TestCase objects can inherit from
"""
from twisted.internet import reactor

from tcc.actor import TCC35mDispatcherWrapper
from tcc.base import AxisStateValDict, RotTypeNameDict
from tcc.parse import CoordCmdNameDict
from tcc.msg import CoordSysNameKeyDict, RotTypeEnumKeyDict

class WrapperTestCase(object):

    def setUp(self):
        """!Set up a test
        """
        self.dw = TCC35mDispatcherWrapper()
        return self.dw.readyDeferred

    def tearDown(self):
        """!Tear down a test
        """
        delayedCalls = reactor.getDelayedCalls()
        for call in delayedCalls:
            call.cancel()
        return self.dw.close()

    @property
    def dispatcher(self):
        """!Return the actor dispatcher that talks to the mirror controller
        """
        return self.dw.dispatcher

    @property
    def actor(self):
        """!Return the tcc actor
        """
        return self.dw.actorWrapper.actor

    @property
    def model(self):
        """!Return the tcc model
        """
        return self.dw.dispatcher.model

    @property
    def cmdQueue(self):
        """!return the cmdQueue on the dispatcher wrapper
        """
        return self.dw.cmdQueue

    @property
    def isMoving(self):
        return self.actor.obj.isMoving()

    def checkCmdFail(self, cmdVar, shouldFail=False):
        """!Check that the cmdVar didFail == shouldFail
        """
        if shouldFail:
            self.assertTrue(cmdVar.didFail)
        else:
            self.assertFalse(cmdVar.didFail)
        return cmdVar

    def checkTrackResult(self, cmdVar, axisCmdState,
        coordSysName=None, coordSysDate=None,
        pos=None,
        rotType=None,
        rotAngle=None,
        checkActor=True,
    ):
        """!Check the result of a track command

        Warning: does not yet check pos or rotAngle

        @param[in] cmdVar  ignored; only present so this can be used as a cmdVar callback
        @param[in] axisCmdState  list of 3 axis state strings, such as "Halted"; see tcc.base.AxisStateValDict for values
        @param[in] pos  string of pos1, pos2[, vel1, vel2[, tai]]
        @param[in] rotType  rotation type as the full string the TCC outputs for the RotType keyword
            (e.g. "Object"); not case sensitive
        @param[in] rotAngle  rotation angle as a float
        @param[in] coordSysName  coordinate system name as the value the TCC outputs for the ObjSys keyword
            (e.g. "Geocentric"); not case sensitive
        @param[in] coordSysDate  coordinate system date (float), or 0 if current
        @param[in] checkActor  if True then check actor state directly, in addition to received keywords;
            usually you should set this False if testing the state as a slew begins (reply code of ">")
            because otherwise the test is needlessly timing-dependent and fragile:
            the actor may have finished the slew by the time the test is performed
        """
        if axisCmdState is not None:
            self.checkAxisCmdState(cmdVar=cmdVar, axisCmdState=axisCmdState, checkActor=checkActor)
        if rotType is not None:
            desRotTypeEnum = RotTypeNameDict[rotType]
            desRotTypeKey = RotTypeEnumKeyDict[desRotTypeEnum]
            self.assertEqual(desRotTypeKey, str(self.model.rotType[0]))
            if checkActor:
                self.assertEqual(desRotTypeEnum, self.dw.actor.obj.rotType)
        if coordSysName is not None:
            self.checkUserSys(cmdVar=cmdVar, coordSysName=coordSysName, coordSysDate=coordSysDate, checkActor=checkActor)

    def checkAxisCmdState(self, cmdVar, axisCmdState, checkActor=True):
        """!Check commanded axis state from axisCmdState keyword and optionally obj.axisCmdState

        @param[in] cmdVar  ignored; only present so this can be used as a cmdVar callback
        @param[in] axisCmdState  list of axis state strings, such as "Halted"; see tcc.base.AxisStateValDict for values
        @param[in] checkActor  if True then check actor state directly, in addition to received keywords
        """
        # use last reported value; if no value ever reported then assume axes are halted
        lastReportedState = tuple(str(val) for val in self.model.axisCmdState)
        self.assertEqual(tuple(axisCmdState), lastReportedState)
        if checkActor:
            self.assertEqual(tuple(axisCmdState), tuple(AxisStateValDict[val] for val in self.actor.obj.axisCmdState))

    def checkUserSys(self, cmdVar, coordSysName, coordSysDate, checkActor=True):
        """!Check that the coordinate system is as specified

        Checks by comparing keywords output by the TCC and optionally be examining
        the TCC actor directly,

        @param[in] cmdVar  ignored; only present so this can be used as a cmdVar callback
        @param[in] coordSysName  full TCC coordinate system name (e.g. geocentric); not case sensitive
        @param[in] coordSysDate  coordinate system date (float), or None if defaulted
            (2000 for FK5, 1950 for FK4, 0 for all other coordinate systems)
        @param[in] checkActor  if True then check actor state directly, in addition to received keywords

        Compare to ObjSys keyword (which shows obj.userSys),
        and also compare directly to the actor's obj block.
        """
        desUserSysBlockName = CoordCmdNameDict[coordSysName.lower()]
        desObjSysKeyName = CoordSysNameKeyDict[desUserSysBlockName]

        # check obsSys keyword
        objSysKeyName, objSysDate = self.model.objSys[:]
        self.assertEqual(desObjSysKeyName, objSysKeyName)
        if coordSysDate is None:
            coordSysDate = {
                "fk5": 2000,
                "fk4": 1950,
            }.get(desUserSysBlockName, 0)
        self.assertAlmostEqual(objSysDate, coordSysDate)

        # check obj.userSys in actor
        if checkActor:
            self.assertEqual(self.actor.obj.userSys.getName(), desUserSysBlockName)
            self.assertAlmostEqual(self.actor.obj.userSys.getDate(), coordSysDate)
