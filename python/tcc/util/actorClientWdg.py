#!/usr/bin/env python2
from __future__ import division, absolute_import

from opscore.actor import ActorDispatcher, CmdVar
import Tkinter
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from RO.Comm.TCPConnection import TCPConnection
import RO.Wdg

__all__ = ["ActorClientWdg"]

class ActorClientWdg(Tkinter.Frame):
    """!Actor client widget: a log window above a command entry bar
    """
    def __init__(self, master, name, host, port):
        """!Construct an ActorClientWdg

        @param[in] master  master widget
        @param[in] name  actor name; must have an associated dictionary in actorkeys.
        @param[in] host  TCP/IP address of actor
        @param[in] port  TCP/IP port of actor
        """
        Tkinter.Frame.__init__(self, master)
        self.name = name
        row = 0
        self.logWdg = RO.Wdg.LogWdg(
            master = self,
            maxLines = 5000,
        )
        self.logWdg.grid(row=row, column=0, sticky="nwes")
        self.grid_rowconfigure(row, weight=1)
        self.grid_columnconfigure(0, weight=1)
        row += 1

        self.cmdWdg = RO.Wdg.CmdWdg(
            master = self,
            maxCmds = 100,
            cmdFunc = self.doCmd,
            helpText = "command to send to hub; <return> to send",
        )
        self.cmdWdg.grid(row=row, column=0, sticky="ew")
        row += 1

        self.conn = TCPConnection(
            name = name,
            host = host,
            port = port,
            readLines = True,
            stateCallback = self._connStateCallback,
        )
        self.dispatcher = ActorDispatcher(
            name = name,
            connection = self.conn,
            logFunc = self._logReply, # receives (msgStr, severity, actor, cmdr)
        )
        self.conn.connect()

    def doCmd(self, cmdStr):
        """!Send a command to the actor.

        Note that dispatching the command automatically logs it.
        """
        cmdVar = CmdVar(actor=self.name, cmdStr=cmdStr)
        self.dispatcher.executeCmd(cmdVar)
        self.logWdg.text.see("end") # if you start a command you probably want to see the replies,
            # even if you were looking at an earlier part of the log
    
    def _logReply(self, msgStr, severity, **kwargs):
        """!Log a reply from the actor

        @param[in] msgStr  reply message (with header)
        @param[in] severity  message severity (an RO.Const.sevX constant)
        Remaining keyword arguments are ignored
        """
        self.logWdg.addMsg(msgStr, severity=severity)
    
    def _connStateCallback(self, conn):
        """!Connection state callback
        """
        state, reason = conn.fullState
        self.logWdg.addOutput("Connection state: %s %s\n" % (state, reason))
