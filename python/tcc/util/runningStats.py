from __future__ import division, absolute_import

import collections

import numpy

__all__ = ["RunningStats"]

class RunningStats(object):
    """!Keep a running record of some value and return statistics on request
    """
    def __init__(self, bufferSize=1000, callEvery=500, callFunc=None):
        """!Construct a RunningStats
        
        @param[in] bufferSize  number of data points on which to compute statistics
        @param[in] callEvery  after this many items are added, call callFunc (if specified)
        @param[in] callFunc  callback function to call after callEvery items are added, or None;
            if specified, must accept one argument: this RunningStats object
        """
        self._buffer = collections.deque(maxlen=bufferSize)
        self._counter = 0
        self.callEvery = int(callEvery)
        self.callFunc = callFunc
        if callEvery > bufferSize:
            raise RuntimeError("callEvery=%s > %s=bufferSize" % (callEvery, bufferSize))

    def addValue(self, val):
        """!Add a measurement and increment the counter
        """
        self._buffer.append(val)
        self._counter += 1
        if self._counter >= self.callEvery and self.callFunc:
            self.callFunc(self)

    def clear(self):
        """!Clear the buffer and reset the counter
        """
        self._buffer.clear()
        self._counter = 0
    
    @property
    def counter(self):
        return self._counter

    def getStats(self):
        """!Return statistics and reset the counter
        
        @return a dict containing:
        - counter: number of value added since last call to clear or getStats
        - num: number of values in the buffer
        - min: minimum value in buffer
        - max: maximum value in buffer
        - median: median value in buffer
        - stdDev: standard deviation of values in buffer
        """
        retVal = dict(
            counter = self._counter,
            num = len(self._buffer),
            min = numpy.min(self._buffer),
            max = numpy.max(self._buffer),
            median = numpy.median(self._buffer),
            stdDev = numpy.std(self._buffer),
        )
        self._counter = 0
        return retVal
