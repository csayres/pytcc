from __future__ import division, absolute_import

__all__ = ["getAxisNames"]

AxisNameList = ("az", "alt", "rot")

def getAxisNames(boolList):
    """!Convert a list of bools (one per axis) to a comma-separated string of axis names
    
    @param[in] boolList  list of at most 3 bools, one per axis
    @return a string listing the names of the selected axes, comma-separated
    """
    return ", ".join([AxisNameList[i] for i, doUse in enumerate(boolList) if doUse])
