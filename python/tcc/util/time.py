from __future__ import division, absolute_import

import RO.PhysConst

from tcc.base import tai

__all__ = ["daySecFromDate", "dateFromSecInDay", "secInDayFromDate"]

_HalfDaySec = RO.PhysConst.SecPerDay / 2.0

def daySecFromDate(date):
    """!Convert a TAI date (MJD, seconds) to (days, seconds in the day)
    """
    fracDays = date / RO.PhysConst.SecPerDay
    day = int(fracDays)
    sec = (fracDays - day) * RO.PhysConst.SecPerDay
    return (day, sec)

def secInDayFromDate(date):
    """!Convert a date to seconds in the day (losing day information)
    
    A convenience wrapper around daySecFromDate, since we rarely care about the day
    (the main use case is computing seconds in the day for the old axis controller interface).
    """
    return daySecFromDate(date)[1]

def dateFromSecInDay(secInDay):
    """!Convert TAI seconds in the day to a full TAI date (MJD, seconds)
    """
    currDate = tai()
    currDay, currSecInDay = daySecFromDate(currDate)
    
    if secInDay > currSecInDay + _HalfDaySec:
        actDay = currDay - 1
    elif secInDay < currSecInDay - _HalfDaySec:
        actDay = currDay + 1
    else:
        actDay = currDay
    return secInDay + (actDay * RO.PhysConst.SecPerDay)

