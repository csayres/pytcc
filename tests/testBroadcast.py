#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Test the Broadcast command
"""
from twisted.internet.defer import Deferred, gatherResults
from twisted.trial.unittest import TestCase
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from RO.StringUtil import quoteStr, unquoteStr
import RO.KeyVariable

import tcc.base.testUtils
tcc.base.testUtils.init(__file__)
from tcc.cmd.broadcast import MsgTypeCodeDict

from tcc.base.wrapperTestCase import WrapperTestCase

class TestBroadcast(WrapperTestCase, TestCase):
    def testBasics(self):
        """Broadcast a few messages and assert that they were received
        """
        deferredList = []
        doneTypes = set([msgType for msgType, msgCode in MsgTypeCodeDict.iteritems() if msgCode in RO.KeyVariable.DoneTypes])
        for msgType in MsgTypeCodeDict:
            for msgStr in (
                "a simple message",
                "a message with \"special\" chars \\();[]",
                "a message with some special chars ();[]",
            ):
                d = Deferred()
                def callFunc(cmdVar, msgStr=msgStr, msgType=msgType):
                    if cmdVar.didFail:
                        d.errback(AssertionError("%s failed" % (cmdVar.cmdStr,)))
                    elif cmdVar.isDone:
                        brdMsgList = cmdVar.getKeyVarData(self.model.broadcast)
                        if msgType.lower() in doneTypes:
                            # broadcast message does not have a cmdID to avoid marking command as failed
                            # (or done when broadcast keyword is sent, though that could change)
                            self.assertEqual(len(brdMsgList), 0)
                        else:
                            self.assertEqual(len(brdMsgList), 1)
                            brdMsgStr = brdMsgList[0][0]
                            self.assertEqual(unquoteStr("\"" + brdMsgStr + "\""), msgStr)
                # looks like: 'broadcast "a message with \\"special\\" chars \\\\();[]"/type=debug'
                d, cmd = self.dw.queueCmd(
                    cmdStr = 'broadcast %s/type=%s' % (quoteStr(msgStr), msgType),
                    keyVars = [self.model.broadcast],
                    callFunc = callFunc,
                )
                deferredList.append(d)

        return gatherResults(deferredList)

if __name__ == '__main__':
    from unittest import main
    main()

