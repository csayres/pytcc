#!/usr/bin/env python2
from __future__ import division, absolute_import

import functools

from twisted.internet.defer import gatherResults
from twisted.trial.unittest import TestCase
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
# import coordConv

import tcc.base.testUtils
tcc.base.testUtils.init(__file__)
from tcc.base.trackTestUtils import TrackCommonMethods

class TestTrackPtErr(TrackCommonMethods, TestCase):

    # @property
    # def instBlock(self):
    #     """Return the instrument block
    #     """
    #     return self.actor.inst

    @property
    def tccActor(self): # clearer synonym for self.actor
        return self.dw.actorWrapper.actor

    def testUnsavedObj(self):
        """Show that one cannot track/pterr=objslew if there is no saved object
        """
        d1, cmd1 = self.dw.queueCmd(
            cmdStr = "track 10,10 obs/pterr=objslew",
            callFunc = functools.partial(self.checkCmdFail, shouldFail=True),
            callCodes = "F:",
        )
        return d1

    def testNoRot(self):
        """Show that one cannot track/pterr unless rotation is specified
        """
        d2, cmd2 = self.dw.queueCmd(
            cmdStr = "track 10,10 obs/pterr=objslew",
            callFunc = functools.partial(self.checkCmdFail, shouldFail=True),
            callCodes = "F:",
        )
        return d2

    def testBasics(self):
        """Show that the basics work
        """
        # Slew to nearby pos ref star; it would be better to confirm that it found the right star,
        # but that requires picking a star from the ref catalog and slewing to a point very near it,
        # and the find functionality is tested in a different unit test
        d2, cmd2 = self.dw.queueCmd(
            "track 10, 10 obs/pterr/rotang=10/rottype=mount",
        )

        # Check that track/pterr=objslew returned to original target
        def checkOriginalTarget(cmdVar):
            obj = self.tccActor.obj
            equatAng, polarAng = [pvt.getPos(obj.updateTime) for pvt in obj.userPos]
            self.assertAlmostEqual(equatAng, 10)
            self.assertAlmostEqual(polarAng, 10)
            self.assertEqual(obj.userSys.getName(), "obs")

        d3, cmd3 = self.dw.queueCmd(
            "track/pterr=objslew",
            callFunc = checkOriginalTarget,
        )

        # saved obj should be cleared
        d4, cmd4 = self.dw.queueCmd(
            cmdStr = "track/pterr=objslew",
            callFunc = functools.partial(self.checkCmdFail, shouldFail=True),
            callCodes = "F:",
        )

        return gatherResults([d2, d3, d4])

    def testNoFindReference(self):
        """Show that one can use the target as the reference
        """
        # Use target as reference
        def checkAtTarget(cmdVar):
            obj = self.tccActor.obj
            equatAng, polarAng = [pvt.getPos(obj.updateTime) for pvt in obj.userPos]
            self.assertAlmostEqual(equatAng, 10)
            self.assertAlmostEqual(polarAng, 10)
            self.assertEqual(obj.userSys.getName(), "obs")

        d2, cmd2 = self.dw.queueCmd(
            "track 10, 10 obs/pterr=(noFindReference)/rotang=10/rottype=mount",
            callFunc = checkAtTarget,
        )

        # Check that track/pterr=objslew returned to original target
        d3, cmd3 = self.dw.queueCmd(
            "track/pterr=objslew",
            callFunc = checkAtTarget,
        )

        # saved obj should be cleared
        d4, cmd4 = self.dw.queueCmd(
            cmdStr = "track/pterr=objslew",
            callFunc = functools.partial(self.checkCmdFail, shouldFail=True),
            callCodes = "F:",
        )

        return gatherResults([d2, d3, d4])

    def testNoRefSlew(self):
        """Show that one can use the target as the reference
        """
        # find but don't slew
        def checkHalted(cmdVar):
            obj = self.tccActor.obj
            self.assertEqual(obj.userSys.getName(), "none")

        d2, cmd2 = self.dw.queueCmd(
            "track 10, 10 obs/pterr=(noRefSlew)/rotang=10/rottype=mount",
        )

        # there should be no saved obj
        d3, cmd3 = self.dw.queueCmd(
            cmdStr = "track/pterr=objslew",
            callFunc = functools.partial(self.checkCmdFail, shouldFail=True),
            callCodes = "F:",
        )

        return gatherResults([d2, d3])


if __name__ == '__main__':
    from unittest import main
    main()

