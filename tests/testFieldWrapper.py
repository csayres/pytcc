#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest
from StringIO import StringIO

import coordConv

import tcc.base

WrapTypeDict = {
    tcc.base.WrapType_None: "None",
    tcc.base.WrapType_Nearest: "Nearest",
    tcc.base.WrapType_Negative: "Negative",
    tcc.base.WrapType_Middle: "Middle",
    tcc.base.WrapType_Positive: "Positive",
    tcc.base.WrapType_NoUnwrap: "NoUnwrap",
}
AxisStateDict = {
    tcc.base.AxisState_NotAvailable: "NotAvailable",
    tcc.base.AxisState_Halted: "Halted",
    tcc.base.AxisState_Slewing: "Slewing",
    tcc.base.AxisState_Halting: "Halting",
    tcc.base.AxisState_Tracking: "Tracking",
    tcc.base.AxisState_BadCode: "BadCode",
}

class TestObj(object):
    """An object containing one of every kind of object that can be wrapped, plus the associated wrapper
    """
    def __init__(self):
        self.intArray = [1, 2, 3]
        self.floatArray = [1.1, 2.2, 3.3]
        self.strArray = ["one", "two", "three"]
        self.cheby = tcc.base.ChebyshevPolynomial()
        self.coordSys = coordConv.FK5CoordSys()
        self.cmdStateEnumArray = [tcc.base.AxisState_Slewing, tcc.base.AxisState_Tracking, tcc.base.AxisState_Halted]
        self.wrapPref = tcc.base.WrapType_Middle
        self.gProbe = tcc.base.VectorGuideProbe()
        self.instPos = tcc.base.InstPosition("NA2")
        self.obsSys = coordConv.ObsCoordSys(3333.3)
        self.pvajtArray = tcc.base.ArrayPVAJT3()
        self.pvtArray = tcc.base.ArrayPVT3()
        self.pxPMRadVel = tcc.base.PxPMRadVel()
        self.intScalar = 1
        self.floatScalar = float("nan")
        self.strScalar = "one"
        coord = coordConv.Coord(1.1, 2.2, 11.1, 22.2, 33.3, 44.4)
        self.pvtCoord = coordConv.PVTCoord(coord, coord, 3000, 0.001)
        fixedCoord = coordConv.Coord(2.2, 3.3, 4.4)
        self.fixedPVTCoord = coordConv.PVTCoord(fixedCoord, fixedCoord, 3000, 0.001)

    @property
    def _WrapperList(self):
        """Create WrapperList cache if it does not already exist
        """
        if hasattr(self, "__WLCache"):
            return self.__WLCache

        wrapperList = tcc.base.WrapperList(name="TestObj", obj=self,
            wrappers = (
                tcc.base.ArrayWrapper("intArray", dtype=int, numElts=3, help="int array"),
                tcc.base.ArrayWrapper("floatArray", dtype=float, numElts=3, help="float array"),
                tcc.base.ArrayWrapper("strArray", dtype=str, numElts=3, help="str array"),
                tcc.base.ChebyshevWrapper("cheby", "Chebyshev polynomial min, max, coeffs"),
                tcc.base.CoordSysWrapper("coordSys", help="coordSys"),
                tcc.base.DocWrapper("Two lines\nof documentation"),
                tcc.base.EnumArrayWrapper("cmdStateEnumArray", valDict=AxisStateDict, numElts=3, help="command state enum array"),
                tcc.base.EnumWrapper("wrapPref", valDict=WrapTypeDict, help="wrap preference enum"),
                tcc.base.GuideProbeVectorWrapper("gProbe", help="guide probes"),
                tcc.base.InstPositionWrapper("instPos", help="instrument position"),
                tcc.base.ObsCoordSysWrapper("obsSys", help="obsSys"),
                tcc.base.PVAJTArrayWrapper("pvajtArray", 3, help="array of 3 tcc.base.PVAJTs"),
                tcc.base.PVTArrayWrapper("pvtArray", 3, help="array of 3 coordConv.PVTs"),
                tcc.base.PxPMRadVelWrapper("pxPMRadVel", help="a PxPMRadVel"),
                tcc.base.ScalarWrapper("intScalar", dtype=int, help="int scalar"),
                tcc.base.ScalarWrapper("floatScalar", dtype=float, help="float scalar"),
                tcc.base.ScalarWrapper("strScalar", dtype=str, help="str scalar"),
                tcc.base.PVTCoordWrapper("pvtCoord", help="pvtCoord"),
                tcc.base.FixedPVTCoordWrapper("fixedPVTCoord", help="fixedPVTCoord"),
            ),
            aliases = (
                ("int", "intScalar"),
                ("float", "floatScalar"),
                ("str", "strScalar"),
            ),
            obsoletes = (
                ("obsolete1", 1),
                ("obsolete2", 1),
            ),
        )

        self.__WLCache = wrapperList
        return self.__WLCache

    def load(self, f):
        """Load data from a file-like object
        """
        self._WrapperList.load(f)
        self.timestamp = tcc.base.tai()

    def dump(self, f):
        """Dump data to a file-like object
        """
        self._WrapperList.dump(f)


class EnumObj(object):
    """An object containing an enum and an enum array
    
    This object uses a simple wrapper form that is constructed at the start, instead of on demand
    """
    def __init__(self):
        self.cmdStateEnumArray = [tcc.base.AxisState_Slewing, tcc.base.AxisState_Tracking, tcc.base.AxisState_Halted]
        self.cmdState = tcc.base.AxisState_BadCode

        AxisStateDict = {
            tcc.base.AxisState_NotAvailable: "NotAvailable",
            tcc.base.AxisState_Halted: "Halted",
            tcc.base.AxisState_Slewing: "Slewing",
            tcc.base.AxisState_Halting: "Halting",
            tcc.base.AxisState_Tracking: "Tracking",
            tcc.base.AxisState_BadCode: "BadCode",
        }

        self._WrapperList = tcc.base.WrapperList(name="TestObj", obj=self,
            wrappers = (
                tcc.base.EnumArrayWrapper("cmdStateEnumArray", valDict=AxisStateDict, numElts=3, help="command state enum array"),
                tcc.base.EnumWrapper("cmdState", valDict=AxisStateDict, help="command state enum"),
            ),
        )

    def load(self, f):
        """Load data from a file-like object
        """
        self._WrapperList.load(f)
        self.timestamp = tcc.base.tai()

    def dump(self, f):
        """Dump data to a file-like object
        """
        self._WrapperList.dump(f)


class TestFieldWrapper(unittest.TestCase):
    def testNewDump(self):
        """Test dump of default-constructed object
        """
        testObj = TestObj()
        expectedData = """
            # TestObj data
            # int array
            intArray 1 2 3
            # float array
            floatArray 1.1 2.2 3.3
            # str array
            strArray 'one' 'two' 'three'
            # Chebyshev polynomial min, max, coeffs
            cheby -1.0000000 1.0000000 0.0000000
            # coordSys
            coordSys fk5 2000.0000
            # Two lines
            # of documentation
            # command state enum array
            cmdStateEnumArray slewing tracking halted
            # wrap preference enum
            wrapPref middle
            # guide probes
            # probe number (starting from 1 and strictly sequential), does probe exist?
            # if and only if the probe exists then this data must follow in order:
            # center of guide probe (guide image x,y unbinned pix)
            # lower left corner of the smallest box containing the probe (guide image x,y unbinned pix)
            # upper right corner of the smallest box containing the probe (guide image x,y unbinned pix)
            # position of rotator with respect to probe center (x,y deg on sky)
            # angle of rotator x axis with respect to guide image (deg)
            # Note: this instrument has no guide probes
            # instrument position
            instPos NA2
            # obsSys
            obsSys 3333.3
            # array of 3 tcc.base.PVAJTs
            pvajtArray1 nan nan nan nan nan
            pvajtArray2 nan nan nan nan nan
            pvajtArray3 nan nan nan nan nan
            # array of 3 coordConv.PVTs
            pvtArray1 nan nan nan
            pvtArray2 nan nan nan
            pvtArray3 nan nan nan
            # a PxPMRadVel
            pxPMRadVel 0.000000 0.000 0.000 0.000
            # int scalar
            intScalar 1
            # float scalar
            floatScalar nan
            # str scalar
            strScalar 'one'
            # pvtCoord
            pvtCoord 1.1000000 2.2000000 0.0000000 0.0000000 3000.000000 18582.415 0.000 22.20000 33.30000 44.40000
            # fixedPVTCoord
            fixedPVTCoord 2.2000000 3.3000000 0.0000000 0.0000000 3000.000000 46878.365 0.000
            """
        expectedLines = [l.strip() for l in expectedData.split("\n") if l.strip()]
        dumpFile = StringIO()
        testObj.dump(dumpFile)
        actualLines = [l.strip() for l in dumpFile.getvalue().split("\n") if l.strip()]
        self.assertEqual(expectedLines, actualLines)

    def testSetLoadDump(self):
        """Test dump after load
        """
        goodData = """
            # TestObj data
            # int array
            intArray 2 3 4
            # float array
            floatArray 2.2 3.3 4.4
            # str array
            strArray 'two' 'three' 'a "complicated" \\'string\\' value'
            # Chebyshev polynomial min, max, coeffs
            cheby 1.1000000 2.2000000 3.3000000 4.4000000 5.5000000
            # coordSys
            coordSys fk4 1950.0000
            # Two lines
            # of documentation
            # command state enum array
            cmdStateEnumArray tracking halted halting
            # wrap preference enum
            wrapPref positive
            # guide probes
            # probe number (starting from 1 and strictly sequential), does probe exist?
            # if and only if the probe exists then this data must follow in order:
            # center of guide probe (guide image x,y unbinned pix)
            # lower left corner of the smallest box containing the probe (guide image x,y unbinned pix)
            # upper right corner of the smallest box containing the probe (guide image x,y unbinned pix)
            # position of rotator with respect to probe center (x,y deg on sky)
            # angle of rotator x axis with respect to guide image (deg)
            gProbe    1  False  # probe number, exists?
            gProbe    2   True  # probe number, exists?
                34.0      35.0  # center of probe (guide image x,y unbinned pix)
                 0.0       0.0  # lower left corner of probe (guide image x,y unbinned pix)
                70.0      70.0  # upper right corner of probe (guide image x,y unbinned pix)
             0.02340   0.03450  # position of rotator with respect to probe center (x,y deg on sky)
                 3.4            # angle of rotator with respect to guide image (deg)
            gProbe    3   True  # probe number, exists?
               350.5     450.5  # center of probe (guide image x,y unbinned pix)
               300.5     400.5  # lower left corner of probe (guide image x,y unbinned pix)
               400.5     500.5  # upper right corner of probe (guide image x,y unbinned pix)
             0.12340   0.22340  # position of rotator with respect to probe center (x,y deg on sky)
                -5.6            # angle of rotator with respect to guide image (deg)
            gProbe    4  False  # probe number, exists?
            # instrument position
            instPos TR1
            # obsSys
            obsSys 4444.4
            # array of 3 tcc.base.PVAJTs
            pvajtArray1 nan 2.200000 3.300000 4.400000 5.5000000
            pvajtArray2 4.400000 5.500000 6.600000 7.700000 8.8000000
            pvajtArray3 7.700000 8.800000 nan 9.900000 1.1000000
            # array of 3 coordConv.PVTs
            pvtArray1 nan 2.200000 3.3000000
            pvtArray2 4.400000 5.500000 6.6000000
            pvtArray3 7.700000 8.800000 nan
            # a PxPMRadVel
            pxPMRadVel 0.010003 0.257 -0.129 -3.512
            # int scalar
            intScalar 2
            # float scalar
            floatScalar 2.2
            # str scalar
            strScalar 'two \\'three\\' "four"'
            # pvtCoord
            pvtCoord 3.0000000 4.0000000 0.4000000 0.6000000 7654321.000000 12345.000 123.450 66.60000 77.70000 88.80000
            # fixedPVTCoord
            fixedPVTCoord 1.1231231 -9.8765432 0.7654321 -0.2121212 7654321.000004 12345.432 1.234
            """
        laodFile = StringIO(goodData)
        testObj = TestObj()
        testObj.load(laodFile)
        dumpFile = StringIO()
        testObj.dump(dumpFile)
        actualLines = [l.strip() for l in dumpFile.getvalue().split("\n") if l.strip()]
        expectedLines = [l.strip() for l in goodData.split("\n") if l.strip()]
        self.assertEqual(expectedLines, actualLines)

    def testEnumWrapper(self):
        """Test enum wrappers with integers, names and invalid values
        """
        enumObj = EnumObj()
        badDataList = (
            "cmdState badvalue",
            "cmdState -4",
            "cmdState  6",
            "cmdStateEnumArray halted slewing badvalue",
            "cmdStateEnumArray halted badvalue tracking",
            "cmdStateEnumArray badvalue halted slewing",
            "cmdStateEnumArray halted slewing -4",
            "cmdStateEnumArray halted -5 tracking",
            "cmdStateEnumArray -6 halted slewing",
            "cmdStateEnumArray halted slewing 10",
            "cmdStateEnumArray halted 11 tracking",
            "cmdStateEnumArray 12 halted slewing",
            "cmdStateEnumArray badvalue -4 10",
            "cmdStateEnumArray halting tracking", # too few values
            "cmdStateEnumArray halting tracking slewing badcode", # too many values
        )
        for badData in badDataList:
            loadFile = StringIO(badData + "\n")
            self.assertRaises(RuntimeError, enumObj.load, loadFile)

        goodDataList = (
            """cmdState halted
            cmdStateEnumArray notAvailable Slewing BadCode
            """,
            """cmdState -1
            cmdStateEnumArray -1 2 5
            """,
            """cmdState BADCODE
            cmdStateEnumArray NOTAVAILABLE HALTING BADCODE
            """
        )
        
        axisNameIntDict = dict((val.lower(), key) for key, val in AxisStateDict.iteritems())
        def toInt(nameOrInt):
            """Convert name (any case) or str representation of an int to an int"""
            try:
                valInt = int(nameOrInt)
            except Exception:
                valInt = axisNameIntDict[nameOrInt.lower()]
            return valInt
        
        for goodData in goodDataList:
            loadFile = StringIO(goodData + "\n")
            enumObj.load(loadFile)
            for line in loadFile.getvalue().split("\n"):
                if not line.strip():
                    continue
                dataList = line.split()
                if dataList[0].lower() == "cmdstate":
                    self.assertEqual(len(dataList), 2)
                    self.assertEqual(enumObj.cmdState, toInt(dataList[1]))
                else:
                    self.assertEqual(len(dataList), 4)
                    self.assertEqual(tuple(enumObj.cmdStateEnumArray), tuple(toInt(val) for val in dataList[1:]))

    def testTooFewValues(self):
        """Test trying to set with too few values
        """
        testObj = TestObj()
        dumpFile = StringIO()
        testObj.dump(dumpFile)
        dataLines = [l.strip() for l in dumpFile.getvalue().split("\n") if l.strip()]
        dataLines = [l for l in dataLines if l[0] not in ("#", "!")]
        for line in dataLines:
            data = line.split()
            truncatedLine = " ".join(data[:-1])
            loadFile = StringIO(truncatedLine + "\n")
            self.assertRaises(RuntimeError, testObj.load, loadFile)

    def testTooManyValues(self):
        """Test trying to set with too many values
        """
        testObj = TestObj()
        dumpFile = StringIO()
        testObj.dump(dumpFile)
        dataLines = [l.strip() for l in dumpFile.getvalue().split("\n") if l.strip()]
        dataLines = [l for l in dataLines if l[0] not in ("#", "!")]
        for line in dataLines:
            key = line.split()[0]
            if key in ("cheby",):
                # skip fields that can accept extra values
                continue
            line += " 1" # append extra data
            loadFile = StringIO(line + "\n")
            self.assertRaises(RuntimeError, testObj.load, loadFile)

    def testComments(self):
        """Test adding comments after data
        """
        testObj = TestObj()
        dumpFile = StringIO()
        testObj.dump(dumpFile)
        dataLines = [l.strip().replace("'", "") for l in dumpFile.getvalue().split("\n")]
        dataLines = [l + " # comment\n" for l in dataLines if l and l[0] not in ("#", "!")]
        loadFile = StringIO("".join(dataLines))
        testObj.load(loadFile)
        newDumpFile = StringIO()
        testObj.dump(newDumpFile)
        self.assertEqual(dumpFile.getvalue(), newDumpFile.getvalue())

    def testWrongDataType(self):
        """Test setting wrong data type
        """
        testObj = TestObj()
        badData = \
            """intScalar a
            floatScalar b
            intArray a b c
            floatArray a b c
            coordSys fk5 a
            obsSys a
            pvtCoord a b c d e f g h i
            """
        for badLine in [l.strip() + "\n" for l in badData.split("\n") if l.strip()]:
            loadFile = StringIO("".join(badLine))
            try:
                self.assertRaises(RuntimeError, testObj.load, loadFile)
            except Exception:
                print "failed on %s" % (badLine,)
                raise


if __name__ == '__main__':
    unittest.main()
