#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest
import tcc.base

import coordConv

# Array classes
# ArrayClasses = (
#     tcc.base.ArrayB2,
#     tcc.base.ArrayB3,
#     tcc.base.ArrayI2,
#     tcc.base.ArrayI3,
#     tcc.base.ArrayD2,
#     tcc.base.ArrayD3,
#     tcc.base.ArrayD5,
#     tcc.base.ArrayPVT2,
#     tcc.base.ArrayPVT3,
#     tcc.base.ArrayPVAJT3,
#     tcc.base.ArrayAxisErr3,
#     tcc.base.ArrayAxisState3,
# )

class TestArray(unittest.TestCase):
    """Test the SWIG wrapper for std::array
    """
    def testBoolArr(self):
        """Test bool arrays
        """
        desData = (True, False, True, False, True)
        for arrCls in (
            tcc.base.ArrayB2,
            tcc.base.ArrayB3,
        ):
            self.basicCheck(arrCls=arrCls, desData=desData)

    def testIntArr(self):
        """Test int arrays
        """
        desData = range(10, 20)
        for arrCls in (
            tcc.base.ArrayI2,
            tcc.base.ArrayI3,
        ):
            self.basicCheck(arrCls=arrCls, desData=desData)

    def testFloatArr(self):
        """Test float arrays
        """
        desData = [(i + 3 / 0.1) for i in range(20)]
        for arrCls in (
            tcc.base.ArrayD2,
            tcc.base.ArrayD3,
            tcc.base.ArrayD5,
        ):
            self.basicCheck(arrCls=arrCls, desData=desData)

    def testPVTArr(self):
        """Test PVT arrays
        """
        desData = [coordConv.PVT(1, 0.1, 5), coordConv.PVT(-3.2, 0.03, 100), coordConv.PVT(79.1, -0.1, 1000), coordConv.PVT(-35.6, -0.11, 7)]
        for arrCls in (
            tcc.base.ArrayPVT2,
            tcc.base.ArrayPVT3,
        ):
            self.basicCheck(arrCls=arrCls, desData=desData)

    def testPVAJTArr(self):
        """Test PVAJT arrays
        """
        desData = [
            tcc.base.PVAJT(1, 0.1, -0.03, 0.005, 5),
            tcc.base.PVAJT(-3.2, 0.03, -0.01, 0.04, 100),
            tcc.base.PVAJT(79.1, -0.1, 4.0, -5.5, 1000),
            tcc.base.PVAJT(-35.6, -5.4, 4.3, -0.11, 7),
        ]
        for arrCls in (
            tcc.base.ArrayPVAJT3,
        ):
            self.basicCheck(arrCls=arrCls, desData=desData)

    def testAxisErrArr(self):
        """Test AxisErr arrays
        """
        desData = (tcc.base.AxisErr_HaltRequested, tcc.base.AxisErr_NoRestart, tcc.base.AxisErr_MinPos, tcc.base.AxisErr_MaxAccel)
        for arrCls in (
            tcc.base.ArrayAxisErr3,
        ):
            self.basicCheck(arrCls=arrCls, desData=desData)

    def testAxisStateArr(self):
        """Test AxisState arrays
        """
        desData = (tcc.base.AxisState_NotAvailable, tcc.base.AxisState_Halted, tcc.base.AxisState_Slewing, tcc.base.AxisState_BadCode)
        for arrCls in (
            tcc.base.ArrayAxisState3,
        ):
            self.basicCheck(arrCls=arrCls, desData=desData)
    
    def basicCheck(self, arrCls, desData):
        """Check an array in several says
        
        @param[in] arrCls  an array cls
        @param[in] desData  an array containing at least 1 element more data than arrCls
        """
        arr = arrCls()
        n = len(arr)
        arr[:] = desData[0:n]
        self.checkEqual(arr, desData[0:n])
        for i in range(n):
            arr[i] = desData[i+1]
        self.checkEqual(arr, desData[1:n+1])
        

    def checkEqual(self, arr1, arr2):
        self.assertEqual(arr1, arr2)
        self.assertTrue(arr1 == arr2)
        self.assertFalse(arr1 != arr2)


if __name__ == '__main__':
    unittest.main()
