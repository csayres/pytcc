#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest

import coordConv
from twistedActor import CommandError

import tcc.base
from tcc.actor import TCCCmdParser
from tcc.parse import getPVTPairPxPMRadVel, ParseError
from coordConv import PVT

class GetPVTPairPxPMRadVelTest(unittest.TestCase):
    def setUp(self):
        self.parser = TCCCmdParser()

    def goodTest(self, cmdStr, coordName, predPVTPair, predPxPMRadVel):
        """Perform a test that should pass

        @param[in] cmdStr  command string
        @param[in] coordName  name of coord parameter
        @param[in] predPVTPair  predicted resulting PVTPair (may be None)
        @param[in] predPxPMRadVel  predicted resulting pxPMRadVel
        """
        try:
            parsedCmd = self.parser.parseLine(cmdStr)
            if predPVTPair is None:
                tai = 5000.0
            else:
                tai = predPVTPair[0].t
            pvtPair, pxPMRadVel = getPVTPairPxPMRadVel(parsedCmd, coordName, tai)
            if pvtPair is None:
                self.assertEqual(pvtPair, predPVTPair)
            else:
                self.assertEqual(tuple(pvtPair), tuple(predPVTPair))
            self.assertAlmostEqual(pxPMRadVel.parallax, predPxPMRadVel.parallax)
            self.assertAlmostEqual(pxPMRadVel.equatPM, predPxPMRadVel.equatPM)
            self.assertAlmostEqual(pxPMRadVel.polarPM, predPxPMRadVel.polarPM)
            self.assertAlmostEqual(pxPMRadVel.radVel, predPxPMRadVel.radVel)
        except Exception:
            print "goodTest failed on cmdStr=%r, coordName=%r, predPVTPair=%s, predPxPMRadVel=%s" % \
                (cmdStr, coordName, predPVTPair, predPxPMRadVel)
            raise
    
    def testGetPVTPairPxPMRadVel(self):
        """Test valid values for getPVTPairPxPMRadVel
        """
        # note: a value of None for a qualifier means omit; a value of "" means supply the qualifier with no value
        def pvtPairIter():
            """Return a list of pvtPair, cmdStr
            """
            tai = round(tcc.base.tai() - 5012, 3)
            for numPos in ((-5.1, 23.4), (0.5, 67.8)):
                for sentVel in (None, (-0.11, 0.21)):
                    for sentTAI in (None, tai, tai + 3000):
                        cmdStrList = []
                        cmdStrList.append("%0.5f, %0.5f" % tuple(numPos),)
                        if sentTAI is not None and sentVel is None:
                            # cannot omit vel if sending TAI
                            sentVel = (0, 0)

                        if sentVel is None:
                            numVel = (0, 0)
                        else:
                            numVel = sentVel
                            cmdStrList.append("%0.5f, %0.5f" % tuple(sentVel))

                        if sentTAI is None:
                            numTAI = tai
                        else:
                            numTAI = sentTAI
                            cmdStrList.append("%0.5f" % (sentTAI,))
                            
                        pvtPair = [PVT(numPos[i], numVel[i], numTAI) for i in range(2)]
                        cmdStr = ", ".join(cmdStrList)
                        yield pvtPair, cmdStr
            
        def pxPMRadVelIter():
            """Return a list of tcc.base.PxPMRadVel, cmdStr
            """
            for pm in (None,  "", (0.1, -0.2), (-0.01, 0.11)): # None means omit; "" means /pm
                for radVel in (None, "", 0.32): # None means omit; "" means /radVel
                    for parallax in (None, "", "-", 0, 0.11, -0.2):  # negative values mean use dist instead of parallax
                                                            # "-" means suppy /dist with no value (means infinity)
                        cmdStrList = []
                        if pm in (None, ""):
                            numPM = (0, 0)
                            if pm == "":
                                cmdStrList.append("/pm")
                        else:
                            numPM = pm
                            cmdStrList.append("/pm=(%0.7f, %0.7f)" % tuple(numPM))
                        
                        if radVel in (None, ""):
                            numRadVel = 0
                            if radVel == "":
                                cmdStrList.append("/rv")
                        else:
                            numRadVel = radVel
                            cmdStrList.append("/rv=%s" % (radVel,))
                        
                        if parallax in (None, "", "-"):
                            numParallax = 0
                            if parallax == "":
                                cmdStrList.append("/px")
                            elif parallax == "-":
                                cmdStrList.append("/dist")
                        else:
                            numParallax = abs(parallax)
                            if parallax >= 0:
                                cmdStrList.append("/px=%s" % (parallax,))
                            else:
                                # negative value means supply dist instead of parallax (but with a positive value)
                                dist = coordConv.distanceFromParallax(-parallax)
                                cmdStrList.append("/dist=%r" % (dist,))

                        cmdStr = "".join(cmdStrList)
                        pxPMRadVel = tcc.base.PxPMRadVel(numParallax, numPM[0], numPM[1], numRadVel)
                        yield pxPMRadVel, cmdStr
        
        coordName = "coordpair"
        for predPVTPair, coordStr in pvtPairIter():
            for predPxPMRadVel, pmStr in pxPMRadVelIter():
                cmdStr = "track %s icrs%s" % (coordStr, pmStr)
                self.goodTest(cmdStr=cmdStr, coordName=coordName, predPVTPair=predPVTPair, predPxPMRadVel=predPxPMRadVel)

    def testNullCommand(self):
        """Test that no coordpair results in None for pvtPair
        """
        self.goodTest(cmdStr="track", coordName="coordpair", predPVTPair=None, predPxPMRadVel=tcc.base.PxPMRadVel())
        
    def testBadCommands(self):
        """Test command strings that should fail to parse

        note that cases involving invalid # of values for the coordinates are tested by testGetPVTList
        so that is not emphasized here

        NOTE: why is /px and /rv acceptable (with no values) but /pm not?
        Make it like the old TCC, whatever that does.
        """
         # bad commands that will either be caught by the parser or getPVTPairPxPMRadVel
        for cmdStr in (
            "track fk5", # coord info required with new coordsys
            "track 1, 2 fk5/px=0.2/dist=45", # /px and /dist cannot appear together
            "track 1, 2 badsys", # unknown coordsys
            "track 1, 2 icrs/px=(1,2)", # /px needs one value (though 0 is OK)
            "track 1, 2 icrs/pm=5", # /pm needs two values
            "track 1, 2 icrs/pm=(1, 2, 3)", # /pm needs two values
            "track 1, 2 icrs/rv=(1, 2)", # /rv needs one value
            "track /px=0.1", # /px, etc. need new numVel
            "track /pm=(0.1, 0.2)", # /px, etc. need new numVel
            "track /rv=0.1", # /px, etc. need new numVel
        ):
            try:
                parsedCmd = self.parser.parseLine(cmdStr)
            except ParseError:
                continue
            self.assertRaises(CommandError, getPVTPairPxPMRadVel, parsedCmd, coordName="coordpair", defTAI=9.8e6)


if __name__ == '__main__':
    unittest.main()
