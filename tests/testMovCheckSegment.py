#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest

import tcc.base
from tcc.mov import checkSegment

class TestMovCheckPath(unittest.TestCase):
    """Test tcc.mov.checkSegment
    """
    def testBasics(self):
        dataList = ( # pA, vA, pB, vB, dt, doPos, doVel, doAccel, doJerk, pMin, pMax, vLim, aLim, jLim, predErrCode
            (-1, -1, 1, -1, 2,  True, True, True, True,   -1.09, 1.09, 2, 6, 6, tcc.base.AxisErr_OK),    # OK
            (-1, -1, 1, -1, 2,  True, True, True, True,   -1.09, 1.09, 2, 6, 5.999, tcc.base.AxisErr_MaxJerk),    # bad (jerk too big)
            (-1, -1, 1, -1, 2,  True, True, True, False,   -1.09, 1.09, 2, 6, 1, tcc.base.AxisErr_OK),    # OK (jerk not checked)
            (-1, -1, 1, -1, 2,  True, True, True, True,   -1.09, 1.09, 2, 5.999, 6, tcc.base.AxisErr_MaxAccel),    # bad (accel too big)
            (-1, -1, 1, -1, 2,  True, True, False, True,   -1.09, 1.09, 2, 5.999, 6, tcc.base.AxisErr_OK),    # OK (accel not checked)
            (-1, -1, 1, -1, 2,  True, True, True, True,   -1.09, 1.09, 1.999, 6, 6, tcc.base.AxisErr_MaxVel),    # bad (vel too big)
            (-1, -1, 1, -1, 2,  True, False, True, True,   -1.09, 1.09, 1.999, 6, 6, tcc.base.AxisErr_OK),    # OK (vel not checked)
            (-1, -1, 1, -1, 2,  True, True, True, True,   -1.09, 1.07, 2, 6, 6, tcc.base.AxisErr_MaxPos),    # bad (pos too big)
            (-1, -1, 1, -1, 2,  True, True, True, True,   -1.07, 1.09, 2, 6, 6, tcc.base.AxisErr_MinPos),    # bad (pos too small)
            (-1, -1, 1, -1, 2,  False, True, True, True,   -1.09, 1.07, 2, 6, 6, tcc.base.AxisErr_OK),    # OK (pos too big but ignored)
            (-1, -1, 1, -1, 2,  False, True, True, True,   -1.07, 1.09, 2, 6, 6, tcc.base.AxisErr_OK),    # OK (pos too small but ignored)
            (-1,  1, 0,  1, 2,  True, False, False, False,    0,    99,   2, 6, 6, tcc.base.AxisErr_OK),    # OK (but starts out-of-range)
            (-1,  0, 0,  0, 2,  True, False, False, False,    0,    99,   2, 6, 6, tcc.base.AxisErr_OK),    # OK (but starts out-of-range)
            (-1,  0, 0, -1, 2,  True, False, False, False,    0,    99,   2, 6, 6, tcc.base.AxisErr_OK),    # OK (but starts out-of-range)
            (-1, -0.2, 0, 1, 2, True, False, False, False,   0,    99,   2, 6, 6, tcc.base.AxisErr_MinPos),    # bad (goes further small)
            ( 1,  -1, 0,  -1, 2,  True, False, False, False,  -99,   0,   2, 6, 6, tcc.base.AxisErr_OK),    # OK (but starts out-of-range)
            ( 1,  0, 0,  0, 2,  True, False, False, False,    -99,   0,   2, 6, 6, tcc.base.AxisErr_OK),    # OK (but starts out-of-range)
            ( 1,  0, 0, 1, 2,  True, False, False, False,     -99,   0,   2, 6, 6, tcc.base.AxisErr_OK),    # OK (but starts out-of-range)
            ( 1,  0.2, 0, -1, 2, True, False, False, False,  -99,   0,   2, 6, 6, tcc.base.AxisErr_MaxPos),    # bad (goes further large)
            (-1, -1, 1, -1, 2,  True, True, True, True,    0,    0,    0, 0, 0, tcc.base.AxisErr_MinPos),    # bad (everything out of range)
            (-1, -1, 1, -1, 2,  False, False, False, False,    0,    0,    0, 0, 0, tcc.base.AxisErr_OK),    # OK (nothing checked)
        )
        for pA, vA, pB, vB, dt, doPos, doVel, doAccel, doJerk, pMin, pMax, vLim, aLim, jLim, predErrCode in dataList:
            errCode = checkSegment(
                pA=pA,
                vA=vA,
                pB=pB,
                vB=vB,
                dt=dt,
                doPos=doPos,
                doVel=doVel,
                doAccel=doAccel,
                doJerk=doJerk,
                axisLim=tcc.base.AxisLim(minPos=pMin, maxPos=pMax, vel=vLim, accel=aLim, jerk=jLim),
            )
            try:
                self.assertEqual(errCode, predErrCode)
            except Exception:
                print "Failed on", pA, vA, pB, vB, dt, doPos, doVel, doAccel, doJerk, pMin, pMax, vLim, aLim, jLim, predErrCode, errCode
            
if __name__ == '__main__':
    unittest.main()
