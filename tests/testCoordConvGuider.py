#!/usr/bin/env python2
from __future__ import division, absolute_import

import os
import unittest

import coordConv

import tcc.base
import tcc.axis
from tcc.parse import makeCoordSys

SecPerDay=3600 * 24.0

dataDir=os.path.abspath(os.path.join((os.path.dirname(__file__)), "data", "coordConvWithFP"))

def coordSysCodeIsFP(coordSysCode):
    return coordSysCode <= -5

class TestCoordConvGuider(unittest.TestCase):
    """Test convertConvWithFP for an instrument that is also a guider
    """
    def comparePVT(self, a, b, tai, maxErr, doWrap=True):
        """Compare two PVTs
        """
        self.assertEqual(a.isfinite(), b.isfinite())
        if a.isfinite():
            for testTAI in (tai, tai + 0.01):
                posA=a.getPos(testTAI)
                posB=b.getPos(testTAI)
                if doWrap:
                    measErr=coordConv.wrapCtr(posA - posB)
                else:
                    measErr=posA - posB
                self.assertLess(abs(measErr), maxErr)
    
    def runOne(self, suffix):
        """The main test
        
        @param[in] suffix  suffix for massccfp_obj_<suffix>.dat

        In the tests to date we only convert between observed and focal plane coordinate systems,
        so we ignore proper motion, parallax and radial velocity.
        """
        # load instrument data
        inst=tcc.base.Inst()
        inst.loadPath(os.path.join(dataDir, "guider_inst.dat"))

        # load axis limit data and update with inst rot limits
        axeLim=tcc.base.AxeLim()
        axeLim.loadPath(os.path.join(dataDir, "axelim.dat"))
        axeLim.setRotLim(inst)
        telMod=tcc.base.TelMod()
        telMod.loadPath(os.path.join(dataDir, "telmod.dat"))
        oldObjPath=os.path.join(dataDir, "massccfp_obj_%s.dat" % (suffix,))
        refObj=tcc.base.loadOldObjData(oldObjPath)
        earth=tcc.base.Earth()
        earth.loadPredictions(os.path.join(dataDir, "earthpred.dat"), refObj.updateTime)
        obj=tcc.base.Obj(refObj)
        taiDate = 4893005197.01000 # from massccfp_out_horizrot.dat

        tcc.axis.computeObj(
            obj=obj,
            doWrap=False,
            doRestart=(True, True, True),
            reportLim=True,
            earth=earth,
            inst=inst,
            telMod=telMod,
            axeLim=axeLim,
            tai=taiDate,
        )

        def simpleFPCoordConv(fromPVTPair, fromCoordSys, toCoordSys):
            """Convert a coordinate system from one focal plane coordinate system to another
            """
            fromPxPMRadVel=tcc.base.PxPMRadVel(0, 0, 0, 0)
            fromDir = coordConv.PVT(0, 0, taiDate)
            toPVTPair, toPxPMRadVel, toDir, scaleChange=tcc.axis.coordConvWithFP(
                fromCoordSys=fromCoordSys,
                fromPVTPair=fromPVTPair,
                fromPxPMRadVel=fromPxPMRadVel,
                fromDir=fromDir,
                toCoordSys=toCoordSys,
                obj=obj,
                inst=inst,
                zeroPM=False,
                useGSWavelen=False,
            )
            return toPVTPair


        for aCoordSysName in ("gprobe", "instrument", "rotator"):
            aCoordSysDate = 1 if aCoordSysName == "gprobe" else None
            aCoordSys = makeCoordSys(aCoordSysName, aCoordSysDate)
            gpCoordSys = makeCoordSys("gprobe", 1)
            instCoordSys = makeCoordSys("instrument", None)

            for pos in ((0, 0), (0.4, -0.3)):
                for vel in ((0, 0), (-0.01, 0.01)):
                    fromPVTPair = [coordConv.PVT(pos[i], vel[i], taiDate) for i in range(2)]

                    aToGPPVTPair = simpleFPCoordConv(fromPVTPair, aCoordSys, gpCoordSys)
                    aToInstPVTPair = simpleFPCoordConv(fromPVTPair, aCoordSys, instCoordSys)
                    self.checkPTVPairs(aToGPPVTPair, aToInstPVTPair)

                    gpToAPVTPair = simpleFPCoordConv(fromPVTPair, gpCoordSys, aCoordSys)
                    instToAPVTPair = simpleFPCoordConv(fromPVTPair, instCoordSys, aCoordSys)
                    self.checkPTVPairs(gpToAPVTPair, instToAPVTPair)

    def checkPTVPairs(self, pvtPair1, pvtPair2, posDelta=0.001, velDelta=0.01):
        """Check that two PVTPairs match

        posDelta is in arcsec
        velDelta is in arcsec/sec
        """
        for i in range(2):
            self.assertAlmostEqual(pvtPair1[i].pos, pvtPair2[i].pos, delta=posDelta/3600)
            self.assertAlmostEqual(pvtPair1[i].vel, pvtPair2[i].vel, delta=velDelta/3600)
            self.assertEqual(pvtPair1[i].t, pvtPair2[i].t)
    
    def testHorizRot(self):
        self.runOne("horizrot")

    def testObjRot(self):
        self.runOne("objrot")

if __name__ == '__main__':
    unittest.main()
