#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Test the Queue command
"""
from twisted.internet.defer import Deferred
from twisted.trial.unittest import TestCase
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")

import tcc.base.testUtils
tcc.base.testUtils.init(__file__)
from tcc.cmd.queue import BatchJob

from tcc.base.wrapperTestCase import WrapperTestCase

class TestQueue(WrapperTestCase, TestCase):

    def testRunTest1(self):
        """Run "queue run=test1" and let it finish
        """
        d1 = Deferred()
        def startedCallback(cmdVar):
            if cmdVar.didFail:
                d1.errback(AssertionError("Failed to start test1"))
            elif not cmdVar.isDone:
                return
            self.assertEquals(self.model.jobStatus[0].lower(), "test1")
            self.assertEquals(self.model.jobStatus[1].lower(), "running")

        self.dw.queueCmd(
            cmdStr = "queue run=test1",
            callFunc = startedCallback,
        )

        def jsCallback(jobStatus):
            # print "jobStatus=", jobStatus[:]
            if None in jobStatus:
                return
            self.assertEquals(jobStatus[0].lower(), "test1")
            jobState = jobStatus[1]
            if jobState in BatchJob.ErrorStates:
                d1.errback(Exception(jobState))
            elif jobState in BatchJob.DoneStates:
                d1.callback(None)

        self.model.jobStatus.addCallback(jsCallback)
        return d1

    def testStopTest1(self):
        """Run "queue run=test1" and stop it
        """
        d1 = Deferred()
        def finishedCallback(cmdVar):
            if cmdVar.didFail:
                d1.errback(AssertionError("Failed to stop test1"))
            elif not cmdVar.isDone:
                return
            self.assertEquals(self.model.jobStatus[0].lower(), "test1")
            self.assertEquals(self.model.jobStatus[1].lower(), "cancelled")
            d1.callback(None)

        def startedCallback(cmdVar):
            if cmdVar.didFail:
                d1.errback(AssertionError("Failed to start test1"))
            elif not cmdVar.isDone:
                return
            self.assertEquals(self.model.jobStatus[0].lower(), "test1")
            self.assertEquals(self.model.jobStatus[1].lower(), "running")
            self.dw.queueCmd(
                cmdStr = "queue stop",
                callFunc = finishedCallback,
            )

        self.dw.queueCmd(
            cmdStr = "queue run=test1",
            callFunc = startedCallback,
        )
        return d1

if __name__ == '__main__':
    from unittest import main
    main()

