from __future__ import division, absolute_import

import os

from twisted.trial.unittest import TestCase
from twisted.internet.defer import gatherResults

from tcc.base import testUtils
testUtils.init(__file__)
from tcc.base.trackTestUtils import TrackCommonMethods, generateChebyFile

DataDir = os.path.join(os.path.abspath(os.path.dirname(__file__)), "data")

class TestTrackShowObj(TrackCommonMethods, TestCase):

    def testTrackCheby(self):
        chebyFilePath = os.path.join(DataDir, "tth_cheby.dat")
        generateChebyFile(chebyFilePath)
        trackCmd = 'track/chebyshev="%s"' % (chebyFilePath,)
        d = self.checkATrackCmd(trackCmd)
        def checkChebyOn(foo=None):
            try:
                # actor check
                self.assertTrue(self.actor.obj.useCheby)
                # keyword check
                # self.model.useCheby == "T"
                # check that the correct output was sent to users (by looking at values on model set by keywords)
                model = self.dw.dispatcher.model
                print "model cheby", model.useCheby
            finally:
                os.unlink(chebyFilePath)
        d.addCallback(checkChebyOn)
        return d


    def testTrackNoCheby(self):
        """Send a simple track command verify that obj block ouput is correct.
        """
        trackCmd = "track 12,12 observed"
        trackStop = "track/stop"
        d = self.checkATrackCmd(trackCmd)
        d2 = self.checkATrackCmd(trackStop)
        def checkChebyOff(foo=None):
            self.assertFalse(self.actor.obj.useCheby)
        d.addCallback(checkChebyOff)
        d2.addCallback(checkChebyOff)
        return gatherResults([d,d2])


    # def checkTrackCmd(self, trackCmd, shouldRotate=False):
    #     """Start next track command from axes halted state.
    #     """
    #     beg = ["Halted"]*3
    #     mid = ["Slewing"]*3
    #     end = ["Tracking"]*3
    #     if not shouldRotate:
    #         mid[2] = "Halted"
    #         end[2] = "Halted"
    #     self.checkAxisCmdState(cmdVar=None, axisCmdState = beg)

    #     def checkSlew(cmdVar):
    #         if cmdVar.lastCode == ">":
    #             self.checkAxisCmdState(cmdVar, axisCmdState = mid)
    #         elif cmdVar.isDone:
    #             self.checkAxisCmdState(cmdVar, axisCmdState = end)

    #     d, cmd = self.dw.queueCmd(
    #         trackCmd,
    #         callFunc = checkSlew,
    #         callCodes = ">:",
    #     )
    #     return d

if __name__ == '__main__':
    from unittest import main
    main()