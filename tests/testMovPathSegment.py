#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest

from tcc.mov import PathSegment

class TestMovComputePath(unittest.TestCase):
    def testBasics(self):
        dataList = (
            (1, 0, 1, 0, 5, True, 1, 1, 0, 0, 0),
            (0, 6, 0, -6, 6, True, 0, 9 - PathSegment.PosOvertravel, 6, -2, 0),
            (0, -6, 0, 6, 6, True, -9 + PathSegment.PosOvertravel, 0, 6, 2, 0),
            (0, 0, 1, -1, 3, True, 0, 1.28 - PathSegment.PosOvertravel, 1, 1.333333333, -1.111111111),
            (-1, -1, 0, 0, 3, True, -1.28 + PathSegment.PosOvertravel, 0, 1, 2.0, -1.111111111),
            (-1, -1, 1, -1, 2, True, -1.0886621079 + PathSegment.PosOvertravel, 1.0886621079 - PathSegment.PosOvertravel, 2.0, 6.0, -6.0),
            (-1, -1, 1, -1, 2, False, 0.0, 0.0, 2.0, 6.0, -6.0),
        )

        for pA, vA, pB, vB, dt, doPos, pMin, pMax, vPeak, aA, j in dataList:
            segment = PathSegment(
                pA=pA,
                vA=vA,
                pB=pB,
                vB=vB,
                dt=dt,
                doPos=doPos,
            )
            try:
                if doPos:
                    self.assertAlmostEqual(segment.pMin, pMin)
                    self.assertAlmostEqual(segment.pMax, pMax)
                else:
                    self.assertEqual(segment.pMin, None)
                    self.assertEqual(segment.pMax, None)
                self.assertAlmostEqual(segment.vPeak, vPeak)
                self.assertAlmostEqual(segment.aA, aA)
                self.assertAlmostEqual(segment.j, j)
            except Exception:
                print "Failed on", pA, vA, pB, vB, dt, doPos
                raise
    
    def testErrors(self):
        self.assertRaises(RuntimeError, PathSegment, 1, 1, 1, 1, 1e-99, True)
            
if __name__ == '__main__':
    unittest.main()
        
