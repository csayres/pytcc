#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest
import numpy
import coordConv
from tcc.base import PVAJT
from tcc.axis import interpolatePVTs

DeltaT = 0.01

def refExtrapolate(pvajt, t):
    """Reference implementation of PVAJT.extrapolate
    """
    dt = t - pvajt.t
    return PVAJT(
        pvajt.pos   + (dt * (pvajt.vel   + (dt * ((0.5 * pvajt.accel) + (dt * pvajt.jerk / 6.0))))),
        pvajt.vel   + (dt * (pvajt.accel + (dt *   0.5 * pvajt.jerk))),
        pvajt.accel + (dt *  pvajt.jerk),
        pvajt.jerk,
        t,
    )

def arrayFromPVAJT(pvajt):
    return (pvajt.pos, pvajt.vel, pvajt.accel, pvajt.jerk, pvajt.t)


class TestPVT(unittest.TestCase):
    def testConstructors(self):
        """Test PVAJT constructors
        """
        pvajt = PVAJT(1.0, 2.0, 3.0, 4.0, 5.0)
        self.assertEquals(pvajt.pos,   1.0)
        self.assertEquals(pvajt.vel,   2.0)
        self.assertEquals(pvajt.accel, 3.0)
        self.assertEquals(pvajt.jerk,  4.0)
        self.assertEquals(pvajt.t,     5.0)
        self.assertTrue(pvajt.isfinite())
        
        pvt = coordConv.PVT(-1.1, -2.2, 99.0)
        pvajt = PVAJT(pvt)
        self.assertEquals(pvajt.pos,   -1.1)
        self.assertEquals(pvajt.vel,   -2.2)
        self.assertEquals(pvajt.accel,  0.0)
        self.assertEquals(pvajt.jerk,   0.0)
        self.assertEquals(pvajt.t,     99.0)
        self.assertTrue(pvajt.isfinite())
        
            
    def testExtrapolate(self):
        """Test pvajt.extrapolate
        """
        pvajt = PVAJT(1.1, 2.2, 0.5, -0.3, 12345.0)
        for dt in (1235.5, 0, -123.3):
            newt = pvajt.t + dt
            self.assertAlmostEqual(
                arrayFromPVAJT(pvajt.extrapolate(newt)),
                arrayFromPVAJT(refExtrapolate(pvajt, newt)))
    
    def testIsValid(self):
        """Test pvajt.isfinite
        """
        pvajt = PVAJT(1, 2, 3, 4, 5)
        
        self.assertTrue(pvajt.isfinite())
        pvajt.pos = numpy.nan
        self.assertFalse(pvajt.isfinite())
        pvajt.pos = 1.0
        self.assertTrue(pvajt.isfinite())

        pvajt.vel = numpy.nan
        self.assertFalse(pvajt.isfinite())
        pvajt.vel = 1.0
        self.assertTrue(pvajt.isfinite())

        pvajt.accel = numpy.nan
        self.assertFalse(pvajt.isfinite())
        pvajt.accel = 1.0
        self.assertTrue(pvajt.isfinite())

        pvajt.jerk = numpy.nan
        self.assertFalse(pvajt.isfinite())
        pvajt.jerk = 1.0
        self.assertTrue(pvajt.isfinite())
        
        pvajt.t = numpy.nan
        self.assertFalse(pvajt.isfinite())
        pvajt.t = 0.0
        self.assertTrue(pvajt.isfinite())

    def testInterpolatePVTs(self):
        """Test interpolatePVTs by comparing pos, vel, time at endpoints
        
        Assumes that PVAJT.extrapolate works correctly.
        """
        t0 = 1000
        for dt in (1.5, 37.6):
            t1 = t0 + dt
            for p0 in (-5, 9):
                for v0 in (-0.1, 0.3):
                    pvt0 = coordConv.PVT(p0, v0, t0)
                    for p1 in (-3, 6):
                        for v1 in (-0.233, 0.34):
                            pvt1 = coordConv.PVT(p1, v1, t1)
                            pvajt0 = interpolatePVTs(pvt0, pvt1)
                            self.assertEqual(pvajt0.pos, pvt0.pos)
                            self.assertEqual(pvajt0.vel, pvt0.vel)
                            self.assertEqual(pvajt0.t,   pvt0.t)
                            pvajt1 = pvajt0.extrapolate(t1)
                            self.assertAlmostEqual(pvajt1.pos, pvt1.pos)
                            self.assertAlmostEqual(pvajt1.vel, pvt1.vel)
                            self.assertAlmostEqual(pvajt1.t,   pvt1.t)
    
    def testEquals(self):
        """Test == and != operators
        """
        def pvajtIter():
            for pos in (45.00001, 45.0):
                for vel in (-35.2, -35.20001):
                    for accel in (0.1, 0.10001):
                        for jerk in (-0.03, -0.030001):
                            for t in (500.0, 500.00001):
                                yield PVAJT(pos, vel, accel, jerk, t)
        
        def refEq(pvajt1, pvajt2):
            return all((
                pvajt1.pos == pvajt2.pos,
                pvajt1.vel == pvajt2.vel,
                pvajt1.accel == pvajt2.accel,
                pvajt1.jerk == pvajt2.jerk,
                pvajt1.t == pvajt2.t,
            ))
        
        for pvajt1 in pvajtIter():
            for pvajt2 in pvajtIter():
                if refEq(pvajt1, pvajt2):
                    self.assertTrue(pvajt1 == pvajt2)
                    self.assertFalse(pvajt1 != pvajt2)
                else:
                    self.assertFalse(pvajt1 == pvajt2)
                    self.assertTrue(pvajt1 != pvajt2)

if __name__ == '__main__':
    unittest.main()
