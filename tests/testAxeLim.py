#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest
from StringIO import StringIO

import tcc.base

class TestAxeLim(unittest.TestCase):
    def testCopyConstructor(self):
        """Test copy constructor and repr
        """
        axeLim = tcc.base.AxeLim()
        axeLimData = """
            # AxeLim data
            # Minimum position of Az, Alt, Rot (deg)
            minPos -180.0 6.0 -360.0
            # Maximum position of Az, Alt, Rot (deg)
            maxPos 360.0 87.2 360.0
            # Maximum velocity of Az, Alt, Rot (deg/sec)
            vel 1.25 1.6 3.5
            # Maximum acceleration of Az, Alt, Rot (deg/sec^2)
            accel 0.15 0.1 0.7
            # Maximum jerk of Az, Alt, Rot (deg/sec^3)
            jerk 0.75 0.5 7.0
            # Fail if any of these bits are high in an axis controller's status word
            badStatusMask 0XCD71CC
            # Warn if any of these bits are high in an axis controller's status word and no badStatusMask bits are high
            warnStatusMask 0X61040033
            # Warn if axis controller and TCC clock disagree by this much (sec)
            maxDTime 0.4
        """
        axeLimFile = StringIO(axeLimData)
        axeLim.load(axeLimFile)
        
        inStrList  = [line.strip() for line in axeLimData.split("\n")   if line.strip()]
        outStrList = [line.strip() for line in repr(axeLim).split("\n") if line.strip()]
        self.assertEqual(inStrList, outStrList)
        
        axeLimCopy = tcc.base.AxeLim(axeLim)
        self.assertEqual(repr(axeLim), repr(axeLimCopy))
        
        azLim = axeLim[0]
        self.assertEqual(azLim.minPos, -180.0)
        self.assertEqual(azLim.maxPos,  360.0)
        self.assertEqual(azLim.vel, 1.25)
        self.assertEqual(azLim.accel, 0.15)
        self.assertEqual(azLim.jerk, 0.75)
        altLim = axeLim[1]
        self.assertEqual(altLim.minPos,  6.0)
        self.assertEqual(altLim.maxPos, 87.2)
        self.assertEqual(altLim.vel, 1.6)
        self.assertEqual(altLim.accel, 0.1)
        self.assertEqual(altLim.jerk, 0.5)
        rotLim = axeLim[2]
        self.assertEqual(rotLim.minPos, -360.0)
        self.assertEqual(rotLim.maxPos,  360.0)
        self.assertEqual(rotLim.vel, 3.5)
        self.assertEqual(rotLim.accel, 0.7)
        self.assertEqual(rotLim.jerk, 7.0)
        
        
    def testSetRotLim(self):
        """Test AxeLim.setRotLim
        """
        axeLim = tcc.base.AxeLim()
        axeLimData = """
            # AxeLim data
            # Minimum position of Az, Alt, Rot (deg)
            minPos -180.0 6.0 -360.0
            # Maximum position of Az, Alt, Rot (deg)
            maxPos 360.0 87.2 360.0
            # Maximum velocity of Az, Alt, Rot (deg/sec)
            vel 1.25 1.6 3.5
            # Maximum acceleration of Az, Alt, Rot (deg/sec^2)
            accel 0.15 0.1 0.7
            # Maximum jerk of Az, Alt, Rot (deg/sec^3)
            jerk 0.75 0.5 7.0
            # Fail if any of these bits are high in an axis controller's status word
            badStatusMask 0XCD71CC
            # Warn if any of these bits are high in an axis controller's status word and no badStatusMask bits are high
            warnStatusMask 0X61040033
            # Warn if axis controller and TCC clock disagree by this much (sec)
            maxDTime 0.4
        """
        axeLimFile = StringIO(axeLimData)
        axeLim.load(axeLimFile)
        self.assertEqual(tuple(axeLim.minPos), (-180.0, 6.0, -360.0))
        self.assertEqual(tuple(axeLim.maxPos), (360.0, 87.2, 360.0))
        self.assertEqual(tuple(axeLim.vel),    (1.25, 1.6, 3.5))
        self.assertEqual(tuple(axeLim.accel),  (0.15, 0.1, 0.7))
        self.assertEqual(tuple(axeLim.jerk),   (0.75, 0.5, 7.0))
        self.assertEqual(axeLim.badStatusMask,  0XCD71CC)
        self.assertEqual(axeLim.warnStatusMask, 0X61040033)
        self.assertEqual(axeLim.maxDTime,       0.4)
        
        inst = tcc.base.Inst()
        for initialRotLim in (
            (-360, 360, 200, 300, 400),
            (0, 0, 0, 0, 0),
            (1, -1, -1, -1, -1),
        ):
            for rotLim in (
                (-180, 720, 300, 200, 500),
                (-720, 180, 100, 400, 300),
                (1, 1, 1, 1, 1)
            ):
                inst.rotLim[:] = rotLim
                for instPosLim in (
                    (-5, 5),
                    (-300, 200),
                    (-9000, 9000)
                ):
                    inst.instPosLim[:] = instPosLim
                    axeLim.setRotLim(inst)
                    predMinPos = max(rotLim[0], instPosLim[0])
                    predMaxPos = min(rotLim[1], instPosLim[1])
                    self.assertEqual(tuple(axeLim.minPos), (-180.0, 6.0, predMinPos))
                    self.assertEqual(tuple(axeLim.maxPos), (360.0, 87.2, predMaxPos))
                    self.assertEqual(tuple(axeLim.vel),    (1.25, 1.6, inst.rotLim[2]))
                    self.assertEqual(tuple(axeLim.accel),  (0.15, 0.1, inst.rotLim[3]))
                    self.assertEqual(tuple(axeLim.jerk),   (0.75, 0.5, inst.rotLim[4]))


if __name__ == '__main__':
    unittest.main()
