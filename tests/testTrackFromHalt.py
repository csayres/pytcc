#!/usr/bin/env python2
from __future__ import division, absolute_import

import os

from twisted.trial.unittest import TestCase

from tcc.base import testUtils
testUtils.init(__file__)
from tcc.base.trackTestUtils import TrackCommonMethods, AllTests, LessTests, generateChebyFile


runAllTests = True

DataDir = os.path.join(os.path.abspath(os.path.dirname(__file__)), "data")

"""Errors to fix:
Traceback (most recent call last):
  File "/Users/rowen/TCC/code/twistedActor/python/twistedActor/dispatcherWrapper.py", line 230, in _cmdCallback
    self.callFunc(self.cmdVar)
  File "/Users/rowen/TCC/code/tcc/python/tcc/base/trackTestUtils.py", line 411, in checkTrackCmdState
    self.checkTrackResult(cmdVar, axisCmdState = begAxisCmdState, **dataDict)
  File "/Users/rowen/TCC/code/tcc/python/tcc/base/wrapperTestCase.py", line 87, in checkTrackResult
    self.assertEqual(rotType.lower(), str(self.model.rotType[0]).lower())
  File "/Users/rowen/LSST/lsst_home2/DarwinX86/anaconda/1.8.0/lib/python2.7/site-packages/twisted/trial/_synctest.py", line 356, in assertEqual
    % (msg, pformat(first), pformat(second)))
FailTest: not equal:
a = 'horizon'
b = 'horiz'

similarly a="object", b="obj"

Traceback (most recent call last):
  File "/Users/rowen/TCC/code/twistedActor/python/twistedActor/dispatcherWrapper.py", line 230, in _cmdCallback
    self.callFunc(self.cmdVar)
  File "/Users/rowen/TCC/code/tcc/python/tcc/base/trackTestUtils.py", line 411, in checkTrackCmdState
    self.checkTrackResult(cmdVar, axisCmdState = begAxisCmdState, **dataDict)
  File "/Users/rowen/TCC/code/tcc/python/tcc/base/wrapperTestCase.py", line 90, in checkTrackResult
    self.checkUserSys(cmdVar=cmdVar, coordSysName=coordSysName, coordSysDate=coordSysDate)
  File "/Users/rowen/TCC/code/tcc/python/tcc/base/wrapperTestCase.py", line 129, in checkUserSys
    self.assertAlmostEqual(objSysDate, coordSysDate)
  File "/Users/rowen/LSST/lsst_home2/DarwinX86/anaconda/1.8.0/lib/python2.7/site-packages/twisted/trial/_synctest.py", line 470, in assertAlmostEqual
    if round(second-first, places) != 0:
TypeError: unsupported operand type(s) for -: 'str' and 'Float'

"""
class TestTrackFromHalt(TrackCommonMethods, TestCase, AllTests if runAllTests else LessTests):
    def testTrackCheby(self):
        chebyFilePath = os.path.join(DataDir, "tth_cheby.dat")
        generateChebyFile(chebyFilePath)
        trackCmd = 'track/chebyshev="%s"' % (chebyFilePath,)
        d = self.checkTrackCmd(trackCmd)
        def checkChebyOn(foo=None):
            try:
                self.assertTrue(self.actor.obj.useCheby)
            finally:
                os.unlink(chebyFilePath)
        d.addCallback(checkChebyOn)
        return d

    def checkTrackCmd(self, trackCmd, shouldRotate=False):
        """Start next track command from axes halted state.
        """
        begAxisCmdState = ["Slewing"]*3
        endAxisCmdState = ["Tracking"]*3
        if not shouldRotate:
            begAxisCmdState[2] = "Halted"
            endAxisCmdState[2] = "Halted"
        self.checkAxisCmdState(cmdVar=None, axisCmdState = ["Halted"]*3)

        d = self.checkATrackCmd(
            cmdStr = trackCmd,
            begAxisCmdState = begAxisCmdState,
            endAxisCmdState = endAxisCmdState,
        )
        return d


if __name__ == '__main__':
    from unittest import main
    main()

