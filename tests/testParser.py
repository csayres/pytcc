#!/usr/bin/env python2
from __future__ import division, absolute_import

import os
import unittest

from tcc.actor import TCCCmdParser

InputCmdsPath = os.path.join(os.path.dirname(__file__), "data", "inputCmds.txt")

class CommandSet(object):
    def __init__(self):
        # my own commands
        goodTestCmds = [
            'set instrum /Keep=scale',
            'Track 1,2 ICRS',
            'AXIS iniT Az',
            'AXIS Stop Az, Alt/Timelim=5',
            'AX sto',
            'Set wave ob=12345,gs=777! aklsjdjkefllkkaj',
        ]

        # commands extracted from 3.5m logs
        with open(InputCmdsPath, "rU") as f:
            inputCmds = [line.strip() for line in f.readlines()]
        self.goodTestCmds = goodTestCmds + inputCmds

class ParserTest(unittest.TestCase):
    def setUp(self):
        self.parser = TCCCmdParser()

    def testGoodCmd(self):
        """does it balk at a bunch of good commmands?
        """
        for line in CommandSet().goodTestCmds:
            try:
                self.parser.parseLine(line)
            except Exception as e:
                raise RuntimeError("failed on: %r, with error %r" % (line,e))

    def testQualInOut(self):
        """Test to see if qualifiers are being handled correctly
        """
        parsedCmd = self.parser.parseLine('AXIS Stop Az, Alt/Timelim=5')
        qualDict = parsedCmd.qualDict
        all = ['timelimit', 'nocheck']
        for qual in qualDict:
            # are all quals accounted for?
            self.assertTrue(qual in all)
            if qual == 'nocheck':
                self.assertFalse(qualDict[qual].boolValue)
            else:
                self.assertTrue(qualDict[qual].boolValue)
            if qual == 'timelimit':
                self.assertEqual(qualDict[qual].valueList[0], 5)

    def testWrongAmt(self):
        """Ensure that parser complains when the wrong amount of values are specified
        """
        wrongAmtCmds = [
            'AXIS Stop Az, Alt/Timelim=(4,2)',
            'ROTATE 1,2,3,4 obj'
        ]
        for cmd in wrongAmtCmds:
            self.assertRaises(Exception, self.parser.parseLine, cmd)

    def testUnpassedQuals(self):
        """Test if unspecified qualifiers are correctly set to false
        """
        parsedCmd = self.parser.parseLine('Axis init')
        qualDict = parsedCmd.qualDict
        quals = ['nocheck', 'timelimit']
        for qual in quals:
            self.assertFalse(qualDict[qual].boolValue)

    def testQualNegation(self):
        """Check to see if negated keywords are handled correctly
        """
        parsedCmd = self.parser.parseLine('Offset Arc /nocoll')
        qualDict = parsedCmd.qualDict
        self.assertFalse(qualDict['collimate'].boolValue)

    def testQualDefaults(self):
        """Test for recognition of passed as default vs passed by user
        """
        line = 'track /Px'
        parsedCmd = self.parser.parseLine(line)
        qualDict = parsedCmd.qualDict
        self.assertTrue(qualDict['px'].valueListDefaulted)
        self.assertEqual(qualDict['px'].valueList[0], 0)
    
        line = 'offset arc /restart'
        parsedCmd = self.parser.parseLine(line)
        qualDict = parsedCmd.qualDict
        self.assertTrue(qualDict['restart'].valueListDefaulted)
        self.assertEqual(qualDict['restart'].valueList, [])

        line = 'offset arc /restart=(az)'
        parsedCmd = self.parser.parseLine(line)
        qualDict = parsedCmd.qualDict
        self.assertFalse(qualDict['restart'].valueListDefaulted)
        self.assertEqual(qualDict['restart'].valueList[0], 'azimuth')

        line = 'AXIS Stop Az, Alt/Timelim=5'
        parsedCmd = self.parser.parseLine(line)
        qualDict = parsedCmd.qualDict
        self.assertFalse(qualDict['timelimit'].valueListDefaulted)

    def testParamInOut(self):
        """Test to see if params are being handled correctly
        """
        parsedCmd = self.parser.parseLine('AXIS Sto Az, Alt/Timelim=5' )
        params = parsedCmd.paramDict.values()
        self.assertEqual(params[0].valueList[0].keyword, 'stop')
        self.assertEqual(params[1].valueList[0].keyword, 'azimuth')
        self.assertEqual(params[1].valueList[1].keyword, 'altitude')

    def testParamDefaults(self):
        """Test to see if parameter defaults are correctly inserted
        when not specified by the user
        """
        parsedCmd = self.parser.parseLine('AXIS Init')
        params = parsedCmd.paramDict.values()
        self.assertEqual(params[0].valueList[0].keyword, 'initialize')
        self.assertEqual(params[1].valueList[0].keyword, 'all')
        self.assertTrue(params[1].defaulted)
        self.assertFalse(params[0].defaulted)

        parsedCmd = self.parser.parseLine('OFFSET Bore')
        params = parsedCmd.paramDict.values()
        self.assertEqual(params[0].valueList[0].keyword, 'boresight')
        self.assertEqual(params[1].valueList, []) # changed the default value
        # self.assertEqual(params[1].valueList[0], 0)
        # self.assertEqual(params[1].valueList[1], 0)
        self.assertTrue(params[1].defaulted)
        self.assertFalse(params[0].defaulted)

        parsedCmd = self.parser.parseLine('track 1,2 FK5')
        params = parsedCmd.paramDict.values()
        self.assertEqual(params[1].valueList[0].valueList[0], 2000.0)
        self.assertTrue(params[1].valueList[0].defaulted)

        parsedCmd = self.parser.parseLine('track')
        params = parsedCmd.paramDict.values()
        self.assertEqual(params[0].valueList, [])
        self.assertTrue(params[0].defaulted)        

    def testUnpassedParams(self):
        """Test that unspecified params (which may default to something)
        have a boolValue of False
        """
        parsedCmd = self.parser.parseLine('track')
        for param in parsedCmd.paramDict.itervalues():
            self.assertFalse(param.boolValue)    

    def testUnpassedParamsNoDefault(self):
        """Test that unspecified params which contain no default value, have a valueList of ()
        And defaulted flag equals False
        """
        parsedCmd = self.parser.parseLine('track')
        coordSet, coordSys = parsedCmd.paramDict.values()
        self.assertFalse(coordSet.boolValue)   
        self.assertFalse(coordSys.boolValue)    
        self.assertEqual(coordSys.valueList, ())
        self.assertFalse(coordSys.defaulted)


    def testBadQualDefault(self):
        """Try to construct a qualifier with a default valueList outside the specified
        range, a CmdError should be raised
        """
        from tcc.parse.parseDefs import Qualifier, CmdDefError
        try:
            Qualifier("badQualDef", numValueRange = [2,2], defValueList = (1))
        except CmdDefError:
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def testBadParamDefault(self):
        """Try to construct a parameter with a default valueList outside the specified
        range, a CmdError should be raised
        """        
        from tcc.parse.parseDefs import ParamElement, CmdDefError
        try:
            ParamElement("badParam", numValueRange = [2,2], defValueList=[1])
        except CmdDefError:
            self.assertTrue(True)
        else:
            self.assertTrue(False)


    def testCallBack(self):
        """A function to call should be passed with each command
        """
        cmd = 'AXIS Stop'
        parsedCmd = self.parser.parseLine(cmd)
        parsedCmd.callFunc
        self.assertTrue(True)

        cmd = 'PING'
        parsedCmd = self.parser.parseLine(cmd)
        parsedCmd.callFunc
        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()
