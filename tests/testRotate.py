#!/usr/bin/env python2
from __future__ import division, absolute_import

import functools

from twisted.trial.unittest import TestCase
from twisted.internet.defer import gatherResults

from tcc.base import testUtils
from tcc.base.trackTestUtils import TrackCommonMethods

testUtils.init(__file__)

class TestRotate(TrackCommonMethods, TestCase):

    def _startTracking(self, trackCmd):
        d, cmd = self.dw.queueCmd(
            trackCmd,
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = ["Tracking", "Tracking", "Halted"]),
        )
        return d

    def _stopTracking(self, trackCmd):
        d, cmd = self.dw.queueCmd(
            trackCmd,
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = ["Halted", "Halted", "Halted"]),
        )
        return d

    def _startRotating(self, rotateCmd):
        d, cmd = self.dw.queueCmd(
            rotateCmd,
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = ["Tracking", "Tracking", "Tracking"]),
        )
        return d

    def _stopRotating(self, stopRotateCmd):
        d, cmd = self.dw.queueCmd(
            stopRotateCmd,
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = ["Tracking", "Tracking", "Halted"]),
        )
        return d

    def testObs(self):
        trackCmd = "track 12,12 observed"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 obj"
        d2 = self._startRotating(rotateCmd)
        return gatherResults([d1, d2])

    def testMount(self):
        trackCmd = "track 12,12 mount"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 mount"
        d2 = self._startRotating(rotateCmd)
        return gatherResults([d1, d2])

    def testHoriz(self):
        trackCmd = "track 12,12 observed"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 horizon"
        d2 = self._startRotating(rotateCmd)
        return gatherResults([d1, d2])

    def testStopObs(self):
        trackCmd = "track 12,12 observed"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 obj"
        d2 = self._startRotating(rotateCmd)
        stopCmd = "rotate /Stop"
        d3 = self._stopRotating(stopCmd)
        return gatherResults([d1, d2, d3])

    def testStopMount(self):
        trackCmd = "track 12,12 mount"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 mount"
        d2 = self._startRotating(rotateCmd)
        stopCmd = "rotate /Stop"
        d3 = self._stopRotating(stopCmd)
        return gatherResults([d1, d2, d3])

    def testNoColl(self):
        trackCmd = "track 12,12 observed"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 obj /NoCollimate"
        d2 = self._startRotating(rotateCmd)
        return gatherResults([d1, d2])

    def testRefCoeff(self):
        trackCmd = "track 12,12 observed"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 obj /RefCoeff"
        d2 = self._startRotating(rotateCmd)
        return gatherResults([d1, d2])

    def testRestart(self):
        trackCmd = "track 12,12 observed"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 obj /Restart"
        d2 = self._startRotating(rotateCmd)
        return gatherResults([d1, d2])

    def testPerf(self):
        trackCmd = "track 12,12 observed"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 obj /perfect"
        d2 = self._startRotating(rotateCmd)
        return gatherResults([d1, d2])

    def testWrap(self):
        trackCmd = "track 12,12 observed"
        d1 = self._startTracking(trackCmd)
        rotateCmd = "rotate 12,0 obj /rotwrap=nearest"
        d2 = self._startRotating(rotateCmd)
        return gatherResults([d1, d2])

if __name__ == "__main__":
    from unittest import main
    main()