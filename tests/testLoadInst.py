#!/usr/bin/env python2
from __future__ import division, absolute_import

import os
import unittest

import RO.SeqUtil

import tcc.base
import tcc.base.testUtils

tcc.base.testUtils.init()
instDir = os.path.join(tcc.base.getDataDir(), "loadInst")

class TestLoadInstFunc(unittest.TestCase):
    """Test the loadInst function
    
    Note: data/makeLoadInstFiles.py generated the instrument data files
    used by this unit test. If the inst block changes you may have to
    edit makeLoadInstFiles.py and use it to generate new data files.
    """
    def testLoadOrder(self):
        """Test that instrument data files are loaded in the correct order.
        """
        for instNum, gcViewNum in (
            (1, 0),
            (1, 1),
            (2, 0),
            (3, 0),
            (4, 0),
        ):
            instName = "inst%d" % (instNum,)
            if gcViewNum > 0:
                gcViewName = "view%d" % (gcViewNum,)
                expectedFileName = "expected_view%d_inst%d.dat" % (gcViewNum, instNum)
            else:
                gcViewName = ""
                expectedFileName = "expected_inst%d.dat" % (instNum,)

            inst = tcc.base.loadInst(instDir=instDir, instName=instName, gcViewName=gcViewName)

            expectedFilePath = os.path.join(instDir, expectedFileName)
            expectedInst = tcc.base.Inst()
            expectedInst.loadPath(expectedFilePath)
            
            try:
                for fieldWrapper in inst._WrapperList.wrapperList:
                    fieldName = fieldWrapper.name
                    if not fieldName:
                        continue

                    val = getattr(inst, fieldName)
                    expectedVal = getattr(expectedInst, fieldName)
                    if fieldName == "gProbe":
                        pass
                    elif fieldName == "instPos":
                        self.assertEqual(val.getName(), expectedVal.getName())
                    elif RO.SeqUtil.isSequence(val):
                        self.assertEqual(tuple(val), tuple(expectedVal))
                    else:
                        self.assertEqual(val, expectedVal)
            except Exception:
                print "Failed on instName=%s, gcViewName=%s" % (instName, gcViewName)
                raise

    def testBadNames(self):
        """Test that loading an instrument with an invalid or ambiguous name fails
        """
        self.assertRaises(Exception, tcc.base.loadInst, instDir=instDir, instName="noSuchInst") # no such instrument
        self.assertRaises(Exception, tcc.base.loadInst, instDir=instDir, instName="ambiguous") # same inst name used for two instrument positions
        self.assertRaises(Exception, tcc.base.loadInst, instDir=instDir, instName="inst") # inst name too short
        self.assertRaises(Exception, tcc.base.loadInst, instDir=instDir, instName="inst1", gcViewName="noSuchView") # no such view
        self.assertRaises(Exception, tcc.base.loadInst, instDir=instDir, instName="inst1", gcViewName="view") # view name too short


if __name__ == '__main__':
    unittest.main()
