#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest

import tcc.base
from tcc.actor import TCCCmdParser
from tcc.parse import getCoordSys

class GetCoordSysTest(unittest.TestCase):
    def setUp(self):
        self.parser = TCCCmdParser()

    def testTrack(self):
        """Test the track command
        """
        self.basicTest("track 10, 12", paramName="coordsys")

    def testConvert(self):
        """Test the convert command, from and to coordinate systems
        """
        self.basicTest("convert 10,12", paramName="fromsys")
        self.basicTest("convert 10,12 icrs", paramName="tosys")
    
    def basicTest(self, cmdPrefix, paramName):
        cmdVerb = cmdPrefix.split()[0]
        isTrack = cmdVerb.lower().startswith("tr")
        currTAI = tcc.base.tai()
        obj = tcc.base.Obj()
        csysNameDateList = (
            # command, (desired coordSys.getName(), desired coordSys.getDate(), desired wasDateSpecified)
            # (which is True if the coordSys has a default date, such as FK5 and GImage)
            # or a date was specified on the command line
            ("FK5", ("fk5", 2000)),
            ("fk5", ("fk5", 2000)),
            ("FK5=2000", ("fk5", 2000)),
            ("FK5=2020.2", ("fk5", 2020.2)),
            ("FK4", ("fk4", 1950)),
            ("FK4=1950", ("fk4", 1950)),
            ("FK4=1975.5", ("fk4", 1975.5)),
            ("IC", ("icrs", 0)),
            ("ICRS=2015.1", ("icrs", 2015.1)),
            ("Galactic", ("gal", 0)),
            ("Gal=2015.1", ("gal", 2015.1)),
            ("Geo", ("appgeo", 0)),
            ("Geocentric=1982.3", ("appgeo", 1982.3)),
            ("Obs", ("obs", 0)),
            ("Observed=%r" % (currTAI,), ("obs", currTAI)),
            ("Observed=%r" % (currTAI + 30075,), ("obs", currTAI + 30075)),
            ("Topo", ("apptopo", 0)),
            ("Topocentric=%r" % (currTAI - 543.21,), ("apptopo", currTAI - 543.21)),
        )
        if isTrack:
            csysNameDateList += (
                ("mount", ("mount", 0)),
                ("none", ("none", 0)),
            )
        else:
            csysNameDateList += (
                ("Instrument", ("instrument", 0)),
                ("Rotator", ("rotator", 0)),
                ("GImage", ("gimage", 1)),
                ("GImage=4", ("gimage", 4)),
                ("GProbe", ("gprobe", 1)),
                ("GProbe=5", ("gprobe", 5)),
                ("gproBE=10", ("gprobe", 10)),
                ("ptCorr", ("ptcorr", 0)),
            )
        for coordSysStr, desNameDate in csysNameDateList:
            cmdStr = cmdPrefix + " " + coordSysStr
            try:
                parsedCmd = self.parser.parseLine(cmdStr)
                coordSys = getCoordSys(parsedCmd.paramDict[paramName])
                desName, desDate = desNameDate
                self.assertEqual(coordSys.getName(), desName)
                # make sure the returned entity is a pointer-to-CoordSys;
                # the following will raise an assertion error if not:
                obj.userSys = coordSys

                # keep the number of significant digits sane (though it doesn't actually seem to matter)
                ndig = 4 if coordSys.getDate() > 10000 else 7
                self.assertAlmostEqual(coordSys.getDate(), desDate, ndig)
            except Exception:
                print "failed on cmdStr=%r" % (cmdStr,)
                raise

        # try ambiguous names and invalid names
        for coordSysStr in (
            # names that are so short as to be ambiguous
            "F",
            "FK",
            "G",
            # invalid names
            "FK6",
            "fka",
            "AppTopo",
            "AppGeo",
            "icrsa",
            "icry",
            "fk55",
            "fk44",
            "observeda",
            "obserx",
            "topocentricb",
            "topi"            
            "Galabctic",
            "geom",
        ):
            for dateStr in ("", "=2000"):
                cmdStr = cmdPrefix + " " + coordSysStr + dateStr
                try:
                    self.assertRaises(
                        Exception,
                        self.parser.parseLine,
                        cmdStr,
                    )                
                except Exception:
                    print "exception not raised for cmdStr=%r" % (cmdStr,)
                    raise

        # try the wrong number of values where the parser catches it
        if isTrack:
            wrongNumValList = (
                "mount=10",
                "none=5",
            )
        else:
            wrongNumValList = (
                "inst=6",
                "rot=7",
                "gimage=(1, 2)",
                "gprobe=(1, 2)",
            )
        for coordSysStr in wrongNumValList:
            cmdStr = cmdPrefix + " " + coordSysStr
            self.assertRaises(
                Exception,
                self.parser.parseLine,
                cmdStr,
            )

if __name__ == '__main__':
    unittest.main()
