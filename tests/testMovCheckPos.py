#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest

import numpy

import tcc.base
from tcc.mov import checkPos

class TestMovCheckPos(unittest.TestCase):
    """Test tcc.mov.checkPos
    """
    def testBasics(self):
        nan = float("nan")
        dataList = ( # pA, vA, pB, vB, dt, doPos, doVel, doAccel, doJerk, pMin, pMax, vLim, aLim, jLim, predErrCode
            (60, 30, 390, 0, 0, 360, tcc.base.AxisErr_OK),
            (60, 30, 30, 30, 0, 30, tcc.base.AxisErr_OK),   #  (minimal range)
            (60, 30, 30, 30, 1, 30, tcc.base.AxisErr_OK),   #  (minimal range)
            (60, 30, 30, 30, 2, 30, tcc.base.AxisErr_OK),   #  (minimal range)
            (60, 30, 30, 30, 3, 30, tcc.base.AxisErr_OK),   #  (minimal range)
            (-10, -1000, 2000, -190.01, 0, 169.99, tcc.base.AxisErr_OK),   #   near range = [-190, 170)
            (-10, -1000, 2000, -190, 0, -190, tcc.base.AxisErr_OK),
            (-10, -1000, 2000, 169.99, 0, 169.99, tcc.base.AxisErr_OK),
            (-10, -1000, 2000, 170, 0, -190, tcc.base.AxisErr_OK),
            (-10, -1000, 2000, 1000, 0, -80, tcc.base.AxisErr_OK),   #  = 1000, - (3 * 360)
            (60, 30, 750, 29.999, 0, 389.999, tcc.base.AxisErr_OK),   #   near range = [30, 390)
            (60, 30, 750, 30, 0, 30, tcc.base.AxisErr_OK),
            (60, 30, 750, 389.99, 0, 389.99, tcc.base.AxisErr_OK),
            (60, 30, 750, 390, 0, 30, tcc.base.AxisErr_OK),
            (60, 30, 750, -1000, 0, 80, tcc.base.AxisErr_OK),   #  = -1000 + (3 * 360)
            (60, 30, 750, 29.999, 1, 389.999, tcc.base.AxisErr_OK),   #   neg. range = [30, 390)
            (60, 30, 750, 30, 1, 30, tcc.base.AxisErr_OK),
            (60, 30, 750, 389.999, 1, 389.999, tcc.base.AxisErr_OK),
            (60, 30, 750, 390, 1, 30, tcc.base.AxisErr_OK),
            (60, 30, 750, 749.999, 1, 389.999, tcc.base.AxisErr_OK),   #  = 749.999, - 360
            (60, 30, 750, 750, 1, 30, tcc.base.AxisErr_OK),
            (60, 80, 750, 79.999, 1, 439.999, tcc.base.AxisErr_OK),   #   neg. range = [80, 440)
            (60, 80, 750, 80, 1, 80, tcc.base.AxisErr_OK),
            (60, 80, 750, 439.999, 1, 439.999, tcc.base.AxisErr_OK),
            (60, 80, 750, 440, 1, 80, tcc.base.AxisErr_OK),
            (60, 30, 750, 209.999, 2, 569.999, tcc.base.AxisErr_OK),   #   mid. range = [210, 570)
            (60, 30, 750, 210, 2, 210, tcc.base.AxisErr_OK),
            (60, 30, 750, 569.999, 2, 569.999, tcc.base.AxisErr_OK),
            (60, 30, 750, 570, 2, 210, tcc.base.AxisErr_OK),
            (60, 30, 750, -150, 2, 210, tcc.base.AxisErr_OK),   #  = -150 + 260
            (60, 30, 750, -150.01, 2, 569.99, tcc.base.AxisErr_OK),
            (60, 80, 310, 310, 2, 310, tcc.base.AxisErr_OK),   #   mid. range = [80, 310] (310 is included!)
            (60, 80, 310, 80, 2, 80, tcc.base.AxisErr_OK),
            (60, 30, 750, 389.999, 3, 749.999, tcc.base.AxisErr_OK),   #   pos. range = [390,750)
            (60, 30, 750, 390, 3, 390, tcc.base.AxisErr_OK),
            (60, 30, 750, 749.999, 3, 749.999, tcc.base.AxisErr_OK),
            (60, 30, 750, 750, 3, 390, tcc.base.AxisErr_OK),
            (60, 30, 750, -1770, 3, 390, tcc.base.AxisErr_OK),   #  = -1770 + (6 * 360)
            (60, 30, 750, -1770.1, 3, 749.9, tcc.base.AxisErr_OK),
            (60, -1000, 50, -475.1, 3, -115.1, tcc.base.AxisErr_OK),   #   pos. range = [-475,-115)
            (60, -1000, 50, -475, 3, -475, tcc.base.AxisErr_OK),
            (60, -1000, 50, -115.1, 3, -115.1, tcc.base.AxisErr_OK),
            (60, -1000, 50, -115, 3, -475, tcc.base.AxisErr_OK),
            (60, 30, 30, 29.999, 0, nan, tcc.base.AxisErr_MinPos),
            (60, 30, 30, 29.999, 1, nan, tcc.base.AxisErr_MinPos),
            (60, 30, 30, 29.999, 2, nan, tcc.base.AxisErr_MinPos),
            (60, 30, 30, 29.999, 3, nan, tcc.base.AxisErr_MinPos),
            (60, 30, 30, 30.001, 0, nan, tcc.base.AxisErr_MaxPos),
            (60, 30, 30, 30.001, 1, nan, tcc.base.AxisErr_MaxPos),
            (60, 30, 30, 30.001, 2, nan, tcc.base.AxisErr_MaxPos),
            (60, 30, 30, 30.001, 3, nan, tcc.base.AxisErr_MaxPos),
            (60, 80, 310, 310.001, 2, nan, tcc.base.AxisErr_MaxPos),
            (60, 80, 310, 79.999, 2, nan, tcc.base.AxisErr_MinPos),
            (60, 30, 390, 0, -1, nan, tcc.base.AxisErr_MinPos),
            (60, 30, 30, 0, -1, nan, tcc.base.AxisErr_MinPos),
        )
        for nearPos, minPos, maxPos, pos, wrapPref, predOutPos, predErrCode in dataList:
            outPos, errCode = checkPos(#pos, minPos, maxPos, nearPos, wrapPref, doWrap
                pos=pos,
                minPos=minPos,
                maxPos=maxPos,
                nearPos=nearPos,
                wrapPref=wrapPref,
                doWrap=True,
            )
            try:
                self.assertEqual(errCode, predErrCode)
                self.assertEqual(numpy.isfinite(outPos), numpy.isfinite(predOutPos))
                if numpy.isfinite(outPos):
                    self.assertAlmostEqual(outPos, predOutPos)
            except Exception:
                print "failed on nearPos=%s, minPos=%s, maxPos=%s, pos=%s, wrapPref=%s, predOutPos=%s, predErrCode=%s" \
                    % (nearPos, minPos, maxPos, pos, wrapPref, predOutPos, predErrCode)
                raise
            
    def testErrors(self):
        dataList = (
            (60, 30, 390, 0, -2),
        )
        for nearPos, minPos, maxPos, pos, wrapPref in dataList:
            self.assertRaises(RuntimeError, checkPos,
                pos=pos,
                minPos=minPos,
                maxPos=maxPos,
                nearPos=nearPos,
                wrapPref=wrapPref,
                doWrap=True,
            )


if __name__ == '__main__':
    unittest.main()
