#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Test functionality of offset command
"""
import itertools
import unittest

from coordConv import PVT
from twistedActor import CommandError
from RO.SeqUtil import asSequence

from tcc.actor import TCCCmdParser
import tcc.base
from tcc.cmd.offset import setObjOffset # replaceOffsetOnBlock

DefTime = 0

class TestOffset(unittest.TestCase):
    def setUp(self):
        self.parser = TCCCmdParser()
        self.obj = tcc.base.Obj()
        self.rotExists = True
        self.attrName = None

    def basicTest(self, cmdStr, tai, fieldName, desPVTList, desBools, expectedFail=False):
        """Run one test

        @param[in] cmdStr  command string
        @param[in] tai  default TAI for offset
        @param[in] fieldName  name of obj field that will be updated
        @param[in] desPVTList  expected new values of the appropriate PVT fields
        @param[in] desBools  desired isNewObj, isNewRot
        """
        try:
            parsedCmd = self.parser.parseLine(cmdStr)
            isNewObj, isNewRot = setObjOffset(obj=self.obj, parsedCmd=parsedCmd, rotExists=self.rotExists, tai=tai)
            self.assertEqual(list((isNewObj, isNewRot)), list(desBools))

            pvtList = asSequence(getattr(self.obj, fieldName))

            self.assertEqual(len(pvtList), len(desPVTList))
            for pvt, desPVT in itertools.izip(pvtList, desPVTList):
                self.assertAlmostEqual(pvt.pos, desPVT.pos)
                self.assertAlmostEqual(pvt.vel, desPVT.vel)
                self.assertAlmostEqual(pvt.t, desPVT.t)
        except Exception:
            if not expectedFail:
                print "basicTest(cmdStr=%r, tai=%s, fieldName=%r, desPVTList=%s, desBools=%s) failed" % \
                    (cmdStr, tai, fieldName, desPVTList, desBools)
            raise

    def testArc(self):
        """Test offset arc
        """
        tai = 10
        fieldName = "userArcOff"
        desBools = (True, False)
        for cmdStr, desPVTList in (
            ("offset arc 1, 2, 3, 4", [PVT(1, 3, tai), PVT(2, 4, tai)]),
            ("offset arc 0, 0, 1.1, 1.2", [PVT(1, 1.1, tai), PVT(2, 1.2, tai)]),
            ("offset arc 0.9, -1.8, -1.1, -1.2", [PVT(1.9, -1.1, tai), PVT(0.2, -1.2, tai)]),
            ("offset arc/pabs -1, 0, 0.2, -0.5", [PVT(-1, 0.2, tai), PVT(0, -0.5, tai)]),
            ("offset arc/vinc 0.1, -0.2, 0.2, 0.4", [PVT(-0.9, 0.4, tai), PVT(-0.2, -0.1, tai)]),
            ("offset arc", [PVT(-0.9, 0, tai), PVT(-0.2, 0, tai)]),
        ):
            self.basicTest(cmdStr=cmdStr, tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools)

    def testGuide(self):
        """Test guide correction
        """
        tai = 10
        fieldName = "guideOff"
        desBools = (True, True)
        for cmdStr, desPVTList in (
            ("offset guide 1, 2, 3, 4, 5, 6", [PVT(1,4,tai), PVT(2, 5, tai), PVT(3, 6, tai)]),
            ("offset guide 0, 0, 0, 1.1, 2.2, 3.3", [PVT(1, 1.1,tai), PVT(2, 2.2, tai), PVT(3, 3.3, tai)]),
            ("offset guide -1.1, 1.1, 2.1", [PVT(-0.1, 0, tai), PVT(3.1, 0, tai), PVT(5.1, 0, tai)]),
        ):
            self.basicTest(cmdStr=cmdStr, tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools)
        try:
            # too many values
            self.basicTest(cmdStr="offset guide 1, 2, 3, 4, 5, 6, 7, 8", tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools, expectedFail=True)
        except CommandError:
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def testBoresight(self):
        """Test instrument offset
        """
        tai = 10
        fieldName = "objInstXY"
        desBools = (True, False)
        cmdStr = "offset bore 1,2,3,4" 
        desPVTList = [PVT(1, 3, tai), PVT(2, 4, tai)]
        self.basicTest(cmdStr=cmdStr, tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools)
        try:
            # too many values
            self.basicTest(cmdStr="offset bore 1, 2, 3, 4, 5, 6", tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools, expectedFail=True)
        except CommandError:
            self.assertTrue(True)
        else:
            self.assertTrue(False)

    def testCalib(self):
        """test calibaration offset
        """
        tai = 3
        fieldName = "calibOff"
        desBools = (True, True)
        cmdStr = "offset calib 2.4, 6.2, 3.4, 5.5, 4.2, 1.6" 
        desPVTList = [PVT(2.4, 5.5, tai), PVT(6.2, 4.2, tai), PVT(3.4, 1.6, tai)]
        self.basicTest(cmdStr=cmdStr, tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools)
        try:
            # too many values
            self.basicTest(cmdStr="offset calib 1, 2, 3, 4, 5, 6, 7, 8", tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools, expectedFail=True)
        except CommandError:
            self.assertTrue(True)
        else:
            self.assertTrue(False)       

    def testRot(self):
        """test rotator offset
        """
        tai = 3
        self.obj.rotUser = PVT(0,0,0) # is initialized to Nans
        fieldName = "rotUser"
        desBools = (False, True)
        cmdStr = "offset rot 2.4, 6.2" 
        desPVTList = [PVT(2.4, 6.2, tai)]
        self.basicTest(cmdStr=cmdStr, tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools)
        try:
            # too many values
            self.basicTest(cmdStr="offset rot 1, 2, 3, 4", tai=tai, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools, expectedFail=True)
        except CommandError:
            self.assertTrue(True)
        else:
            self.assertTrue(False)   

    def testTime(self):
        """Test offset behavior with time not neglected. Mostly wanna know if positions
        are propagating correctly
        """
        fieldName = "userArcOff"
        desBools = (True, False)
        pvt1, pvt2 = PVT(1,2,0), PVT(1,0,0)
        newPos1 = 1 + pvt1.getPos(1)
        newPos2 = 1 + pvt2.getPos(1)
        for cmdStr, desPVTList in (
            ("offset arc 1,1,2,0,0", [pvt1, pvt2]),
            ("offset arc 1, 1, 2, 0, 1", [PVT(newPos1, 2, 1), PVT(newPos2, 0, 1)]),
        ):
            self.basicTest(cmdStr=cmdStr, tai=desPVTList[0].t, fieldName=fieldName, desPVTList=desPVTList, desBools=desBools)    


if __name__ == "__main__":
    unittest.main()
