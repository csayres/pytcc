#!/usr/bin/env python2
from __future__ import division, absolute_import
"""testOffset.py doesn't actually start up any communication, this version does, perhaps testOffset.py
is no longer needed?
"""
import functools
import itertools

from twisted.trial.unittest import TestCase
from twisted.internet.defer import gatherResults

from tcc.base import testUtils
from tcc.base.offsetTestUtils import Tests
from tcc.cmd.offset import OffsetNumAxesAttrDict

testUtils.init(__file__)

def formatOffsetStr(offPosVelList):
    """Format a set of offsets

    @param[in] offPosVelList  a list of pairs of (offPos, offVel), one per axis
    @return a string comma-separated values in this order: offPos0, offPos1, ..., offVel0, offVel1,...
    """
    posList, velList = zip(*offPosVelList)
    return ", ".join(["%0.7f" % (val,) for val in posList + velList])

class TestOffsetFromHalt(Tests, TestCase):

    def _checkOffsetResult(self, foo, objAttr, offPosVelList):
        """Check the result of an offset

        @param[in] foo  dummyArg for cmd (passed via callback)
        @param[in] objAttr  name of obj attribute that contains the expected offset
        @param[in] offPosVelList  list of (offPos, offVel), one per axis
        """
        pvtList = getattr(self.actor.obj, objAttr)
        if len(offPosVelList) == 1:
            pvtList = [pvtList]
        self.assertEqual(len(offPosVelList), len(pvtList))
        for offPosVel, pvt in itertools.izip(offPosVelList, pvtList):
            self.assertAlmostEqual(offPosVel[0], pvt.pos)
            self.assertAlmostEqual(offPosVel[1], pvt.vel)

    def checkOffset(self, offsetName, offPos=-1.123, offVel=-0.763, isComputed=False):
        """Check one offset
        """
        nAxes, objAttr = OffsetNumAxesAttrDict[offsetName.lower()]
        offPosVelList = [(offPos + i, offVel + i) for i in range(nAxes)]
        offStr = formatOffsetStr(offPosVelList)
        cmdStr = "offset %s %s" % (offsetName, offStr)
        if isComputed:
            cmdStr += "/computed"
        d, cmd = self.dw.queueCmd(cmdStr)
        allHalted = ["Halted"]*3
        if isComputed:
            cmd.addCallback(functools.partial(self.checkAxisCmdState, axisCmdState = allHalted), callCodes=">")
        d.addCallback(functools.partial(self.checkAxisCmdState, axisCmdState = allHalted))
        d.addCallback(functools.partial(self._checkOffsetResult, objAttr=objAttr, offPosVelList=offPosVelList))
        return d

    def testArc(self):
        return self.checkOffset("arc")

    def testArcComp(self):
        return self.checkOffset("arc", isComputed=True)

    def testBore(self):
        return self.checkOffset("boresight")

    def testInstPlane(self):
        return self.checkOffset("instPlane")

    def testGuideCorrection(self):
        return self.checkOffset("guideCorrection")

    def testCalibration(self):
        return self.checkOffset("calibration")

    def testRot(self):
        # start and stop rotation to put object block in a good state
        trackCmd = "track 12,12 observed /rottype=object /rotangle=0"
        d1, cmd1 = self.dw.queueCmd(trackCmd)
        trackStop = "track /stop"
        d2, cmd2 = self.dw.queueCmd(trackStop)
        return self.checkOffset("rotator")


class TestOffsetFromTrack(TestOffsetFromHalt):
    def checkOffset(self, offsetName, offPos=1.123, offVel=0.763, isComputed=False):
        # start things tracking/rotating
        trackCmd = "track 12,12 observed /rottype=object /rotangle=0"
        d1, cmd1 = self.dw.queueCmd(trackCmd)
        allTracking = ["Tracking"]*3
        d1.addCallback(self.checkCmdFail)
        cmd1.addCallback(functools.partial(self.checkAxisCmdState, axisCmdState = allTracking))

        # generate the command put it on the queue
        nAxes, objAttr = OffsetNumAxesAttrDict[offsetName.lower()]
        offPosVelList = [(offPos + i, offVel + i) for i in range(nAxes)]
        offStr = formatOffsetStr(offPosVelList)
        cmdStr = "offset %s %s" % (offsetName, offStr)
        if isComputed:
            cmdStr += "/computed"
        d2, cmd2 = self.dw.queueCmd(cmdStr)
        if isComputed:
            allSlewing = ["Slewing"]*3
            cmd2.addCallback(functools.partial(self.checkAxisCmdState, axisCmdState = allSlewing, checkActor = False), callCodes=">")
        cmd2.addCallback(functools.partial(self.checkAxisCmdState, axisCmdState = allTracking))
        d2.addCallback(functools.partial(self._checkOffsetResult, objAttr=objAttr, offPosVelList=offPosVelList))
        return gatherResults([d1, d2])

    def testRot(self):
        return self.checkOffset("rotator")


if __name__ == '__main__':
    from unittest import main
    main()
