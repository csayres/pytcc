#!/usr/bin/env python2
from __future__ import division, absolute_import

import itertools
import unittest

import numpy.random
from twistedActor import CommandError

import tcc.base
from tcc.actor import TCCCmdParser
from tcc.parse import getPVTList, ParseError
from coordConv import PVT

class GetPVTListTest(unittest.TestCase):
    def setUp(self):
        self.parser = TCCCmdParser()

    def goodTest(self, numAxes, posList, velList, tai):
        """Perform a test that should pass

        @param[in] numAxes  number of axes`
        @param[in] posList  list of numAxes positions (floats), or None to default
        @param[in] posList  list of numAxes velocities (floats), or None to default
        @param[in] tai  tai date, or None to default
        """
        valStrList = []
        if posList is None:
            posList = [0.0]*numAxes
        else:
            valStrList += ["%0.9f" % pos for pos in posList]
            if numAxes > 2 and len(posList) == 2:
                posList = list(posList) + [0]*(numAxes-2)

        if velList is None:
            velList = [0.0]*numAxes
        elif valStrList: # pos values provided
            valStrList += ["%0.9f" % vel for vel in velList]

        if tai is None:
            tai = tcc.base.tai()
        elif valStrList: # pos and vel values provided
            valStrList.append("%0.9f" % (tai,))

        desPVTList = [PVT(pos, vel, tai) for pos, vel in itertools.izip(posList, velList)]
        assert len(desPVTList) == numAxes # failure indicates a bug in this test

        if valStrList:
            cmdStr = "offset calib %s" % (", ".join(valStrList),)
        else: # don't provide a coordsys if no coord info provided
            cmdStr = "offset calib"
        parsedCmd = self.parser.parseLine(cmdStr)
        pvtParam = parsedCmd.paramDict["coordset"]
        pvtList, numValues = getPVTList(pvtParam, numAxes=numAxes, defTAI=tai)
        self.assertEqual(len(pvtList), numAxes)
        self.assertEqual(numValues, len(valStrList))
        for pvt, desPVT in itertools.izip(pvtList, desPVTList):
            self.assertAlmostEquals(pvt.pos, desPVT.pos)
            self.assertAlmostEquals(pvt.vel, desPVT.vel)
            self.assertAlmostEquals(pvt.t, desPVT.t)
    
    def testGetPVTList(self):
        """Test valid values for getPVTList
        """
        currTAI = tcc.base.tai()
        for numAxes in (1, 2, 3):
            # test default everything
            self.goodTest(numAxes=numAxes, posList=None, velList=None, tai=None)

            for posInd in range(3):
                if numAxes > 2:
                    # try just two positions
                    posList = numpy.random.uniform(-90, 90, 2)
                    self.goodTest(numAxes=numAxes, posList=posList, velList=None, tai=None)

                posList = numpy.random.uniform(-90, 90, numAxes)
                self.goodTest(numAxes=numAxes, posList=posList, velList=None, tai=None)

                for velInd in range(3):
                    velList = numpy.random.uniform(-9, 9, numAxes)
                    self.goodTest(numAxes=numAxes, posList=posList, velList=velList, tai=None)

                    for tai in (currTAI - 1000, 5, 999.1):
                        self.goodTest(numAxes=numAxes, posList=posList, velList=velList, tai=tai)

    def testBadCommands(self):
        """Test command strings that should fail to parse
        """
        for numAxes in (1, 2, 3):
            validValueSet = set((0, numAxes, 2*numAxes, 1 + (2*numAxes)))
            if numAxes > 2:
                validValueSet.add(2)
            for numValues in range(1, 4 + (2*numAxes)):
                valList = list(numpy.random.uniform(-90, 90, numValues))
                if numValues in validValueSet:
                    # test an invalid value (non-float)
                    for i in range(len(valList)):
                        goodVal = valList[i]
                        try:
                            valList[i] = "a"
                            cmdStr = "offset calib %s" % (", ".join("%s" % (val,) for val in valList),)
                            self.assertRaises(ParseError, self.parser.parseLine, cmdStr)
                        finally:
                            valList[i] = goodVal
                else:
                    # test incorrect number of floats
                    cmdStr = "offset calib %s" % (", ".join("%0.3f" % (val,) for val in valList),)
                    parsedCmd = self.parser.parseLine(cmdStr)
                    pvtParam = parsedCmd.paramDict["coordset"]
                    self.assertRaises(CommandError, getPVTList, pvtParam, numAxes=numAxes, defTAI=999)


if __name__ == '__main__':
    unittest.main()
