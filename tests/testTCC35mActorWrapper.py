#!/usr/bin/env python2
from __future__ import division, absolute_import

from twisted.trial.unittest import TestCase

from tcc.actor import TCC35mActorWrapper
import tcc.base.testUtils
tcc.base.testUtils.init(__file__)

class TestTCC35mActorCtrlWrapper(TestCase):
    """Test basics of TCC35mActorCtrlWrapper
    """
    def setUp(self):
        self.aw = TCC35mActorWrapper()
        return self.aw.readyDeferred
    
    def tearDown(self):
        return self.aw.close()
    
    def testSetUpTearDown(self):
        self.assertFalse(self.aw.didFail)
        self.assertFalse(self.aw.isDone)
        self.assertTrue(self.aw.isReady)


if __name__ == '__main__':
    from unittest import main
    main()
