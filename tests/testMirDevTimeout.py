#!/usr/bin/env python2
from __future__ import division, absolute_import

import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from twisted.trial.unittest import TestCase
from twisted.internet.defer import Deferred

from tcc.base import testUtils
testUtils.init(__file__)
from mirrorCtrl.mirrors import mir35mSec
from tcc.mir import MirrorDeviceWrapper

# disable extra move time so setting fastTimeout in the fake Galil actually allows a move command to time out
from mirrorCtrl.galilDevice import GalilDevice
GalilDevice.MoveExtraTime = 0

class TestMirDevTimeout(TestCase):
    """ Test that timeouts are propagated from galil time estimates to mirror device move
    device commands.
    """
    def setUp(self):
        self.dw = MirrorDeviceWrapper(
            name="sec",
            mirror=mir35mSec,
        )
        return self.dw.readyDeferred

    def tearDown(self):
        d = self.dw.close()
        return d

    def testTimeout(self):
        """command a big move, expect a timeout
        """
        self.dw.controllerWrapper.deviceWrapperList[0].server.fastTimeout = True
        d = Deferred()
        def cb(cmdVar):
            try:
                self.assertTrue(cmdVar.didFail)
                self.assertTrue("time" in cmdVar.textMsg.lower())
                d.callback(None)
            except Exception as e:
                d.errback(e)
        devCmd = self.dw.device.startCmd("move 2000, 0, 0")
        devCmd.addCallback(cb)
        return d


if __name__ == '__main__':
    from unittest import main
    main()