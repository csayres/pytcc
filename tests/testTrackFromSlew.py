#!/usr/bin/env python2
from __future__ import division, absolute_import

import functools

from twisted.trial.unittest import TestCase
from twisted.internet.defer import gatherResults, Deferred
from twisted.internet import reactor

from twistedActor import CmdWrapper
from opscore.actor import CmdVar

from tcc.base import testUtils
testUtils.init(__file__)
from tcc.base.trackTestUtils import TrackCommonMethods, LessTests, AllTests

# Run every test on all three flavors of tracking (from halt, from tracking, from slewing)
# else run all on from halt, and a subset on from tracking/slewing
runAllTests = False


class TestTrackFromSlew(TrackCommonMethods, TestCase, AllTests if runAllTests else LessTests):
    """Override checkTrackCmd method to set the telescope slewing (and superseding previous track command) before tests are run
    """
    def checkTrackCmd(self, trackCmd, shouldRotate=False, shouldStop=False):
        """Start next track command from axes halted state.
        """
        outerD = Deferred()

        beg = ["Slewing"]*3
        mid = ["Slewing"]*3
        end = ["Tracking"]*3
        # start a nice long slew so there's plenty of time to supersede it
        maxAz= self.actor.axeLim.maxPos[1]
        startTrackCmd = "track 15, %s mount" % (maxAz - 5)
        if shouldRotate:
            startTrackCmd += "/rottype=mount/rotangle=20"
        else:
            beg[2] = "Halted"
            mid[2] = "Halted"
            end[2] = "Halted"

        cmd2 = CmdVar(
                actor = self.dw._dictName,
                cmdStr = trackCmd,
                # callFunc = self.checkCmdFail
            )

        def checkSlew(cmdVar, mid, end):
            if cmdVar.lastCode == ">":
                self.checkAxisCmdState(cmdVar, axisCmdState = mid, checkActor = False)
            elif cmdVar.isDone:
                self.checkAxisCmdState(cmdVar, axisCmdState = end)
                outerD.callback("All Done!")
        if shouldStop:
            # note, no rotation
            assert(not shouldRotate)
            wrappedCmdVar = CmdWrapper(
                cmd2,
                callFunc=functools.partial(checkSlew, mid = ["Halting"]*2+["Halted"], end = ["Halted"]*3),
                callCodes=">:",
            )
        else:
            wrappedCmdVar = CmdWrapper(
                cmd2,
                callFunc=functools.partial(checkSlew, mid = mid, end = end),
                callCodes=">:",
            )

        def startThisDuringSlew(foo=None):
            self.checkAxisCmdState(cmdVar=None, axisCmdState = beg) # send track command when telescope is slewing
            wrappedCmdVar.startCmd(self.dispatcher)

        def slewCmdCallback(cmdVar):
            if cmdVar.lastCode == ">":
                timeForSlew = self.dispatcher.model.slewDuration.getValue()
                # print 'slew time: ', timeForSlew
                reactor.callLater(timeForSlew/2., startThisDuringSlew)
            elif cmdVar.isDone:
                # check that this slew was superseded
                if not cmdVar.lastReply.keywords[0].values \
                    or cmdVar.lastReply.keywords[0].values[0] != "SlewSuperseded":
                        self.fail("Slew not superseded")

        d1, cmd1 = self.dw.queueCmd(
            startTrackCmd,
            callFunc = slewCmdCallback,
            callCodes = ">:"
        )
        return gatherResults([d1, wrappedCmdVar.deferred, outerD])

    def testTrackStop(self):
        """Start slewing then stop it, overwritten to allow for the fact that the original track command is fired on a timer
        """
        stopCmd = "track /stop"
        d = self.checkTrackCmd(stopCmd, shouldStop=True)
        return d

    def testTrackOffset(self):
        # doesn't work correctly with axes moving
        pass



if __name__ == '__main__':
    from unittest import main
    main()