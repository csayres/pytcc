#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Test the Convert command against tcc.axis.coordConvWithFP

This tests how the TCC parses the Convert command arguments and passes them to tcc.axis.coordConvWithFP.
The function tcc.axis.coordConvWithFP is tested by tests/testCoordConvWithFPAgainstTCC.
"""
import functools

from twisted.internet.defer import Deferred #, gatherResults
from twisted.trial.unittest import TestCase
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")

import coordConv

import tcc.axis
import tcc.base.testUtils
tcc.base.testUtils.init(__file__)
import tcc.base
from tcc.parse import makeCoordSys
from tcc.base.wrapperTestCase import WrapperTestCase

NullPxPMRadVel = tcc.base.PxPMRadVel()

InstDir =  tcc.base.getInstDir()

class TestConvertCmd(WrapperTestCase, TestCase):
    """Test the Convert command

    This merely checks the convert command against the internal code that the convert command runs,
    so it is primarily a check of parsing and default velocity and date handling. Handling all
    the subtleties involved turned out to be quite a challenge.
    """
    def setUp(self):
        self.deferred = Deferred()
        return WrapperTestCase.setUp(self)

    def setObj(self):
        currTAI = tcc.base.tai()
        self.actor.inst = tcc.base.loadInst(
            instDir = InstDir,
            instName = "dis",
        )
        self.actor.axeLim.setRotLim(self.actor.inst)
        self.actor.obj.userSys = makeCoordSys("galactic", 2000)
        self.actor.obj.rotType = tcc.base.RotType_Object
        self.actor.obj.rotUser = coordConv.PVT(0, 0, currTAI)
        self.actor.obj.userPos[0] = coordConv.PVT(20, 0, currTAI)
        self.actor.obj.userPos[1] = coordConv.PVT(70, 0, currTAI)
        for i in range(3):
            self.actor.obj.targetMount[i] = coordConv.PVT(0, 0, currTAI)
            self.actor.obj.actMount[i] = coordConv.PVT(0, 0, currTAI)
            self.actor.obj.axisCmdState[i] = tcc.base.AxisState_Tracking
        tcc.axis.computeObj(
            obj = self.actor.obj,
            doWrap = False,
            doRestart = [True]*tcc.base.NAxes,
            reportLim = False,
            earth = self.actor.earth,
            inst = self.actor.inst,
            telMod = self.actor.telMod,
            axeLim = self.actor.axeLim,
            tai = currTAI,
        )

    def tearDown(self):
        del self.deferred
        return WrapperTestCase.tearDown(self)

    @property
    def instBlock(self):
        """Return the instrument block
        """
        return self.dw.actorWrapper.actor.inst

    def testFPToICRS(self):
        """Test focal plane to FK5
        """
        self.setObj()
        def cmdObjIter():
            for fromSysStr, fromCoordSys, fromSysExplicit in fpSysIter(self.actor.inst):
                totSysName = "fk5"
                toSysDate = 1975
                toSysExplicit = True
                toSysStr = "fk5=%0.1f" % (toSysDate,)
                toCoordSys = makeCoordSys(totSysName, toSysDate)
                for pvtStr, fromPVTPair, fromPVTExplicit in pvtIter(doFP=True):
                    cmdStr = "convert %s %s %s" % (pvtStr, fromSysStr, toSysStr)
                    currTAI = tcc.base.tai()
                    yield CmdObject(
                        cmdStr = cmdStr,
                        fromPVTPair = fromPVTPair,
                        fromPVTExplicit = fromPVTExplicit,
                        fromCoordSys = fromCoordSys,
                        fromSysExplicit = fromSysExplicit,
                        toCoordSys = toCoordSys,
                        toSysExplicit = toSysExplicit,
                        tai = currTAI,
                    )
        self.runOneCmd(cmdObjIter())
        return self.deferred

    def testICRSToFP(self):
        """Test galactic to focal plane
        """
        self.setObj()
        def cmdObjIter():
            for toSysStr, toCoordSys, toSysExplicit in fpSysIter(self.actor.inst):
                fromSysName = "galactic"
                fromSysDate = 1963
                fromSysExplicit = True
                fromSysStr = "galactic=%0.1f" % (fromSysDate,)
                fromCoordSys = makeCoordSys(fromSysName, fromSysDate)
                for pvtStr, fromPVTPair, fromPVTExplicit in pvtIter(doFP=True):
                    cmdStr = "convert %s %s %s" % (pvtStr, fromSysStr, toSysStr)
                    currTAI = tcc.base.tai()
                    yield CmdObject(
                        cmdStr = cmdStr,
                        fromPVTPair = fromPVTPair,
                        fromPVTExplicit = fromPVTExplicit,
                        fromCoordSys = fromCoordSys,
                        fromSysExplicit = fromSysExplicit,
                        toCoordSys = toCoordSys,
                        toSysExplicit = toSysExplicit,
                        tai = currTAI,
                    )
        self.runOneCmd(cmdObjIter())
        return self.deferred


    def testFPToFP(self):
        """Test focal plane to focal plane coordinates
        """
        self.setObj()
        def cmdObjIter():
            for fromSysStr, fromCoordSys, fromSysExplicit in fpSysIter(self.actor.inst):
                for toSysStr, toCoordSys, toSysExplicit in fpSysIter(self.actor.inst):
                    for pvtStr, fromPVTPair, fromPVTExplicit in pvtIter(doFP=True):
                        cmdStr = "convert %s %s %s" % (pvtStr, fromSysStr, toSysStr)
                        currTAI = tcc.base.tai()
                        yield CmdObject(
                            cmdStr = cmdStr,
                            fromPVTPair = fromPVTPair,
                            fromPVTExplicit = fromPVTExplicit,
                            fromCoordSys = fromCoordSys,
                            fromSysExplicit = fromSysExplicit,
                            toCoordSys = toCoordSys,
                            toSysExplicit = toSysExplicit,
                            tai = currTAI,
                        )
        self.runOneCmd(cmdObjIter())
        return self.deferred

    def testExplicitVelT(self):
        """Test a few coordinate systems with coords that have explicit velocity and time
        """
        self.setObj()
        def cmdObjIter():
            for fromSysStr, fromCoordSys, fromSysExplicit in smallSysDateIter():
                for toSysStr, toCoordSys, toSysExplicit in smallSysDateIter():
                    for pvtStr, fromPVTPair, fromPVTExplicit in pvtIter(doVel=True):
                        cmdStr = "convert %s %s %s" % (pvtStr, fromSysStr, toSysStr)
                        currTAI = tcc.base.tai()
                        yield CmdObject(
                            cmdStr = cmdStr,
                            fromPVTPair = fromPVTPair,
                            fromPVTExplicit = fromPVTExplicit,
                            fromCoordSys = fromCoordSys,
                            fromSysExplicit = fromSysExplicit,
                            toCoordSys = toCoordSys,
                            toSysExplicit = toSysExplicit,
                            tai = currTAI,
                        )
        self.runOneCmd(cmdObjIter())
        return self.deferred

    def testNonFPToNonFP(self):
        """Test conversion between two non-focal-plane coordinate systems

        Break into two tests:
        - test different csys and dates, all with the same nonzero vel and time
        - test one csys with no time and with no vel
        """
        self.setObj()
        def cmdObjIter():
            for fromSysStr, fromCoordSys, fromSysExplicit in skySysIter():
                if fromCoordSys.isMean():
                    pxPMList = list(pxPMIter())
                else:
                    pxPMList = [("", NullPxPMRadVel)]
                for fromPxPMStr, fromPxPMRadVel in pxPMList:
                    for toSysStr, toCoordSys, toSysExplicit in skySysIter():
                        for pvtStr, fromPVTPair, fromPVTExplicit in pvtIter():
                            cmdStr = "convert %s %s %s%s" % (pvtStr, fromSysStr, toSysStr, fromPxPMStr)
                            currTAI = tcc.base.tai()
                            yield CmdObject(
                                cmdStr = cmdStr,
                                fromPVTPair = fromPVTPair,
                                fromPVTExplicit = fromPVTExplicit,
                                fromCoordSys = fromCoordSys,
                                fromSysExplicit = fromSysExplicit,
                                toCoordSys = toCoordSys,
                                toSysExplicit = toSysExplicit,
                                tai = currTAI,
                                fromPxPMRadVel = fromPxPMRadVel,
                            )
        self.runOneCmd(cmdObjIter())
        return self.deferred

    def runOneCmd(self, cmdObjIter):
        try:
            cmdObj = cmdObjIter.next()
            d, cmd = self.dw.queueCmd(
                cmdStr = cmdObj.cmdStr,
                callFunc = functools.partial(self.assertConvertOK, cmdObjIter, cmdObj),
                callCodes = ":F",
            )
        except StopIteration:
            self.deferred.callback("")
        except Exception as e:
            self.deferred.errback(e)

    def assertConvertOK(self, cmdObjIter, cmdObj, cmdVar):
        if cmdVar.didFail:
            print "**********"
        try:
            model = self.model

            if cmdVar.didFail:
                textMsg = model.text[0]
                self.deferred.errback(RuntimeError(textMsg))
                return

            convPVTPair = model.convPos.valueList
            convTAI = convPVTPair[0].t # time at which conversion occurred
            equatPM = float(model.convPM[0])
            polarPM = float(model.convPM[1])
            parallax = float(model.convPM[2])
            radVel = float(model.convPM[3])

            convPxPMRadVel = tcc.base.PxPMRadVel(parallax, equatPM, polarPM, radVel)
            adjCmdObj = cmdObj.copy(convTAI)

            # update obj block so focal-plane math is correct
            obj = tcc.base.Obj(self.actor.obj)
            tcc.axis.computeObj(
                obj = obj,
                doWrap = False,
                doRestart = [True]*tcc.base.NAxes,
                reportLim = False,
                earth = self.actor.earth,
                inst = self.actor.inst,
                telMod = self.actor.telMod,
                axeLim = self.actor.axeLim,
                tai = convTAI,
            )

            toPVTPair, toPxPMRadVel, toDir, scaleChange=tcc.axis.coordConvWithFP(
                fromCoordSys = adjCmdObj.fromCoordSys,
                fromPVTPair = adjCmdObj.fromPVTPair,
                fromPxPMRadVel = adjCmdObj.fromPxPMRadVel,
                fromDir = coordConv.PVT(0, 0, convTAI),
                toCoordSys = adjCmdObj.toCoordSys,
                obj = obj,
                inst = self.actor.inst,
                zeroPM = False,
                useGSWavelen = False,
            )

            for i in range(2):
                self.assertAlmostEqual(convPVTPair[i].vel, toPVTPair[i].vel, places=5)
                deltaPos = coordConv.wrapCtr(convPVTPair[i].pos - toPVTPair[i].pos)
                self.assertAlmostEqual(deltaPos, 0, places=5)

            # the TCC reports parallax and proper motion to 4 digits, so testing further will fail
            self.assertAlmostEqual(convPxPMRadVel.parallax, toPxPMRadVel.parallax, places=4)
            self.assertAlmostEqual(convPxPMRadVel.equatPM, toPxPMRadVel.equatPM, places=4)
            self.assertAlmostEqual(convPxPMRadVel.polarPM, toPxPMRadVel.polarPM, places=4)
            self.assertAlmostEqual(convPxPMRadVel.radVel, toPxPMRadVel.radVel, places=4)
        except Exception as e:
            print "Failed on ", cmdObj
            msgStr = "Failed on %r: %s" % (cmdObj.cmdStr, e)
            self.deferred.errback(RuntimeError(msgStr))
            raise
        self.runOneCmd(cmdObjIter)


def skySysIter():
    """Iterator that returns a sky-based coordinate system (as opposed to focal plane)

    @return these values:
    - coord sys  string for convert command (e.g. "ICRS=2000")
    - coordSys: coordConv.CoordSys object
    - dateExplicit: if True, the date is explicit and should not be touched
    """
    for name in ("ICRS", "FK5", "FK4", "Geocentric", "topoCentric", "observed"):
        for date in (None, 1995):
            dateExplicit = date is not None
            retStr = name
            if dateExplicit:
                retStr += "=%0.1f" % (date,)
            yield retStr, makeCoordSys(name, date), dateExplicit

def fpSysIter(inst):
    for name in ("Instrument", "GProbe", "GImage"):
        if name.lower() in ("gprobe", "gimage"):
            dateList = range(1, 1 + len(inst.gProbe))
        else:
            dateList = [None]
        for date in dateList:
            dateExplicit = date is not None
            retStr = name
            if dateExplicit:
                retStr += "=%d" % (date,)
            yield retStr, makeCoordSys(name, date), dateExplicit

def smallSysDateIter():
    """Iterator that returns a few a mean coordinate systems

    Does not return a coordConv.coordSys object because that may go out of date before it's used

    @return these values:
    - coord sys  string for convert command (e.g. "ICRS=2000")
    - coordSys: coordConv.CoordSys object
    - dateExplicit: if True, the date is explicit and should not be touched
    """
    for name in ("ICRS", "Geocentric"):
        for date in (None, 1995):
            dateExplicit = date is not None
            retStr = name
            if dateExplicit:
                retStr += "=%0.1f" % (date,)
            yield retStr, makeCoordSys(name, date), dateExplicit

def pvtIter(doVel=False, doFP=False):
    """Yield position, velocity, time as a string, a tcc.base.ArrayPVT2 and a flag

    @param[in] doVel  if True then return some values with explicit vel and time
    @param[in] doFP  if True then the positions are small focal-plane positions

    @return these values:
    - a string for the Convert command containing pos, maybe vel, maybe t
    - a pair of PVTs as a tcc.base.ArrayPVT2
    - dateExplicit: if True, the date is explicit and should not be touched
    """
    if doFP:
        posPairList = ((0.5, -0.1), (-0.01, 0.2))
    else:
        posPairList = ((-10.0, 80.2), (42.5, -10.0))
    for posPair in posPairList:
        if doVel:
            velPairList = ((-0.0012, 0),) # (None, (-0.0012, 0), (0, 0.0023))
        else:
            velPairList = (None,)
        for velPair in velPairList:
            if velPair is None:
                dtList = (None,)
            else:
                dtList = (None, -53.2)
            for dt in dtList:
                currTAI = tcc.base.tai()
                t = currTAI
                dateExplicit = False
                retStrList = ["%0.6f, %0.6f" % tuple(posPair)]
                if velPair is None:
                    velPair = (0, 0)
                else:
                    retStrList.append("%0.6f, %0.6f" % tuple(velPair))
                    if dt is not None:
                        dateExplicit = True
                        t += dt
                        retStrList.append("%0.7f" % (t,))
                retStr = ", ".join(retStrList)
                pvtPair = tcc.base.ArrayPVT2()
                for i in range(2):
                    pvtPair[i].pos = posPair[i]
                    pvtPair[i].vel = velPair[i]
                    pvtPair[i].t = t
                yield retStr, pvtPair, dateExplicit

def pxPMIter():
    """Return a few nonzero parallaxes and such

    @return:
    - pxStr: string representation of parallax, etc. for convert command
    - PxPMRadVel: the corresponding values as a tcc.base.PxPMRadVel
    """
    for pxPM in (
        tcc.base.PxPMRadVel(0.001, -0.002, 0.003, 0.0012),
        tcc.base.PxPMRadVel(0.0012, 0.002, -0.003, -0.0012),
    ):
        cmdStr = "/Px=%0.5f/PM=(%0.5f, %0.5f)/RV=%0.5f" % (pxPM.parallax, pxPM.equatPM, pxPM.polarPM, pxPM.radVel)
        yield cmdStr, pxPM

class CmdObject(object):
    def __init__(self,cmdStr, fromPVTPair, fromPVTExplicit, fromCoordSys, fromSysExplicit,
            toCoordSys, toSysExplicit, tai, fromPxPMRadVel=NullPxPMRadVel):
        """Construct a CmdObject

        @param[in] cmdStr  convert command string
        @param[in] fromPVTPair  from position as a pair of coordConv.PVT
        @param[in] fromPVTExplicit  true if time in fromPVTPair is explicitly specified
        @param[in] fromCoordSys  from coordinate system as a coordConv.CoordSys
        @param[in] fromSysExplicit  true if from coordinate system date explicitly specified
            (warning: forced false if coordSys is FK4 or FK5, as those have explicit default dates)
        @param[in] toSysExplicit  to coordinate system date explicitly specified
            (warning: forced false if coordSys is FK4 or FK5, as those have explicit default dates)
        @param[in] tai  TAI date; used to update PVTs and coordSys that don't have explicit dates
        @param[in] fromPxPMRadVel  from parallax, proper motion and radial velocity
            (defaults to a fixed object infinitely far away)
        """
        self.cmdStr = cmdStr
        self.fromPVTExplicit = fromPVTExplicit
        if fromPVTExplicit:
            self.fromPVTPair = fromPVTPair
        else:
            self.fromPVTPair = tcc.base.ArrayPVT2()
            for i in range(2):
                # time wasn't specified, so it ended up being tai
                self.fromPVTPair[i] = fromPVTPair[i]
                self.fromPVTPair[i].t = tai
        # fix the cases where the coordinate system has a fixed default date
        fromSysExplicit = fromSysExplicit or fromCoordSys.getName().lower() in ("fk4", "fk5")
        toSysExplicit = toSysExplicit or toCoordSys.getName().lower() in ("fk4", "fk5")

        # fix the case that a date was specified for fromPVT and no date was specified for a coordSys
        if fromPVTExplicit and not fromSysExplicit:
            fromCoordSys.setDate(fromCoordSys.dateFromTAI(fromPVTPair[0].t))
        if fromPVTExplicit and not toSysExplicit:
            toCoordSys.setDate(toCoordSys.dateFromTAI(fromPVTPair[0].t))

        self.fromSysExplicit = fromSysExplicit
        self.fromCoordSys = fromCoordSys
        self.toSysExplicit = toSysExplicit
        self.toCoordSys = toCoordSys
        self.fromPxPMRadVel = fromPxPMRadVel
        self.tai = tai

    def __repr__(self):
        return "cmdStr=%r; fromPVTPair=%s, fromPVTExplicit=%s, fromCoordSys=%s, fromSysExplicit=%s, toCoordSys=%s, toSysExplicit=%s, tai=%0.7f, fromPxPMRadVel=%s" % \
            (self.cmdStr, self.fromPVTPair, self.fromPVTExplicit, self.fromCoordSys, self.fromSysExplicit, self.toCoordSys, self.toSysExplicit, self.tai, self.fromPxPMRadVel)

    def copy(self, taiDate):
        """Return a new CmdObject that is a copy at the specified date
        """
        return CmdObject(
            cmdStr = self.cmdStr,
            fromPVTPair = self.fromPVTPair,
            fromPVTExplicit = self.fromPVTExplicit,
            fromCoordSys = self.fromCoordSys,
            fromSysExplicit = self.fromSysExplicit,
            toCoordSys = self.toCoordSys,
            toSysExplicit = self.toSysExplicit,
            fromPxPMRadVel = self.fromPxPMRadVel,
            tai = taiDate,
        )


if __name__ == '__main__':
    from unittest import main
    main()

