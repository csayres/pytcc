#!/usr/bin/env python2
from __future__ import division, absolute_import

import glob
import os
import re
import unittest

import coordConv

import tcc.base
import tcc.axis

SecPerDay=3600 * 24.0

dataDir=os.path.join((os.path.dirname(__file__)), "data")

class TestComputeObj(unittest.TestCase):
    """Test computeObj by comparing to the TCC
    """
    def comparePVT(self, a, b, tai, maxErr, doWrap=True):
        """Compare two PVTs
        """
        self.assertEqual(a.isfinite(), b.isfinite())
        if a.isfinite():
            for testTAI in (tai, tai + 0.01):
                posA=a.getPos(testTAI)
                posB=b.getPos(testTAI)
                if doWrap:
                    measErr=coordConv.wrapCtr(posA - posB)
                else:
                    measErr=posA - posB
                self.assertLess(abs(measErr), maxErr)

    def comparePVTCoord(self, a, b, tai, maxErr=1.0e-9):
        """Assert two ZPMCoords are equal at the specified time
        """
        for testTAI in (tai, tai + 0.01):
            coordA=a.getCoord(testTAI)
            coordB=b.getCoord(testTAI)
            self.assertEqual(coordA.isfinite(), coordB.isfinite())
            if coordA.isfinite():
                self.assertLess(coordA.angularSeparation(coordB), maxErr)
    
    def runOneSet(self, telModPath, oldObjDir, maxRotErr=1e-4, printOnly=False):
        """Test computation of computeObj against a set of given object files
        
        @param[in] telModPath  full path to telescope model
        @param[in] oldObjDir  full path to directory containing object files from the old TCC;
            all files of the form obj_<inst>_<run>.dat will be processed,
            where <run> is an integer and <inst> is the instrument name
        @param[in] maxRotErr  rotator accuracy; set lower if the telescope model contains terms that affect
            the rotator, since the new code corrects the rotator and the old code does not
            unfortunately this reduces accuracy even in cases when rotType=mount
        @param[in] printOnly  just print a summary of results; do not test the results
        """
        isFirst=True
        axeLim=tcc.base.AxeLim()
        axeLim.loadPath(os.path.join(dataDir, "axelim.dat"))
        earth=tcc.base.Earth()
        telMod=tcc.base.TelMod()
        telMod.loadPath(telModPath)

        if not os.path.isdir(oldObjDir):
            raise RuntimeError("Cannot find oldObjDir=%r" % (oldObjDir,))
        oldObjPathList=glob.glob(os.path.join(oldObjDir, "obj_*.dat"))
        print "Testing %d files in dir %s using telescope model %s" % (len(oldObjPathList), oldObjDir, telModPath)
        instName=None
        for oldObjPath in oldObjPathList:
            objFileName=os.path.basename(oldObjPath)
            newInstName=re.search("obj_(.+)_\d+\.dat", objFileName).groups()[0]
            if newInstName != instName:
                inst=tcc.base.loadInst(os.path.join(dataDir, "inst"), newInstName)
                instName=newInstName
                axeLim.setRotLim(inst)
        
            # refObj=reference results from the old TCC
            refObj=tcc.base.loadOldObjData(oldObjPath)
        
            currTAI=refObj.targetMount[0].t
            earth.loadPredictions(os.path.join(dataDir, "earthpred_2013-04-12.dat"), currTAI)
            
            # obj=object data that starts as a copy of refObj, but is updated by calling computeObj
            # hence the user-set values should be identical and all computed values should be updated
            obj=tcc.base.Obj(refObj)
            tcc.axis.computeObj(
                obj=obj,
                doWrap=False,
                doRestart=(True, True, True),
                reportLim=True,
                earth=earth,
                inst=inst,
                telMod=telMod,
                axeLim=axeLim,
                tai=currTAI,
            )
            
            for i in range(3):
                if refObj.targetMount[i].t <= 0:
                    refObj.targetMount[i]=coordConv.PVT()
            azAltMount=coordConv.Coord(obj.targetMount[0].getPos(currTAI), obj.targetMount[1].getPos(currTAI))
            refAzAltMount=coordConv.Coord(refObj.targetMount[0].getPos(currTAI), refObj.targetMount[1].getPos(currTAI))
            azAltMountErr=refAzAltMount.angularSeparation(azAltMount)

            if printOnly:
                obsCoord=obj.obsPos.getCoord(currTAI)
                refObsCoord=refObj.obsPos.getCoord(currTAI)
                obsErr=refObsCoord.angularSeparation(obsCoord)
                atPole, obsAz, obsAlt=obsCoord.getSphPos()
            
                if isFirst:
                    print "    Obs Az      Obs Alt  Obs Err     Mt Az       Mt Alt    Mt Err     Mt Rot    Rot Err    File"
                    isFirst=False
                print "%11.4f %11.4f %8.4f %11.4f %11.4f %8.4f %11.4f %8.4f  %s" % (
                    obsAz, obsAlt, obsErr,
                    obj.targetMount[0].getPos(currTAI), obj.targetMount[1].getPos(currTAI), azAltMountErr,
                    obj.targetMount[2].getPos(currTAI), coordConv.wrapCtr(obj.targetMount[2].getPos(currTAI) - refObj.targetMount[2].getPos(currTAI)),
                    oldObjPath,
                )
                continue

            try:
                self.comparePVTCoord(obj.zpmUserPos, refObj.zpmUserPos, tai=currTAI, maxErr=1.0e-7)
                self.comparePVTCoord(obj.netUserPos, refObj.netUserPos, tai=currTAI, maxErr=1.0e-7)
                self.comparePVTCoord(obj.obsPos, refObj.obsPos, tai=currTAI, maxErr=1.0e-2)
                self.comparePVT(obj.objUserObjAzAng, refObj.objUserObjAzAng, tai=currTAI, maxErr=1.0e-2)
                self.comparePVT(obj.objAzInstAng,    refObj.objAzInstAng,    tai=currTAI, maxErr=maxRotErr)
                # the new TCC computes a few obscure angles in some cases where the old one does not
                if refObj.rotAzRotAng.isfinite():
                    self.comparePVT(obj.rotAzRotAng,     refObj.rotAzRotAng,     tai=currTAI, maxErr=maxRotErr)
                if refObj.rotAzObjAzAng.isfinite():
                    self.comparePVT(obj.rotAzObjAzAng,   refObj.rotAzObjAzAng,   tai=currTAI, maxErr=maxRotErr)
                self.comparePVT(obj.objUserInstAng,  refObj.objUserInstAng,  tai=currTAI, maxErr=maxRotErr)
                self.comparePVT(obj.spiderInstAng,   refObj.spiderInstAng,   tai=currTAI, maxErr=maxRotErr)
                self.comparePVT(obj.rotPhys,         refObj.rotPhys,         tai=currTAI, maxErr=maxRotErr)

                for i in range(2):
                    self.assertTrue(obj.targetMount[i].isfinite())
                    self.assertTrue(refObj.targetMount[i].isfinite())
                azAltMount=coordConv.Coord(obj.targetMount[0].getPos(currTAI), obj.targetMount[1].getPos(currTAI))
                refAzAltMount=coordConv.Coord(refObj.targetMount[0].getPos(currTAI), refObj.targetMount[1].getPos(currTAI))
                azAltMountErr=refAzAltMount.angularSeparation(azAltMount)
                self.assertLess(azAltMountErr, 1.0e-2)
                self.comparePVT(obj.targetMount[2], refObj.targetMount[2], tai=currTAI, maxErr=maxRotErr)
                for i in range(3):
                    # catch a bug whereby target rot time != currTAI for mount rotation
                    self.assertAlmostEqual(obj.targetMount[i].t, currTAI)
                
                self.assertEqual(tuple(obj.axisErrCode), tuple(refObj.axisErrCode))
                
            except Exception:
                print "\n*** runOneSet failed on oldObjPath=%r ***\n" % (oldObjPath,)
                print "** Old TCC obj ***\n%s" % (refObj,)
                print "** New TCC obj ***\n%s" % (obj,)
                raise

    def testNullTelMod(self):
        """Test a set of object files made with a null telescope model
        """
        telModPath=os.path.join(dataDir, "telmod_null.dat")
        oldObjDir= os.path.join(dataDir, "oldobj_null_telmod")
        self.runOneSet(telModPath=telModPath, oldObjDir=oldObjDir)
                
    def testWithTelMod(self):
        """Test computation from computeObj compared to old TCC
        """
        telModPath=os.path.join(dataDir, "telmod_35m_2013-03-19.dat")
        oldObjDir= os.path.join(dataDir, "oldobj_35m")
        self.runOneSet(telModPath=telModPath, oldObjDir=oldObjDir, maxRotErr=0.2)


if __name__ == '__main__':
    unittest.main()
