#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Test functionality of offset command
"""
import os
import subprocess
import unittest

import tcc.base.testUtils

tcc.base.testUtils.init()
EarthDir = os.path.join(tcc.base.getDataDir(), "earthfilt")
OutPath = os.path.join(tcc.base.getDataDir(), "earthfilt", "temp.dat")

class TestEarthFilt(unittest.TestCase):
    def tearDown(self):
        if os.path.isfile(OutPath):
            os.unlink(OutPath)

    def testBadFiles(self):
        """Test files that should fail for various reasons
        """
        for badFileName in ("baddata", "badend", "badorder"):
            inPath = os.path.join(EarthDir, "%s.dat" % (badFileName,))
            retCode = subprocess.call(["earthFilt.py", inPath, OutPath])
            self.assertNotEqual(retCode, 0)

    def testGood(self):
        """Test files that should work
        """
        for goodFileName in ("good10", "good365", "twoleapsec"):
            inPath = os.path.join(EarthDir, "%s.dat" % (goodFileName,))
            desPath = os.path.join(EarthDir, "pred%s.dat" % (goodFileName,))
            retCode = subprocess.call(["earthFilt.py", inPath, OutPath])
            self.assertEqual(retCode, 0)
            with open(OutPath, "rU") as actFile:
                actData = actFile.read()
            with open(desPath, "rU") as desFile:
                desData = desFile.read()
            self.assertEqual(actData, desData)


if __name__ == "__main__":
    unittest.main()
