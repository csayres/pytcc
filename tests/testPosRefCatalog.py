#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest
import itertools

import coordConv

import tcc.actor
import tcc.axis
import tcc.base
import tcc.base.testUtils

tcc.base.testUtils.init()

class GetCoordSysTest(unittest.TestCase):
    def setUp(self):
        self.tccActor = tcc.actor.FakeTCC35mActor(userPort=0)
        self.posRefCatalog = self.tccActor.posRefCatalog

    def tearDown(self):
        self.posRefCatalog = None
        self.tccActor = None

    def testFindStar(self):
        """Test the findStar method
        """
        self.tccActor.inst = tcc.base.loadInst(tcc.base.getInstDir(), "spicam")
        self.tccActor.axeLim.setRotLim(self.tccActor.inst)
        obj = self.tccActor.obj
        currTAI = tcc.base.tai()
        for axis in range(3):
            obj.axisCmdState[axis] = tcc.base.AxisState_Tracking

        # start with a target star that uses a mean equatorial angle = middle of catalog range
        meanEquatAng = (self.posRefCatalog[0].polarAng + self.posRefCatalog[1].polarAng) / 2.0
        obj.userPos[0] = coordConv.PVT(0, 0, currTAI)
        obj.userPos[1] = coordConv.PVT(meanEquatAng, 0, currTAI)
        obj.userSys = self.posRefCatalog.coordSys
        obj.rotUser = coordConv.PVT(0, 0, currTAI)
        obj.rotType = tcc.base.RotType_Object
        obj.azWrapPref = tcc.base.WrapType_Negative
        tcc.axis.computeObj(
            obj = obj,
            doWrap = True,
            doRestart = [True]*3,
            reportLim = False,
            earth = self.tccActor.earth,
            inst = self.tccActor.inst,
            telMod = self.tccActor.telMod,
            axeLim = self.tccActor.axeLim,
            tai = currTAI,
        )

        # find the nearest star
        starData1 = self.posRefCatalog.findStar(self.tccActor, magRange=(0, 100), tai=currTAI)

        # use the found star as a target and search for a nearby star; it should be the same star
        obj.userSys = self.posRefCatalog.coordSys
        obj.userPos = starData1.pvtPair
        obj.userPxPMRadVel = starData1.pxPMRadVel
        tcc.axis.computeObj(
            obj = obj,
            doWrap = True,
            doRestart = [True]*3,
            reportLim = False,
            earth = self.tccActor.earth,
            inst = self.tccActor.inst,
            telMod = self.tccActor.telMod,
            axeLim = self.tccActor.axeLim,
            tai = currTAI,
        )
        starData2 = self.posRefCatalog.findStar(self.tccActor, magRange=(0, 100), tai=currTAI)
        for axis in range(2):
            self.assertEquals(starData1.pvtPair[axis], starData2.pvtPair[axis])
        self.assertEquals(starData1.mag, starData2.mag)
        self.assertEquals(starData1.name, starData2.name)

        # change magnitude limits to excluded the star and search again; it should find a different star
        # that is farther away and has magnitude in bounds; do this twice, once for minMag, once for maxMag
        minMag3 = starData2.mag + 1e-10
        starData3 = self.posRefCatalog.findStar(self.tccActor, magRange=(minMag3, 100), tai=currTAI)
        for axis in range(2):
            self.assertNotEquals(starData2.pvtPair[axis], starData3.pvtPair[axis])
        self.assertGreaterEqual(starData3.mag, minMag3)
        self.assertNotEquals(starData2.name, starData3.name)

        maxMag4 = starData2.mag - 1e-10
        starData4 = self.posRefCatalog.findStar(self.tccActor, magRange=(0, maxMag4), tai=currTAI)
        for axis in range(2):
            self.assertNotEquals(starData2.pvtPair[axis], starData4.pvtPair[axis])
        self.assertLessEqual(starData4.mag, maxMag4)
        self.assertNotEquals(starData2.name, starData4.name)

        # change azimuth limits to exclude the star and search again; it should find a different star
        # that is farther away
        mountAz2 = obj.targetMount[0].getPos(currTAI)
        self.tccActor.axeLim.minPos[0] = mountAz2 + 1e-5 # delta must be greater than 0.01" to be safe
        starData5 = self.posRefCatalog.findStar(self.tccActor, magRange=(0, 100), tai=currTAI)
        for axis in range(2):
            self.assertNotEquals(starData2.pvtPair[axis], starData5.pvtPair[axis])
        self.assertNotEquals(starData2.mag, starData5.mag)
        self.assertNotEquals(starData2.name, starData5.name)

    def testIndexIter(self):
        """Test the indexIter method, as well as __len__ and __getitem__
        """
        catLength = len(self.posRefCatalog)
        self.assertEqual(catLength, 63)

        minPolarAng = self.posRefCatalog[0].polarAng
        maxPolarAng = self.posRefCatalog[-1].polarAng

        # start at halfway between first two points - epsilon, or less: should go up from 0
        self.checkIteration(minPolarAng, range(0, 5))
        polarAng = (minPolarAng + self.posRefCatalog[1].polarAng - 1e-10) / 2.0
        self.checkIteration(polarAng, range(0, 5))

        # start at halfway between last two points + epsilon, or more; should go down from end
        self.checkIteration(maxPolarAng, range(catLength-1, catLength-5, -1))
        polarAng = (maxPolarAng + self.posRefCatalog[-2].polarAng + 1e-5) / 2.0
        self.checkIteration(polarAng, range(catLength-1, catLength-5, -1))

        # start halfway between first two elements + epsilon; expect 1, 0, 2, 3...
        polarAng = (minPolarAng + self.posRefCatalog[1].polarAng + 1e-10) / 2.0
        self.checkIteration(polarAng, (1, 0, 2, 3, 4))

        # start halfway between last two elements - epsilon; expect next to last, last, 2nd to last, 3rd to last...
        polarAng = (maxPolarAng + self.posRefCatalog[-2].polarAng - 1e-5) / 2.0
        self.checkIteration(polarAng, (catLength - 2, catLength - 1, catLength - 3, catLength - 4))

    def checkIteration(self, polarAng, predIndList):
        """Check an iteration sequence against predicted indices
        """
        prevDeltaPolarAng = -1e-10
        for ind, predInd in itertools.izip(self.posRefCatalog.indexIter(polarAng), predIndList):
#            print "ind=%s, predInd=%s" % (ind, predInd)
            self.assertEqual(ind, predInd)
            deltaPolarAng = abs(self.posRefCatalog[ind].polarAng - polarAng)
            self.assertLess(prevDeltaPolarAng, deltaPolarAng)
            prevDeltaPolarAng = deltaPolarAng


if __name__ == '__main__':
    unittest.main()
