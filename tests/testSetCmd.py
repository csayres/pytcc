#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Test various SET commands, not including SET BLOCK
"""
import functools

from twisted.internet.defer import gatherResults
from twisted.trial.unittest import TestCase
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")

import tcc.base.testUtils
tcc.base.testUtils.init(__file__)

from tcc.base.wrapperTestCase import WrapperTestCase

class TestSetCmd(WrapperTestCase, TestCase):

    @property
    def instBlock(self):
        """Return the instrument block
        """
        return self.dw.actorWrapper.actor.inst

    def testSetTime(self):
        # note also runs showTime
        d1, cmd1 = self.dw.queueCmd("set time")
        return d1

    def assertFocusEqual(self, cmdVar, focusVal):
        """Assert that the focus value on the block matches focusVal
        @param[in] cmdVar, passed via callback, ignored
        @param[in] focusVal  float, a focus value
        """
        self.assertEqual(self.instBlock.secUserFocus, focusVal)
        self.assertEqual(self.dispatcher.model.secFocus[0], focusVal)

    def testSetFocus(self):
        focusVal = 5.0
        cmdStr = "set focus=%i"%focusVal
        d1, cmd1 = self.dw.queueCmd(
            cmdStr,
            callFunc = functools.partial(self.assertFocusEqual, focusVal=focusVal),
        )
        cmdStr = "set focus=%i /incremental"%focusVal
        d2, cmd2 = self.dw.queueCmd(
            cmdStr,
            callFunc = functools.partial(self.assertFocusEqual, focusVal=focusVal*2.),
        )
        # next try specifying no focus value, should just run collimation
        # with the current focus value remaining
        cmdStr = "set focus"
        d3, cmd3 = self.dw.queueCmd(
            cmdStr,
            callFunc = functools.partial(self.assertFocusEqual, focusVal=focusVal*2.),
        )
        return gatherResults([d1, d2, d3])

    def assertImCenterEqual(self, cmdVar, value):
        """Assert that the image center value on the block matches value
        @param[in] cmdVar, passed via callback, ignored
        @param[in] value  center position [x,y]
        """
        for ind, val in enumerate(value):
            print 'ind, val', ind, val, self.instBlock.iim_ctr[ind]
            self.assertEqual(self.instBlock.iim_ctr[ind], val)
            self.assertEqual(self.dispatcher.model.iimCtr[ind], val)

    def testSetImCenter(self):
        value1 = [200, 300]
        d0, cmd0 = self.dw.queueCmd(
            "set inst=dis",
        )
        d1, cmd1 = self.dw.queueCmd(
            "set imcenter=(%s, %s)" % (value1[0], value1[1]),
            callFunc = functools.partial(self.assertImCenterEqual, value=value1),
        )
        value2 = [452.24, 24.17]
        cmdStr = "set imcenter=(%s, %s)" % (value2[0], value2[1])
        d2, cmd2 = self.dw.queueCmd(
            cmdStr,
            callFunc = functools.partial(self.assertImCenterEqual, value=value2),
        )
        return gatherResults([d0, d1, d2])

    def checkInst(self, cmdVar, instName):
        self.assertEqual(self.dispatcher.model.inst[0], instName)
        self.assertEqual(self.actor.inst.instName, instName)
        instRotIDDict = {
            "agile": 2,
            "echelle": 0,
        }
        predRotID = instRotIDDict.get(instName, 1)
        self.assertEqual(self.actor.inst.rotID, predRotID, instName + 'instblock')
        self.assertEqual(self.dispatcher.model.rotID[0], predRotID, instName + 'model')
        rotDev = self.actor.axisDevSet["rot"]
        if self.actor.inst.hasRotator():
            self.assertNotEqual(predRotID, 0)
            self.assertEqual(rotDev.name, "rot%d" % (predRotID,))
        else:
            self.assertEqual(predRotID, 0)
            self.assertTrue(rotDev is None)

    def testSetInstrumentName(self):
        instruments = ["echelle", "dis", "nicfps", "spicam", "tspec"]
        deferredList = []
        for instrument in instruments:
            d, cmd = self.dw.queueCmd(
                "set instrument = %s" % instrument,
                callFunc = functools.partial(self.checkInst, instName=instrument),
            )
            deferredList.append(d)
        return gatherResults(deferredList)

    def testSetInstrumentSlew(self):
        self.assertFalse(self.actor.inst.hasRotator())
        d0, cmd = self.dw.queueCmd(
            "set instrument = echelle",
        )
        d1, cmd = self.dw.queueCmd(
            "track 12, 12 mount",
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = ["Tracking", "Tracking", "NotAvailable"]),
        )
        d2, cmd2 = self.dw.queueCmd(
            "set instrument = dis",
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = ["Tracking", "Tracking", "Halted"]),
        )
        d3, cmd3 = self.dw.queueCmd(
            "set instrument = echelle",
            callFunc = functools.partial(self.checkAxisCmdState, axisCmdState = ["Tracking", "Tracking", "NotAvailable"]),
        )
        return gatherResults([d0, d1, d2, d3])

if __name__ == '__main__':
    from unittest import main
    main()

