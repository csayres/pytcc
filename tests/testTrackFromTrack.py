#!/usr/bin/env python2
from __future__ import division, absolute_import

from twisted.trial.unittest import TestCase
from twisted.internet.defer import gatherResults

from tcc.base import testUtils
testUtils.init(__file__)
from tcc.base.trackTestUtils import TrackCommonMethods, LessTests, AllTests

# Run every test on all three flavors of tracking (from halt, from tracking, from slewing)
# else run all on from halt, and a subset on from tracking/slewing
runAllTests = False
class TestTrackFromTrack(TrackCommonMethods, TestCase, AllTests if runAllTests else LessTests):
    """Override checkTrackCmd method to set the telescope tracking before tests are run
    """
    def checkTrackCmd(self, trackCmd, shouldRotate=False, shouldStop=False):
        """Start next track command from axes halted state.
        """
        begAxisCmdState1 = ["Slewing"]*3
        endAxisCmdState1 = ["Tracking"]*3

        if shouldStop:
            begAxisCmdState2 = ["Halting"]*3
            endAxisCmdState2 = ["Halted"]*3
        else:
            begAxisCmdState2 = ["Slewing"]*3
            endAxisCmdState2 = ["Tracking"]*3

        startTrackCmd = "track 15,15 observed"
        if shouldRotate:
            startTrackCmd += "/rottype=mount/rotangle=20"
        else:
            begAxisCmdState1[2] = "Halted"
            endAxisCmdState1[2] = "Halted"
            begAxisCmdState2[2] = "Halted"
            endAxisCmdState2[2] = "Halted"

        d1 = self.checkATrackCmd(
            cmdStr = startTrackCmd,
            begAxisCmdState = begAxisCmdState1,
            endAxisCmdState = endAxisCmdState1,
        )

        d2 = self.checkATrackCmd(
            cmdStr = trackCmd,
            begAxisCmdState = begAxisCmdState2,
            endAxisCmdState = endAxisCmdState2,
        )

        return gatherResults((d1, d2))

    def testTrackOffset(self):
        # doesn't work correctly with axes moving
        pass

if __name__ == '__main__':
    from unittest import main
    main()