#!/usr/bin/env python2
from __future__ import division, absolute_import

import itertools
import unittest

from coordConv import PVT

from tcc.base import AxisLim
from tcc.mov import slew

class TestMovSlew(unittest.TestCase):
    """Test tcc.mov.slew
    """
    def testBasics(self):
        dataList = (
            (0, -2, 3, 4, 0, 6, 5, 5),  # corresponds to fullSlew test 2
            (0, -2, 3, 4, 8, 6, 5, 5),  # same, but "tmin" large enough to extend the slew
            (1, 2, 3, 4, 0, 6, 7, 1.4), # first 2-node test
            (1, 2, 3, 4, 0, 4.9, 6, 2), # second 2-node test
            (1, 2, 3, 4, 0, 5, 6, 2),   # third 2-node test
        )
        predPVTListList = (
            (
                PVT(0.0, -2.0, 0.0),
                PVT(-1.166666666666667, 0.5, 1.0),
                PVT(3.3333333333333437e-02, 3.5, 1.6),
                PVT(5.2, 6.0, 2.6),
                PVT(25.6, 6.0, 6.0),
                PVT(29.56666666666667, 5.0, 6.7),
                PVT(32.6, 4.0, 7.4),
            ),
            (
                PVT(0.0, -2.0, 0.0),
                PVT(-1.866666666666667, 2.0, 2.8),
                PVT(11.2, 6.0, 5.6),
                PVT(40.6, 6.0, 10.5),
                PVT(53.06666666666667, 5.0, 12.7),
                PVT(62.6, 4.0, 14.9),
            ),
            (
                PVT(1.0, 2.0, 0.0),
                PVT(23.0, 4.0, 5.0),
            ),
            (
                PVT(1.0, 2.0, 0.0),
                PVT(51.0, 4.0, 12.0),
            ),
            (
                PVT(1.0, 2.0, 0.0),
                PVT(39.0, 4.0, 9.0),
            ),
        )

        self.assertEqual(len(dataList), len(predPVTListList))

        for (pA, vA, pB, vB, tMin, vLim, aLim, jLim), predPVTList in itertools.izip(dataList, predPVTListList):
            axisLim = AxisLim(minPos=0, maxPos=0, vel=vLim, accel=aLim, jerk=jLim)
            pvtList = slew(
                pA=pA,
                vA=vA,
                pB=pB,
                vB=vB,
                tMin=tMin,
                axisLim=axisLim,
            )
            self.assertEqual(len(pvtList), len(predPVTList))
            for pvt, predPVT in itertools.izip(pvtList, predPVTList):
                self.assertAlmostEqual(pvt.pos, predPVT.pos)
                self.assertAlmostEqual(pvt.vel, predPVT.vel)
                self.assertAlmostEqual(pvt.t,   predPVT.t)


if __name__ == '__main__':
    unittest.main()

