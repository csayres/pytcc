#!/usr/bin/env python2
from __future__ import division, absolute_import

from twisted.trial.unittest import TestCase
from twisted.internet.defer import gatherResults, Deferred

from twistedActor import UserCmd

import tcc.base
from tcc.base import testUtils
testUtils.init(__file__)
from tcc.base.trackTestUtils import TrackCommonMethods

runAllTests = False
class TestInitAndStopFromTrack(TrackCommonMethods, TestCase):

    @property
    def actor(self):
        return self.dw.actor

    @property
    def obj(self):
        return self.actor.obj

    @property
    def axisErrCode(self):
        return self.obj.axisErrCode

    @property
    def axisCmdState(self):
        return self.obj.axisCmdState

    @property
    def targetMount(self):
        return self.obj.targetMount

    def checkAxisHalted(self):
        for axisInd in [0,1]:
            self.assertTrue(self.axisCmdState[axisInd] in [tcc.base.AxisState_Halted, tcc.base.AxisState_Halting])
            self.assertTrue(self.axisErrCode[axisInd]==tcc.base.AxisErr_HaltRequested)
            self.assertFalse(self.targetMount[axisInd].isfinite())

    def checkAxisTracking(self):
        for axisInd in [0,1]:
            self.assertTrue(self.axisCmdState[axisInd]==tcc.base.AxisState_Tracking)
            self.assertTrue(self.axisErrCode[axisInd]==tcc.base.AxisErr_OK)
            self.assertTrue(self.targetMount[axisInd].isfinite())

    def _testHaltingCommand(self, cmdStr):
        """cmdStr: a command string that should halt the axes immediately
        """
        d2 = Deferred()
        def sendInit(cb, self=self, d2=d2):
            def setD2Done(aCmd):
                if aCmd.isDone:
                    self.checkAxisHalted()
                    d2.callback("done")
            self.checkAxisTracking()
            userCmd = UserCmd(cmdStr=cmdStr)
            userCmd.addCallback(setD2Done)
            userCmd.parsedCmd = self.actor.cmdParser.parseLine(userCmd.cmdBody)
            userCmd.parsedCmd.callFunc(self.actor, userCmd)
            self.actor.updateTracking()
            self.checkAxisHalted()

        d1, cmd1 = self.dw.queueCmd(
            "track 12,12 observed",
        )
        d1.addCallback(sendInit)
        return gatherResults([d1, d2])


    def testInit(self):
        """Set the tcc tracking, issue axis init and verify that the obj block is set to a good
        halted state immediately
        """
        return self._testHaltingCommand("axis init")


    def testStop(self):
        """Set the tcc tracking, issue axis stop and verify that the obj block is set to a good
        halted state immediately
        """
        return self._testHaltingCommand("axis stop")


if __name__ == '__main__':
    from unittest import main
    main()