#!/usr/bin/env python2
from __future__ import division, absolute_import

import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from twisted.trial.unittest import TestCase
from twisted.internet.defer import Deferred, gatherResults
from twisted.internet import reactor

import tcc.base.testUtils
tcc.base.testUtils.init(__file__)
from tcc.base.wrapperTestCase import WrapperTestCase
from tcc.msg.udpPacket import UDPListener, UDPSender

class TestUDP(WrapperTestCase, TestCase):
    """Test that UDP signals are being correctly broadcasted
    """
    def testUDP(self):
        """simplest test
        """
        # force broadcasts to non-default port
        # so we will only read udp messages from
        # this process (rather than others that may be running via scons)
        udpPort = 1236 # default value is 1235
        self.actor.udpPort = udpPort
        self.actor.udpSender.stopListening()
        self.actor.udpSender = UDPSender(udpPort)
        self.actor.udpSender.startListening()
        
        d = Deferred()
        def checkReceivedPackets(*args):
            try:
                self.assertGreater(len(self.listener.packetsReceived), 0)

                # look only at the last packet
                packet = self.listener.packetsReceived[-1]
                self.assertEqual(packet["coordSys"].strip("\x00"), 'obs')
                self.assertAlmostEqual(packet["objNetPos.pos1"], 11.0)
                self.assertAlmostEqual(packet["objNetPos.vel1"], 0.0)
                self.assertAlmostEqual(packet["objNetPos.pos2"], 10.0)
                d.callback("OK")
            except Exception as e:
                d.errback(e)

        d0, cmd0 = self.dw.queueCmd("set instrument=echelle")
        d1, cmd1 = self.dw.queueCmd("track 11, 10 observed")

        def trackingCallback(cmdVar):
            if cmdVar.isDone and not cmdVar.didFail:
                # start a UDP listener
                self.listener = UDPListener(self.actor.udpPort)
                self.listener.startListening()
                # let the TCC track for a few seconds before checking the last packet
                reactor.callLater(5, checkReceivedPackets)

        cmd1.addCallback(trackingCallback)
        return gatherResults((d0, d1, d))

    def tearDown(self):
        self.listener.stopListening()
        return WrapperTestCase.tearDown(self)

if __name__ == '__main__':
    from unittest import main
    main()