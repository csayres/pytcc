#!/usr/bin/env python2
from __future__ import division, absolute_import

import os
import unittest

import coordConv

import tcc.base
import tcc.axis
from tcc.parse import makeCoordSys, OldCoordSysCodeNameDict

SecPerDay=3600 * 24.0

dataDir=os.path.abspath(os.path.join((os.path.dirname(__file__)), "data", "coordConvWithFP"))

def coordSysCodeIsFP(coordSysCode):
    return coordSysCode <= -5

class TestCoordConvWithFP(unittest.TestCase):
    """Test convertConvWithFP against TCC results
    """
    def comparePVT(self, a, b, tai, maxErr, doWrap=True):
        """Compare two PVTs
        """
        self.assertEqual(a.isfinite(), b.isfinite())
        if a.isfinite():
            for testTAI in (tai, tai + 0.01):
                posA=a.getPos(testTAI)
                posB=b.getPos(testTAI)
                if doWrap:
                    measErr=coordConv.wrapCtr(posA - posB)
                else:
                    measErr=posA - posB
                self.assertLess(abs(measErr), maxErr)
    
    def runOne(self, suffix):
        """The main test
        
        @param[in] suffix  suffix for massccfp_out_<suffix>.dat and massccfp_obj_<suffix>.dat

        Format of massccfp_out_<suffix>.dat, the data file written by old TCC, is a header of 3 lines followed by many lines of data
        as described by the 3rd line:
            # UT1_min_TAI, poleWander(2)
              -35.03254739      0.2241925203E-04  0.7933791377E-04
            # fromSys, fromDateOrGPNum, fromPos1, fromPos2, fromVel1, fromVel2, fromTAI, fromPM1, fromPM2, fromParallax, fromRadVel, refCoA, refCoB, toSys, toDateOrGPNum, toPos1, toPos2, toVel1, toVel2, toTAI, toPM1, toPM2, toParallax, toRadVel, toFromAng.pos, toFromAng.vel, toFromAng.t, TAI, LAST
            #       1     2    3     4     5     6     7    8    9     10      11       12      13        14    15   16    17    18     19   20   21   22   23       24       25                26           27         28    29
            -3   330.000000000000        45.0000000000000       0.000000000000000E+000   4890506358.56000        35.0000000000000       0.000000000000000E+000   4890506358.56000       0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000  1.200000000000000E-002  2.000000000000000E-002          -3   330.302349606045        44.7100560777909       0.000000000000000E+000   4890506358.56000        35.1780357948374       0.000000000000000E+000   4890506358.56000       0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000  0.218697011757769      -1.264766069652978E-010   4890506358.56000        4890506358.56000        330.302349606045

        In the tests to date we only convert between observed and focal plane coordinate systems,
        so we ignore proper motion, parallax and radial velocity.
        """
        # load instrument data (which is in old TCC format)
        inst=tcc.base.loadOldInstData(os.path.join(dataDir, "massccfp_inst.dat"))
        
        # load axis limit data and update with inst rot limits
        axeLim=tcc.base.AxeLim()
        axeLim.loadPath(os.path.join(dataDir, "axelim.dat"))
        axeLim.setRotLim(inst)
        telMod=tcc.base.TelMod()
        telMod.loadPath(os.path.join(dataDir, "telmod.dat"))
        oldObjPath=os.path.join(dataDir, "massccfp_obj_%s.dat" % (suffix,))
        refObj=tcc.base.loadOldObjData(oldObjPath)
        earth=tcc.base.Earth()
        earth.loadPredictions(os.path.join(dataDir, "earthpred.dat"), refObj.updateTime)
        obj=tcc.base.Obj(refObj)

        isFirst=True
        inPath=os.path.join(dataDir, "massccfp_out_%s.dat" % (suffix,))
        with file(inPath) as infile:
            infile.next() # skip header 1
            earthDataStr=infile.next()
            ut1MinTAI, poleX, poleY=[float(val) for val in earthDataStr.split()]

            infile.next() # skip header 2
            lineNum=3
            for line in infile:
                lineNum += 1
                line=line.strip()
                if not line or line[0] in ("#", "!"):
                    continue
                try:
                    fromPVTPair=tcc.base.ArrayPVT2()
                    predToPVTPair=tcc.base.ArrayPVT2()
                    predToFromAng=coordConv.PVT()
                    fromSysCode, fromDateOrGPNum, fromPVTPair[0].pos, fromPVTPair[1].pos, fromPVTPair[0].vel, \
                    fromPVTPair[1].vel, fromTAI, fromEquatPM, fromPolarPM, fromParallax, fromRadVel, \
                    refCoA, refCoB, \
                    toSysCode, toDateOrGPNum, predToPVTPair[0].pos, predToPVTPair[1].pos, predToPVTPair[0].vel, \
                    predToPVTPair[1].vel, predToTAI, predToEquatPM, predToPolarPM, predToParallax, predToRadVel, \
                    predToFromAng.pos, predToFromAng.vel, predToFromAng.t, taiDate, LAST=[float(val) for val in line.split()]
                    fromSysCode=int(fromSysCode)
                    toSysCode=int(toSysCode)

                    for i in range(2):
                        fromPVTPair[i].t=fromTAI
                        predToPVTPair[i].t=predToTAI
                    fromPxPMRadVel=tcc.base.PxPMRadVel(fromParallax, fromEquatPM, fromPolarPM, fromRadVel)
                    predToPxPMRadVel=tcc.base.PxPMRadVel(predToParallax, predToEquatPM, predToPolarPM, predToRadVel)

                    fromSysName=OldCoordSysCodeNameDict[fromSysCode]
                    toSysName=OldCoordSysCodeNameDict[toSysCode]
                    if fromSysName.lower() in ("observed", "topocentric"):
                        # the old TCC used LAST; rather than trying to convert, just use current
                        fromDateOrGPNum=None
                    if toSysName.lower() in ("observed", "topocentric"):
                        toDateOrGPNum=None
                    fromCoordSys=makeCoordSys(fromSysName, fromDateOrGPNum)
                    toCoordSys=makeCoordSys(toSysName, toDateOrGPNum)
                    fromDir=coordConv.PVT(0, 0, taiDate)
                    
                    if isFirst or obj.updateTime != taiDate:
                        if not isFirst:
                            print "********* SURPRISE! ***********"
                        isFirst=False
                        tcc.axis.computeObj(
                            obj=obj,
                            doWrap=False,
                            doRestart=(True, True, True),
                            reportLim=True,
                            earth=earth,
                            inst=inst,
                            telMod=telMod,
                            axeLim=axeLim,
                            tai=taiDate,
                        )
                        # perform a brief sanity-check (confirms that inst.instPos set correctly)
                        self.comparePVT(obj.rotPhys, refObj.rotPhys, tai=obj.updateTime, maxErr=1e-4)

                    toPVTPair, toPxPMRadVel, toDir, scaleChange=tcc.axis.coordConvWithFP(
                        fromCoordSys=fromCoordSys,
                        fromPVTPair=fromPVTPair,
                        fromPxPMRadVel=fromPxPMRadVel,
                        fromDir=fromDir,
                        toCoordSys=toCoordSys,
                        obj=obj,
                        inst=inst,
                        zeroPM=False,
                        useGSWavelen=False,
                    )
                except Exception as e:
                    print "Could not process line %s of input file %r\nline=%r\nerror=%s" % (lineNum, inPath, line, e)
                    raise

                try:
#                     if toSysCode < -7 or fromSysCode < -7:
#                         continue
                    if coordSysCodeIsFP(toSysCode):
                        for axis in range(2):
                            self.assertAlmostEqual(toPVTPair[axis].t, predToPVTPair[axis].t)
                            if toSysCode == -9:
                                # guide image coords are in pixels
                                deltaPos=0.2
                                deltaVel=0.1
                            else:
                                deltaPos=2e-5
                                deltaVel=1e-5
                            self.assertAlmostEqual(toPVTPair[axis].pos, predToPVTPair[axis].pos, delta=deltaPos)
                            self.assertAlmostEqual(toPVTPair[axis].vel, predToPVTPair[axis].vel, delta=deltaVel)
                    else:
                        # equatorial; use angSep for position, at least
                        predToPVTCoord=tcc.base.pvtCoordFromPVTPair(predToPVTPair, predToPxPMRadVel)
                        toPVTCoord=tcc.base.pvtCoordFromPVTPair(toPVTPair, toPxPMRadVel)
                        angSep=predToPVTCoord.angularSeparation(toPVTCoord)
                        self.assertAlmostEqual(angSep.pos, 0, delta=2e-5)
                        self.assertAlmostEqual(angSep.vel, 0, delta=1e-5)
                        
                        self.assertAlmostEqual(predToPVTCoord.getTAI(), toPVTCoord.getTAI())

                    if toCoordSys.isMean():
                        self.assertAlmostEqual(toPxPMRadVel.parallax, predToPxPMRadVel.parallax)
                        self.assertAlmostEqual(toPxPMRadVel.equatPM,  predToPxPMRadVel.equatPM)
                        self.assertAlmostEqual(toPxPMRadVel.polarPM,  predToPxPMRadVel.polarPM)
                        self.assertAlmostEqual(toPxPMRadVel.radVel,   predToPxPMRadVel.radVel)
                    else:
                        self.assertEqual(toPxPMRadVel.parallax, 0)
                        self.assertEqual(toPxPMRadVel.equatPM, 0)
                        self.assertEqual(toPxPMRadVel.polarPM, 0)
                        self.assertEqual(toPxPMRadVel.radVel, 0)

                except Exception as e:
                    print "\nFailed on line %s of input file %r with error=%s" % (lineNum, inPath, e)
                    print "fromCoordSys=%s, fromPos=%s, toCoordSys=%s" % (fromCoordSys.getName(), fromPVTPair, toCoordSys.getName())
                    print "old TCC toPVTPair=%s" % (predToPVTPair,)
                    print "new TCC toPVTPair=%s" % (toPVTPair,)
                    raise
    
    def testHorizRot(self):
        self.runOne("horizrot")

    def testObjRot(self):
        self.runOne("objrot")

if __name__ == '__main__':
    unittest.main()
