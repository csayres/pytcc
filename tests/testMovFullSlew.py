#!/usr/bin/env python2
from __future__ import division, absolute_import

import itertools
import unittest

from coordConv import PVT

from tcc.mov import fullSlew

class TestMovFullSlew(unittest.TestCase):
    """Test tcc.mov.fullSlew
    """
    def testBasics(self):
        """Compare to the same slews computed by the old TCC
        """
        dataList = (
            (0, -2, 1, 3, 0.1, 4, 5),
            (0, -2, 3, 4, 1, 6, 5),
            (1, 2, 3, 4, 5, 6, 7),
            (0, 2, 15, -3, 0.2, 5, 4),
            (0, 2, 1, -3, 0.2, 5, 4),
        )
        predPVTListList = (
            (
                PVT(0.0, -2.0, 0.0),
                PVT(-0.1916666666666667, -1.750, 0.10    ),
                PVT(0.9083333333333334, 3.750, 1.20    ),
                PVT(1.30, 4.0, 1.30    ),
                PVT(15.10, 4.0, 4.750    ),
                PVT(15.49166666666667, 3.750, 4.850    ),
                PVT(15.84166666666667, 3.250, 4.950    ),
                PVT(16.150, 3.0, 5.050    ),
            ),
            (
                PVT(0.0, -2.0, 0.0),
                PVT(-1.166666666666667, 0.50, 1.0    ),
                PVT(3.3333333333333437E-02, 3.50, 1.60    ),
                PVT(5.20, 6.0, 2.60    ),
                PVT(25.60, 6.0, 6.0    ),
                PVT(29.56666666666667, 5.0, 6.70    ),
                PVT(32.60, 4.0, 7.40    ),
            ),
            (
                PVT(1.0, 2.0, 0.0),
                PVT(8.040677746940152, 3.70052908571790, 2.742932726531129    ),
                PVT(21.30060466939189, 5.401058171435799, 5.485865453062257    ),
                PVT(34.73662128980211, 4.70052908571790, 8.085941036736243    ),
                PVT(45.74406648164091, 4.0, 10.68601662041023    ),
            ),
            (
                PVT(0.0, 2.0, 0.0),
                PVT(0.4266666666666667, 2.40, 0.20    ),
                PVT(2.351666666666667, 4.60, 0.750    ),
                PVT(3.3250, 5.0, 0.950    ),
                PVT(3.3406250, 5.0, 0.9531250    ),
                PVT(4.313958333333333, 4.60, 1.1531250    ),
                PVT(6.113958333333333, -2.60, 2.9531250    ),
                PVT(5.5406250, -3.0, 3.1531250    ),
            ),
            (
                PVT(0.0, 2.0, 0.0),
                PVT(0.3733333333333333, 1.60, 0.20    ),
                PVT(-1.951666666666667, -4.60, 1.750    ),
                PVT(-2.9250, -5.0, 1.950    ),
                PVT(-5.98750, -5.0, 2.56250    ),
                PVT(-6.960833333333334, -4.60, 2.76250    ),
                PVT(-8.160833333333333, -3.40, 3.06250    ),
                PVT(-8.78750, -3.0, 3.26250    ),
            ),        
        )

        self.assertEqual(len(dataList), len(predPVTListList))

        for (pA, vA, pB, vB, tJerk, vMax, aMax), predPVTList in itertools.izip(dataList, predPVTListList):
            pvtList = fullSlew(
                pA=pA,
                vA=vA,
                pB=pB,
                vB=vB,
                tJerk=tJerk,
                vMax=vMax,
                aMax=aMax,
            )
            self.assertEqual(len(pvtList), len(predPVTList))
            for pvt, predPVT in itertools.izip(pvtList, predPVTList):
                self.assertAlmostEqual(pvt.pos, predPVT.pos)
                self.assertAlmostEqual(pvt.vel, predPVT.vel)
                self.assertAlmostEqual(pvt.t,   predPVT.t)


if __name__ == '__main__':
    unittest.main()

