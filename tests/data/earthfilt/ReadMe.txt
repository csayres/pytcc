Files to help test bin/earthFilt.py

Files whose names begin with "bad" have various errors that should cause earthFilt.py to fail.
Files whose names begin with "pred" are the expected output from earthFilt.py when
processing the correspondingly named file without the "pred" prefix.

The test will create a file "temp.dat" but it should delete the file afterwards.