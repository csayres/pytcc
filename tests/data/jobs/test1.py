"""Example batch job
"""

def run(sr):
    yield sr.waitCmd("show time")
    yield sr.waitSec(0.2)

def end(sr):
    sr.startCmd('broadcast "end"')
