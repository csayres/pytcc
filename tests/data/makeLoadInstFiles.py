#!/usr/bin/env python2
from __future__ import division, absolute_import

import os.path

import RO.SeqUtil

import tcc.base
import tcc.base.testUtils

tcc.base.testUtils.init()
instDir = os.path.join(tcc.base.getDataDir(), "loadInst")

# list of numeric field names
NumericFieldNameList = (
    "secUserFocus",
    "gcUserFocus",
    "scaleFac",
    "primPistCoef",
    "primXTiltCoef",
    "primYTiltCoef",
    "primXTransCoef",
    "primYTransCoef",

    "secPistCoef",
    "secXTiltCoef",
    "secYTiltCoef",
    "secXTransCoef",
    "secYTransCoef",
    "tertPistCoef",
    "tertXTiltCoef",
    "tertYTiltCoef",
    
    "tertXTransCoef",
    "tertYTransCoef",
    "secPistTempCoef",
    "maxScaleFac",
    "iim_ctr",
    "iim_scale",
    "iim_minXY",
    "iim_maxXY",
    "inst_foc",

    "rot_inst_xy",
    "rot_inst_ang",
    "instPosLim",
    "rotID",
    "rot_fixed_phys",
    "rot_offset",
    "rot_scale",
    "rotLim",

    "gcamID",
    "gim_ctr",
    "gim_scale",
    "gim_minXY",
    "gim_maxXY",
    "ptErrProbe",

    "gmechID",
    "gcNomFocus",
)

# dict of fileType: groupSize
GroupSizeDict = dict(
    inst=8,
    gcView=4,
    instPos=2,
    default=1,
)

class ItemData(object):
    def __init__(self, ind, instNum, gcViewNum, instPosNum):
        """Construct an ItemData
        
        @param[in] ind  index of numeric field in NumericFieldNameList
        @param[in] instNum  instrument number
        @param[in] gcViewNum  guide camera view number
        @param[in] instPosNum  instrument position number
        """
        self.ind = ind
        self.fieldName = NumericFieldNameList[ind]
        self.instNum = instNum
        self.gcViewNum = gcViewNum
        self.instPosNum = instPosNum
    
    @property
    def numIn8(self):
        return 1 + (self.ind % 8)
    
    def getDominant(self):
        """Return which file should dominate (one of inst, gcView, instPos, default or None)
        
        @param[in] useGCView  if True we are using a guide camera view file
        """
        for fileType in ("inst", "gcView", "instPos", "default"):
            if fileType == "gcView" and self.gcViewNum == 0:
                continue
            groupSize = GroupSizeDict[fileType]
            if (self.ind/groupSize) % 2:
                return fileType
        return "None"
    
    def getFileVal(self, fileType):
        """Get numeric value for this field for this kind of file
        """
        if fileType == "inst":
            numPrefix = (1000 * self.instNum) + (10 * self.instPosNum)
        elif fileType == "gcView":
            numPrefix = (100 * self.gcViewNum) + (10 * self.instPosNum)
        elif fileType == "instPos":
            numPrefix = (10 * self.instPosNum)
        elif fileType == "default":
            numPrefix = 0
        elif fileType == "None":
            return 0
        else:
            raise RuntimeError("Unknown fileType value: %r" % (fileType,))

        group8Num = 1 + (self.ind / 8)
        numVal = numPrefix + group8Num + (0.1 * self.numIn8)
        return numVal
    
    def getExpectedVal(self):
        """Get numeric value for field
        
        @param[in] instNum  instrument number; 0 if not an inst file or expected result file
        @param[in] gcViewNum  guide camera view number; 0 if not a view file or expected result file
        @param[in] instPosNum  instrument position number; 0 if default file or expected result file
        
        @return numeric value; 0 if all items in this field should be 0
        """
        dominant = self.getDominant()
        if dominant == "inst":
            numPrefix = (1000 * self.instNum) + (10 * self.instPosNum)
        elif dominant == "gcView":
            numPrefix = (100 * self.gcViewNum) + (10 * self.instPosNum)
        elif dominant == "instPos":
            numPrefix = (10 * self.instPosNum)
        elif dominant == "default":
            numPrefix = 0
        elif dominant == "None":
            return 0
        else:
            raise RuntimeError("Unknown dominant value: %r" % (dominant,))

        group8Num = 1 + (self.ind / 8)
        numVal = numPrefix + group8Num + (0.1 * self.numIn8)
        return numVal
    
    def getFieldStr(self, inst, numVal):
        """Return formatted string for this field
        """
        if numVal:
            valMult = 1
        else:
            valMult = 0 # all items in this field should be 0
        instVal = getattr(inst, self.fieldName)
        if RO.SeqUtil.isSequence(instVal):
            valList = [valMult * (numVal + (0.01 * (ind + 1))) for ind in range(len(instVal))]
        else:
            valList = [type(instVal)(valMult * numVal)]
        return "%s  %s" % (self.fieldName, "  ".join(str(val) for val in valList))
    

def instIter(instNum, gcViewNum, instPosNum):
    """An iterator that returns an ItemData for each numeric field in Inst
    """
    for ind in range(len(NumericFieldNameList)):
        yield ItemData(
            ind = ind,
            instNum = instNum,
            gcViewNum = gcViewNum,
            instPosNum = instPosNum,
        )

def getFileName(instNum, gcViewNum, instPosNum, fileType):
    """Create file name for an instrument data file
    
    @param[in] instNum  instrument number
    @param[in] gcViewNum  guide camera view number
    @param[in] instPosNum  instrument position number
    @param[in] fileType  one of "inst", "gcView", "instPos", "default"
    """
    if fileType == "inst":
        fileName = "i_bc%d_inst%d" % (instPosNum, instNum)
    elif fileType == "gcView":
        fileName = "v_bc%d_view%d" % (instPosNum, instNum)
    elif fileType == "instPos":
        fileName = "ip_bc%d" % (instPosNum)
    elif fileType == "default":
        fileName = "default"
    else:
        raise RuntimeError("Unrecognized file type: %r" % (fileType,))
    return fileName

def makeFile(instNum, gcViewNum, instPosNum, fileType):
    """Create an instrument data file
    
    @param[in] fileName  name of file (including suffix)
    @param[in] numPrefix  numeric prefix for field values
    @param[in] groupSize  how many to omit for each groupSize * 2 fields
    """
    fileName = getFileName(instNum=instNum, gcViewNum=gcViewNum, instPosNum=instPosNum, fileType=fileType)
    print "makeFile(instNum=%s, gcViewNum=%s, instPosNum=%s); fileName=%s" % (instNum, gcViewNum, instPosNum, fileName)
    
    inst = tcc.base.Inst()
    filePath = os.path.join(instDir, fileName + ".dat")

    groupSize = GroupSizeDict[fileType]

    with file(filePath, "w") as f:
        f.write("instPos  CA1\n")
        f.write("instName  badInstName\n")
        f.write("gcViewName  badGCViewName\n")

        for itemData in instIter(instNum=instNum, gcViewNum=gcViewNum, instPosNum=instPosNum):
            if itemData.numIn8 == 1:
                f.write("\n")

            if (itemData.ind/groupSize) % 2 == 0:
                # comment out this line
                prefixStr = "# "
            else:
                prefixStr = ""
            numVal = itemData.getFileVal(fileType=fileType)
            f.write("%s%s\n" % (prefixStr, itemData.getFieldStr(inst=inst, numVal=numVal)))

def makeExpectedFile(instNum, gcViewNum, instPosNum):
    print "makeExpectedFile(instNum=%s, gcViewNum=%s, instPosNum=%s)" % (instNum, gcViewNum, instPosNum)
    inst = tcc.base.Inst()
    if gcViewNum > 0:
        fileName = "expected_view%d_inst%d" % (gcViewNum, instNum)
    else:
        fileName = "expected_inst%d" % (instNum,)
    filePath = os.path.join(instDir, fileName + ".dat")
    with file(filePath, "w") as f:
        f.write("instPos  BC%d\n" % (instPosNum,))
        f.write("instName  inst%s\n" % (instNum,))
        if gcViewNum > 0:
            f.write("gcViewName  view%s\n" % (gcViewNum,))
        else:
            f.write("gcViewName  ''\n")

        for itemData in instIter(instNum=instNum, gcViewNum=gcViewNum, instPosNum=instPosNum):
            if itemData.numIn8 == 1:
                f.write("\n")
        
            numVal = itemData.getExpectedVal()
            f.write("%s\n" % itemData.getFieldStr(inst=inst, numVal=numVal))

def makeAllFiles():
    """Make all loadInst test files
    """
    makeFile(instNum=0, gcViewNum=0, instPosNum=0, fileType="default")
    fileNameSet = set()

    for instNum, instPosNum, gcViewNum in (
        (1, 1, 0),
        (1, 1, 1),
        (2, 1, 0),
        (3, 2, 0),
        (4, 2, 0),
    ):
        for fileType in ("inst", "gcView", "instPos"): # default handled above (since there is only one)
            if fileType == "gcView" and gcViewNum == 0:
                continue

            fileName = getFileName(instNum=instNum, gcViewNum=gcViewNum, instPosNum=instPosNum, fileType=fileType)
            if fileName in fileNameSet:
                continue
            
            makeFile(instNum=instNum, gcViewNum=gcViewNum, instPosNum=instPosNum, fileType=fileType)
            fileNameSet.add(fileName)
        
        makeExpectedFile(instNum=instNum, gcViewNum=gcViewNum, instPosNum=instPosNum)

def showOrder():
    """Show the expected order of fields"""

if __name__ == "__main__":
    makeAllFiles()
