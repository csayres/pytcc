How to generate this data on a TCC:

! If you want to generate new input data for the TCC, tweak makeMassCCFPData.py as desired
! and run it to generate a file "massccfp_in.dat".

! Copy massccfp_in.dat and massccfp_inst.dat to taxe:[.test]
! Run the TCC
$ telrun
! Set instrument data
set inst=spicam     ! needed to set the rotator ("set block inst" won't enable the new rotator)
set block inst/in=massccfp_inst.dat
axis status         ! to be sure the rotator is present
show block inst     ! to be sure the correct data was loadeda
! Track an observed position with horizon rotation
! The exact values don't matter, but I like to use round numbers
track 45, 35 obs/rottype=horiz/rotang=75
! Wait for the slew to start (TCCStatus="SSS"), then...
offset/computed/pabs boresight 0.011, -0.034
! Wait for the resulting slew to end (TCCStatus="TTT")...
show obj            ! check Boresight, RotType and RotPos
! Quit the TCC and run masscoordconvwithfp.
quit
$ run masscoordconvwithfp
! if it complains that the appgeo data changed, run it again
! Rename the output data to indicate the rotation used
$ rename massccfp_obj.dat; massccfp_obj_horizrot.dat;
$ rename massccfp_out.dat; massccfp_out_horizrot.dat;
! Save massccfp_out_horizrot.dat and massccfp_obj_horizrot.dat

Now run again with object rotation, using "objrot" instead of "horizrot" for file names:
$ telrun
rotate -55 object
! Wait for the slew to start (TCCStatus="SSS"), then...
offset/computed/pabs boresight 0.074, 0.022
! Wait for the resulting slew to end (TCCStatus="TTT")...
show obj            ! check Boresight, RotType and RotPos
! Quit the TCC and run masscoordconvwithfp
quit
$ run masscoordconvwithfp
! if it complains that the appgeo data changed, run it again
! Rename the output data to indicate the rotation used
$ rename massccfp_obj.dat; massccfp_obj_objrot.dat;
$ rename massccfp_out.dat; massccfp_out_objrot.dat;
! Save massccfp_out_objrot.dat and massccfp_obj_objrot.dat
