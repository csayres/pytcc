#!/usr/bin/env python2
from __future__ import division, absolute_import
"""
Generate data for the old TCC program massCoordConvWithFP in ...src/subr/axe/test

Each line should contain:
  fromSysCode fromDate fromPos1 fromPos2 fromVel1 fromVel2 fromTAI fromPM1 fromPM2 fromParallax fromRadVel refCoA refCoB toSysCode toDate
  where:
  - fromDate/toDate is guide probe number for guide image or guide probe coordinates
  - Mount, Phys and None coordinates are not supported

Subtleties:
- Do not convert from FK4 with zero PM and nonzero radVel; the TCC treats this as zero space motion,
  but the new conversion code does not (and could not easily do so without some ugly hacks).

Old coordinate system codes:
    -9 = "GuideImage",
    -8 = "GuideProbe",
    -7 = "Rotator",
    -6 = "Instrument",

    -5 = "Mount",
    -4 = "Physical",

    -3 = "Observed",
    -2 = "AppTopo",
    -1 = "AppGeo",

    0 = "None",

    1 = "FK4",
    2 = "FK5",
    3 = "Gal",
    4 = "ICRS",
"""
import itertools

# start simple: obs and FP assuming the telescope is tracking a given base observed position.
# that makes it easier to generate the necessary data for the old TCC
# and avoids having to work out reasonable values for ICRS, etc.

# to support all coordinate systems:
# - pick a TAI reasonably some minutes in the future
# - generate an obj block at that TAI date using the desired instrument data
#   and with some specified input observed position (use observed to make the az/at speeds low)
# - use that information to generate sensible values of ICRS, Gal, etc. (values that aren't
#   too far from the boresight, so focal plane conversions make sense); these are only required
#   for conversions to/from focal plane coordinates
# - generate an old obj block at the approximate date (e.g. by fake tracking the same observed position)
# - run massCoordConvWithFP.exe on the old TCC (while the AppGeo data is fresh!)
baseObsPos = (45, 35)

coordSysList = (-3, -6, -7, -8, -9)

def getDateIter(sysCode):
    """Return a list of dates given a coordinate system code
    """
    if sysCode > 0:
        # mean
        dateList = (1960, 2010)
    elif sysCode == -1:
        # apparent geocentric; 0 is relevant
        dateList = (0, 1999.5, 2015)
    elif sysCode in (-8, -9):
        # guide image or guide probe; return a list of probe numbers
        dateList = (1, 2)
    else:
        # apparent topocentric or focal plane; 0 is the only reasonable choice
        dateList = (0,)
    return dateList

def getPosVelIter(sysCode):
    """Return an iterator over pos1, pos2, vel1, vel2 given a coordinate system code
    """
    if sysCode <= -6:
        # focal-plane based system
        pos1List = (0, 1.05)
        pos2List = (0, -0.31)
    elif sysCode == -3:
        pos1List = (baseObsPos[0], baseObsPos[0] - 0.5)
        pos2List = (baseObsPos[1], baseObsPos[1] + 0.1)
    else:
        raise RuntimeError("sysCode=%s not yet supported" % (sysCode,))
    vel1List = (0, -0.01)
    vel2List = (0, 0.02)
    return itertools.product(pos1List, pos2List, vel1List, vel2List)

def getPMPxRVIter(sysCode):
    """Get an iterator over proper motion 1, proper motion 2, parallax and radial velocity given a coordinate system code
    """
    valList = [(0, 0, 0, 0)]
    if fromSysCode > 0:
        # from mean; parallax, etc. are relevant
        valList += [
            (5, -3, 7, 10),
        ]
    return valList

def getRefCoIter(fromSysCode, toSysCode):
    if fromSysCode > -2 and toSysCode > -2:
        # convert between mean, apparent geocentric or apparent topocentric coordinates; refraction coefficients not used
        return [(1.2e-2,  -1.3e-5)]
    else:
        # refraction coefficients will (probably) be used
        return [(1.2e-2,  -1.3e-5), (2.2e-2, -1.7e-5)]

fromTAI = 0 # could specify a value but sane results require it to be close to current TAI when using it
            # (unless vel1 and vel2 are tiny)
for fromSysCode in coordSysList:
    for fromDate in getDateIter(fromSysCode):
        for fromPos1, fromPos2, fromVel1, fromVel2 in getPosVelIter(fromSysCode):
            for fromPM1, fromPM2, fromParallax, fromRadVel in getPMPxRVIter(fromSysCode):
                for toSysCode in coordSysList:
                    for toDate in getDateIter(toSysCode):
                        for refCoA, refCoB in getRefCoIter(fromSysCode, toSysCode):
                            print fromSysCode, fromDate, fromPos1, fromPos2, fromVel1, fromVel2, fromTAI, \
                                fromPM1, fromPM2, fromParallax, fromRadVel, refCoA, refCoB, toSysCode, toDate
