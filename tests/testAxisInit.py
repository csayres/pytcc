#!/usr/bin/env python2
from __future__ import division, absolute_import

import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")

from twisted.trial.unittest import TestCase
from twisted.internet.defer import Deferred
from twisted.internet import reactor

from opscore.actor import CmdVar

from tcc.actor import TCC35mDispatcherWrapper
from tcc.base import testUtils
testUtils.init(__file__)


class TestAxisInit(TestCase):

    def addCmdCallbackPass(self, thisVar):
        self.addCmdCallback(thisVar, shouldFail=False)

    def addCmdCallbackFail(self, thisVar):
        self.addCmdCallback(thisVar, shouldFail=True)

    def addCmdCallback(self, thisVar, shouldFail=False):
        self.assertTrue(thisVar.didFail==shouldFail)

    def sendCmd(self, cmdStr, deferred=None, shouldFail=False):
        def checkFail(thisVar):
            self.assertTrue(thisVar.didFail==shouldFail)
            if deferred is not None:
                deferred.callback("foo")
        cmdVar = CmdVar(
            actor = self.dw._dictName,
            cmdStr = cmdStr,
            callFunc=checkFail
        )
        self.dw.dispatcher.executeCmd(cmdVar)
        return cmdVar

    def setUp(self):
        self.dw = TCC35mDispatcherWrapper()
        return self.dw.readyDeferred

    def testInitGotStatus(self):
        # self.sendCmd("axis status")
        print "testInitGotStatus"
        azDev = self.dw.actor.axisDevSet.get("az")
        azInd = self.dw.actor.axisDevSet.getIndex("az")
        azStatusWordFudged = self.dw.actor.obj.axisStatusWord[azInd] + 1
        self.dw.actor.obj.axisStatusWord[azInd] = azStatusWordFudged
        azDev.status.statusWord = azStatusWordFudged
        # manually put this device status on the deviceSet
        self.dw.actor.axisDevSet._prevReportedStatusList[azInd] = azDev.status.copy()
        # this will trigger azStat output (it is only output if a change was detected)
        d1, cmd = self.dw.queueCmd("axis init")
        def checkGotStatus(cmd):
            if cmd.isDone:
                # if they are no longer equal, the model has been updated
                self.assertFalse(azStatusWordFudged == self.dw.actor.obj.axisStatusWord[azInd])
                self.assertFalse(cmd.didFail)
        cmd.addCallback(checkGotStatus)
        return d1

    def testInit(self):
        # self.sendCmd("axis status")
        print "testInit"
        d1, cmd = self.dw.queueCmd("axis init")
        cmd.addCallback(self.addCmdCallbackPass)
        return d1

    def testInitInterrupt(self):
        print "testInitInterrupt"
        self.sendCmd("axis status", shouldFail=True)
        d1, cmd = self.dw.queueCmd("axis init")
        cmd.addCallback(self.addCmdCallbackPass)
        return d1

    def testInitInterrupt2(self):
        print "testInitInterrupt2"
        d = Deferred()
        self.sendCmd("axis status", shouldFail=True)
        self.sendCmd("axis init", deferred=d, shouldFail=False)
        return d

    def testInitInterrupt3(self):
        print "testInitInterrupt3"
        d = Deferred()
        self.sendCmd("axis init", shouldFail=True)
        self.sendCmd("axis init", deferred=d, shouldFail=False)
        return d

    def _timingTests(self, timeDelay, shouldFail):
        d = Deferred()
        self.sendCmd("axis status", shouldFail=shouldFail)
        kwArgs = {"cmdStr":"axis init", "deferred":d, "shouldFail": False}
        reactor.callLater(timeDelay, self.sendCmd, **kwArgs)
        return d

    def testTime1(self):
        print "testTime1"
        return self._timingTests(0.01, True)

    def testTime2(self):
        print "testTime2"
        return self._timingTests(0.08, False)

    def testInitStop(self):
        print "testInitStop"
        d = Deferred()
        print "starting init test"
        self.sendCmd("axis init", shouldFail=False)
        self.sendCmd("axis stop", deferred=d, shouldFail=False)
        return d

    def tearDown(self):
        print "tearDown"
        d = self.dw.close()
        return d



if __name__ == '__main__':
    from unittest import main
    main()

