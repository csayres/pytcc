#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest
import os
import itertools

from tcc.cmd.track import loadChebyshevFile
from tcc.base import Obj
from tcc.base import testUtils
testUtils.init(__file__)
from tcc.base.trackTestUtils import generateChebyFile
from tcc.actor.tccCmdParser import TCCCmdParser

DataDir = os.path.join(os.path.abspath(os.path.dirname(__file__)), "data")

class FakeActor(object):
    """just needs a writeToOneUser method
    """
    def writeToOneUser(self, *args, **kwargs):
        # do nothing
        pass
    def writeToUsers(self, *args, **kwargs):
        # do nothing
        pass

class FakeUserCmd(object):
    """just needs a parsedCmd attribute
    """
    def __init__(self, line2parse):
        """@param[in] line2parse  line of text to run through the parser
        """
        self.parser = TCCCmdParser()
        self.parsedCmd = self.parser.parseLine(line2parse)

class TestLoadCheby(unittest.TestCase):
    objBlkAttrs = {
            "useCheby": None,
            "userSys": "getName",
            "chebyUser1": "__repr__",
            "chebyUser2": "__repr__",
            "chebyDist": "__repr__",
        }

    def getValueList(self, obj):
        """Return a list of values to compare before and after loading a cheby file

        @param[in] obj, instance of the object block
        """
        valueList = []
        for attr1, attr2 in self.objBlkAttrs.iteritems():
            value = getattr(obj, attr1)
            if attr2 is not None:
                value = getattr(value, attr2)()
            valueList.append(value)
        return valueList

    def testLoadCheby(self):
        obj = Obj()
        valListPrior = self.getValueList(obj)
        chebyFilePath = os.path.join(DataDir, "tlc_cheby.dat")
        generateChebyFile(chebyFilePath)
        try:
            trackCmd = 'track/chebyshev="%s"' % (chebyFilePath,)
            userCmd = FakeUserCmd(trackCmd)
            actor = FakeActor()
            loadChebyshevFile(actor, userCmd, obj)
            valListAfter = self.getValueList(obj)
            for val1, val2 in itertools.izip(valListPrior, valListAfter):
                self.assertFalse(val1==val2)
        finally:
            os.unlink(chebyFilePath)



if __name__ == "__main__":
    unittest.main()