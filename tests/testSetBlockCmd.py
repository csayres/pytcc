#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Test various SET commands, not including SET BLOCK
"""
import os
import copy
import functools

from twisted.internet import reactor
from twisted.internet.defer import gatherResults
from twisted.trial.unittest import TestCase
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from opscore.actor import CmdVar

import tcc.base
import tcc.base.testUtils
tcc.base.testUtils.init(__file__)

from tcc.cmd.setBlock import blockAttrDict
from tcc.base.wrapperTestCase import WrapperTestCase

DataDir = tcc.base.getDataDir()

class TestSetBlockCmd(WrapperTestCase, TestCase):

    def getBlock(self, blockName):
        """Get a block from self.tccActor, given its name
        """
        return getattr(self.actor, blockAttrDict[blockName])

    def _keyVals2Lines(self, parValDict):
        """Convert dictionary of parameter names and values to command lines
        @param[in] parValDict, keyed by parameter name value to be set
        """
        lines = []
        for par, val in parValDict.iteritems():
            # if value is a list
            try:
                strVal = ','.join([str(v) for v in val])
            except TypeError:
                # value is not a list, simply cast it as a string
                strVal = str(val)
            lines.append('2 ' + par + ' ' + strVal) # leading integer is present in cmds arriving at tccActor.parseAndDispatchCmd
        lines.append('2 ^Z') # exit
        return lines

    def _doBlock(self, blockName, cmdStr, parValDict):
        """Test the specified parameters of the specified block

        @param[in] blockName  name of block (case doesn't matter)
        @param[in] cmdStr  command string with input file specified
        @param[in] parValDict  a dictionary, keyed by block attribute name, value to be set on block

        Read the current valueList on the block, set new valueList and check that the new values are correct.
        Then revert to old valueList by loading from a .dat file, check that values were reset to their original
        values. Thus test both ways of manipulating the block: manually, and automatically (via file).
        """
        origParValDict = copy.deepcopy(parValDict) # will hold original values
        def setOrigVals(foo):
            block = self.getBlock(blockName)
            for par in parValDict:
                # initialize original values
                origParValDict[par] = getattr(block, par)
        d1, cmd1 = self.dw.queueCmd(cmdStr, callFunc=self.checkCmdFail) # set block from file
        cmd1.addCallback(setOrigVals)
        lines = self._keyVals2Lines(parValDict)
        # enter raw text mode...
        d2, cmd2 = self.dw.queueCmd(
            "set block " + blockName.lower(),
            callFunc = self.checkCmdFail,
        )
        cmd2.addCallback(functools.partial(self._verifyBlock, blockName=blockName, parValDict=parValDict))
        # dispatch raw input lines....callLater
        # to allow setBlock to hijack the socket
        reactor.callLater(0.1, self._sendRawLines, lines)
        # when that command finishes, check that the block has been updated
        d3, cmd3 = self.dw.queueCmd(
            cmdStr,
            callFunc = self.checkCmdFail,
        ) # set block from file
        cmd3.addCallback(functools.partial(self._verifyBlock, blockName=blockName, parValDict=origParValDict))
        return gatherResults([d1, d2, d3])

    def _sendRawLines(self, lines):
        """Turn a line into a cmdVar, dispatch it
        """
        # dispatch cmd var lines....
        for line in lines:
            # dispatch a line of raw text
            cmdVar = CmdVar (
                cmdStr = line,
                actor = "tcc",
            )
            self.dispatcher.executeCmd(cmdVar)

    def _verifyBlock(self, cmdVar, blockName, parValDict):
        """Verify that param=value for parValDict have been successfully
        put on the block.
        """
        block = self.getBlock(blockName)
        for par, val in parValDict.iteritems():
                blockVal = getattr(block, par)
                if RO.SeqUtil.isSequence(blockVal):
                    self.assertEqual(tuple(blockVal), tuple(val))
                else:
                    self.assertEqual(blockVal, val)

    def testSetBlockTune1(self):
        parValDict = {
            'slewFudge': 2.390,
            'maxUsers' : 40,
        }
        cmdStr = "set block tune/input=tune"
        return self._doBlock(blockName="Tune", cmdStr=cmdStr, parValDict = parValDict)

    def testSetBlockTune2(self):
        parValDict = {
            'slewFudge': 2.390,
            'maxUsers' : 40,
        }
        cmdStr = 'set block tune/input="%s"' % ("tune.dat",)
        return self._doBlock(blockName="Tune", cmdStr=cmdStr, parValDict = parValDict)

    def testSetBlockTune3(self):
        parValDict = {
            'slewFudge': 2.390,
            'maxUsers' : 40,
        }
        cmdStr = 'set block tune/input="%s"' % (os.path.join(DataDir, "tune"))
        return self._doBlock(blockName="Tune", cmdStr=cmdStr, parValDict = parValDict)

    def testSetBlockTune4(self):
        parValDict = {
            'slewFudge': 2.390,
            'maxUsers' : 40,
        }
        cmdStr = 'set block tune/input="%s"' % (os.path.join(DataDir, "tune.dat"))
        return self._doBlock(blockName="Tune", cmdStr=cmdStr, parValDict = parValDict)

    def testSetBlockAxeLim(self):
        parValDict = {
            'maxDTime': 2.390,
            'maxPos': [6, 6, 6],
        }
        cmdStr = "set block axelim/input=axelim"
        return self._doBlock(blockName="AxeLim", cmdStr=cmdStr, parValDict = parValDict)

    def testSetBlockObj(self):
        parValDict = {
            'rotType': 1,
            'useCheby': True
        }
        cmdStr = "set block obj/input=obj"
        return self._doBlock(blockName="Obj", cmdStr=cmdStr, parValDict = parValDict)

    def testSetBlockWeath(self):
        parValDict = {
            'airTemp': 16.,
            'secFrontTemp': 20.,
        }
        cmdStr = 'set block weath/input="%s"' % (os.path.join(DataDir, "weath.dat"),)
        return self._doBlock(blockName="Weath", cmdStr=cmdStr, parValDict = parValDict)

# TelMod will need special handling; it can be loaded from a file, but not set field-by-field
#     def testTelMod(self):
#         parValDict = {
#             'IE',
#             'CA',
#         ]
#         valueList = [16., -20.]
#         return self._doBlock(blockName="TelMod", cmdStr=cmdStr, parValDict = parValDict)

    # def testSetInst(self):
    #     instList=["dis", "echelle", "nicfps", "spicam", "tspec"]
if __name__ == '__main__':
    from unittest import main
    main()

