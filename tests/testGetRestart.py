#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest

from twistedActor import CommandError
from tcc.actor import TCCCmdParser
from tcc.parse import getRestart

class GetRestartTest(unittest.TestCase):
    def setUp(self):
        self.parser = TCCCmdParser()

    def testOffset(self):
        """Test the offset command, which defaults to not restarting any axes
        """
        self.basicTest("offset arc 0, 0", defValue=(False, False, False))

    def testRotate(self):
        """Test the rotate command, which defaults to restarting only the rotator
        """
        self.basicTest("rotate 0 obj", defValue=(False, False, True))

    def testTrack(self):
        """Test the track command, which defaults to restarting all axes
        """
        self.basicTest("track", defValue=(True, True, True))

    def basicTest(self, cmdPrefix, defValue):
        for restartStr, desResWithRot in (
            ("", defValue),
            ("/Restart", (True, True, True)),
            ("/Restart=Az", (True, False, False)),
            ("/Restart=Tel1", (True, False, False)),
            ("/Restart=(Az, Tel1)", (True, False, False)),
            ("/Restart=Alt", (False, True, False)),
            ("/Restart=Rot", (False, False, True)),
            ("/Restart=All", (True, True, True)),
            ("/Restart=(Az)", (True, False, False)),
            ("/Restart=(All, noalt, norot)", (True, False, False)),
            ("/Restart=(Alt)", (False, True, False)),
            ("/Restart=(all, norot, noaz)", (False, True, False)),
            ("/Restart=(Rot)", (False, False, True)),
            ("/Restart=(all, noalt, noaz)", (False, False, True)),
            ("/Restart=(All)", (True, True, True)),
            ("/Restart=(Az, Alt)", (True, True, False)),
            ("/Restart=(Az, Tel2)", (True, True, False)),
            ("/Restart=(All, NoRot)", (True, True, False)),
            ("/Restart=(Az, Rot)", (True, False, True)),
            ("/Restart=(All, NoAlt)", (True, False, True)),
            ("/Restart=(All, NoTel2)", (True, False, True)),
            ("/Restart=(Rot, Alt)", (False, True, True)),
            ("/Restart=(all, noaz)", (False, True, True)),
            ("/Restart=(noaz)", (False, True, True)),
            ("/Restart=(noalt)", (True, False, True)),
            ("/Restart=(norot)", (True, True, False)),
            ("/Restart=(noall)", (False, False, False)),
            ("/NoRestart", (False, False, False)),
        ):
            cmdStr = cmdPrefix + restartStr
            parsedCmd = self.parser.parseLine(cmdStr)

            for rotExists in (False, True):
                desRes = list(desResWithRot)
                if not rotExists:
                    desRes[2] = False

                res = getRestart(
                    qual = parsedCmd.qualDict["restart"],
                    rotExists = rotExists,
                )

                try:
                    self.assertEqual(tuple(res), tuple(desRes))
                except Exception:
                    print "failed on cmdStr=%r; defValue=%s; rotExists=%s; res=%s; desRes=%s" % (cmdStr, defValue, rotExists, res, desRes)
                    raise

        for restartStr in (
            "/Restart=(az, noaz)",
            "/Restart=(az, alt, noaz)",
            "/Restart=(all, az, noaz)",
            "/Restart=(rot, az, alt, noaz)",
            "/Restart=(tel1, noaz)",
            "/Restart=(tel1, tel2, noaz)",
            "/Restart=(tel2, tel1, rot, noaz)",
            "/Restart=(alt, noalt)",
            "/Restart=(az, alt, noalt)",
            "/Restart=(rot, az, alt, noalt)",
            "/Restart=(tel2, noalt)",
            "/Restart=(tel1, tel2, noalt)",
            "/Restart=(tel2, all, noalt)",
            "/Restart=(rot, norot)",
            "/Restart=(az, rot, norot)",
            "/Restart=(rot, az, alt, norot)",
            "/Restart=(all, rot, norot)",
            "/NoRestart=all",
            "/NoRestart=(all)",
            "/NoRestart=(az, alt)",
        ):
            cmdStr = cmdPrefix + restartStr
            parsedCmd = self.parser.parseLine(cmdStr)

            for rotExists in (False, True):
                try:
                    self.assertRaises(
                        CommandError,
                        getRestart,
                        qual = parsedCmd.qualDict["restart"],
                        rotExists = rotExists,
                    )
                except Exception:
                    print "exception not raised for cmdStr=%r; defValue=%s; rotExists=%s" % (cmdStr, defValue, rotExists)
                    raise


if __name__ == '__main__':
    unittest.main()
