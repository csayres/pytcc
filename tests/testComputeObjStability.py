#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest

import coordConv

import tcc.actor
import tcc.axis
import tcc.base
import tcc.base.testUtils

tcc.base.testUtils.init()

class ComputeObjStabilityTest(unittest.TestCase):
    """Test that computeObj is stable with varying targetMount
    """
    def setUp(self):
        self.tccActor = tcc.actor.FakeTCC35mActor(userPort=0)
        self.posRefCatalog = self.tccActor.posRefCatalog

    def tearDown(self):
        self.posRefCatalog = None
        self.tccActor = None

    def testStability(self):
        """Test stability

        tcc.axis.computeObj ought to produce the same result (to within 0.01"),
        regardless of the initial value of targetMount.
        However, until 2014-03-21 it did not (due to a misfeature in mountFromObs); hence this test.
        """
        self.tccActor.inst = tcc.base.loadInst(tcc.base.getInstDir(), "spicam")
        self.tccActor.axeLim.setRotLim(self.tccActor.inst)
        obj = self.tccActor.obj
        currTAI = tcc.base.tai()
        # use a target sufficiently high that it is always up
        obj.userPos[0] = coordConv.PVT(0, 0, currTAI)
        obj.userPos[1] = coordConv.PVT(85, 0, currTAI)
        obj.userSys = coordConv.makeCoordSys("icrs", 2000)
        obj.rotUser = coordConv.PVT(0, 0, currTAI)
        obj.rotType = tcc.base.RotType_Object
        obj.azWrapPref = tcc.base.WrapType_Negative
        for axis in range(3):
            obj.axisCmdState[axis] = tcc.base.AxisState_Tracking
            obj.targetMount[axis] = coordConv.PVT(15, 0, currTAI)

        for iter in range(3):
            prevTargetMount = [coordConv.PVT(val) for val in obj.targetMount]
            tcc.axis.computeObj(
                obj = obj,
                doWrap = True,
                doRestart = [True]*3,
                reportLim = False,
                earth = self.tccActor.earth,
                inst = self.tccActor.inst,
                telMod = self.tccActor.telMod,
                axeLim = self.tccActor.axeLim,
                tai = currTAI,
            )
            if iter > 0:
                # don't test the first iteration because targetMount was initialized to crazy values
                for axis in range(3):
                    self.assertTrue(obj.targetMount[axis].isfinite())
                    self.assertAlmostEqual(
                        coordConv.wrapNear(obj.targetMount[axis].pos, prevTargetMount[axis].pos),
                        obj.targetMount[axis].pos, places=5)
                    self.assertAlmostEqual(obj.targetMount[axis].vel, prevTargetMount[axis].vel, places=5)
                    self.assertEqual(obj.targetMount[axis].t, prevTargetMount[axis].t)



if __name__ == '__main__':
    unittest.main()
