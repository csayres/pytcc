#!/usr/bin/env python2
from __future__ import division, absolute_import

import os
import unittest

from tcc.base import Earth, Obj

FilePath = os.path.join(os.path.abspath(os.path.dirname(__file__)), "data", "earthpred_brief.dat")

def wtAvg(val0, val1, frac):
    """Compute weighted average: val0 * (1-frac) + val1 * frac
    """
    if not (0 <= frac <= 1):
        raise RuntimeError("frac=%s; must be in range [0, 1]" % (frac,))
    return (val0 * (1.0 - frac)) + (val1 * frac)

class TestEarth(unittest.TestCase):
    def testCopyConstructor(self):
        """Test copy constructor
        """
        forTAI = 4848595236.1
        earth = Earth()
        with file(FilePath) as f:
            earth.loadPredictions(f, forTAI=forTAI)
        earthCopy = Earth(earth)
        self.assertEqual(repr(earth), repr(earthCopy))
        
    def testLoadPredictions(self):
        """Test Earth.loadPredictions, including a leap second
        """
        tai1 = None
        tai2 = None
        ut1_tai1 = None
        poleX1 = None
        poleY1 = None
        ut1_tai1Slope1 = None
        dxSlope1 = None
        dySlope1 = None

        for forTAI, predUTC, ut1_utc, ut1_tai0, poleX0, poleY0 in (
            (4848595236.1, 56118, -36, -34.58700, 0.1080, 0.4062),
            (4848508836.1, 56117, -36, -34.58682, 0.1065, 0.4067),
            (4848422435.1, 56116, -35, -34.58658, 0.1050, 0.4072),
            (4848336035.1, 56115, -35, -34.58634, 0.1035, 0.4075),
        ):
            tai0 = round(forTAI)
            poleX0 /= 3600.0
            poleY0 /= 3600.0
            earth = Earth()
            with file(FilePath) as f:
                earth.loadPredictions(f, forTAI=forTAI)
            self.assertEqual(earth.utcDay, predUTC)
            self.assertEqual(earth.startTAI[0], tai0)
            self.assertEqual(earth.ut1_taiIntercept[0], ut1_tai0)
            self.assertAlmostEqual(earth.poleInterceptX[0], poleX0)
            self.assertAlmostEqual(earth.poleInterceptY[0], poleY0)
            
            if tai1 != None:
                self.assertEqual(earth.startTAI[1], tai1)
                if tai2 is not None:
                    self.assertEqual(earth.startTAI[2], tai2)
                self.assertEqual(earth.ut1_taiIntercept[1], ut1_tai1)
                self.assertAlmostEqual(earth.poleInterceptX[1], poleX1)
                self.assertAlmostEqual(earth.poleInterceptY[1], poleY1)
                
                dt = tai1 - tai0
                ut1_tai1Slope0 = (ut1_tai1 - ut1_tai0) / dt
                dxSlope0 = (poleX1 - poleX0) / dt
                dySlope0 = (poleY1 - poleY0) / dt
                self.assertAlmostEqual(earth.ut1_taiSlope[0], ut1_tai1Slope0)
                self.assertAlmostEqual(earth.poleSlopeX[0], dxSlope0)
                self.assertAlmostEqual(earth.poleSlopeY[0], dySlope0)
                
                if ut1_tai1Slope1 is not None:
                    self.assertAlmostEqual(earth.ut1_taiSlope[1], ut1_tai1Slope1)
                    self.assertAlmostEqual(earth.poleSlopeX[1], dxSlope1)
                    self.assertAlmostEqual(earth.poleSlopeY[1], dySlope1)

                ut1_tai1Slope1 = ut1_tai1Slope0
                dxSlope1 = dxSlope0
                dySlope1 = dySlope0
            
            tai2 = tai1
            tai1 = tai0
            ut1_tai1 = ut1_tai0
            poleX1 = poleX0
            poleY1 = poleY0

    def testLoadPredictionErrors(self):
        """Test exceptions raised by Earth.loadPredictions
        """
        for forTAI in (4848336034.9999, 4848681636.0001):
            with file(FilePath) as f:
                earth = Earth()
                self.assertRaises(RuntimeError, earth.loadPredictions, f, forTAI)

    def testUpdateSite(self):
        """Test earth.updateSite"""
        data = (
            (4848336035, -35, -34.58634, 0.1035, 0.4075),
            (4848422435, -35, -34.58658, 0.1050, 0.4072),
            (4848508836, -36, -34.58682, 0.1065, 0.4067),
            (4848595236, -36, -34.58700, 0.1080, 0.4062),
            (4848681636, -36, -34.58711, 0.1096, 0.4057),
        )
        for i in range(4):
            tai0, utc_tai0, ut1_tai0, poleX0, poleY0 = data[i+0]
            tai1, utc_tai1, ut1_tai1, poleX1, poleY1 = data[i+1]
            poleX0 /= 3600.0
            poleY0 /= 3600.0
            poleX1 /= 3600.0
            poleY1 /= 3600.0
            earth = Earth()
            with file(FilePath) as f:
                earth.loadPredictions(f, forTAI=tai0 + 0.1)
            
            for frac in (0.0001, 0.66, 0.9999):
                obj = Obj()
                site = obj.site
                earth.updateSite(site, wtAvg(tai0, tai1, frac))
                self.assertEqual(site.utc_tai, utc_tai0)
                self.assertAlmostEqual(site.ut1_tai, wtAvg(ut1_tai0, ut1_tai1, frac))
                self.assertAlmostEqual(site.poleX, wtAvg(poleX0, poleX1, frac))
                self.assertAlmostEqual(site.poleY, wtAvg(poleY0, poleY1, frac))

    def testComputeError(self):
        """Test error handling in earth.compute"""
        for tai in (
            4848336035,
            4848422435,
            4848508836,
            4848595236,
        ):
            obj = Obj()
            site = obj.site
            loadTAI = tai + 0.1
            minTAI = tai - 0.0001
            earth = Earth()
            with file(FilePath) as f:
                earth.loadPredictions(f, forTAI=loadTAI)
            
            maxTAI = earth.startTAI[2] + 0.0001
                
            self.assertRaises(RuntimeError, earth.updateSite, site, minTAI)
            self.assertRaises(RuntimeError, earth.updateSite, site, maxTAI)


if __name__ == '__main__':
    unittest.main()
