This package provides telescope control for telescopes at Apache Point Observatory.

Documentation includes:

- the main documentation page: doc/index.html
- installation instructions: doc/OperatorsManual.html#InstallingSoftware
- software license: doc/license.txt
