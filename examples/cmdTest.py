#!/usr/bin/env python2
from __future__ import division, absolute_import
"""
A simple program that will parse tcc command input from the command line.
Output is printed to the screen.
"""
from tcc.actor import TCCCmdParser

parser = TCCCmdParser()

while True:
    cmdStr = raw_input('tcc> ')
    try:
        parsedCmd = parser.parseLine(cmdStr)
    except Exception as e:
        print "Failed to parse %r: %s" % (cmdStr, e)
        continue
    
    parsedCmd.printData()
