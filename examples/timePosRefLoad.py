#!/usr/bin/env python2
import os.path
import time

import tcc.axis
import tcc.base

dataDir = tcc.base.getDataDir()
fullPosRefCatPath = os.path.join(dataDir, "posRefCatalog.dat")

startTime = time.time()
posRefCat = tcc.axis.PosRefCatalog(fullPosRefCatPath)
duration = time.time() - startTime
numStars = len(posRefCat)
print "Loaded %s stars in %0.1f seconds: %0.1f stars/second" % (numStars, duration, numStars/duration)