#!/usr/bin/env python2
from __future__ import division, absolute_import

import os
import sys

import coordConv

import tcc.base
import tcc.axis

UseCurrTAI = False
ShowBlocks = True

if UseCurrTAI:
    print "Using current TAI; the earth orientation predictions must be up-to-date"
    currTAI = tcc.base.tai()
    dataDir = tcc.base.getDataDir()
else:
    print "Using a saved TAI and saved data from tests/data"
    currTAI = 4848595200.0
    dataDir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "tests", "data")
instDir = os.path.join(dataDir, "inst")

earthFilePath = os.path.join(dataDir, "earthpred_brief.dat")
print "earth file =", earthFilePath
print "currTAI =", currTAI
earth = tcc.base.Earth()
with file(earthFilePath, "rU") as f:
    earth.loadPredictions(f, forTAI=currTAI)
if ShowBlocks:
    earth.dump(sys.stdout)
    print

weath = tcc.base.Weath()
with file(os.path.join(dataDir, "weath.dat"), "rU") as f:
    weath.load(f)
if ShowBlocks:
    weath.dump(sys.stdout)
    print

obj = tcc.base.Obj()
obj.userPos[0] = coordConv.PVT(0, 0, currTAI)
obj.userPos[1] = coordConv.PVT(10, 0, currTAI)
userSysName = "obs"

obj.userSys = coordConv.makeCoordSys(userSysName, 2000) # correct the date next
obj.userSys.setDate(obj.userSys.dateFromTAI(currTAI))
obj.rotType = tcc.base.RotType_Object
obj.rotUser = coordConv.PVT(0, 0, currTAI)
obj.userArcOff[0] = coordConv.PVT(0, 0, currTAI)
obj.userArcOff[1] = coordConv.PVT(0, 0, currTAI)
obj.objInstXY[0] = coordConv.PVT(0, 0, currTAI)
obj.objInstXY[1] = coordConv.PVT(0, 0, currTAI)
obj.guideOff[0] = coordConv.PVT(0, 0, currTAI)
obj.guideOff[1] = coordConv.PVT(0, 0, currTAI)
obj.guideOff[2] = coordConv.PVT(0, 0, currTAI)
obj.calibOff[0] = coordConv.PVT(0, 0, currTAI)
obj.calibOff[1] = coordConv.PVT(0, 0, currTAI)
obj.calibOff[2] = coordConv.PVT(0, 0, currTAI)
obj.site.wavelen = 5500

site = obj.site # for convenient access

earth.updateSite(obj.site, currTAI)
weath.updateSite(obj.site)

if ShowBlocks:
    print "Obj block before conversion"
    print obj
    print

# compute refraction coefficients
print "obj.site:"
print obj.site
print

axeLim = tcc.base.AxeLim()
with file(os.path.join(dataDir, "axeLim.dat"), "rU") as f:
    axeLim.load(f)
    # note: rotator limits must be set from loaded inst data

inst = tcc.base.loadInst(instDir, "centered_na2")
axeLim.setRotLim(inst)
if ShowBlocks:
    print axeLim
    print
    print inst
    print

telmodPath = os.path.join(dataDir, "telmod_null.dat")
telMod = tcc.base.TelMod(telmodPath)
if ShowBlocks:
    telMod.dump(sys.stdout)
    print

doWrap = True
reportLim = False
doRestart = [True, True, True]
tcc.axis.computeObj(
        obj=obj,
        doWrap=doWrap,
        doRestart=doRestart,
        reportLim=reportLim,
        earth=earth,
        inst=inst,
        telMod=telMod,
        axeLim=axeLim,
        tai=currTAI,
)

print "Obj block after conversion:"
print obj
