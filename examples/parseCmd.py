#!/usr/bin/env python2
from __future__ import division, absolute_import
"""
A simple program that will parse tcc command input from the command line.
Output is printed to the screen.
"""
import Tkinter
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from twistedActor import ActorDevice
import RO.Wdg
import pyparsing as pp

DefaultHost = "localhost"
DefaultPort = 3532 # 3.5m secondary mirror controller

def doPP(replyLine):
    """Parse a reply from a mirrorCtrl
    """
    aNum = pp.Word(pp.nums)
    msgCode = pp.oneOf("> d i w : f !", caseless=True)
    ppOut = aNum.setResultsName("userID") + aNum.setResultsName("cmdID") + msgCode.setResultsName("msgCode") + pp.restOfLine
    return ppOut.parseString(replyLine, parseAll=True)
    
    
class MirrorDevice(ActorDevice):
    """A Mirror Device
    """
    def __init__(self, host, port):
        ActorDevice.__init__(self, 'mirror', host, port)
    
    def handleReply(self, replyStr):
        """Each line received...do something
        """
#        print 'reply: ', replyStr
        ppOut = doPP(replyStr)
        if ppOut.msgCode == ":":
            print 'cmd' + str(ppOut.cmdID) + ' finished'
        elif ppOut.msgCode in ["f", "!"]:
            print 'cmd' + str(ppOut.cmdID) + ' failed'
        

class LogWdg(Tkinter.Frame):
    def __init__(self, master, host=DefaultHost, port=DefaultPort):
        print "Trying to connect to port %s" % (port,)
        Tkinter.Frame.__init__(self, master)
        row = 0
        self.logWdg = RO.Wdg.LogWdg(
            master = self,
            maxLines = 5000,
        )
        self.logWdg.grid(row=row, column=0, sticky="nwes")
        self.grid_rowconfigure(row, weight=1)
        self.grid_columnconfigure(0, weight=1)
        row += 1

        self.cmdWdg = RO.Wdg.CmdWdg(
            master = self,
            maxCmds = 100,
            cmdFunc = self.doCmd,
            helpText = "command to send to hub; <return> to send",
        )
        self.cmdWdg.grid(row=row, column=0, sticky="ew")
        row += 1

        self.actorDevice = MirrorDevice(
            host = host,
            port = port
        )
        self.actorDevice.conn.connect()

    def doCmd(self, cmdStr):
        """Handle commands typed into the command bar.
        Note that dispatching the command automatically logs it.
        """
        self.actorDevice.startCmd(cmdStr)
        self.logWdg.text.see("end")
    
    def logReply(self, msgStr, *args, **kwargs):
        self.logWdg.addOutput(msgStr + "\n")
    
    def connStateCallback(self, conn):
        state, reason = conn.fullState
        self.logWdg.addOutput("Connection state: %s %s\n" % (state, reason))


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    else:
        port = DefaultPort
    
    from twisted.internet import reactor
    import twisted.internet.tksupport
    root = Tkinter.Tk()
    twisted.internet.tksupport.install(root)
    
    logWdg = LogWdg(master=root, port=port)
    logWdg.pack()

    reactor.run()
