#include <iostream>

#include "coordConv/coordConv.h"
#include "tcc/obsFromNetUser.h"

namespace tcc {

    void obsFromNetUser(
        Obj &obj,
        double tai
    ) {
        // compute obj.obsPos and obj.objUserObjAzAng
        if ((obj.userSys->getName() == "mount") || (obj.userSys->getName() == "none")) {
            // obsPos and objUserObjAzAng are unknown
            obj.obsPos = coordConv::PVTCoord();
            obj.objUserObjAzAng.invalidate(tai);
            obj.objUserInstAng.invalidate(tai);
            obj.objAzInstAng.invalidate(tai);
        } else {
            // Set userDir to the direction of instrument y axis in user coordinates at netUserPos.
            // userDir = direction of instrument y in user coordinates
            //         = 90 - objUserInstAng
            coordConv::PVT userDir;
            if (obj.rotType == RotType_Object) {
                /*
                rotUser specifies user-inst angle at zpmUserPos (the un-arc-offset position).
                
                To support drift scanning we must keep the arc-inst angle the same at any point
                along the arc (including netUserPos) as at zpmUserPos. (In fact, arc-inst angle
                is held to 90 for drift scanning, so charge can be shifted along -y. But the
                angle doesn't matter for this computation, so long as it is constant.)
                Thus:
                rotUser + fromArcOrient = objUserInstAng + toArcOrient
                objUserInstAng = rotUser + fromArcOrient - toArcOrient
                               = rotUser - arcUserNoArcUserAng
                */
                obj.objUserInstAng = coordConv::wrapCtr(obj.rotUser - obj.arcUserNoArcUserAng);
                userDir = -obj.objUserInstAng + 90.0;
            } else if (obj.rotType == RotType_Horizon) {
                // rotUser specifies user-inst angle at netUserPos (to orient a slit relative to the horizon
                /// at the offset position), rather than at zpmUser.
                // objAzInstAng = rotUser
                // Problem: how to compute objUserInstAng to compute userDir, since we don't know
                // how azimuth orientation relates to user orientation until we convert user->obs?
                // Solution: perform an initial computation to determine the relative orientation;
                // we just need an approximation of objUserInstAng, since userDir need not be exact
                obj.objAzInstAng = obj.rotUser;
                double approxObjUserObjAzAng, dumScaleChange;
                obj.obsSysPtr->convertFrom(approxObjUserObjAzAng, dumScaleChange, *obj.userSys, obj.netUserPos.getCoord(), 0.0, obj.site, tai);
                coordConv::PVT approxObjUserObjInstAng = obj.objAzInstAng + approxObjUserObjAzAng;
                userDir = -approxObjUserObjInstAng + 90.0;
            } else {
                // not rotating; rotUser is ignored and userDir is irrelevant
                // <x>InstAng cannot be computed yet, but may be computed in mountFromObs
                userDir = coordConv::PVT(0, 0, tai);
                obj.objUserInstAng.invalidate(tai);
                obj.objAzInstAng.invalidate(tai);
            }

            coordConv::PVT obsDir;
            double dumScaleChange;
            obj.obsPos = obj.obsSysPtr->convertFrom(obsDir, dumScaleChange, *obj.userSys, obj.netUserPos, userDir, obj.site);
            obj.objUserObjAzAng = obsDir - userDir;

            if (obj.rotType == RotType_Object) {
                obj.objAzInstAng = coordConv::wrapCtr(obj.objUserInstAng - obj.objUserObjAzAng);
            } else if (obj.rotType == RotType_Horizon) {
                obj.objUserInstAng = coordConv::wrapCtr(obj.objAzInstAng + obj.objUserObjAzAng);
            }

            // diagnostic print for a mysterious problem
            if (std::abs(obj.objUserInstAng.vel) > 1.0) {
                std::cerr << "objFromNetUser warning: obj.objUserInstAng=" << obj.objUserInstAng
                    << "; obj.objUserObjAzAng=" << obj.objUserObjAzAng
                    << "; obj.objAzInstAng=" << obj.objAzInstAng
                    << "; obj.arcUserNoArcUserAng=" << obj.arcUserNoArcUserAng
                    << "; obj.rotUser=" << obj.rotUser
                    << "; obj.rotType=" << obj.rotType
                    << std::endl;
            }
        }
    }

}
