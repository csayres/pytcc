#include <iostream>
#include "tcc/cheby.h"

namespace tcc {

    std::string ChebyshevPolynomial::__repr__() const {
        std::ostringstream os;
        os << *this;
        return os.str();
    }

    std::ostream &operator<<(std::ostream &os, ChebyshevPolynomial const &cheby) {
        os << "ChebyshevPolynomial((";
        bool isFirst = false;
        for (std::vector<double>::const_iterator iter = cheby.params.begin();
            iter != cheby.params.end(); ++iter) {
            if (isFirst) {
                isFirst = false;
            } else {
                os << ", ";
            }
            os << *iter;
        }
        os << "), " << cheby.getMinX() << ", " << cheby.getMaxX() << ")";
        return os;
    }

}
