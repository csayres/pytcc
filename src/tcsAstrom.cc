#include <sstream>
#include <stdexcept>
#include "coordConv/physConst.h"
#include "tcc/telConst.h"
#include "tcc/tcsAstrom.h"
#include "tcc/tcsShim.h"

namespace tcc {

    TcsAstrom::TcsAstrom(
        double wavelen              ///< [in] wavelength (Angstroms)
    ) { 
        int stat;

        /* Mount tracking frame */
        stat = tcsIast(
            AZEL_TOPO,                      /* type of coordinate system */
            0,                              /* equinox */
            wavelen / coordConv::AngstromsPerMicron,    /* wavelength (micrometres) */
            &m_ast);
        if (stat != 0) {
            std::ostringstream os;
            os << "tcsIast mount error: " << stat;
            throw std::runtime_error(os.str());
        }

        /* Rotator tracking frame */
        stat = tcsIast(
            AZEL_TOPO,                      /* type of coordinate system */
            0,                              /* equinox */
            wavelen / coordConv::AngstromsPerMicron,    /* wavelength (micrometres) */
            &r_ast);
        if (stat != 0) {
            std::ostringstream os;
            os << "tcsIast rot error: " << stat;
            throw std::runtime_error(os.str());
        }
    };
    
    void TcsAstrom::initTarget(
        TARG &tar,
        coordConv::PVTCoord const &obsPos,
        double tai
    ) const {
        coordConv::PVT obsPVT[2];
        obsPos.getSphPVT(obsPVT[0], obsPVT[1]);

        double dumv;
        double tcsPos[2];
        for (int i = 0; i < 2; ++i) {
            bool isAz = (i == 0) && (m_ast.cosys == AZEL_TOPO);
            tcspk::tcsFromPVT(tcsPos[i], dumv, obsPVT[i], tai, isAz);
        }
        int stat = tcsItar (
            tcsPos[0], tcsPos[1],
            &tar);
        if (stat != 0) {
            std::ostringstream os;
            os << "tcsItar error: " << stat;
            throw std::runtime_error(os.str());
        }
    }
    
    std::string TcsAstrom::__repr__() const {
        std::ostringstream os;
        os << *this;
        return os.str();
    }

    std::ostream &operator<<(std::ostream &os, TcsAstrom const &tcsAstrom) {
        os << "# TcsAstrom" << std::endl;
        
        os << "m_ast # ASTROM struct for the object" << std::endl;
        os << tcsAstrom.m_ast;
        os << "r_ast: ASTROM struct for the rotator" << std::endl;
        os << tcsAstrom.r_ast;

        return os;
    }
}
