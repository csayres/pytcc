#include "boost/make_shared.hpp"

#include "tcc/obj.h"

namespace tcc {

    Obj::Obj()
    :
        name(),
        userSys(boost::make_shared<coordConv::NoneCoordSys>()),
        userPos(),
        userPxPMRadVel(),
        useCheby(false),
        rotType(RotType_None),
        rotUser(),
        azWrapPref(WrapType_Middle),
        rotWrapPref(WrapType_Middle),
        mag(coordConv::DoubleNaN),
        site(MeanLong, MeanLat, Elevation),
        gsSite(MeanLong, MeanLat, Elevation),
        chebyUser1(),
        chebyUser2(),
        chebyDist(),
        
        userArcOff(),
        objInstXY(),
        guideOff(),
        calibOff(),
        // doArcOffTDICorr(false),

        updateTime(0),
        slewEndTime(0),

        // arcVelCorr(),
        // prevArcVelCorr(),
        zpmUserPos(),
        netUserPos(),
        arcUserNoArcUserAng(),
        obsSysPtr(boost::make_shared<coordConv::ObsCoordSys>()),
        obsPos(),
        objUserObjAzAng(),
        objAzInstAng(),
        objUserInstAng(),
        // mechObjXY(),
        // mechInstAng(),
        // rotAzMechAng(),
        rotAzObjAzAng(),
        rotAzRotAng(),
        spiderInstAng(),
        rotPhys(),
        targetMount(),
//         pathMount(),
        axisCmdState(),
        axisErrCode(),
        axisIsSignificant(),
        actMount(),
        axisStatusWord(),
        axisStatusTime()
    {
        coordConv::PVT const NullPVT = coordConv::PVT();
        coordConv::PVT const ZeroPVT = coordConv::PVT(0, 0, 1);

        for (int axis = 0; axis < NAxes; ++axis) {
            targetMount[axis] = NullPVT;
//             pathMount[axis] = tcc::PVAJT();
            axisCmdState[axis] = AxisState_Halted;
            axisErrCode[axis] = AxisErr_HaltRequested;
            axisIsSignificant[axis] = false;
            actMount[axis] = NullPVT;
            axisStatusWord[axis] = 0;
            axisStatusTime[axis] = 0;

            // initalize offsets to 0
            if (axis < 2) {
                userArcOff[axis] = ZeroPVT;
                objInstXY[axis] = ZeroPVT;
            }
            guideOff[axis] = ZeroPVT;
            calibOff[axis] = ZeroPVT;

        }
    }

    bool Obj::isMoving() {
        for (int axis = 0; axis < NAxes; ++axis) {
            if (isMoving(axis)) {
                return true;
            }
        }
        return false;
    }

    bool Obj::isSlewing() {
        for (int axis = 0; axis < NAxes; ++axis) {
            if (isSlewing(axis)) {
                return true;
            }
        }
        return false;
    }
}
