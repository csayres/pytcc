#include "slalib.h"
#include "coordConv/physConst.h"
#include "tcc/weath.h"

namespace tcc {

    Weath::Weath()
    :
        airTemp(10.0),
        secTrussTemp(10.0),
        primFrontTemp(),
        primBackFrontTemp(),
        secFrontTemp(),
        secBackFrontTemp(),
        press(71787),
        humid(0.2),
        tempLapseRate(6.5),
        windSpeed(0),
        windDir(0),
        timestamp(0)
    { };

    void Weath::computeRefCo(double &refCoA, double &refCoB, double wavelen, double elev, double lat) const {
        double const eps = 1e-10; // precision required to terminate iteration (radian)
        double refCoARad, refCoBRad;
        slaRefco(
            elev,                                       // height of the observer above sea level (metre) (m)
            airTemp + coordConv::DegK_DegC,             // ambient temperature at the observer (K)
            press / coordConv::PascalsPerMillibar,      // pressure at the observer (hPa = millibar)
            humid,                                      // relative humidity (fraction)
            wavelen / coordConv::AngstromsPerMicron,    // wavelength (micron)
            lat * coordConv::RadPerDeg,                 // latitude of the observer (radian, astronomical)
            tempLapseRate / 1000.0,                     // temperature lapse rate in the troposphere (K/metre)
            eps,                                        // precision required to terminate iteration (radian)
            &refCoARad,                                 // tan z coefficient (radian)
            &refCoBRad);                                // tan^3 z coefficient (radian)
            refCoA = refCoARad / coordConv::RadPerDeg;
            refCoB = refCoBRad / coordConv::RadPerDeg;
    }
    
    void Weath::updateSite(coordConv::Site &site) const {
        computeRefCo(site.refCoA, site.refCoB, site.wavelen, site.elev, site.meanLat);
    };

}
