/*
Earth orientation predictions
*/
#include <stdexcept>
#include "coordConv/physConst.h"
#include "tcc/earth.h"

namespace tcc {
    
    Earth::Earth()
    :
        utcDay(0),
        startTAI(),
        ut1_taiIntercept(),
        poleInterceptX(),
        poleInterceptY(),
        ut1_taiSlope(),
        poleSlopeX(),
        poleSlopeY()
    {};

    void Earth::updateSite(coordConv::Site &site, double tai) const {
        int ind;
        for (ind = 0; ind < 2; ++ind) {
            if ((tai >= startTAI.at(ind)) && (tai < startTAI.at(ind + 1))) {
                break;
            }
        }
        if (ind > 1) {
            throw std::runtime_error("No earth orientation data for this TAI date");
        }

        double deltaT = tai - startTAI.at(ind);
        site.utc_tai = (utcDay * coordConv::SecPerDay) - startTAI.at(0);
        site.ut1_tai = ut1_taiIntercept.at(ind) + (ut1_taiSlope.at(ind) * deltaT);
        double poleX = poleInterceptX.at(ind) + (poleSlopeX.at(ind) * deltaT);
        double poleY = poleInterceptY.at(ind) + (poleSlopeY.at(ind) * deltaT);
        site.setPoleWander(poleX, poleY);
    }

}
