#include <iomanip>
#include <stdexcept>
#include "tcc/pxPMRadVel.h"

namespace tcc {

    PxPMRadVel::PxPMRadVel()
    :
        parallax(0), equatPM(0), polarPM(0), radVel(0)
    {}

    PxPMRadVel::PxPMRadVel(double parallax, double equatPM, double polarPM, double radVel)
    :
        parallax(parallax), equatPM(equatPM), polarPM(polarPM), radVel(radVel)
    {}

    bool PxPMRadVel::operator==(PxPMRadVel const &rhs) {
        return (this->parallax == rhs.parallax) \
            && (this->equatPM == rhs.equatPM) \
            && (this->polarPM == rhs.polarPM) \
            && (this->radVel == rhs.radVel);
    }
    
    bool PxPMRadVel::operator!=(PxPMRadVel const &rhs) {
        return !(*this == rhs);
    }

    std::string PxPMRadVel::__repr__() const {
        std::ostringstream os;
        os << *this;
        return os.str();
    }

    std::ostream &operator<<(std::ostream &os, PxPMRadVel const &pxPMRadVel) {
        std::ios_base::fmtflags oldFlags = os.flags();
        std::streamsize const oldPrecision = os.precision();
        os  << std::fixed
            << "PxPMRadVel("
            << "parallax=" << std::setprecision(6) << pxPMRadVel.parallax
            << ", equatPM=" << std::setprecision(3) << pxPMRadVel.equatPM
            << ", polarPM=" << pxPMRadVel.polarPM
            << ", radVel=" << pxPMRadVel.radVel
            << ")"
            << std::setprecision(oldPrecision);
        os.flags(oldFlags);
        return os;
    }

}
