#include <sstream>
#include <stdexcept>
#include "tcs.h"
#include "coordConv/physConst.h"
#include "tcc/tcsShim.h"
#include "tcc/tcsPOrig.h"

namespace tcc {

    TcsPOrig::TcsPOrig(
        double objInstX,
        double objInstY,
        Inst const &inst
    ) { 
        double objRotInstX, objRotInstY; /// object - rotator position in instrument frame (deg)
        objRotInstX = objInstX - inst.rot_inst_xy[0];
        objRotInstY = objInstY - inst.rot_inst_xy[1];

        double porX, porY;  // object position in rotator frame (deg)
        coordConv::rot2D(
            porX,
            porY,
            objRotInstX,
            objRotInstY,
            -inst.rot_inst_ang);

        int stat;
        stat = tcsIpor (
            porX * coordConv::RadPerDeg,    // x position (focal lengths = radians in our case)
            porY * coordConv::RadPerDeg,    // y position (focal lengths = radians in our case)
            &porig);
        if (stat != 0) {
            std::ostringstream os;
            os << "tcsIpor error: " << stat;
            throw std::runtime_error(os.str());
        }
    };
    
    std::string TcsPOrig::__repr__() const {
        std::ostringstream os;
        os << *this;
        return os.str();
    }

    std::ostream &operator<<(std::ostream &os, TcsPOrig const &tcsPOrig) {
        os << "# TcsPOrig telescope information" << std::endl;
        os << tcsPOrig.porig;

        return os;
    }

}
