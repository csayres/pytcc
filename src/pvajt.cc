#include <sstream>
#include "tcc/pvajt.h"

namespace {
    const double DeltaT = 0.01;
}

namespace tcc {

    PVAJT::PVAJT(double pos, double vel, double accel, double jerk, double t)
    :
        pos(pos),
        vel(vel),
        accel(accel),
        jerk(jerk),
        t(t)
    { };
    
    PVAJT::PVAJT(coordConv::PVT const &pvt)
    :
        pos(pvt.pos),
        vel(pvt.vel),
        accel(0),
        jerk(0),
        t(pvt.t)
    { };
    
    PVAJT::PVAJT()
    :
        pos(coordConv::DoubleNaN),
        vel(coordConv::DoubleNaN),
        accel(coordConv::DoubleNaN),
        jerk(coordConv::DoubleNaN),
        t(coordConv::DoubleNaN)
    { };


    std::string PVAJT::__repr__() const {
        std::ostringstream os;
        os << *this;
        return os.str();
    }

    std::ostream &operator<<(std::ostream &os, PVAJT const &pvajt) {
        os << "PVAJT(" << pvajt.pos << ", " << pvajt.vel << ", " 
            << pvajt.accel << ", " << pvajt.jerk << ", " << pvajt.t << ")";   
        return os;
    }

}
