#include <cmath>
#include <boost/tr1/array.hpp> // so array works with old and new compilers
#include "coordConv/coordConv.h"
#include "tcc/basics.h"
#include "tcc/basicMountFromObs.h"
#include "tcc/mountFromObs.h"

const int MAXITER = 10; // maximum numer of iterations of basicMountFromObs
const double MAX_MOUNT_ERR_DEG = 0.01 / 3600.0; // maximum mount error (deg) when iterating basicMountFromObs

namespace {
    
    /***
    Check that all inputs in Obj are finite
    
    @return true if all of obsPos, objArcOffset, guideOff, calibOff and objInstXY are finite
    */
    bool inputsAreFinite(tcc::Obj const &obj) {
        if (!obj.obsPos.isfinite()) return false;
        for (int i = 0; i < 2; ++i) {
            if (!obj.objInstXY.at(i).isfinite()) return false;
        }
        for (int i = 0; i < 3; ++i) {
            if (!obj.guideOff.at(i).isfinite()) return false;
            if (!obj.calibOff.at(i).isfinite()) return false;
        }
        return true;
    }
    
}

namespace tcc {

    void mountFromObs(
        Obj &obj,
        Inst const &inst,
        TelMod const &telMod,
        double tai
    ) {
        if ((obj.userSys->getName() == "mount") || (obj.userSys->getName() == "none")) {
            if (obj.userSys->getName() == "mount") {
                for (int axis = 0; axis < 2; ++axis) {
                    obj.targetMount.at(axis) = obj.userPos.at(axis).copy(tai);
                }
            } else {
                for (int axis = 0; axis < 2; ++axis) {
                    obj.targetMount.at(axis).invalidate(tai);
                }
            }
            
            if (obj.rotType == RotType_Mount) {
                coordConv::PVT const rotMount = obj.rotUser.copy(tai);
                obj.targetMount.at(2) = rotMount;
                obj.rotPhys = wrapCtr((rotMount - inst.rot_offset) / inst.rot_scale);
            } else {
                obj.targetMount.at(2).invalidate(tai);
                obj.rotPhys.invalidate(tai);
            }
            obj.spiderInstAng.invalidate(tai);
        
        } else if (!inputsAreFinite(obj)) {
            obj.targetMount.at(0).invalidate(tai);
            obj.targetMount.at(1).invalidate(tai);

            if (obj.rotType == RotType_Mount) {
                coordConv::PVT const rotMount = obj.rotUser.copy(tai);
                obj.targetMount.at(2) = rotMount;
                obj.rotPhys = wrapCtr((rotMount - inst.rot_offset) / inst.rot_scale);
            } else {
                obj.targetMount.at(2).invalidate(tai);
                obj.rotPhys.invalidate(tai);
            }
            obj.spiderInstAng.invalidate(tai);
        
        } else {
            bool tryAgain = true;
            std::tr1::array<double, 3> prevMountPos;
            for (int iter = 0; iter < MAXITER && tryAgain; ++iter) {
                for (int axis = 0; axis < 3; ++axis) {
                    prevMountPos[axis] = obj.targetMount[axis].getPos(tai);
                }
                basicMountFromObs(obj, inst, telMod, tai);
                // std::cout << "after basicMountFromObs iter " << iter
                //     << " obj.targetMount = " << obj.targetMount[0]
                //     << ", " << obj.targetMount[1] << ", " << obj.targetMount[2] << std::endl;
                tryAgain = false;
                for (int axis = 0; axis < 3; ++axis) {
                    if (obj.targetMount[axis].isfinite()) {
                        double posErr = std::abs(obj.targetMount[axis].pos - prevMountPos[axis]);
                        if (posErr > MAX_MOUNT_ERR_DEG or !std::isfinite(posErr)) {
                            tryAgain = true;
                        }
                    }
                }
            }
        }
    }

}
