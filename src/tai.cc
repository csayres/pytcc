#include <sys/time.h>
#include "coordConv/physConst.h"
#include "tcc/basics.h"

namespace tcc {

    double tai() {
        double unixTime;
        timeval tv;
        gettimeofday(&tv, NULL);
        unixTime = (static_cast<double>(tv.tv_usec) * 1.0e-6) + static_cast<double>(tv.tv_sec);
        return unixTime + coordConv::MJD_UnixTime;
    }

}
