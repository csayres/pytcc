#include <sstream>
#include <stdexcept>
#include "tcs.h"
#include "tcc/tcsShim.h"
#include "tcc/tcsTScope.h"

namespace tcc {

    TcsTScope::TcsTScope(
        TcsSite const &tcsSite,
        InstPosition const &instPos
    ) { 
        /*
        *                BC1
        *            -----------
        *          /             \
        *     TR1 / SK1       SK2 \ TR2
        *        / MB1   ---   MB2 \
        *     |--|     / PF1 \     |--|
        * NA1 |  |     | CA1 |     |  | NA2
        *     |--|     \     /     |--|
        *        \       ---       /
        *     TR4 \ SK4       SK3 / TR3
        *          \ MB4     MB3 /
        *            -----------
        *                BC2
        *
        * Note: NA1 = TCSpk Nasmyth RIGHT
        *
        *    front view of mirror cell
        */
        ROTLOC rotLoc;         /* OTA, Nasmyth L/R, or coude L/R */
        if (instPos.getName() == "NA1") {
            rotLoc = NASMYTH_R; // "right" as viewed from behind the primary mirror looking at the sky
        } else if (instPos.getName() == "NA2") {
            rotLoc = NASMYTH_L;
        } else {
            rotLoc = OTA;
        }

        SITE &tsite = const_cast<SITE&>(tcsSite.tsite);
        int stat = tcsItel(
            &tsite,
            tcspk::FL,      /* focal length (user units) */
            ALTAZ,          /* mount type, usually EQUAT or ALTAZ */
            0,              /* needed for GIMBAL case only */
            0,              /* needed for GIMBAL case only */
            0,              /* needed for GIMBAL case only */
            rotLoc,         /* rotator location */
            tcspk::RNoGo,   /* pole avoidance distance (radians) */
            &tel);          /* [out] telescope */
        if (stat != 0) {
            std::ostringstream os;
            os << "tcsItel error: " << stat;
            throw std::runtime_error(os.str());
        }
    };
    
    std::string TcsTScope::__repr__() {
        std::ostringstream os;
        os << *this;
        return os.str();
    }

    std::ostream &operator<<(std::ostream &os, TcsTScope const &tcsTScope) {
        os << "# TcsTScope telescope information" << std::endl;
        os << "tcsTScope.tel: TCSpk telescope info" << std::endl;
        os << tcsTScope.tel;

        return os;
    }

}
