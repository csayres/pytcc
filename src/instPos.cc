#include <sstream>
#include <stdexcept>
#include "tcc/instPos.h"

namespace tcc {
    InstPosition::InstPosition(std::string const &name) {
        setName(name);
    }
    
    void InstPosition::setName(std::string const &name) {
        std::map<std::string, double>::const_iterator orientIter = _nameAngMap.find(name);
        if (orientIter == _nameAngMap.end()) {
            std::ostringstream os;
            os << "No instrument position with name: " << name;
            throw std::runtime_error(os.str());
        }
        _name = name;
        _rotAzMechConst = orientIter->second;
    }

}

// once using C++11 one can make _nameAngMap a static const and initialize it as follows
// std::map<std::string, double> tcc::InstPosition::_nameAngMap = {
//   {"PF1", -90.0},
//   ....
// };
namespace {
    std::map<std::string, double> createNameConstMap() {
        std::map<std::string, double> nameAngMap;
        nameAngMap["PF1"] =  -90.0;
        nameAngMap["CA1"] =  -90.0;
        nameAngMap["BC1"] =  +90.0;
        nameAngMap["BC2"] =  -90.0;
        nameAngMap["NA1"] =  -90.0;
        nameAngMap["NA2"] =  -90.0;
        nameAngMap["TR1"] =  +45.0;
        nameAngMap["TR2"] = +135.0;
        nameAngMap["TR3"] = -135.0;
        nameAngMap["TR4"] =  -45.0;
        nameAngMap["SK1"] =    0.0;
        nameAngMap["SK2"] =  180.0;
        nameAngMap["SK3"] =    0.0;
        nameAngMap["SK4"] =  180.0;
        nameAngMap["MB1"] =    0.0;
        nameAngMap["MB2"] =  180.0;
        nameAngMap["MB3"] =    0.0;
        nameAngMap["MB4"] =  180.0;
        nameAngMap["?"] =      0.0;
        return nameAngMap;
    }
}

std::map<std::string, double> const tcc::InstPosition::_nameAngMap = createNameConstMap();
