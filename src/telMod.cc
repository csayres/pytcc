/*
* Pointing model block
*/
#include <cstdio>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include "coordConv/physConst.h"
#include "tcc/telMod.h"

const float RadPerArcsec = coordConv::RadPerDeg / coordConv::ArcsecPerDeg;

#define MaxTermNameLen 9    /* maximum term name length */

namespace tcc {

    void TelMod::init() {
        int errCode = tcsMinit(&_model);
        if (errCode == 0) {
            return;
        } else if (errCode == 1) {
            throw std::runtime_error("Insufficient room for all terms");
        }
        throw std::runtime_error("Unknown error");
    }
    
    void TelMod::loadPath(std::string const &filePath) {
        init();
        int retVal = tcsIntpm(filePath.c_str(), &_model);
        if (retVal == 0) {
            return;
        } else if (retVal == -1) {
            throw std::runtime_error("Unable to initialize model");
        } else if (retVal == -2) {
            throw std::runtime_error("Unable to open input file");
        } else if (retVal == -3) {
            throw std::runtime_error("I/O error or premature EOF");
        } else if (retVal == -4) {
            throw std::runtime_error("Unrecognized record");
        } else if (retVal == -5) {
            throw std::runtime_error("Unable to add term to model");
        }
        throw std::runtime_error("Unknown error");
    }
    
    int TelMod::addTerm(
        std::string const &termName,
        double termValue
    ) {
        if (termName.size() > MaxTermNameLen) {
            throw std::runtime_error("Invalid term name: too long");
        }
        int retVal = tcsAddtrm(termName.c_str(), termValue * RadPerArcsec, &_model);
        if (retVal > 0) {
            return retVal;
        } else if (retVal == -2) {
            throw std::runtime_error("No room left in model");
        } else if (retVal == -1) {
            throw std::runtime_error("Unrecognized term");
        } else if (retVal == 0) {
            throw std::runtime_error("No room in repertoire for new generic term");
        }
        throw std::runtime_error("Unknown error");
    }
    
    int TelMod::getNumTerms() const {
        for (int i = 0; i < MAXTRM; ++i) {
            if (!_model.model[i]) {
                return i;
            }
        }
        return MAXTRM;
    }
    
    std::vector<std::string> TelMod::getTermNames() const {
        std::vector<std::string> termNames;
        for (int i = 0; i < MAXTRM; ++i) {
            int termInd = _model.model[i] - 1;
            if (termInd < 0) break;
            termNames.push_back(_model.coeffn[termInd]);
        }
        return termNames;
    }

    double TelMod::getTerm(
        std::string const &termName
    ) const {
        if (termName.size() > MaxTermNameLen) {
            throw std::runtime_error("Invalid term name: too long");
        }
        int coeffInd;
        double coeffVal;
        int retVal = tcsQnatrm(
            const_cast<char *>(termName.c_str()),
            const_cast<TPMOD *>(&_model),
            &coeffVal,
            &coeffInd
        );
        if (retVal != 0) {
            throw std::runtime_error("Term not found in model");
        }
        return coeffVal / RadPerArcsec;
    }

    void TelMod::setTerm(
        std::string const &termName,
        double termValue
    ) {

        int retVal = tcsSterm(termName.c_str(), termValue * RadPerArcsec, &_model);
        if (retVal == 0) {
            return;
        } else if (retVal == -1) {
            throw std::runtime_error("Term not found in model");
        }
        throw std::runtime_error("Unknown error");
    }

    std::string TelMod::__repr__() {
        std::ostringstream os;
        os << *this;
        return os.str();
    }

    std::ostream &operator<<(std::ostream &os, TelMod const &telMod) {
        std::vector<std::string> termNames = telMod.getTermNames();

        std::ios_base::fmtflags f = os.flags(std::ios::right | std::ios::fixed | std::ios::showpos);
        std::streamsize p = os.precision(4);
        try {
            os << "# TelMod telescope pointing model" << std::endl;
            os << "# (the first two lines are ignored)" << std::endl;
            for (std::vector<std::string>::const_iterator nameIter = termNames.begin(); nameIter != termNames.end(); ++nameIter) {
                double val = telMod.getTerm(*nameIter);
                os << "  " << std::setw(9) << std::setiosflags(std::ios::right) << *nameIter
                    << std::setw(9) << val << std::setiosflags(std::ios::left) << std::endl;
            }
            os << "END" << std::endl;
            os.flags(f);
            os.precision(p);
        } catch (...) {
            os.flags(f);
            os.precision(p);
            throw;
        }
        return os;
    }

}
