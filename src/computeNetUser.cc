#include <cmath>
#include <sstream>
#include <stdexcept>
#include "coordConv/coordConv.h"
#include "tcc/computeNetUser.h"

namespace tcc {

    void computeNetUser(
        Obj &obj,
        double tai
    ) {
        // compute zpmUserPos
        if (obj.useCheby) {
            std::vector<coordConv::Coord> zpmUserVec;
            for (int tInd = 0; tInd < 2; ++tInd) {
                double tempTAI = tai + (tInd * DeltaTForVel);
                double userEquat = obj.chebyUser1(tempTAI);
                double userPolar = obj.chebyUser2(tempTAI);
                double parallax = coordConv::parallaxFromDistance(obj.chebyDist(tai));
                zpmUserVec.push_back(coordConv::Coord(userEquat, userPolar, parallax));
            }
            obj.zpmUserPos = coordConv::PVTCoord(zpmUserVec[0], zpmUserVec[1], tai, DeltaTForVel);
        } else {
            if ((obj.userPos[0].t != obj.userPos[1].t) && (obj.userPos[0].isfinite() || obj.userPos[1].isfinite())) {
                std::ostringstream os;
                os << "obj.userPos times don't match: " << obj.userPos[0] << ", " << obj.userPos[1];
                throw std::runtime_error(os.str());

               throw std::runtime_error("obj.userPos[0].t != obj.userPos[1].t");
            }
            coordConv::PVT distPVT = coordConv::PVT(
                coordConv::parallaxFromDistance(obj.userPxPMRadVel.parallax), 0, tai);
            coordConv::PVTCoord userPVTCoord = coordConv::PVTCoord(
                obj.userPos[0].copy(tai),
                obj.userPos[1].copy(tai),
                distPVT,
                obj.userPxPMRadVel.equatPM,
                obj.userPxPMRadVel.polarPM,
                obj.userPxPMRadVel.radVel
            );

            if (obj.userSys->isMean()) {
                obj.zpmUserPos = obj.userSys->removePM(userPVTCoord); // old TCC used obj.userPos[0].t)
            } else {
                obj.zpmUserPos = userPVTCoord;
            }
        }

        // compute netUserPos and arcUserNoArcUser
        if ((obj.userSys->getName() == "mount") || (obj.userSys->getName() == "none")) {
            // ignore user offset for mount coords, since it can mess up the wrap
            obj.netUserPos = obj.zpmUserPos;
            obj.arcUserNoArcUserAng = coordConv::PVT(0, 0, tai);
        } else {
            coordConv::PVT arcDist, fromArcOrient, toArcOrient;
            coordConv::polarFromXY(arcDist, fromArcOrient, obj.userArcOff.at(0), obj.userArcOff.at(1), tai);
            obj.netUserPos = obj.zpmUserPos.offset(toArcOrient, fromArcOrient, arcDist);
            obj.arcUserNoArcUserAng = toArcOrient - fromArcOrient;
        }
    }

}
