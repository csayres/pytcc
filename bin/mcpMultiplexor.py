#!/usr/bin/env python2
from twisted.internet import reactor

from twistedActor import startSystemLogging

from tcc.actor.mcpMultiplexor import MCPMultiplexor

LocalDebug = False # if true, talk to the azimuth controller started by emulate35m.py

if LocalDebug:
    MCPHost = "localhost"
    MCPPort = 3534
else:
    MCPHost = "t-g-sdss-2"
    MCPPort = 3900
    startSystemLogging(MCPMultiplexor.Facility)

if __name__ == "__main__":
    multiplexor = MCPMultiplexor(
        mcpHost = MCPHost,
        mcpPort = MCPPort,
    )
    reactor.run()
