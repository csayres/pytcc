#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Run the 2.5m TCC actor
"""
from twisted.internet import reactor
from twistedActor import startSystemLogging

from tcc.actor.tcc25mActor import TCC25mActor

startSystemLogging(TCC25mActor.Facility)
tcc = TCC25mActor()
reactor.run()
