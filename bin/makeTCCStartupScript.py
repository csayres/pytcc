#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Make a tcc startup script for a specified telescope

$ setup tcc
$ sudo makeTCCStartupScript 35m >/usr/local/bin/tcc
$ sudo chmod +x /usr/local/bin/tcc
"""
import argparse

from twistedActor import makeStartupScript
from tcc.actor.tcc35mActor import TCC35mActor
from tcc.actor.tcc25mActor import TCC25mActor

# dict of telescope name (lowercase): makeStartupScript argument dict
# all telescopes use actorName="tcc" and pkgName="tcc"
TelescopeDict = {
    "35m": dict(
        binScript = "tcc35m.py",
        userPort = TCC35mActor.UserPort,
        facility = TCC35mActor.Facility,
    ),
    "25m": dict(
        binScript = "tcc25m.py",
        userPort = TCC25mActor.UserPort,
        facility = TCC25mActor.Facility,
    ),
}

TelescopeList = sorted(TelescopeDict.iterkeys())

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    argParser.add_argument(
        "telescope",
        help = "Name of telescope: one of %s" % (TelescopeList,),
    )
    
    telescope = argParser.parse_args().telescope
    argDict = TelescopeDict.get(telescope.lower())
    if argDict is None:
        argParser.error("Unrecognized telescope %r; must be one of %s" % (telescope, TelescopeList))

    startupScript = makeStartupScript(actorName="tcc", pkgName="tcc", **argDict)
    print startupScript
