#!/usr/bin/env python2
"""Simple script that listens to UDP broadcasts from a TCC.
"""
from tcc.msg.udpPacket import UDPListener
from twisted.internet import reactor

UDPPort35m = 1235
UDPPortTccDev = 1200

if __name__ == "__main__":
    listener = UDPListener(UDPPort35m)
    reactor.run()
