#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Make a startup script for the MCP multiplexor

$ setup tcc
$ sudo makeTCCStartupScript >/usr/local/bin/multiplexor
$ sudo chmod +x /usr/local/bin/multiplexor
"""
from twistedActor import makeStartupScript

from tcc.actor.mcpMultiplexor import MCPMultiplexor

userPortStr = ", ".join("%s=%s" % (key, val) for key, val in MCPMultiplexor.AxisPortDict.iteritems())

if __name__ == '__main__':
    startupScript = makeStartupScript(
        actorName="mcpMultiplexor",
        pkgName="tcc",
        binScript = "mcpMultiplexor.py",
        userPort = userPortStr,
        facility = MCPMultiplexor.Facility,
    )
    print startupScript
