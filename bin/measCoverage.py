#!/usr/bin/env python2
from __future__ import division, absolute_import

import unittest
import coverage

cov = coverage.coverage(
	source = [
		"tcc",
		"mirrorCtrl",
	]
)
testSuite = unittest.defaultTestLoader.discover("../tests/")
cov.start()

text_runner = unittest.TextTestRunner().run(testSuite)

cov.stop()
cov.save()
cov.html_report(directory='covhtml')
cov.erase()
