#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Run the 3.5m TCC actor
"""
from twisted.internet import reactor
from twistedActor import startSystemLogging

from tcc.actor.tcc35mActor import TCC35mActor

startSystemLogging(TCC35mActor.Facility)
tcc = TCC35mActor()
reactor.run()
