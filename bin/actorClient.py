#!/usr/bin/env python
"""GUI actor client.
"""
import sys
import Tkinter
import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from twisted.internet import reactor
import twisted.internet.tksupport
from tcc.util import ActorClientWdg

def startClient(name, host, port):
    root = Tkinter.Tk()
    twisted.internet.tksupport.install(root)
    root.geometry("1000x500+300+200")
    actorClientWdg = ActorClientWdg(
        master = root,
        name = name,
        host = host,
        port = port,
    )
    actorClientWdg.pack(fill="both", expand=True)

    reactor.run()


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print """Usage: actorClient.py name host port, where:
name: name of client's actor keyword dictionary (without the final ".py")
host: IP address of host
port: port of host
"""
        sys.exit(1)
    
    name = sys.argv[1]
    host = sys.argv[2]
    port = int(sys.argv[3])
    

    startClient(name=name, host=host, port=port)
