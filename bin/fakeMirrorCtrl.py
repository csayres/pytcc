#!/usr/bin/env python2
from __future__ import division, absolute_import

import argparse
import itertools
import sys

import RO.SeqUtil
import mirrorCtrl
from mirrorCtrl.mirrors import mir35mSec, mir35mTert, mir25mPrim, mir25mSec
from mirrorCtrl.fakeGalil import FakeGalilFactory
from twisted.internet.defer import Deferred

class MirData(object):
    """An object that holds a name of a mirror, and the corresponding
    mirror, fake galil factory, galil device.
    Used to construct a MirrorBundle with the correct elements
    """
    def __init__(self, name, mirror, MirDev, FakeGalilFactory):
        """Construct a MirData

        @param[in] name  mirror name, one of: "sec35m", "tert35m", "prim25m", "sec25m"
        @param[in] mirror  one of the mirror objects in mirrorCtrl.mirrors
        @param[in] MirDev  a mirrorCtrl.GalilDevice-like class
        @param[in] FakeGalilFactory  a mirrorCtrl.fakeGalil factory
        """
        self.name = name
        self.mirror = mirror
        self.MirDev = MirDev
        self.FakeGalilFactory = FakeGalilFactory

class MirrorBundle(object):  #note this object copied/modified from code I wrote earlier from examples/emulate35m.py
    """Object for MirrorCtrl, GalilDevice, and Fake Galil
    """
    def __init__(self, mirData, mirPort, galilPort=0, verbose=False, wakeUpHomed=True):
        """Construct a MirrorBundle

        @param mirData  data for this mirror; an instance of MirData
        @param mirPort  port on which the mirrorCtrl listens
        @param galilPort  port on which fake galil listens; if 0 then use a random value
        @param verbose  should the fake Galil be in verbose mode?
        @param wakeUpHomed  should the fake Galil wake up in homed mode?
        """
        self.mirData = mirData
        self.mirPort = int(mirPort)
        self.galilPort = int(galilPort)
        self.fakeGalil = self.mirData.FakeGalilFactory(verbose=verbose, wakeUpHomed=wakeUpHomed, mirror=self.mirData.mirror)
    
    def startUp(self, *args):
        """Start everythin up in order
        """
        self.startFakeGalil()
        d = self.startMirrorCtrl()
        return d

    def startFakeGalil(self):
        """Start the fake Galil on a randomly chosen port; return the port number
        """
        portObj = reactor.listenTCP(port=self.galilPort, factory=self.fakeGalil)
        if self.galilPort == 0:
            self.galilPort = portObj.getHost().port
        print "Started fake %s Galil on port %d" % (self.mirData.name, self.galilPort)
   
    def startMirrorCtrl(self):
        """Start mirror controller
        """
        mirDev = self.mirData.MirDev(
            mirror = self.mirData.mirror,
            host = 'localhost',
            port = self.galilPort,            
        )
        
        d = Deferred()
        def connCallback(conn, d=d):
            #print "mirror controller conn state=", conn.state
            if conn.isConnected:
                print "Fake %s mirror controller ready on port %d" % (self.mirData.name, self.mirPort)
                d.callback("success")
        
        mirDev.conn.addStateCallback(connCallback)
        self.mirActor = mirrorCtrl.MirrorCtrl(
            device = mirDev,
            userPort = self.mirPort        
        )
        return d

DefMirNameList = ("sec35m", "tert35m", "prim25m", "sec25m")
DefPortList = (5001, 5002, 5003, 5004)
MirObjList = (mir35mSec, mir35mTert, mir25mPrim, mir25mSec)
GalilDevList = (mirrorCtrl.GalilDevice, mirrorCtrl.GalilDevice, mirrorCtrl.GalilDevice, mirrorCtrl.GalilDevice25Sec)
FakeGalilFactoryList = (FakeGalilFactory, FakeGalilFactory, FakeGalilFactory)
NameDataDict = dict((name, i) for i, name in enumerate(DefMirNameList))

MirDataDict = {}
for name, mir, dev, fake in itertools.izip(DefMirNameList, MirObjList, GalilDevList, FakeGalilFactoryList):
    MirDataDict[name] = MirData(name, mir, dev, fake)

if __name__ == '__main__':
    from twisted.internet import reactor
    argParser = argparse.ArgumentParser()
    argParser.add_argument("mirror", help="mirror names (one or more of: sec35m tert35m prim25m sec25m); not case-sensitive", nargs="*", default=DefMirNameList)
    argParser.add_argument("-p", "--port", type=int, help="port of each mirror controller", nargs="*", default=DefPortList)
    argParser.add_argument("-", "--unhomed", action="store_true", help="fake Galil wakes up NOT homed")
    argParser.add_argument("-v", "--verbose", action="store_true", help="print all Galil commands and replies?")
    
    namespace = argParser.parse_args()
    portList = RO.SeqUtil.asList(namespace.port)
    mirNameList = RO.SeqUtil.asList(namespace.mirror)
    bundles = []
    if len(portList) != len(mirNameList):
        argParser.error("you must specify one port per mirror")
    for ind, mirName in enumerate(mirNameList):
        try:
            dataInd = NameDataDict[mirName]
        except Exception:
            print "Unknown mirror name %r; must be one of %s" % (mirName, DefMirNameList)
            sys.exit(1)
            
        # construct a bundle for each mirror
        bundles.append(
            MirrorBundle(
                mirPort=portList[ind],
                mirData=MirDataDict[mirName],
                verbose=namespace.verbose,
                wakeUpHomed=not namespace.unhomed,                
            )
        )
    for bundle in bundles:
        bundle.startUp()
    reactor.run()    
