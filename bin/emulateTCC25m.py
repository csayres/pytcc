#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Start up 3.5m Secondary and Tertiary MirrorCtrl/GalilDevice pairs,
each talks to a seperate FakeGalil. Connect a Secondary and Tertiary ActorDevice
to each MirrorCtrl.

"""
import collections
import sys
import traceback
import Tkinter

import RO.Comm.Generic
RO.Comm.Generic.setFramework("twisted")
from twisted.internet import defer, reactor
import twisted.internet.tksupport
from twistedActor import startFileLogging
import RO.Wdg
from mirrorCtrl.mirrors import mir25mSec, mir25mPrim
from mirrorCtrl.mirrorCtrlWrapper import MirrorCtrlWrapper
# from mirrorCtrl.perturbActuators import getActEqEncMir, getActRandMove

try:
    startFileLogging("emulateTCC25m")
except KeyError:
   # don't start logging
   pass


from tcc.actor import TCCActor
from tcc.mir import MirrorDevice
from tcc.axis import SDSSAxisDevice, MCPMultiplexor
from tcc.axis.fakeAxisCtrlWdg import FakeAxisCtrlSetWdg
from tcc.util import ActorClientWdg

UserPort = 25000
UDPPort = 25010
MCPPort = 25300
MirNamePortDict = collections.OrderedDict((
    ("sec", 25320),
    ("prim", 25310),
))


AxisNameList = ("alt", "az", "rot1")

mcpMultiplexor = MCPMultiplexor(name="mcpMultiplexor", host="localhost", port=MCPPort)

root = Tkinter.Tk()
root.geometry("900x400+200+50")
twisted.internet.tksupport.install(root)
root.wm_withdraw()


RO.Wdg.stdBindings(root)

class MockTCC25mActor(TCCActor):
    def __init__(self):
        self.name = "mockTCC25m"
        axisDict =   dict((name,   SDSSAxisDevice(name=name, mcpMultiplexor=mcpMultiplexor)) for name in AxisNameList)
        mirrorDict = dict((name, MirrorDevice(name=name, host='localhost', port=port))
            for name, port in MirNamePortDict.iteritems())
        TCCActor.__init__(self,
            name = self.name,
            axisDict = axisDict,
            mirrorDict = mirrorDict,
            userPort = UserPort,
            udpPort = UDPPort,
            connectRot = True,
        )
        self.server.addStateCallback(self._serverStateCallback)
        self.actorClientWdg = None

    def _serverStateCallback(self, sock):
        """Actor server state callback; use to construct a client window
        """
        if sock.isReady:
            tccTopLevel = RO.Wdg.Toplevel(
                master = root,
                title = self.name,
                geometry = "1000x500+300+200",
            )
            self.actorClientWdg = ActorClientWdg(
                master = tccTopLevel,
                name = "tcc", # name of keyword dictionary
                host = "localhost",
                port = UserPort,
            )
            self.actorClientWdg.pack(fill="both", expand=True)

print "Start fake axis controllers"
axisCtrlTL = RO.Wdg.Toplevel()
axisCtrlWdg = FakeAxisCtrlSetWdg(master = axisCtrlTL, namePortList=[("mcp", MCPPort)])
axisCtrlWdg.pack(fill="both", expand=True)

print "Start fake mirror controllers"
# perterb the acutator locations (from true) to emulate the effect of a non-perfect mirror model
# mir35mSecPert = getActRandMove(mir35mSec)
# mir35mTertPert = getActEqEncMir(mir35mTert)
secCtrlWrapper  = MirrorCtrlWrapper(name="mockSec25m",  mirror=mir25mSec,  userPort=MirNamePortDict["sec"])
tertCtrlWrapper = MirrorCtrlWrapper(name="mockPrim25m", mirror=mir25mPrim, userPort=MirNamePortDict["prim"])

def makeTCC(*args):
    try:
        MockTCC25mActor()
        # tccWdg = TCCWdg(master=root)
        # tccWdg.pack(fill="both", expand=True)
    except Exception:
        print >>sys.stderr, "Error starting fake TCC"
        traceback.print_exc(file=sys.stderr)

d = defer.gatherResults((
    axisCtrlWdg.readyDeferred,
    secCtrlWrapper.readyDeferred,
    tertCtrlWrapper.readyDeferred,
))
d.addCallback(makeTCC)

def startupFailed(d):
    print "Startup failed, d=%s" % (d,)
    reactor.stop()
d.addErrback(startupFailed)

reactor.run()

