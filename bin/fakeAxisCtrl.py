#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Run one or more fake axis controllers (by default run az, alt and rot)
"""
import argparse
import Tkinter

import RO.SeqUtil

from tcc.axis.fakeAxisCtrlWdg import FakeAxisCtrlSetWdg

DefaultAxis = ("az", "alt", "rot1")
DefaultPort = (6001, 6002, 6003)

if __name__ == '__main__':
    from twisted.internet import reactor
    import twisted.internet.tksupport
    argParser = argparse.ArgumentParser()
    argParser.add_argument("axis", help="axis names (one or more of az alt rot); not case-sensitive", nargs="*", default=DefaultAxis)
    argParser.add_argument("-p", "--port", type=int, help="port of each axis controller", nargs="*", default=DefaultPort)
    argParser.add_argument("--nolog", action="store_true", help="do not log replies (use for very slow connections)?")
    
    namespace = argParser.parse_args()
    portList = RO.SeqUtil.asList(namespace.port)
    axisList = RO.SeqUtil.asList(namespace.axis)
    logReplies = not namespace.nolog
    if len(portList) != len(axisList):
        argParser.error("you must specify one port per axis")
    namePortList = zip(axisList, portList)

    print "Creating fake %s" % (", ".join("%s on port %s" % (name, port) for name, port in namePortList))
    root = Tkinter.Tk()
    twisted.internet.tksupport.install(root)

    wdg = FakeAxisCtrlSetWdg(
        master=root,
        namePortList=namePortList,
        logReplies=logReplies,
    )
    wdg.pack(side="top", fill="both", expand=True)

    reactor.run()
