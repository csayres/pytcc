#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Convert IERS Bulletin A to earthpred.dat, which is what the TCC uses

This program requires only the RO package, and that is only so it can report the date range of the predictions
relative to the current date. Eventually it should probably be rewritten to use astropy.time,
but give it some time to stabilize, first.
"""
import argparse
import os
import re
import sys
import time

from RO.Astro.Tm import mjdFromPyTuple

class IERSBullAFilter(object):
    """A function-like class that converts IERS Bulletin A data into a form used by the TCC

    Simply construct the object to make it perform the task
    """
    def __init__(self, inPath, outPath):
        """Construct a filter and run it

        @param[in] inPath  path to input file (an IERS Bulletin A)
        @param[in] outPath  path to output file (typically "earthpred.dat" in $TCC_DATA_DIR)
        """
        currUTCDate = mjdFromPyTuple(time.gmtime())
        currUTCDay = int(round(currUTCDate))
        with open(inPath, "rU") as self.inFile:
            firstUTCDay, utc_tai = self.getLeapSec()

            self.findPredStart()

            with open(outPath, "w") as self.outFile:
                self.outFile.write("! Earth Orientation Predictions (in TCC format)\n\n")
                self.outFile.write("! UTC   UTC-TAI   UT1-TAI     dX       dY\n")
                self.outFile.write("! days    sec       sec     arcsec   arcsec\n")

                desUTCDay = firstUTCDay
                nPredWritten = 0
                nLeapSecFound = 0
                amSkipping = False # a flag to support printing only one "skipping" message
                desUTCDay = firstUTCDay
                prev_ut1_utc = None
                for line in self.inFile:
                    line = line.strip()

                    if line.startswith("These predictions"):
                        break # end of predictions
                    data = line.split()
                    if len(data) != 7:
                        print("Could not parse %r as a prediction" % (line,))
                        sys.exit(1)

                    utcDay = int(data[3])
                    dX = float(data[4])
                    dY = float(data[5])
                    ut1_utc = float(data[6])

                    if utcDay == desUTCDay:
                        amSkipping = False
                    elif utcDay < desUTCDay and nPredWritten == 0:
                        # initial prediction starts earlier than desUTCDay; skip these records
                        if not amSkipping:
                            print "Skipping %d early predictions at beginning of data" % (desUTCDay - utcDay,)
                        amSkipping = True
                        continue
                    else:
                        print("Next prediction had the wrong UTC; giving up")
                        sys.exit(1)

                    # compute new utc_tai and ut1_tai
                    # the fancy math detects a change of leap-seconds in the prediction table
                    # and updates utc_tai accordingly
                    if prev_ut1_utc is None:
                        delta_utc_tai = 0
                    else:
                        delta_utc_tai = int(round(prev_ut1_utc - ut1_utc))
                    prev_ut1_utc = ut1_utc
                    nLeapSecFound = nLeapSecFound + abs(delta_utc_tai)
                    utc_tai = utc_tai + delta_utc_tai
                    ut1_tai = ut1_utc + utc_tai

                    # write prediction in TCC format
                    self.outFile.write("%6d   %4.0f  %10.5f  %7.4f  %7.4f\n" % \
                        (utcDay, utc_tai, ut1_tai, dX, dY))
                    nPredWritten = nPredWritten + 1
                    desUTCDay = utcDay + 1

                startDeltaDays = currUTCDay - firstUTCDay
                lastUTCDay = desUTCDay - 1
                endDeltaDays = lastUTCDay - currUTCDay

                print "Wrote %d predictions to %r" % (nPredWritten, outPath)
                print "Current UTC date = %0.1f" % (currUTCDate,)
                print "Prediction range = %d to %d" % (firstUTCDay, lastUTCDay)
                if startDeltaDays < 0:
                    print "DATA TOO NEW: data starts %d days AFTER TODAY!" % (-startDeltaDays,)
                else:
                    print "Data starts %d days before today" % (startDeltaDays,)
                if endDeltaDays < 2:
                    if endDeltaDays < 0:
                        print "DATA TOO OLD: data ends %d days BEFORE TODAY!" % (-endDeltaDays,)
                    else:
                        print "DATA TOO OLD: data ends ONLY %d days AFTER TODAY!" % (endDeltaDays,)
                else:
                    print "Data ends %d days after today" % (endDeltaDays,)

                if nLeapSecFound > 0:
                    print "Data includes %d leap seconds" % (nLeapSecFound,)

    def getLeapSec(self):
        """Read self.inFile until number of leap seconds found

        @return two values:
        - utc: UTC date at which UTC - TAI was determined
        - utc_tai: UTC - TAI (seconds) at the specified UTC
        """
        # look for a line like: TAI-UTC(MJD 55134) = 34.0 
        leapSecRE = re.compile(r"TAI-UTC\(MJD (\d+)\) *= *(\d+\.\d+)")
        for line in self.inFile:
            line = line.strip()
            m = leapSecRE.match(line)
            if m:
                utcDay = int(m.group(1))
                utc_tai = -float(m.group(2))
                return utcDay, utc_tai
        raise RuntimeError("UTC - TAI not found")

    def findPredStart(self):
        """Read self.inFile until just before the first prediction
        """
        predHeaderRE = re.compile(r"MJD +x\(arcsec\) +y\(arcsec\) +UT1-UTC\(sec\)")
        for line in self.inFile:
            line = line.strip()
            if predHeaderRE.match(line):
                return
        raise RuntimeError("Prediction header not found")

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    dataDir = os.environ["TCC_DATA_DIR"]
    defaultInPath = os.path.join(dataDir, "iersbulla.dat")
    defaultOutPath = os.path.join(dataDir, "earthpred.dat")

    argParser.add_argument(
        "inpath",
        nargs = "?",
        help = "path to input file (IERS Bulletin A); default=%r" % (defaultInPath,),
        default = defaultInPath,
    )
    argParser.add_argument(
        "outpath",
        nargs = "?",
        help = "path to output file; default=%r" % (defaultOutPath,),
        default = defaultOutPath,
    )
    
    namespace = argParser.parse_args()

    IERSBullAFilter(
        inPath = namespace.inpath,
        outPath = namespace.outpath,
    )
