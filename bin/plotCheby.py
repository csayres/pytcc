#!/usr/bin/env python2
from __future__ import division, absolute_import
"""Plot a set of chebyshev polynomials
"""
import argparse
import os

import numpy
import matplotlib.pyplot as plt

import tcc.base

def plotCheby(chebyPath):
    """Plot the data in a chebychev tracking file
    """
    obj = tcc.base.Obj()
    obj.loadPath(chebyPath)
    plt.figure(1)
    print "name     min      max      max |vel|"
    for i, name in enumerate(("User1", "User2", "Dist")):
        posNum = i*2 + 1
        velNum = posNum + 1
        cheby = getattr(obj, "cheby%s" % (name,))
        posArr, velArr, tArr = computeData(cheby)
        print "%8s %8.1f %8.1f %8.1f" % (name, posArr.min(), posArr.max(), numpy.max(numpy.abs(velArr)))
        plt.subplot(6, 1, posNum)
        plt.ylabel("pos")
        plt.title(name)
        plt.plot(posArr)
        plt.subplot(6, 1, velNum)
        plt.ylabel("vel")
        plt.plot(velArr)
    plt.show()

def computeData(cheby, numPts=100, deltaT=0.1):
    """Compute position and velocity for one Chebyshev polynomial

    Return three numpy arrays:
    - position (deg)
    - velocity (deg/sec)
    - TAI date (MJD, sec)
    """
    tBeg = cheby.getMinX()
    tEnd = cheby.getMaxX()
    tArr = numpy.linspace(tBeg, tEnd-deltaT, numPts)
    posArr = numpy.array([cheby(t) for t in tArr])
    velArr = numpy.array([(cheby(t + deltaT) - cheby(t))/deltaT for t in tArr])
    return posArr, velArr, tArr

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    dataDir = os.environ["TCC_DATA_DIR"]

    argParser.add_argument(
        "chebyfile",
        help = "chebyshev data file (default dir=%r)" % (dataDir,),
    )
    
    namespace = argParser.parse_args()
    chebyPath = os.path.join(dataDir, namespace.chebyfile)

    plotCheby(chebyPath)
